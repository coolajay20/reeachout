<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
class Cronjob_Controller extends CI_Controller  
{ 
    function __construct() 
    { 
        parent::__construct();    
         
        /* Load form and campaign helper */  
        $this->load->helper('form');        
        $this->load->helper('campaign');
 
        /* Load form validation library */  
        $this->load->library('form_validation'); 

        /* Changing error delimiters Globally */ 
        $this->form_validation->set_error_delimiters('', '');      
    }    
    
    public function campaign()
    {
        startCampaign();
    } 
}

?>