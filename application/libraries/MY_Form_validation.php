<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation 
{
    public function __construct($rules = array())
    {
        // Pass the $rules to the parent constructor.
        parent::__construct($rules);
        // $this->CI is assigned in the parent constructor, no need to do it here.
    }
    
    function mobile_number($value)
    {
    	$this->CI->form_validation->set_message('mobile_number', "{field} must be a ten digit number.");
    	
    	return (preg_match('/^[0-9]{10}$/', $value)? true : false);
    }
    
    function phone_number($value)
    {
    	$this->CI->form_validation->set_message('phone_number', "Please enter a valid phone number.");
    	
    	return (preg_match('/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/', $value)? true : false);
    }
    
    function post_code($value)
    {
    	$this->CI->form_validation->set_message('post_code', "{field} must be valid.");
    	
    	return (preg_match('/^[0-9]{5,7}$/', $value)? true : false);
    }

    function valid_login($value)
    {
    	$this->CI->form_validation->set_message('valid_login', "{field} is not valid.");
	$query = $this->CI->db->select('id, isActive, isConfirmed, szUniqueKey')
                ->from('tbl_users')
                ->where(array('isDeleted'=>0))
                ->group_start()
                ->where(array('szEmail'=>$value))
                ->or_where(array('szUserName'=>$value))
                ->group_end()
                ->get();
    	if($query->num_rows() > 0)
        {
            $rows = $query->result_array();
            if((int)$rows[0]['isActive'] != 1)
            {
                $this->CI->form_validation->set_message('valid_login', "{field} is suspended. Please contact admin for more details.");
                return false;
            }
            /*else if((int)$rows[0]['isConfirmed'] != 1)
            {
                $base_url = base_url() . "resendActivationLink/{$rows[0]['szUniqueKey']}";
                $this->CI->form_validation->set_message('valid_login', "{field} is not activated yet. Please follow the link that we have send on your registered email address or <a href=\"{$base_url}\">click here to resend the activation link</a>.");
                return false;
            }*/
            return true;
        }
        return  false;
    }

    function registered_email($value)
    {
    	$this->CI->form_validation->set_message('registered_email', "{field} is not registered.");
        $query = $this->CI->db->select('id')
            ->from('tbl_users')
            ->where(array('isDeleted'=>0, 'isActive'=>1))
            ->where(array('szEmail'=>$value))
            ->get();
    	return $query->num_rows() > 0;
    }

    public function valid_password($password, $login) 
    {
        $this->CI->form_validation->set_message('valid_password', "{field} is not correct.");
		
        $query = $this->CI->db->select('szPassword')
                ->from('tbl_users')
                ->where(array('isDeleted'=>0, 'isActive'=>1))
                ->group_start()
                ->where(array('szEmail'=>$login))
                ->or_where(array('szUserName'=>$login))
                ->group_end()
                ->get();
        if($query->num_rows() > 0)
        {
            $rows = $query->result_array();
            return (trim($rows[0]['szPassword']) === encrypt(trim($password)));
        }
        else
        {
            return  false;
        }
    }

    public function is_unique($str, $field)
    {
        $this->CI->form_validation->set_message('is_match', "{field} already exists.");
        if (substr_count($field, ':')==5){
            list($table,$field,$field1,$field1_val,$id_field,$id_val) = explode(':', $field);
            $query = $this->CI->db->limit(1)->where($field,$str)->where($id_field.' != ',$id_val)->where($field1,$field1_val)->where('isDeleted', 0)->get($table);
        } else if (substr_count($field, ':')==3){
            list($table,$field,$id_field,$id_val) = explode(':', $field);
            $query = $this->CI->db->limit(1)->where($field,$str)->where($id_field.' != ',$id_val)->where('isDeleted', 0)->get($table);
        } else {
            list($table, $field)=explode(':', $field);
            $query = $this->CI->db->limit(1)->get_where($table, array($field => $str));
        }
        return $query->num_rows() === 0; 
    }
    
    public function is_match($str, $field)
    {
        $this->CI->form_validation->set_message('is_match', "{field} not matching.");
        if (substr_count($field, ':')==3){
            list($table,$field,$id_field,$id_val) = explode(':', $field);
            $query = $this->CI->db->limit(1)->where($field,($field == 'szPassword' ? encrypt($str) : $str))->where($id_field,$id_val)->get($table);
        } else {
            list($table, $field)=explode(':', $field);
            $query = $this->CI->db->limit(1)->get_where($table, array($field => $str));
        }
      
        return $query->num_rows() > 0; 
    }
    
    public function unique_plan($planId, $id)
    {
    	$this->CI->form_validation->set_message('unique_plan', "Plan already exists.");
		
		$query = $this->CI->db->get_where('tbl_plans', array('id != '=>$id, 'isDeleted'=>0, 'szPlanID'=>$planId));
        return $query->num_rows() === 0;
    }
}
?>