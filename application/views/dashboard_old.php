	<section id="content_wrapper">
		<!-- Start: Topbar -->
      	<header id="topbar" class="alt">
        	<div class="topbar-left">
          		<ol class="breadcrumb">
            		<li class="crumb-active">
              			Dashboard
            		</li>
          		</ol>
        	</div>        	
     	</header>
      	<!-- End: Topbar -->

      	<!-- Begin: Content -->
      	<section id="content" class="animated fadeIn">
            <div class="tray tray-center" style="height: 621px;">
              <div class="row">
                <div class="col-sm-4">
                  <div class="panel panel-tile text-center br-a br-grey">
                    <div class="panel-body">
                      <h1 class="fs30 mt5 mbn"><?php echo $iTotalUsers;?></h1>
                      <h6 class="text-system">TOTAL USERS</h6>
                    </div>
                    <div class="panel-footer br-t p12">
                      <span class="fs11">
                        <a href="<?php echo base_url()?>users"><i class="fa fa-arrow-right pr5"></i> View All Users</a>
                      </span>
                    </div>
                  </div>
                </div> 
                <div class="col-sm-4">
                  <div class="panel panel-tile text-center br-a br-grey">
                    <div class="panel-body">
                      <h1 class="fs30 mt5 mbn"><?php echo $iTotalActiveSubscriptions;?></h1>
                      <h6 class="text-warning">TOTAL SUBSCRIPTIONS</h6>
                    </div>
                    <div class="panel-footer br-t p12">
                      <span class="fs11">
                        <a href="<?php echo base_url()?>subscriptions"><i class="fa fa-arrow-right pr5"></i> View All Subscriptions</a>
                      </span>
                    </div>
                  </div>
                </div> 	                      
            </div>	 
				
        </div>
    </section>
</section>