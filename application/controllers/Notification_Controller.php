<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Notification_Controller extends CI_Controller   
{
    function __construct() 
    { 
        parent::__construct(); 

        /* Load Notification Model */
        $this->load->model('Notification_Model'); 
		
        /* Load form helper */  
        $this->load->helper('form');

        /* Load form validation library */  
        $this->load->library('form_validation'); 

        /* Changing error delimiters Globally */ 
        $this->form_validation->set_error_delimiters('', '');   
    }
    
    public function index() 
    { 
        $this->all();
    } 
    
    public function all($arParams=array())
    {
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        /*else if(!hasPermission('manage.campaigns'))
        {
            redirect(base_url() . 'dashboard', 'refresh');
        }*/
        else if(trim($this->input->get('p_func')) == 'Get_Notifications')
        {
            $arLoginUser = $this->session->userdata('login_user');
            list($iLimit, $iOffset, $iDrawPage, $szKeyword, $arFilters, $szSortField, $szSortOrder) = parseDTRequest($this);
            $iTotalRecords = $this->Notification_Model->getAllNotifications($arFilters, $szKeyword, true);
            $arNotifications = $this->Notification_Model->getAllNotifications($arFilters, $szKeyword, false, $iLimit, $iOffset, $szSortField, $szSortOrder); 
            $response = array("draw"=>$iDrawPage, "recordsTotal"=>$iTotalRecords, "recordsFiltered"=>$iTotalRecords, "data"=>array()); 
            
            if(!empty($arNotifications))
            {
                $i = 0;
                foreach($arNotifications as $template)
                {					
                    $szActionLinks = '<ul>';
                    $szActionLinks .= "<li><a href='".base_url()."notifications/edit/".$template['szUniqueKey']."' class='text-info'><i class='fa fa-edit'></i> Edit</a></li>";                    
                    $szActionLinks .= '</ul>';
                    
                    $response['data'][$i][0] = "<p class='fs16'><span class='text-info'>{$template['szTitle']}</p>"; 
                    $response['data'][$i][1] = $template['iUserCount'];
                    $response['data'][$i][2] = convert_date($template['dtAddedOn'],2);
                    $response['data'][$i][3] = '<button class="btn btn-sm btn-rounded" data-toggle="popover" data-placement="bottom" data-html="true" data-content="'.$szActionLinks.'"><i class="fa fa-ellipsis-h"></i></button>';
                    $i++;				
                }
            }
            echo json_encode($response,true);
        }
        else
        {
            $data['szFavIcon'] = "fa-envelope";
            $data['szPageName'] = "All Notifications";
            $data['szMetaTagTitle'] = "Notifications";		
            $data['isLoadDataTable'] = true;
            $data['isLoadBootBox'] = true;
            $data['arLoginUser'] = $this->session->userdata('login_user');
            
            $data = array_merge($data, $arParams);

            $this->load->view('layout/header', $data);
            $this->load->view('notification/list');
            $this->load->view('layout/footer');
        }
    }
    
    public function edit($szUserKey)
    {
        $this->add($szUserKey);
    }
    
    public function add($szUserKey='')
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }
        /*else if(!hasPermission('manage.notifications'))
        {
            redirect(base_url().'dashboard', 'refresh');
        }*/
        else
        {
            // set/initialize varibales
            $data['idNotification'] = 0;
            $data['isFormError'] = false;
            $data['arNotificationDetails'] = array();
            $data['isDetailsSaved'] = false;
            $szUserKey = trim($szUserKey);		
            $data['arLoginUser'] = $this->session->userdata('login_user');

            if($szUserKey != '')
            {
                $arNotificationDetails = $this->Notification_Model->getNotificationByUniqueKey($szUserKey);
                if(!empty($arNotificationDetails))
                {
                    $data['idNotification'] = $arNotificationDetails['id'];
                    $data['arNotificationDetails'] = $arNotificationDetails;
                }
            }
               
            if($this->input->post('p_func') == 'Save Notification Details')
            {
                $this->form_validation->set_rules('arNotification[szTitle]', 'Title', "trim|required");
                $this->form_validation->set_rules('arNotification[szContent]', 'Content', 'trim|required');
                if ($this->form_validation->run() == true)
                { 
                    $arNotificationDetails = $this->input->post('arNotification', true);
                    if($data['idNotification'] > 0) $arNotificationDetails['id'] = $data['idNotification'];

                    if($data['idNotification'] = $this->Notification_Model->saveNotificationDetails($arNotificationDetails, $data['arLoginUser']['id']))
                    {
                        $data['isDetailsSaved'] = true;
                        $data['arNotificationDetails'] = $this->Notification_Model->getNotificationByID($data['idNotification']);
                    }
                }
                else
                {
                    $data['isFormError'] = true;
                    //print_r($this->form_validation->error_array());die;
                }
            }
            
            $data['szMetaTagTitle'] = "Notifications";
            $data['szPageName'] = ($data['idNotification'] > 0 ? "Edit Notification" : "Add New Notification");	
            $data['isLoadBootBox'] = true;
            $data['isLoadAdminForm'] = true;
            $data['isLoadValidationScript'] = true;
            $data['isLoadEditorScript'] = true;

            $this->load->view('layout/header', $data);
            $this->load->view('notification/add_edit');
            $this->load->view('layout/footer');
        }
    }
}
?>