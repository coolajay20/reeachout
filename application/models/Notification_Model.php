<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_Model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getAllNotifications($arFilters=array(), $szKeyword='', $isOnlyCount=false, $iLimit=10, $iOffset=0, $szSortField='n.dtAddedOn', $szSortOrder='DESC')
    {
        /*if ($this->db->table_exists('tbl_testing') )
        {
          // table exists
            echo "Yes";
        }
        else
        {
          // table does not 
          echo "No";
        }die;*/

        // set fields to select
        if($isOnlyCount)
            $this->db->select('n.id');
        else
            $this->db->select('n.*, COUNT(n.id) AS iUserCount');
        
        // from table
        $this->db->from('tbl_notifications n'); 
        $this->db->join('tbl_notification_user_mapping m', 'm.idNotification = n.id');
        
        // where
        $this->db->where('n.isDeleted',0);
                
        // set custom filters
        setCustomRecordFilters($this, $arFilters);

        // set keyword filter
        $szKeyword = trim($szKeyword);
        if($szKeyword != '')
        {
            $this->db->group_start();
            $this->db->like('szTitle', $szKeyword);
            $this->db->group_end();
        }
        
        // group by id        
        $this->db->group_by('n.id');
        
        // retrive records
        if($isOnlyCount)
        {
            return $this->db->get()->num_rows();
        }
        else
        {
            $query = $this->db->order_by($szSortField, $szSortOrder)->limit($iLimit, $iOffset)->get();
            //var_dump($this->db->last_query());die;
            return ($query->num_rows() > 0 ? $query->result_array() : array());
        }
    }
	
    function getNotificationByUniqueKey($szUniqueKey)
    {
        $arDetails = array();
        $szUniqueKey = trim($szUniqueKey);
        if($szUniqueKey != '')
        {
            $query = $this->db->get_where('tbl_notifications', array('szUniqueKey'=>$szUniqueKey));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    }
	
    function getNotificationByID($idNotification)
    {
        $arDetails = array();
        $idNotification = (int)$idNotification;
        if($idNotification > 0)
        {
            $query = $this->db->get_where('tbl_notifications', array('id'=>$idNotification));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    }

    function deleteNotification($arNotification, $iDeletedBy)
    {
        $idNotification = (int)$arNotification['id'];
        $this->db->where(array('id'=>$idNotification));
        $this->db->update('tbl_notifications', array('isDeleted'=>1, 'iDeletedBy'=>$iDeletedBy, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        if($this->db->affected_rows())
        {
            // delete permission mapping
            $this->db->where('idNotification', $idNotification)->update('tbl_notification_user_mappings', array('isDeleted'=>1, 'iDeletedBy'=>$iDeletedBy, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
            
             // add user activity
            addUserActivity($idNotification, 'tbl_notifications', "Notification {$arNotification['szTitle']} deleted.");
            
            return true;
        }
        return false;
    }

    function saveNotificationDetails($data, $idLoginUser)
    {		
        $data = sanitize_array_values($data, array('szContent'));
        $idNotification = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);

        if($idNotification > 0)
        {
            // get old details
            $arOldData = $this->getNotificationByID($idNotification); 
            
            $data['iUpdatedBy'] = $idLoginUser;
            $data['dtUpdatedOn'] = date('Y-m-d H:i:s');
            $this->db->where(array('id'=>$idNotification));
            $this->db->update('tbl_notifications', $data);
            
            // add user activity
            addUserActivity($idNotification, 'tbl_notifications', "Notification {$data['szTitle']} updated.", $data, $arOldData);
        }
        else
        {
            $data['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_notifications');
            $data['iAddedBy'] = $idLoginUser;
            $data['dtAddedOn'] = date('Y-m-d H:i:s');
            $query = $this->db->insert('tbl_notifications', $data);
            $idNotification = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);
            
            // add user mapping
            $this->addNotificationUsers($idNotification);
            
            // add user activity
            addUserActivity($idNotification, 'tbl_notifications', "New notification {$data['szTitle']} added.", $data);
        }
        return $idNotification;
    }
    
    function getNotificationUsers($idNotification)
    {
        $arUsers = array();
        $idNotification = (int)$idNotification;
        $query = $this->db->get_where('tbl_notification_user_mapping', array('idNotification'=>$idNotification));
        if($query->num_rows() > 0)
        {
            foreach($query->result_array() as $row)
            {
                $arUsers[] = $row;
            }
        }
        return $arUsers;
    }
    
    function addNotificationUsers($idNotification)
    {
        $idNotification = (int)$idNotification;
        $query = $this->db->get_where('tbl_users', array('isDeleted'=>0, 'idRole !='=>1));
        if($query->num_rows() > 0)
        {
            foreach($query->result_array() as $row)
            {
                $this->db->insert('tbl_notification_user_mapping', array('idNotification'=>$idNotification, 'idUser'=>$row['id']));
            }
        }
        return true;
    }
}
?>