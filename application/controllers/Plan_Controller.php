<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        else if(!hasPermission('manage.settings'))
        {
            redirect(base_url() . 'dashboard', 'refresh');
        }
        else
        {
            /* Load Plan Model */
            $this->load->model('Plan_Model');

            /* Load form helper */ 
            $this->load->helper('form');

            /* Load form validation library */ 
            $this->load->library('form_validation');

            /* Changing error delimiters Globally */
            $this->form_validation->set_error_delimiters('', '');
        }
    }

    public function index($arParams=array())
    {
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        else
        {
            $data['szPageName'] = "Membership (Admin)";
            $data['szMetaTagTitle'] = "Plans";
            $data['arLoginUser'] = $this->session->userdata('login_user');
            $data['isLoadDataTable'] = true;
            $data['arPlans'] = $this->Plan_Model->getAllPlans();

            $data = array_merge($data, $arParams);
            
            // set breadcrumb
            $data['arBreadCrumb'][] = array('name'=>'Dashboard', 'link'=>'dashboard');
            $data['arBreadCrumb'][] = array('name'=>'Plans', 'link'=>'plans');

            $this->load->view('layout/header', $data);
            $this->load->view('plans/list');
            $this->load->view('layout/footer');
        }
    }

    public function edit($szPlanKey='')
    {
        $this->add($szPlanKey);		
    }

    public function add($szPlanKey='')
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }				
        else
        {
            $data['idPlan'] = 0;
            $data['isFormError'] = false;
            $data['isDetailsSaved'] = false;
            $data['arPlanDetails'] = array();
            $data['arLoginUser'] = $this->session->userdata('login_user');	

            if($this->input->post('p_func') == 'Save Plan Details')
            {			
                $idPlan = (int)$this->input->post('arPlan[id]');

                //Set validation rules
                $this->form_validation->set_rules('arPlan[szPlanID]', 'Plan ID', "trim|alpha_dash|required|is_unique[tbl_plans:szPlanID:id:{$idPlan}]");
                $this->form_validation->set_rules('arPlan[szPlanName]', 'Plan name', "trim|required|is_unique[tbl_plans:szPlanName:id:{$idPlan}]");
                $this->form_validation->set_rules('arPlan[fPlanAmount]', 'Plan amount', 'trim|required|numeric');
                $this->form_validation->set_rules('arPlan[szPlanInterval]', 'Plan interval', 'trim|required');

                if($this->form_validation->run() == TRUE) 
                {				
                    $arPlanDetails = $this->input->post('arPlan');
                    if($data['idPlan'] = $this->Plan_Model->savePlanDetails($arPlanDetails, $data['arLoginUser']['id']))
                    {
                        $data['isDetailsSaved'] = true;
                        
                        // Load Stripe API Library
                        $this->load->library('CI_Stripe');
                        \Stripe\Stripe::setApiKey(STRIPE_ACCESS_KEY);
                        
                        // check if plan exists on Stripe
                        $isPlanExistsOnStripe = true;
			try {
                            $plan = \Stripe\Plan::retrieve($arPlanDetails['szPlanID']);
                            write_log("subscriptions/plans", "Plan {$arPlanDetails['szPlanID']} exists on stripe.\n");
                        }
                        catch (Exception $e) {
                            $isPlanExistsOnStripe = false;
                            write_log("subscriptions/plans", "Retrive plan error: (".$e->getMessage().").\n");
                        }
                        
                        // save/update on stripe                        
                        if($isPlanExistsOnStripe)
                        {
                            try {
                                $plan = \Stripe\Plan::retrieve($arPlanDetails['szPlanID']);
                                $plan->name = $arPlanDetails['szPlanName'];
                                $plan->metadata->yearly_amount = round($arPlanDetails['fPlanAmountYearly']*100);
                                $plan->metadata->lifetime_amount = round($arPlanDetails['fPlanAmountLifeTime']*100);
                                $plan->metadata->trail_period = $arPlanDetails['iTrialPeriod'];
                                $plan->save();
                                write_log("subscriptions/plans", "Plan {$arPlanDetails['szPlanID']} updated on stripe successfully.\n");
                            } catch(Exception $e) {
                                write_log("subscriptions/plans", "Update plan error: (".$e->getMessage().").\n");	
                            }					
                        }
                        else
                        {
                            try	{
                                $plan = \Stripe\Plan::create(array(
                                    "name" => $arPlanDetails['szPlanName'],
                                    "id" => $arPlanDetails['szPlanID'],
                                    "interval" => 'month',
                                    "currency" => "usd",
                                    "amount" => round($arPlanDetails['fPlanAmount']*100),
                                    "metadata[yearly_amount]" => round($arPlanDetails['fPlanAmountYearly']*100),
                                    "metadata[lifetime_amount]" => round($arPlanDetails['fPlanAmountLifeTime']*100),
                                    "metadata[trail_period]"=>$arPlanDetails['iTrialPeriod']
                                ));
                                write_log("subscriptions/plans", "Plan {$arPlanDetails['szPlanID']} updated on stripe successfully.\n");
                            } catch (Exception $e){
                                write_log("subscriptions/plans", "Create plan error: (".$e->getMessage().").\n");
                            }
                        }
                        
                        // save role permissions
                        $this->Plan_Model->addPlanFeatures($data['idPlan'], $this->input->post('arPlanFeatures', true), $data['arLoginUser']['id']);
                    }
                }
                else
                {
                    $data['isFormError'] = true;
                }
            }

            if($szPlanKey)
            {
                $data['arPlanDetails'] = $this->Plan_Model->getPlanByUniqueKey($szPlanKey);
                $data['idPlan'] = (isset($data['arPlanDetails']['id']) ? (int)$data['arPlanDetails']['id'] : 0);
            }
            else if($data['idPlan'] > 0)
            {
                $data['arPlanDetails'] = $this->Plan_Model->getPlanByID($data['idPlan']);
            }

            $data['szPageName'] = "Plans";
            $data['szMetaTagTitle'] = ($data['idPlan'] > 0 ? 'Edit Plan' : 'Add New Plan');	
            $data['isLoadAdminForm'] = true;
            $data['isLoadCMSScripts'] = true;
            $data['isLoadValidationScript'] = true;
            
            // get all the active features
            $this->load->model('Features_Model');
            $data['arFeatures'] = $this->Features_Model->getAllFeatures(true);
            
            // set breadcrumb
            $data['arPlanFeatures'] = array();
            $data['arBreadCrumb'][] = array('name'=>'Dashboard', 'link'=>'dashboard');
            $data['arBreadCrumb'][] = array('name'=>'Plans', 'link'=>'plans');
            if(!empty($data['arPlanDetails']))
            {
                $data['arBreadCrumb'][] = array('name'=>'Edit Plan');
                
                $data['arPlanFeatures'] = $this->Plan_Model->getPlanFeatures($data['arPlanDetails']['id']);
            }
            else
                $data['arBreadCrumb'][] = array('name'=>'Add New Plan');

            $this->load->view('layout/header', $data);
            $this->load->view('plans/add_edit');
            $this->load->view('layout/footer');
        }
    }

    function delete($szPlanKey='')
    {
        if(!is_user_login())
        {
                redirect(base_url().'login', 'refresh');
        }	

        $message = array();
        $szPlanKey = trim($szPlanKey);
        if($szPlanKey != '')
        {
                $arPlanDetails = $this->Plan_Model->getPlanByUniqueKey($szPlanKey);
                if(!empty($arPlanDetails))
                {
                        if($arPlanDetails['isDeleted'] == 0)
                        {
                            $arLoginUser = $this->session->userdata('login_user');
                            $this->Plan_Model->deletePlan($arPlanDetails, $arLoginUser['id']);
                            $message['szSuccessMessage'] = 'Plan has been deleted successfully';
                        }
                        else
                        {
                            $message['szErrorMessage'] = "Plan is already deleted.";
                        }
                }
                else
                {
                    $message['szErrorMessage'] = "Plan not found.";
                }
        }
        else
        {
            $message['szErrorMessage'] = "Invalid Plan delete request.";
        }

        $this->index($message);
    }
}
?>