<?php if(isset($szSuccessMessage) && $szSuccessMessage != ''){?>
<div class="alert alert-success p5"><?php echo $szSuccessMessage;?></div>
<?php } if(isset($szErrorMessage) && $szErrorMessage != ''){?>
<div class="alert alert-danger p5"><?php echo $szErrorMessage;?></div>
<?php }?>

<div class="panel panel-visible" id="spy3">
	<div class="panel-heading">
		<p class="text-left">
			<a href="<?php echo base_url();?>campaigns/templates/add" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New Email</a>
		</p>
	</div>
	<div class="panel-body pn">
		<table class="table table-responsive table-striped table-hover dt-dynamic" id="customer_data_listing" cellspacing="0" width="100%" data-url="<?php echo base_url();?>campaigns/templates/all" data-func="Get_Templates">
			<thead>
				<tr>             	
					<th data-sort="szName">Name</th>
                    <th data-sort="szSubject">Subject</th>
                    <th data-sort="idCampaign">Campaign ID</th>
					<th data-sort="dtAddedOn">Date Added</th>
					<th data-class="action-links">Actions</th>
				</tr>
			</thead>
		</table>
	</div>
</div>