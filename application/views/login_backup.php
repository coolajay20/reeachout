<!DOCTYPE html>

<html>



<head>

  <!-- Meta, title, CSS, favicons, etc. -->

  <meta charset="utf-8">

  <title>Login | Bitcoin Labs</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">



  <!-- Font CSS (Via CDN) -->

  <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>



  <!-- Theme CSS -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/skin/default_skin/css/theme.css">

  

  <!-- Admin Forms CSS -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/css/admin-forms.css">



  <!-- Favicon -->

  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!--[if lt IE 9]>

    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

    <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->

</head>



<body class="admin-validation-page external-page external-alt sb-l-c sb-r-c">



  <!-- Start: Main -->

  <div id="main" class="animated fadeIn">



    <!-- Start: Content-Wrapper -->

    <section id="content_wrapper">



      <!-- Begin: Content -->

      <section id="content">



        <div class="admin-form theme-info mw500" id="login">



          <!-- Login Logo -->

          <div class="row table-layout">

        	<img src="<?php echo base_url();?>assets/images/logo.png" title="Bitcoin Labs" class="center-block img-responsive" style="max-width: 275px;">            

          </div>



          <!-- Login Panel/Form -->

          <div class="panel mt30 mb25">

            <form method="post" action="<?php echo base_url();?>login" id="login-form" class="validate-form">

              <div class="panel-body bg-light p25 pb15">

              

              	<!-- Email address Input -->

              	<?php $szEmailError = form_error('szEmail');?>

             	<div class="section">

                  <label for="szEmail" class="field-label text-muted fs18 mb10">Username or Email Address</label>

                  <label for="szEmail" class="field prepend-icon<?php if(!empty($szEmailError)){?> state-error<?php }?>">

                    <input type="text" name="szEmail" id="szEmail" class="gui-input required" placeholder="Username or Email address" value="<?php echo set_input_value('szEmail');?>">                    

                    <label for="szEmail" class="field-icon">

                      <i class="fa fa-user"></i>

                    </label>

                  </label>

                  <?php if(!empty($szEmailError)){?><em class="state-error" for="szEmail"><?php echo $szEmailError;?></em><?php }?>

                </div>



                <!-- Password Input -->

                <?php $szPasswordError = form_error('szPassword');?>

                <div class="section">

                  <label for="szPassword" class="field-label text-muted fs18 mb10">Password</label>

                  <label for="szPassword" class="field prepend-icon<?php if(!empty($szPasswordError)){?> state-error<?php }?>">

                    <input type="password" name="szPassword" id="szPassword" class="gui-input required" placeholder="Password">                    

                    <label for="password" class="field-icon">

                      <i class="fa fa-lock"></i>

                    </label>

                  </label>

                  <?php if(!empty($szPasswordError)){?><em class="state-error" for="szPassword"><?php echo $szPasswordError;?></em><?php }?>

                </div>

              </div>



              <div class="panel-footer clearfix">

                <button type="submit" class="button btn-success mr10 pull-right">Sign In</button>

                <label class="switch ib switch-success mt10">

                  <input type="checkbox" name="remember" id="remember" value="1" checked>

                  <label for="remember" data-on="YES" data-off="NO"></label>

                  <span>Remember me</span>

                </label>

              </div>

			  <input type="hidden" name="p_func" value="Log In">

            </form> 

            <div class="panel-body bg-light p25 pb15">

                <div class="row"> 

                    <div class="col-md-4 col-spaced">

                        <a class="btn btn-block btn-social btn-facebook" href="#">

                            <i class="fa fa-facebook"></i>

                            Facebook

                        </a>

                    </div> 

                    <div class="col-md-4 col-spaced">

                        <a class="btn btn-block btn-social btn-twitter"  href="#">

                            <i class="fa fa-twitter"></i>

                            Twitter

                        </a>

                    </div> 

                    <div class="col-md-4 col-spaced">

                        <a class="btn btn-block btn-social btn-google"  href="#">

                            <i class="fa fa-google-plus"></i>

                            Google+

                        </a>

                    </div>

                </div> 

            </div>

    </div>



		  <div class="login-links">

            <p>

              <a href="<?php echo base_url();?>forgot-password" class="active" title="Sign In">Forgot Password?</a>

            </p>

          </div>



        </div>



      </section>

      <!-- End: Content -->



    </section>

    <!-- End: Content-Wrapper -->



  </div>

  <!-- End: Main -->





  <!-- BEGIN: PAGE SCRIPTS -->



  <!-- jQuery -->

  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery-1.11.1.min.js"></script>

  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  

  <!-- jQuery Validate Plugin-->

  <script src="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  

  <!-- jQuery Validate Addon -->

  <script src="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>



  <!-- Theme Javascript -->

  <script src="<?php echo base_url();?>theme/assets/js/utility/utility.js"></script>

  <script src="<?php echo base_url();?>theme/assets/js/demo/demo.js"></script>

  <script src="<?php echo base_url();?>theme/assets/js/main.js"></script>



  <!-- Page Javascript -->

  <script type="text/javascript">

  jQuery(document).ready(function() {



    "use strict";



    // Init Theme Core      

    Core.init();



    // Init Demo JS

    Demo.init();

  });

  // Init Base URL

  var base_url = '<?php echo base_url();?>';

  </script>

  

  <!-- Custom Validation Script -->

  <script src="<?php echo base_url();?>assets/js/custom.validate.js?<?php echo filemtime('./assets/js/custom.validate.js');?>"></script>

  <script type="text/javascript">

  jQuery(document).ready(function() {

 	validate_form();

  });

  </script>



  <!-- END: PAGE SCRIPTS -->



</body>



</html>