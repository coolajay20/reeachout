<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	

class User_Model extends CI_Model

{
    function saveUserDetails($data, $idLoginUser)
    {		
        $data = sanitize_array_values($data);
        $idUser = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);
        if($idUser > 0)
        {
            // get old details

            $arOldData = $this->getUserByID($idUser);

            // update user record
            $data['iUpdatedBy'] = $idLoginUser;
            $data['dtUpdatedOn'] = date('Y-m-d H:i:s');
            $this->db->where(array('id'=>$idUser));
            $this->db->update('tbl_users', $data);
			
            // add user activity
            addUserActivity($idUser, 'tbl_users', "Updated profile details for {$data['szFirstName']} {$data['szLastName']}.", $data, $arOldData);
        }
        else
        {
            // add user record
            if(empty($data['szUniqueKey']))
            {
                $data['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_users');
            }
            $data['iCreatedBy'] = $idLoginUser;
            $data['dtCreatedOn'] = date('Y-m-d H:i:s');
            $query = $this->db->insert('tbl_users', $data);
            $idUser = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);

            // add user activity
            addUserActivity($idUser, 'tbl_users', "New profile details added for {$data['szFirstName']} {$data['szLastName']}.", $data);
        } 
        return $idUser;
    }

    function getUserByEmail($szEmail)
    {
        $arUserDetails = array();
        $szEmail = trim($szEmail);
        if($szEmail != '')
        {
            $this->db->from('tbl_users');
            $this->db->where(array('szEmail'=>$szEmail, 'isDeleted'=>0));
            $query = $this->db->get();
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arUserDetails = $rows[0];
            }
        }
        return $arUserDetails;
    }

    function getUserByLogin($szLogin)
    {
        $arUserDetails = array();
        $szLogin = trim($szLogin);
        if($szLogin != '')
        {
            $this->db->from('tbl_users')
                ->where(array('isDeleted'=>0))
                ->group_start()
                ->where(array('szEmail'=>$szLogin))
                ->or_where(array('szUserName'=>$szLogin))
                ->group_end();

            $query = $this->db->get();
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arUserDetails = $rows[0];
            }
        }
        return $arUserDetails;
    }

    function getUserByUniqueKey($szUniqueKey)
    {
        $arUserDetails = array();
        $szUniqueKey = trim($szUniqueKey);
        if($szUniqueKey != '')
        {
            $query = $this->db->get_where('tbl_users',array('szUniqueKey'=>$szUniqueKey, 'isDeleted'=>0));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arUserDetails = $rows[0];
            }
        }
        return $arUserDetails;
    }

    function getUserByID($idUser)
    {
        $arUserDetails = array();
        $idUser = (int)$idUser;
        if($idUser > 0)
        {
            $query = $this->db->get_where('tbl_users',array('id'=>$idUser, 'isDeleted'=>0));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arUserDetails = $rows[0];
            }
        }
        return $arUserDetails;
    }
	
	
    function validateUser($username)
    {
        $arUserDetails = array();
        $username = trim($username);
        if(!is_null($username))
        {
			//check if exists as login username
            $query = $this->db->get_where('tbl_users',array('szUserName'=>$username, 'isDeleted'=>0, 'isConfirmed'=>1));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arUserDetails = $rows[0];
            }
				if(empty(($arUserDetails))) {
					//if couldn't find as login username, check if exists as usi username
					$query = $this->db->get_where('tbl_users',array('szUSIUserName'=>$username, 'isDeleted'=>0, 'isConfirmed'=>1));
					
					if($query->num_rows() > 0)	{
						$rows = $query->result_array();
						$arUserDetails = $rows[0];
					}
				}
        }
        return $arUserDetails;
    }

    function resetConfirmationLinkTime($szKey, $dtDateTime)
    {
        $szKey = trim($szKey);
        $this->db->where(array('szUniqueKey' => $szKey));
        $this->db->update('tbl_users', array('dtLastLinkOn' => $dtDateTime));
        return $this->db->affected_rows() > 0;
    }

    function setActiveStatus($szKey)
    {
        $szKey = trim($szKey);
        $this->db->where(array('szUniqueKey' => $szKey));
        $this->db->update('tbl_users', array('isActive' => 1));
        return $this->db->affected_rows() > 0;
    }

    function updateUserPassword($arUserDetails, $szNewPassword)
    {
        $idUser = (int)$arUserDetails['id'];
        $szNewPassword = encrypt(trim($szNewPassword));               

        $this->db->where(array('id' => $idUser));
        $this->db->update('tbl_users', array('szPassword' => $szNewPassword));
        if($this->db->affected_rows() > 0)
        {
            // add user activity
            addUserActivity($idUser, 'tbl_users', "Password changed.");
            return true;
        }
        return false;
    }

    function getUsers($arFilters=array(), $szKeyword='', $isOnlyCount=false, $iLimit=10, $iOffset=0, $szSortField='szFirstName', $szSortOrder='ASC')
    {
        // set fields to select
        if($isOnlyCount)
            $this->db->select('u.id');
        else
            $this->db->select("u.*, (CASE WHEN MAX(s.tCurrentTermEnd) IS NOT NULL THEN (CASE WHEN MAX(s.tCurrentTermEnd) > ".time()." THEN (CASE WHEN s.tTrialEnd > ".time()." THEN 'Trial' ELSE 'Active' END) ELSE 'Cancelled' END) ELSE 'Z' END) AS szSubStatus, MAX(s.tCurrentTermEnd) AS tCurrentTermEnd, s.szPlanName");

        // from table
        $this->db->from('tbl_users u');
        $this->db->join('tbl_user_subscriptions s', 's.idUser = u.id', 'left');

        // where
        $this->db->where('u.isDeleted',0);

        // set custom filters
        setCustomRecordFilters($this, $arFilters);

        // set keyword filter
        $szKeyword = trim($szKeyword);
        if($szKeyword != '')
        {
            $this->db->group_start();
            $this->db->like('u.szFirstName', $szKeyword);
            $this->db->or_like('u.szLastName', $szKeyword);
            $this->db->or_like('u.szEmail', $szKeyword);
            $this->db->group_end();
        }
        
        // goup by
        $this->db->group_by('u.id');
        
        // retrive records
        if($isOnlyCount)
        {
            return $this->db->get()->num_rows();
        }
        else
        {
            if($szSortField == 'szSubStatus' || $szSortField == 'szPlanName')
            {
                $query = $this->db->order_by($szSortField, $szSortOrder)->order_by('tCurrentTermEnd', 'DESC')->limit($iLimit, $iOffset)->get();
            }
            else
            {
                $query = $this->db->order_by($szSortField, $szSortOrder)->limit($iLimit, $iOffset)->get();
            }

            //echo $this->db->last_query();die;
            return ($query->num_rows() > 0 ? $query->result_array() : array());
        }
    }

    function deleteUser($arUserDetails, $iDeletedBy)
    {
        // delete user
        $idUser = (int)$arUserDetails['id'];
        $this->db->where(array('id'=>$idUser));
        $this->db->update('tbl_users', array('isDeleted'=>1, 'iDeletedBy'=>$iDeletedBy, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        if($this->db->affected_rows() > 0)
        {
            // add user activity
            addUserActivity($idUser, 'tbl_users', "User {$arUserDetails['szFirstName']} {$arUserDetails['szLastName']} deleted.");
            return true;
        }
        return false;
    }

    function loginUserBySocialMedia($arUserDetails)
    {
        if(!empty($arUserDetails))
        {
            set_user_login($arUserDetails, true);

            // add login history
            addLoginHistory($arUserDetails);
        }
    }

    function getCountries($idCountry=0)
    {
        $idCountry = (int)$idCountry;
        $this->db->from('tbl_countries');
        if($idCountry > 0)
            $this->db->where('id', $idCountry);
        $this->db->order_by('szName', 'ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0 ? $query->result_array() : array());
    }

    function getCountryByName($szCountryName)
    {
        $this->db->select('id');
        $this->db->from('tbl_countries');  
        $this->db->where('LOWER(szName)', strtolower($szCountryName)); 
        $this->db->or_where('LOWER(szCode2)', strtolower($szCountryName));
        $this->db->or_where('LOWER(szCode3)', strtolower($szCountryName));
        $query = $this->db->get();
        $idCountry = false;
        if($query->num_rows() > 0)
	{
            $rows = $query->result_array();
            $idCountry = $rows[0]['id'];
	} 
        return $idCountry;
    }

    function getUserLastLogin($idUser)
    {
	$dtLogin = '';
	$query = $this->db->select('dtCreatedOn as dtLogin')->from('tbl_user_activity')->where(array('idUser'=>$idUser, 'szActivity'=>'Logged in.'))->order_by('dtCreatedOn', 'DESC')->limit(1)->get();
	if($query->num_rows() > 0)
	{
            $rows = $query->result_array();
            $dtLogin = $rows[0]['dtLogin'];
	}

	return $dtLogin;
    }

    function getUserActivity($idUser=0, $szKeyword='', $isCountOnly=false, $iLimit=0, $iOffset=0, $szSortField='a.dtCreatedOn', $szSortOrder='DESC', $isRemoveExtras=true,$isNewOnly=false)
    {
    	$arResult = ($isCountOnly ? 0 : array());
        $idUser = (int)$idUser;

        if($isCountOnly)
            $this->db->select('a.id');
        else
            $this->db->select('a.*, u.szFirstName, u.szLastName, u.szUniqueKey');
        
        $this->db->from('tbl_user_activity a');
	if ($isRemoveExtras){
            $this->db->where('a.szActivity !=', "Logged In.");
	} if($isNewOnly) {
            $this->db->where('a.isNew', 1);
            $this->db->where('date(a.dtCreatedOn)', 'CURDATE()', false);
        }
            
        $this->db->join('tbl_users u', 'u.id = a.idUser');
        if($idUser > 0)
            $this->db->where('a.idUser', $idUser);
        
        // set keyword filter
        $szKeyword = trim($szKeyword);
        if($szKeyword != '')
        {
            $this->db->group_start();
            $this->db->like('u.szFirstName', $szKeyword);
            $this->db->or_like('u.szLastName', $szKeyword);
            $this->db->or_like('a.szActivity', $szKeyword);
            $this->db->or_like('a.szIPAddress', $szKeyword);
            $this->db->or_like('a.szUserAgent', $szKeyword);
            $this->db->group_end();
        }

        if(!$isCountOnly)
        {
            if($iLimit > 0)
                $this->db->limit($iLimit, $iOffset);
            $this->db->order_by($szSortField, $szSortOrder);
        }
        
        $query = $this->db->get();
    	if($query->num_rows() > 0)
        {
            if($isCountOnly)
                $arResult = $query->num_rows();
            else
                $arResult = $query->result_array();
        }
        return $arResult;
    }
    
    function getAllUsers()
    {  
        $this->db->select("u.*"); 
        // from table
        $this->db->from('tbl_users u'); 
        // where
        $this->db->where('u.isDeleted',0);
      
        $szSortField = "szFirstName";
        $szSortOrder = "ASC";
        $query = $this->db->order_by($szSortField, $szSortOrder)->get();  
        //echo $this->db->last_query();die;
        return ($query->num_rows() > 0 ? $query->result_array() : array()); 
    }
}
?>