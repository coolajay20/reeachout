<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        else if(!hasPermission('manage.permissions'))
        {
            redirect(base_url() . 'dashboard', 'refresh');
        }
        else
        {
            /* Load Permission Model */
            $this->load->model('Permission_Model');

            /* Load form helper */ 
            $this->load->helper('form');

            /* Load form validation library */ 
            $this->load->library('form_validation');

            /* Changing error delimiters Globally */
            $this->form_validation->set_error_delimiters('', '');
        }        
    }

    public function index($arParams=array())
    {
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        else
        {
            $data['szMetaTagTitle'] = $data['szPageName'] = "Access";
			$data['szMetaTagTitle'] = "Permissions";
            $data['arLoginUser'] = $this->session->userdata('login_user');
            $data['isLoadDataTable'] = true;
            $data['arPermissions'] = $this->Permission_Model->getAllPermissions();

            $data = array_merge($data, $arParams);

            // set breadcrumb
            #$data['arBreadCrumb'][] = array('name'=>'Dashboard', 'link'=>'dashboard');
            #$data['arBreadCrumb'][] = array('name'=>'Permissions', 'link'=>'permissions');

            $this->load->view('layout/header', $data);
            $this->load->view('permissions/list');
            $this->load->view('layout/footer');
        }
    }

    public function edit($szPermissionKey='')
    {
        $this->add($szPermissionKey);		
    }

    public function add($szPermissionKey='')
    {
        if(!is_user_login())
        {
                redirect(base_url().'login', 'refresh');
        }
        else
        {
            $data['idPermission'] = 0;
            $data['isFormError'] = false;
            $data['isDetailsSaved'] = false;
            $data['arPermissionDetails'] = array();
            $data['arLoginUser'] = $this->session->userdata('login_user');	

            if($this->input->post('p_func') == 'Save Permission Details')
            {			
                $idPermission = (int)$this->input->post('arPermission[id]');

                //Set validation rules
                $this->form_validation->set_rules('arPermission[szPermissionName]', 'Permission name', "required|is_unique[tbl_permissions:szPermissionName:id:{$idPermission}]");
                $this->form_validation->set_rules('arPermission[szDisplayName]', 'Permission display name', "required|is_unique[tbl_permissions:szDisplayName:id:{$idPermission}]");

                if($this->form_validation->run() == TRUE) 
                {				
                    $arPermissionDetails = $this->input->post('arPermission');
                    if($data['idPermission'] = $this->Permission_Model->savePermissionDetails($arPermissionDetails, $data['arLoginUser']['id']))
                    {
                        $data['isDetailsSaved'] = true;
                    }
                }
                else
                {
                    $data['isFormError'] = true;
                }
            }

            if($szPermissionKey)
            {
                $data['arPermissionDetails'] = $this->Permission_Model->getPermissionByUniqueKey($szPermissionKey);
                $data['idPermission'] = (isset($data['arPermissionDetails']['id']) ? (int)$data['arPermissionDetails']['id'] : 0);
            }
            else if($data['idPermission'] > 0)
            {
                $data['arPermissionDetails'] = $this->Permission_Model->getPermissionByID($data['idPermission']);
            }

            $data['szPageName'] = "Permissions";
            $data['szMetaTagTitle'] = ($data['idPermission'] > 0 ? 'Edit Permission' : 'Add New Permission');	
            $data['isLoadAdminForm'] = true;
            $data['isLoadCMSScripts'] = true;
            $data['isLoadValidationScript'] = true;
            
            // set breadcrumb
            $data['arBreadCrumb'][] = array('name'=>'Dashboard', 'link'=>'dashboard');
            $data['arBreadCrumb'][] = array('name'=>'Permissions', 'link'=>'permissions');
            if(!empty($data['arPlanDetails']))
                $data['arBreadCrumb'][] = array('name'=>'Edit Permission');
            else
                $data['arBreadCrumb'][] = array('name'=>'Add New Permission');

            $this->load->view('layout/header', $data);
            $this->load->view('permissions/add_edit');
            $this->load->view('layout/footer');
        }
    }

    function delete($szPermissionKey='')
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }	

        $message = array();
        $szPermissionKey = trim($szPermissionKey);
        if($szPermissionKey != '')
        {
            $arPermissionDetails = $this->Permission_Model->getPermissionByUniqueKey($szPermissionKey);
            if(!empty($arPermissionDetails))
            {
                if($arPermissionDetails['isRemovable'] == 1)
                {
                    if($arPermissionDetails['isDeleted'] == 0)
                    {
                        $arLoginUser = $this->session->userdata('login_user');
                        $this->Permission_Model->deletePermission($arPermissionDetails, $arLoginUser['id']);
                        $message['szSuccessMessage'] = "Permission {$arPermissionDetails['szDisplayName']} has been deleted successfully";
                    }
                    else
                    {
                        $message['szErrorMessage'] = "Permission {$arPermissionDetails['szDisplayName']} is already deleted.";
                    }
                }
                else
                {
                    $message['szErrorMessage'] = "Permission {$arPermissionDetails['szDisplayName']} is not removable.";
                }
            }
            else
            {
                $message['szErrorMessage'] = "Permission not found.";
            }
        }
        else
        {
            $message['szErrorMessage'] = "Invalid Permission delete request.";
        }

        $this->index($message);
    }
}
?>