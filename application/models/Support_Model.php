<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	

class Support_Model extends CI_Model

{

    function saveTicketDetails($data, $idLoginUser)
    {		
        $data = sanitize_array_values($data);
        $idTicket = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);
        
        if($idTicket > 0)
        {
            // get old details
            $arOldData = $this->getTicketByID($idTicket);
                    
            // update user record
            $data['dtUpdatedOn'] = date('Y-m-d H:i:s');
            $this->db->where(array('id'=>$idTicket));
            $this->db->update('tbl_support_tickets', $data);
            
            // add user activity
            addUserActivity($idTicket, 'tbl_support_tickets', "Your support ticket was updated.", $data, $arOldData);
        }
        else
        {
            // add ticket to database
            if(empty($data['szUniqueKey']))
            {
                $data['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_support_tickets');
            }
            $data['dtAddedOn'] = date('Y-m-d H:i:s');
            $data['szStatus'] = 1; 
            write_log('debug', "Add support ticket: ".print_R($data,true)." \n");
            
            $query = $this->db->insert('tbl_support_tickets', $data);
            $idTicket = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);
            write_log('debug', "Add support ticket: ".print_R($this->db,true)." \n");
            
            // add user activity
            addUserActivity($idTicket, 'tbl_support_tickets', "New support ticket was created.", $data);
        }
        return $idTicket;
    }
	
    function getTicketByUniqueKey($szUniqueKey)
    {
        $arTicketDetails = array();
        $szUniqueKey = trim($szUniqueKey);
        if($szUniqueKey != '')
        {
            $query = $this->db->get_where('tbl_support_tickets',array('szUniqueKey'=>$szUniqueKey, 'isDeleted'=>0));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arTicketDetails = $rows[0];
            }
        }
        return $arTicketDetails;
    }
	
   function getTicketByID($idTicket)
    {
        $arTicketDetails = array();
        $idTicket = (int)$idTicket;
        if($idTicket > 0)
        {
            $query = $this->db->get_where('tbl_support_tickets',array('id'=>$idTicket, 'isDeleted'=>0));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arTicketDetails = $rows[0];
            }
        }
        return $arTicketDetails;
    }

    function getTickets($idUser=NULL, $arFilters=array(), $szKeyword='', $isOnlyCount=false, $iLimit=25, $iOffset=0, $szSortField='dtAddedOn', $szSortOrder='DESC')
    {
        // set fields to select
        if($isOnlyCount)
            $this->db->select('id');
        else
            $this->db->select('*');
        
        // from table
        $this->db->from('tbl_support_tickets u');
        // where
        $this->db->where('u.isDeleted',0);
		
		if(!is_null($idUser)){
			$this->db->where('idUser',(int)$idUser);
		}
        
        // set custom filters
        setCustomRecordFilters($this, $arFilters);

        // set keyword filter
        $szKeyword = trim($szKeyword);
        if($szKeyword != '')
        {
            $this->db->group_start();
            $this->db->like('dtAddedOn', $szKeyword);
            $this->db->group_end();
        }
        
        // retrive records
        if($isOnlyCount)
        {
            return $this->db->get()->num_rows();
        }
        else
        {
            $query = $this->db->order_by($szSortField, $szSortOrder)->limit($iLimit, $iOffset)->get();
            //echo $this->db->last_query();
            return ($query->num_rows() > 0 ? $query->result_array() : array());
        }
    }

    function deleteTicket($arTicketDetails, $iDeletedBy)
    {
        // delete user
        $idTicket = (int)$arTicketDetails['id'];
        $this->db->where(array('id'=>$idTicket));
        $this->db->update('tbl_support_tickets', array('isDeleted'=>1, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        if($this->db->affected_rows() > 0)
        {
            // add user activity
            addUserActivity($idTicket, 'tbl_support_tickets', "Ticket {$arTicketDetails['szName']} deleted.");
            
            return true;
        }
        return false;
    }
    
    function updateStatus($arTicketKeys, $szStatus)
    {   
        $this->db->where_in('szUniqueKey', $arTicketKeys);
        $this->db->update('tbl_support_tickets', array('szStatus'=>$szStatus, 'dtUpdatedOn'=>date('Y-m-d H:i:s')));
        //echo $this->db->last_query();
        if($this->db->affected_rows() > 0)
        {
            // add user activity
            addUserActivity(1, 'tbl_support_tickets', count($arTicketKeys). " ticket moved successfully."); 
            return true;
        }
        return false;
    } 
    
    function addSupportThread($data)
    { 
        $data['isDeleted'] = 0; 
        $query = $this->db->insert('tbl_support_ticket_thread', $data);
        $idTicket = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0); 
    } 
    
    function getSupportThread($idSupportTicket)
    {
        if($idSupportTicket>0)
        {
            $this->db->select('*');
            // from table
            $this->db->from('tbl_support_ticket_thread stt');
            // where
            $this->db->where('stt.isDeleted',0); 
            $this->db->where('idSupportTicket',(int)$idSupportTicket); 
 
            $query = $this->db->get();
            //echo $this->db->last_query();
            return ($query->num_rows() > 0 ? $query->result_array() : array()); 
        } 
    }
    
    function getThreadCount($idSupportTicket,$iUserType){
        
        if($idSupportTicket>0 && $iUserType)
        {
            if($iUserType==1){ 
                $this->db->select('count(*) as iNumCount, iSeenByCustomer as iSeenBy'); //Fetching data for customer
            }
            else {
                $this->db->select('count(*) as iNumCount, iSeenByAdmin as iSeenBy'); //Fetching data for Admin
            }
            
            // from table
            $this->db->from('tbl_support_ticket_thread stt');
            // where
            $this->db->where('stt.isDeleted',0); 
            $this->db->where('stt.idSupportTicket',(int)$idSupportTicket); 
            if($iUserType==1){ 
                $this->db->group_by('iSeenByCustomer');
            }
            else {
                $this->db->group_by('iSeenByAdmin');
            } 
            $query = $this->db->get(); 
            $arResult = ($query->num_rows() > 0 ? $query->result_array() : array()); 
            
            $arrThreadCount = array('iNumUnread' => 0 , 'iNumRread' => 0); //Initiliazing array with default values
            if(!empty($arResult))
            {
                foreach($arResult as $arResults)
                {
                    if($arResults['iSeenBy']==1){
                        $arrThreadCount['iNumRread'] = $arResults['iNumCount'];
                    }
                    else {
                        $arrThreadCount['iNumUnread'] = $arResults['iNumCount'];
                    }
                }
            }
            return $arrThreadCount;
        } 
    }
    
    function resetThreadCounter($idSupportTicket,$iUserType){ 
        
        if($idSupportTicket>0 && $iUserType)
        {
            $this->db->where('idSupportTicket', $idSupportTicket);
            if($iUserType==1){ 
                $this->db->update('tbl_support_ticket_thread', array('iSeenByCustomer'=>1));
            }
            else {
                $this->db->update('tbl_support_ticket_thread', array('iSeenByAdmin'=>1));
            }
//            echo $this->db->last_query();
            return true;
        } 
    }
}

?>