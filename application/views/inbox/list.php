<?php if(isset($szSuccessMessage) && $szSuccessMessage != ''){?>
<div class="alert alert-success p5"><?php echo $szSuccessMessage;?></div>
<?php } if(isset($szErrorMessage) && $szErrorMessage != ''){?>
<div class="alert alert-danger p5"><?php echo $szErrorMessage;?></div>
<?php }?>

<div class="panel panel-visible" id="spy3">
        <?php if(!empty($arMessageDetails)){?>
        <div class="panel-heading">
		<p class="text-left">
		    <?php echo $arMessageDetails['szTitle'];?>
		</p>
	</div>
        <?php }?>
	<div class="panel-body pn">
            <?php if(!empty($arMessageDetails)){?>
            <?php echo nl2br($arMessageDetails['szContent']);?>
            <p class="mt10"><a href="/inbox" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> Back to Inbox</a></p>
            <?php } else {?>
		<table class="table table-responsive table-striped table-hover dt-dynamic" id="customer_data_listing" cellspacing="0" width="100%" data-url="<?php echo base_url();?>inbox" data-func="Get_Notifications">
			<thead>
				<tr>             	
                                    <th data-sort="n.szTitle">Title</th>
                                    <th data-sort="n.dtAddedOn">Date Added</th>
                                    <th>Action</th>
				</tr>
			</thead>
		</table>
            <?php }?>
	</div>
</div>