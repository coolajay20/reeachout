<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_Model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getAllRoles()
    {
        $this->db->from('tbl_roles');
        $this->db->order_by('szRoleName', 'ASC');
        $this->db->where(array('isDeleted'=>0));

        $query = $this->db->get();
        return ($query->num_rows() > 0 ? $query->result_array() : array());
    }
	
    function getRoleByUniqueKey($szUniqueKey)
    {
        $arDetails = array();
        $szUniqueKey = trim($szUniqueKey);
        if($szUniqueKey != '')
        {
            $query = $this->db->get_where('tbl_roles', array('szUniqueKey'=>$szUniqueKey));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    }
	
    function getRoleByID($idRole)
    {
        $arDetails = array();
        $idRole = (int)$idRole;
        if($idRole > 0)
        {
            $query = $this->db->get_where('tbl_roles', array('id'=>$idRole));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    }

    function deleteRole($arRole, $iDeletedBy)
    {
        $idRole = (int)$arRole['id'];
        $this->db->where(array('id'=>$idRole));
        $this->db->update('tbl_roles', array('isDeleted'=>1, 'iDeletedBy'=>$iDeletedBy, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        if($this->db->affected_rows())
        {
            // delete permission mapping
            $this->db->where('idRole', $idRole)->update('tbl_role_permissions', array('isDeleted'=>1, 'iDeletedBy'=>$iDeletedBy, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
            
             // add user activity
            addUserActivity($idRole, 'tbl_roles', "Role {$arRole['szDisplayName']} deleted.");
            
            return true;
        }
        return false;
    }

    function saveRoleDetails($data, $idLoginUser)
    {		
        $data = sanitize_array_values($data);
        $idRole = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);

        if($idRole > 0)
        {
            // get old details
            $arOldData = $this->getRoleByID($idRole); 
            
            $data['iUpdatedBy'] = $idLoginUser;
            $data['dtUpdatedOn'] = date('Y-m-d H:i:s');
            $this->db->where(array('id'=>$idRole));
            $this->db->update('tbl_roles', $data);
            
            // add user activity
            addUserActivity($idRole, 'tbl_roles', "Role {$data['szDisplayName']} updated.", $data, $arOldData);
        }
        else
        {
            $data['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_roles');
            $data['iAddedBy'] = $idLoginUser;
            $data['dtAddedOn'] = date('Y-m-d H:i:s');
            $query = $this->db->insert('tbl_roles', $data);
            $idRole = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);
            
            // add user activity
            addUserActivity($idRole, 'tbl_roles', "New role {$data['szDisplayName']} added.", $data);
        }
        return $idRole;
    }
    
    function getRolePermissions($idRole)
    {
        $arPermissions = array();
        $idRole = (int)$idRole;
        $this->db->select('idPermission');
        $this->db->from('tbl_role_permissions');
        $this->db->where(array('isDeleted'=>0, 'idRole'=>$idRole));

        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            foreach($query->result_array() as $row)
            {
                $arPermissions[] = $row['idPermission'];
            }
        }
        return $arPermissions;
    }
    
    function addRolePermission($idRole, $arPermissions, $idLoggedInUser)
    {
        $idRole = (int)$idRole;
        $idLoggedInUser = (int)$idLoggedInUser;
        if(!empty($arPermissions))
        {
            // add new permission
            $arRolePermission = $this->getRolePermissions($idRole);
            foreach($arPermissions as $idPermission)
            {
                if(!in_array($idPermission, $arRolePermission))
                {
                    // add new permission
                    $this->db->insert('tbl_role_permissions', array('idRole'=>$idRole, 'idPermission'=>$idPermission, 'iAddedBy'=>$idLoggedInUser));
                }
            }
            
            // delete the unused permissions
            if(!empty($arRolePermission))
            {
                $arPermissionToDelete = array_diff($arRolePermission, $arPermissions);
                if(!empty($arPermissionToDelete))
                {
                    foreach($arPermissionToDelete as $idPermission)
                    {
                        $this->db->where('idRole',$idRole)->where('idPermission', $idPermission)->update('tbl_role_permissions', array('isDeleted'=>1, 'iDeletedBy'=>$idLoggedInUser, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
                    }
                }
            }
        }
        else
        {
            // delete all role permissions
            $this->db->where('idRole',$idRole)->update('tbl_role_permissions', array('isDeleted'=>1, 'iDeletedBy'=>$idLoggedInUser, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        }
    }
}
?>