<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>New subscriber added to your account at szSiteName!</title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
Dear szFirstName,
<br /><br />
A new subscriber with following details has successfully added into your account.<br /><br /> 
Name #: szContactFirstName szContactLastName<br />
Email: szContactEmail<br /> 
Phone: szContactPhone<br /> 
<br />
<br />
To see more details about this contact please click <a href="szSiteName">here</a> to login into your account.
<br />
<br />
Best regards, <br />
The szSiteName Team
</td>
</tr>
</table>
</div>
</body>
</html>