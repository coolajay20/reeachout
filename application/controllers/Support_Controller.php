<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Support_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /* Load Marketing Model */
        $this->load->model('Support_Model');

        /* Load User Model */ 
        $this->load->model('User_Model');
        
        /* Load Email Model */ 
        $this->load->model('Email_Model');
                            
        /* Load form helper */ 
        $this->load->helper('form');

        /* Load form validation library */ 
        $this->load->library('form_validation');

        /* Changing error delimiters Globally */
        $this->form_validation->set_error_delimiters('', '');     	     

    }

    public function index()
    { 
        if(!is_user_login()) {
			
            redirect(base_url() . 'login', 'refresh');
			
        } else {
			
			$this->add();
			
		}
    }

    function add($szUserKey='')
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }
        else
        {
            // set/initialize varibales
            $data['idTicket'] = 0;
            $data['isFormError'] = false;
            $data['arTicketDetails'] = array();
            $data['isDetailsSaved'] = false;
            $szUserKey = trim($szUserKey);		
            $data['arLoginUser'] = $this->session->userdata('login_user');

            if($szUserKey != '')
            {
                $arTicketDetails = $this->Support_Model->getTicketByUniqueKey($szUserKey);
                if(!empty($arTicketDetails))
                {
                    $data['idTicket'] = $arTicketDetails['id'];
                    $data['arTicketDetails'] = $arTicketDetails;
                }
            }
            
            if($this->input->post('p_func') == 'Save Ticket Details')
            {
                $this->form_validation->set_rules('arTicket[szMessage]', 'Ticket Message', "trim|required");
                if(hasPermission('manage.users'))
                {
                    $this->form_validation->set_rules('arTicket[idUser]', 'Customer', "trim|required");
                }
                if ($this->form_validation->run() == true)
                { 
                    $arTicketDetails = $this->input->post('arTicket', true);
                            
                    if($data['idTicket'] > 0) {
                        $arTicketDetails['id'] = $data['idTicket'];
                    }
                    
                    if(!hasPermission('manage.users'))
                    { 
                        $arTicketDetails['idUser'] = $data['arLoginUser']['id'];
                    }   
                    if($data['idTicket'] = $this->Support_Model->saveTicketDetails($arTicketDetails, $data['arLoginUser']['id']))
                    {
                        $arTicketDetails = $this->Support_Model->getTicketByID($data['idTicket']);
                        $data['isDetailsSaved'] = true;
                        $data['arTicketDetails'] = $arTicketDetails; 
                        
                        $arPostData = array();
                        $arPostData['szMessage'] = $arTicketDetails['szMessage'];
                        $arPostData['idCustomer'] = $arTicketDetails['idUser']; 
                        $this->addTicketThread($arTicketDetails, $arPostData);
                    }
                }
                else
                {
                    $data['isFormError'] = true;
                }
            }
			
            // load the tickets
            $data['arTickets'] = $this->Support_Model->getTickets($data['arLoginUser']['id']);

            $data['szPageName'] = "Help Desk";
            $data['szFavIcon'] = "fa-support";
            $data['szMetaTagTitle'] = ($data['idTicket'] > 0 ? "Edit Ticket" : "Add New Ticket");	
            $data['isLoadAdminForm'] = true;
            $data['isLoadValidationScript'] = true;
            $data['isLoadDTPickerScript'] = true;
            
            if(hasPermission('manage.users'))
            {
                $data['isAdmin'] = true;
                $data['arUserDetails'] = $this->User_Model->getAllUsers();
            }
            else
            {
                $data['isAdmin'] = false;
                $data['arUserDetails'] = array();
            }

            $this->load->view('layout/header', $data);
            $this->load->view('support/add_edit');
            $this->load->view('layout/footer');
        }
    }
	
    function all($szStatus=false)
    {  
        if(!is_user_login() || !hasPermission('manage.users'))
        {
            redirect(base_url().'login', 'refresh');
        }
        else
        {
            if(empty($szStatus))
            {
                redirect(base_url().'support/all/new', 'refresh');
                die;
            }
            if(trim($this->input->get('p_func')) == 'Get_Tickets') 
            {      
                $idStatus = $this->getStatus($szStatus); 
                $ticketSearchAry['where'] = array('szStatus'=>$idStatus); 
                 
                $szSortField = 'dtAddedOn';
                $szSortOrder = 'DESC';
                
                $response = array();
                list($iLimit, $iOffset, $iDrawPage, $szKeyword, $arFilters) = parseDTRequest($this); 
              
                $iTotalRecords = $this->Support_Model->getTickets(NULL,$ticketSearchAry, $szKeyword, true); 
                $arTickets = $this->Support_Model->getTickets(NULL,$ticketSearchAry, $szKeyword, false, $iLimit, $iOffset, $szSortField, $szSortOrder);
                $response = array("draw"=>$iDrawPage, "recordsTotal"=>$iTotalRecords, "recordsFiltered"=>$iTotalRecords, "data"=>array()); 
                
                if(hasPermission('manage.users'))
                {
                    $iUserType = 2; //Admin
                }
                else
                {
                    $iUserType = 1; //Admin
                }
                if(!empty($arTickets))
                {
                    $i = 0; 
                    foreach($arTickets as $ticket)
                    { 
                        if(get_userdetail($ticket['idUser'],'szAvatarImage') != '') {
                            $szImageFileName = get_userdetail($ticket['idUser'],'szAvatarImage');
                            $szImagePath = USERS_UPLOAD_PATH."".$szImageFileName; 
                            if(file_exists($szImagePath))
                            {
                                $avatar_src = BASE_USERS_UPLOAD_URL . $szImageFileName;
                            }
                            else
                            {
                                $avatar_src = base_url() . 'assets/images/profile.png';
                            } 
                        } else { 
                            $avatar_src = base_url() . 'assets/images/profile.png';
                        }
                        $arThreadDetails = $this->Support_Model->getThreadCount($ticket['id'],$iUserType);
                       
                        $szCheckbox = '<div class="ckbox ckbox-success">'
                                . '<input type="checkbox" name="arTicket[szUniqueKey][]" value="'. $ticket['szUniqueKey'].'" id="'. $ticket['szUniqueKey'].'">'
                                . '<label for="'. $ticket['szUniqueKey'].'"></label>';
                        switch ($ticket['szType']) {
                            case "0" :
                                $ticket['szType'] = "General Inquiry";
                                $fa = "fa-info";
                                break;
                            case "1" :
                                $ticket['szType'] = "Technical Assistance";
                                $fa = "fa-cogs";
                                break;
                            case "2":
                                $ticket['szType'] = "Billing / Refunds";
                                $fa = "fa-dollar";
                                break;
                            case "3":
                                $ticket['szType'] = "Improvements";
                                $fa = "fa-magic";
                                break;
                            case "4":
                                $ticket['szType'] = "Bug";
                                $fa = "fa-bug";
                                break;
                        }
                        $szType = '<i class="fa '.$fa.' tooltips" data-toggle="tooltip" title="" data-original-title="'.$ticket['szType'].'"></i>';
                        $szAvtarImage = '<div class="media">
                            <a href="#" class="pull-left"> 
                              <img alt="" src="'. $avatar_src .'" class="media-object">
                            </a>
                            <div class="media-body">
                                <span class="media-meta pull-right">'. convert_date($ticket['dtAddedOn'],1) .'</span>
                                <h4 class="text-primary">'. get_userdetail($ticket['idUser'],'szFirstName') .' '. get_userdetail($ticket['idUser'],'szLastName') .'</h4>
                                <small class="text-muted"></small>
                                <p class="email-summary"><strong>'. $ticket['szTitle']. '</strong> '. $string = substr($ticket['szMessage'],0) .'</p>
                            </div>
                        </div>';
                        $szPriority = '';
                        switch ($ticket['szPriority']) {
                            case "0" :
                                $szPriority = "Normal";
                                break;
                            case "1" :
                                $szPriority = "Medium";
                                break;
                            case "2":
                                $szPriority = "High";
                                break;
                            case "3":
                                $szPriority = "Urgent";
                                break;												 
                        }
                        /*
                        $szStatus = '';
                        switch ($ticket['szStatus']) {
                            case "0" :
                                $szStatus = "Closed";
                                break;
                            case "1" :
                                $szStatus = "New";
                                break;
                            case "2":
                                $szStatus = "Pending";
                                break;
                        }
                        * 
                        */
                        if(isset($arThreadDetails['iNumUnread']) && $arThreadDetails['iNumUnread']>0){
                            $messagecount = $arThreadDetails['iNumUnread'];
                            $szClass=" unread-messages";
                        } else {
                            $messagecount = $arThreadDetails['iNumRread'];
                            $szClass=" read-messages";
                        }
                        
                        $szPriorityStr = '<a href="'.base_url()."support/ticket/".$ticket['szUniqueKey'].'" class="tp-icon'.$szClass.'"><i class="glyphicon glyphicon-envelope"></i><span class="badge" style="top:0px;">'. ($messagecount > 0 ? $messagecount : '') .'</span></a>';   
                        $szPriorityStr .= '<br>'.$szPriority; 
                        //$szPriorityStr .= " ".print_R($arThreadDetails,true);
                        
                        $szActionLinks = '<li><a href="'.base_url()."support/ticket/".$ticket['szUniqueKey'].'">View/Reply</a></li>';   
                        $szLinks = '<div class="btn-group"><a data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-cog"></i></a><ul role="menu" class="dropdown-menu pull-right">'.$szActionLinks.'</ul></div>';
                        
                        $response['data'][$i][0] = $szCheckbox;
                        $response['data'][$i][1] = $szType;
                        $response['data'][$i][2] = $szAvtarImage;
                        $response['data'][$i][3] = $szPriorityStr;
                        //$response['data'][$i][4] = $szStatus;
                        $response['data'][$i][4] = $szLinks;
                        $i++;
                    }  
                } 
                echo json_encode($response,true);
            }
            else 
            { 
                // set/initialize varibales
                $data['arLoginUser'] = $this->session->userdata('login_user');
  
                $data['szPageName'] = "Help Desk";
                $data['szFavIcon'] = "fa-support"; 
                $data['isLoadDataTable'] = true;
                $data['szType'] = $szStatus;
                
                if(strtolower($szStatus) =='new')
                {
                    $data['szMetaTagTitle'] = "New Tickets";
                }
                else if(strtolower($szStatus) =='pending')
                {
                    $data['szMetaTagTitle'] = "Pending Tickets";
                }
                else if(strtolower($szStatus) =='closed')
                {
                    $data['szMetaTagTitle'] = "Closed Tickets";
                }
                else if(strtolower($szStatus) =='trash')
                {
                    $data['szMetaTagTitle'] = "Trashed Tickets";
                }
                
                //Total Closed record
                $ticketSearchAry['where'] = array('szStatus'=>'0'); 
                $iTotalClosedRecords = $this->Support_Model->getTickets(NULL,$ticketSearchAry, '', true); 
                
                //Total New record
                $ticketSearchAry['where'] = array('szStatus'=>'1'); 
                $iTotalNewRecords = $this->Support_Model->getTickets(NULL,$ticketSearchAry, '', true); 

                //Total Pending record
                $ticketSearchAry['where'] = array('szStatus'=>'2'); 
                $iTotalPendingRecords = $this->Support_Model->getTickets(NULL,$ticketSearchAry, '', true); 
                
                //Total Trashed record
                $ticketSearchAry['where'] = array('szStatus'=>'3'); 
                $iTotalTrashedRecords = $this->Support_Model->getTickets(NULL,$ticketSearchAry, '', true); 
                
                $data['iTotalClosedRecords'] = (int)$iTotalClosedRecords;
                $data['iTotalNewRecords'] = (int)$iTotalNewRecords;
                $data['iTotalPendingRecords'] = (int)$iTotalPendingRecords;
                $data['iTotalTrashedRecords'] = (int)$iTotalTrashedRecords;
                
                $this->load->view('layout/header', $data);
                $this->load->view('support/list');
                $this->load->view('layout/footer');
            } 
        }
    }
    
    function ticket($szTicketKey='')
    {
        if(!is_user_login()) {
            redirect(base_url().'login', 'refresh'); 
        } else { 
            $data = array();
            $szTicketKey = trim($szTicketKey);
            $data['arLoginUser'] = $this->session->userdata('login_user');
			
            if($szTicketKey != '') {
                $arTicketDetails = $this->Support_Model->getTicketByUniqueKey($szTicketKey);
				
                if(!empty($arTicketDetails)) {
                    /*
                    * Either 'Admin' or 'Customer the ticket belongs' to can see the comments for the Support Ticket.
                    */
                    if(hasPermission('manage.users') || $data['arLoginUser']['id']==$arTicketDetails['idUser'])
                    { 
                        if(hasPermission('manage.users'))
                        {
                            $iUserType = 2; //Admin
                        }
                        else
                        {
                            $iUserType = 1; //Admin
                        }
                        $this->Support_Model->resetThreadCounter($arTicketDetails['id'], $iUserType);        
                        
                        $szStatus = $this->getStatusById($arTicketDetails['szStatus']);
                        $szCategory = $this->getCategoryId($arTicketDetails['szType']);
                        $data['arTicketThreadDetails'] = $this->Support_Model->getSupportThread($arTicketDetails['id']);                    
                        $data['arTicketDetails'] = $arTicketDetails; 

                        $data['szPageName'] = "Help Desk";
                        $data['szFavIcon'] = "fa-support"; 
                        $data['isLoadDataTable'] = true;
                        $data['szType'] = $szStatus;
                        $data['szCategory'] = $szCategory; 

                        if(strtolower($szStatus) =='new')
                        {
                            $data['szMetaTagTitle'] = "New Tickets";
                        }
                        else if(strtolower($szStatus) =='pending')
                        {
                            $data['szMetaTagTitle'] = "Pending Tickets";
                        }
                        else if(strtolower($szStatus) =='closed')
                        {
                            $data['szMetaTagTitle'] = "Closed Tickets";
                        }
                        else if(strtolower($szStatus) =='trash')
                        {
                            $data['szMetaTagTitle'] = "Trashed Tickets";
                        } 
                        $this->load->view('layout/header', $data);
                        $this->load->view('support/details');
                        $this->load->view('layout/footer');
                    } 	
                } else { 	
                    $data['szErrorMessage'] = 'Support ticket record not exists.';
                } 
            } else { 
                $data['szErrorMessage'] = 'Support ticket record not exists.'; 
            }
        }   
    }
    
    function updateStatus() {
        if($this->input->post('p_func') == 'Update ticket status')
        {
            $arTicket = $this->input->post('arTicket', true);
            if(isset($arTicket['szUniqueKey']) && count($arTicket['szUniqueKey'])>0)
            {
                $idStatus = $this->getStatus($arTicket['szStatus']);
                $this->Support_Model->updateStatus($arTicket['szUniqueKey'],$idStatus); 
                echo "SUCCESS||||";
                die;
            }
            else
            {
                echo "ERROR||||";
                echo "Please select at-least 1 row.";
                die;
            } 
        }  
    }
    
    function getStatus($szStatus){
        $idStatus = ''; 
        if(!empty($szStatus))
        { 
            if(strtolower($szStatus)=='new') {
                $idStatus = 1;
            } else if(strtolower($szStatus)=='pending') { 
                $idStatus = 2;
            } else if(strtolower($szStatus)=='trash') {  
                $idStatus = 3;
            } else if(strtolower($szStatus)=='closed') {  
                $idStatus = 0;
            }
        }
        return $idStatus;
    }
    
    function getStatusById($idStatus){
        $szStatus = '';  
        if($idStatus==1) {
            $szStatus = 'new';
        } else if($idStatus==2) { 
            $szStatus = 'pending';
        } else if($idStatus==3) {  
            $szStatus = 'trash';
        } else {  
            $szStatus = 'closed';
        } 
        return $szStatus;
    }
    
    function getCategoryId($idCategory){
        $szCategory = '';
        switch ($idCategory) {
            case "0" :
                $szCategory = "General Inquiry"; 
                break;
            case "1" :
                $szCategory = "Technical Assistance"; 
                break;
            case "2":
                $szCategory = "Billing / Refunds"; 
                break;
            case "3":
                $szCategory = "Improvements"; 
                break;
            case "4":
                $szCategory = "Bug"; 
                break;
        }
        return $szCategory; 
    }
    
    function addComments(){ 
        if(!is_user_login())
        {
            echo "REDIRECT||||";
            die; 
        }
        else
        {
            if($this->input->post('p_func') == 'Add comments')
            {
                $arLoginUser = $this->session->userdata('login_user');
                $arComments = $this->input->post('arComments', true);
                $szUniqueKey = $arComments['szUniqueKey'];
                $arTicketDetails = $this->Support_Model->getTicketByUniqueKey($szUniqueKey);  
            
                /*
                * Either 'Admin' or 'Customer the ticket belongs' to can post comments for the Support Ticket.
                */
                if(hasPermission('manage.users') || $arLoginUser['id']==$arTicketDetails['idUser'])
                {
                    $arPostData = array();
                    $arPostData['szMessage'] = $arComments['szMessage'];
                    $arPostData['idCustomer'] = $arLoginUser['id'];
                    $this->addTicketThread($arTicketDetails,$arPostData);  
                    if(hasPermission('manage.users')){
                        /*
                        *  Once admin reply for the thread then we automatically move the thread to 'Closed' status
                        */
                        $idStatus = '0'; //0. Closed
                        $this->Support_Model->updateStatus($szUniqueKey,$idStatus); 
                    }
                    echo "SUCCESS||||";
                    die;
                }
                else
                {
                    echo "ERROR||||";
                    echo "You don't have permission to add comment for this ticket. Please try again later.";
                    die;
                } 
            }
            else
            {
                echo "ERROR||||";
                echo "Invalid request!";
                die;
            }
        }
    }
    
    
    function addTicketThread($arTicketDetails,$arPostData){
        if(!empty($arTicketDetails) && $arPostData) {
            //Logged in user
            $arLoginUser = $this->session->userdata('login_user'); 
            if(hasPermission('manage.users'))
            {
                $iUserType = 2;
                $iSeenByAdmin = 1;
                $iSeenByCustomer = 0; 
                $idCustomer = $arTicketDetails['idUser'];
            }
            else
            {
                $iUserType = 1;
                $iSeenByAdmin = 0;
                $iSeenByCustomer = 1; 
                $idCustomer = $arLoginUser['id'];
            }
            
            
            $arUserDetails = $this->User_Model->getUserByID($idCustomer);

            $addSupportThread = array();
            $addSupportThread['idSupportTicket'] = $arTicketDetails['id'];
            $addSupportThread['idUser'] = $arLoginUser['id'];
            $addSupportThread['szMessage'] = $arPostData['szMessage'];
            $addSupportThread['dtCreatedOn'] = date('Y-m-d H:i:s');
            $addSupportThread['idCreatedBy'] = $arLoginUser['id'];
            $addSupportThread['iUserType'] = $iUserType;
            $addSupportThread['iSeenByCustomer'] = $iSeenByCustomer;
            $addSupportThread['iSeenByAdmin'] = $iSeenByAdmin;
            $addSupportThread['szIPAddress'] = __REMOTE_ADDR__;
            $addSupportThread['szBrowser'] = __HTTP_USER_AGENT__;
            $addSupportThread['szReferrer'] = __HTTP_REFERER__;
            $this->Support_Model->addSupportThread($addSupportThread);
            
            
            $szSupportPageUrl = base_url().'support/ticket/'.$arTicketDetails['szUniqueKey'];
            $szSupportPageLink = '<a href="'.$szSupportPageUrl.'">clicking here</a>';
            
            $arEmailData = array(); 
            $arEmailData['szFirstName'] = $arUserDetails['szFirstName'];
            $arEmailData['szLastName'] = $arUserDetails['szLastName']; 
            $arEmailData['szEmail'] = $arUserDetails['szEmail'];     
            $arEmailData['szSiteName'] = WEBSITE_NAME;  
            $arEmailData['szSupportPageUrl'] = $szSupportPageUrl;
            $arEmailData['szSupportPageLink'] = $szSupportPageLink;
            
            if($iUserType==1)
            {  
                //Sending confirmation email to customer
                $this->Email_Model->sendSystemEmails($arEmailData,'CUSTOMER_NEW_SUPPORT_TICKET'); 

                //Sending Acknowledgement email to site admin
                $szSupportEmail = CUSTOMER_SUPPORT_EMAIL; 
                $arEmailData['szEmail'] = $szSupportEmail;     
                $this->Email_Model->sendSystemEmails($arEmailData,'ADMIN_NEW_SUPPORT_TICKET_ACK');
            }
            else if($iUserType==2)
            {
                //Sending Acknowledgement email to customer
                $this->Email_Model->sendSystemEmails($arEmailData,'CUSTOMER_NEW_SUPPORT_TICKET_ACK'); 
            }
        }
    }
    
    function importOldRecords(){ 
        $arTickets = $this->Support_Model->getTickets(NULL,array(),'',false,100);
        if(!empty($arTickets)){
            foreach($arTickets as $arTicketss)
            {
                $addSupportThread = array();
                $addSupportThread['idSupportTicket'] = $arTicketss['id'];
                $addSupportThread['idUser'] = $arTicketss['idUser'];
                $addSupportThread['szMessage'] = $arTicketss['szMessage'];
                $addSupportThread['dtCreatedOn'] = $arTicketss['dtAddedOn'];
                $addSupportThread['idCreatedBy'] = $arTicketss['idUser'];
                $addSupportThread['iUserType'] = 1;
                $addSupportThread['iSeenByCustomer'] = 1;
                $addSupportThread['iSeenByAdmin'] = 0;
                $addSupportThread['szIPAddress'] = __REMOTE_ADDR__;
                $addSupportThread['szBrowser'] = __HTTP_USER_AGENT__;
                $addSupportThread['szReferrer'] = __HTTP_REFERER__;
                //$this->Support_Model->addSupportThread($addSupportThread);
            }
        }
    } 
}
?>