<form method="post" action="<?php echo base_url();?>users/<?php echo ($idUser > 0 ? "edit/{$arUserDetails['szUniqueKey']}" : "add");?>" id="profile-form" class="validate-form">

		<?php if($isDetailsSaved){?>
		<div class="alert alert-success p5">
		<i class="fa fa-check pr10"></i>
		<strong> Congratulations!</strong>
		User details has been saved successfully.
		</div>
		<?php } if($isFormError=== TRUE){?>
		<div class="alert alert-danger p5">
			<i class="fa fa-times pr10"></i>
			<strong> Invalid Data!</strong>
			Please fix the errors below and try again.
		</div>
		<?php } else if($isFormError != ''){?>
		<div class="alert alert-danger p5">
			<i class="fa fa-times pr10"></i>
			<strong> Sorry!</strong>
			<?php echo $isFormError;?>
		</div>
		<?php }?>

		<div class="row">
		  	<!-- Personal Details Column -->
			<div class="col-md-4">
				<div class="panel panel-primary panel-border top mb15">
					<div class="panel-heading">
						<span class="panel-title">Personal Details</span>
					</div>
					<div class="panel-body">
					
						<!-- First Name Field -->
						<div class="form-group <?php if(!empty(form_error('arUser[szFirstName]'))){?>has-error<?php }?>">
						  <label class="col-sm-3 control-label">First Name <span class="asterisk">*</span></label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-user"></i></span>
							  <input type="text" name="arUser[szFirstName]" id="szFirstName" class="form-control required" placeholder="First name" value="<?php echo set_input_value('arUser[szFirstName]', (isset($arUserDetails['szFirstName']) ? $arUserDetails['szFirstName'] : ''));?>">
							</div>
						  </div>
						</div>
										
						<!-- Last Name Field -->
						<div class="form-group <?php if(!empty(form_error('arUser[szLastName]'))){?>has-error<?php }?>">
						  <label for="szLastName" class="col-sm-3 control-label">Last Name <span class="asterisk">*</span></label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-user"></i></span>
							  <input type="text" name="arUser[szLastName]" id="szLastName" class="form-control required" placeholder="Last name" value="<?php echo set_input_value('arUser[szLastName]', (isset($arUserDetails['szLastName']) ? $arUserDetails['szLastName'] : ''));?>">
							</div>
						  </div>
						</div>
											
						<!-- Phone # Field -->
						<div class="form-group <?php if(!empty(form_error('arUser[szPhone]'))){?>has-error<?php }?>">
						  <label for="szPhone" class="col-sm-3 control-label">Phone # <span class="asterisk">*</span></label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-mobile-phone"></i></span>
							  <input type="text" name="arUser[szPhone]" id="szPhone" class="form-control" placeholder="Phone Number" value="<?php echo set_input_value('arUser[szPhone]', (isset($arUserDetails['szPhone']) ? $arUserDetails['szPhone'] : ''));?>">
							</div>
						  </div>
						</div>
					
						<!-- Address Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szAddress]'))){?>has-error<?php }?>">
						  <label for="szAddress" class="col-sm-3 control-label">Address</label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-book"></i></span>
							  <input type="text" name="arUser[szAddress]" id="szAddress" class="form-control" placeholder="Address" value="<?php echo set_input_value('arUser[szAddress]', (isset($arUserDetails['szAddress']) ? $arUserDetails['szAddress'] : ''));?>">
							</div>
						  </div>
						</div>
						
						<!-- City Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szCity]'))){?>has-error<?php }?>">
						  <label for="szCity" class="col-sm-3 control-label">City</label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-map-signs"></i></span>
							  <input type="text" name="arUser[szCity]" id="szCity" class="form-control" placeholder="City" value="<?php echo set_input_value('arUser[szCity]', (isset($arUserDetails['szCity']) ? $arUserDetails['szCity'] : ''));?>">
							</div>
						  </div>
						</div>
						
						<!-- Country Field -->
						<div class="form-group <?php if(!empty(form_error('arUser[idCountry]'))){?>has-error<?php }?>">
						  <label for="idCountry" class="col-sm-3 control-label">Country</label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  	<span class="input-group-addon"><i class="fa fa-globe"></i></span>
								<select name="arUser[idCountry]" id="idCountry" class="form-control" placeholder="Country" onchange="changeStateFieldForCRM(this, 'state-field');">
									<option value="">Select Country...</option>
									<?php foreach($arCountries as $country){?>
									<option value="<?php echo $country['id'];?>" <?php echo (set_input_value('arUser[idCountry]', (isset($arUserDetails['idCountry']) ? $arUserDetails['idCountry'] : 0)) == $country['id'] ? 'selected' : '');?>><?php echo $country['szName'];?></option>
									<?php }?>
								</select>
							</div>
						  </div>
						</div>
						
					</div>
				</div>
			</div>

			<!-- Social Networks Column -->
			<div class="col-md-4"> 
				<div class="panel panel-primary panel-border top mb15">
					<div class="panel-heading">
						<span class="panel-title">Social Networks</span>
					</div>
					<div class="panel-body">
		
						<!-- Facebook Link Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szFacebookLink]'))){?>has-error<?php }?>">
						  <label for="szFacebookLink" class="col-sm-3 control-label">Facebook</label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
							  <input type="text" name="arUser[szFacebookLink]" id="szFacebookLink" class="form-control" placeholder="Facebook" value="<?php echo set_input_value('arUser[szFacebookLink]', (isset($arUserDetails['szFacebookLink']) ? $arUserDetails['szFacebookLink'] : ''));?>">
							</div>
						  </div>
						</div>
						
						<!-- LinkedIn Link Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szLinkedInID]'))){?>has-error<?php }?>">
						  <label for="szLinkedInID" class="col-sm-3 control-label">LinkedIn</label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
							  <input type="text" name="arUser[szLinkedInID]" id="szLinkedInID" class="form-control" placeholder="LinkedIn" value="<?php echo set_input_value('arUser[szLinkedInID]', (isset($arUserDetails['szLinkedInID']) ? $arUserDetails['szLinkedInID'] : ''));?>">
							</div>
						  </div>
						</div>
							
						<!-- Twitter Link Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szTwitterID]'))){?>has-error<?php }?>">
						  <label for="szTwitterID" class="col-sm-3 control-label">Twitter</label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
							  <input type="text" name="arUser[szTwitterID]" id="szTwitterID" class="form-control" placeholder="Twitter" value="<?php echo set_input_value('arUser[szTwitterID]', (isset($arUserDetails['szTwitterID']) ? $arUserDetails['szTwitterID'] : ''));?>">
							</div>
						  </div>
						</div>
						
						<!-- Dribbble Link Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szTwitterID]'))){?>has-error<?php }?>">
						  <label for="szTwitterID" class="col-sm-3 control-label">Dribbble</label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-dribbble"></i></span>
							  <input type="text" name="arUser[szTwitterID]" id="szTwitterID" class="form-control" placeholder="Dribbble" value="<?php echo set_input_value('arUser[szTwitterID]', (isset($arUserDetails['szTwitterID']) ? $arUserDetails['szTwitterID'] : ''));?>">
							</div>
						  </div>
						</div>
						
						<!-- Google+ Link Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szGoogleLink]'))){?>has-error<?php }?>">
						  <label for="szGoogleLink" class="col-sm-3 control-label">Google+</label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
							  <input type="text" name="arUser[szGoogleLink]" id="szGoogleLink" class="form-control" placeholder="Google+" value="<?php echo set_input_value('arUser[szGoogleLink]', (isset($arUserDetails['szGoogleLink']) ? $arUserDetails['szGoogleLink'] : ''));?>">
							</div>
						  </div>
						</div>
					
						<!-- Skype ID Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szSkypeID]'))){?>has-error<?php }?>">
						  <label for="szSkypeID" class="col-sm-3 control-label">Skype ID</label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-skype"></i></span>
							  <input type="text" name="arUser[szSkypeID]" id="szSkypeID" class="form-control" placeholder="Skype ID" value="<?php echo set_input_value('arUser[szSkypeID]', (isset($arUserDetails['szSkypeID']) ? $arUserDetails['szSkypeID'] : ''));?>">
							</div>
						  </div>
						</div>
			
					</div>
				</div>
			</div>

			<!-- Profile Details Column -->
			<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<span class="panel-title">Profile Details</span>
					</div>
					<div class="panel-body" >
						
						<!-- Email Address Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szEmail]'))){?>has-error<?php }?>">
						  <label for="szEmail" class="col-sm-3 control-label">Email Address <span class="asterisk">*</span></label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
							  <input type="text" name="arUser[szEmail]" id="szEmail" class="form-control required email" placeholder="Email Address" value="<?php echo set_input_value('arUser[szEmail]', (isset($arUserDetails['szEmail']) ? $arUserDetails['szEmail'] : ''));?>">
							</div>
						  </div>
						</div>
				
						<!-- USI Username Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szUSIUserName]'))){?>has-error<?php }?>">
						  <label for="szUSIUserName" class="col-sm-3 control-label">USI Username <span class="asterisk">*</span></label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
							  <input type="text" name="arUser[szUSIUserName]" id="szUSIUserName" class="form-control required" placeholder="USI Username" value="<?php echo set_input_value('arUser[szUSIUserName]', (isset($arUserDetails['szUSIUserName']) ? $arUserDetails['szUSIUserName'] : ''));?>">
							</div>
						  </div>
						</div>
						
						<!-- Username Field  -->
						<?php if(!hasPermission('manage.users')){?>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="readonlyinput">Username</label>
							<div class="col-sm-6">
								<div class="input-group mb15">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input type="text" value="<?php echo $arUserDetails['szUserName'];?>" id="readonlyinput" class="form-control" readonly>
								</div>
							</div>
						</div>
						<?php } else {?>
						<div class="form-group <?php if(!empty(form_error('arUser[szUserName]'))){?>has-error<?php }?>">
						  <label for="szUserName" class="col-sm-3 control-label">Username <span class="asterisk">*</span></label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
							  <input type="text" name="arUser[szUserName]" id="szUserName" class="form-control required" placeholder="Username" value="<?php echo set_input_value('arUser[szUserName]', (isset($arUserDetails['szUserName']) ? $arUserDetails['szUserName'] : ''));?>">
							</div>
						  </div>
						</div>
						<?php }?>
						
						<!-- Language Field  -->
						<div class="form-group">
						  <label class="col-sm-3 control-label"><?php echo lang('edit_user_field_language');?></label>
						  <div class="col-sm-5">
							<select name="arUser[szLanguage]" class="form-control mb15">
							  <option value="0" <?php if($arUserDetails['szLanguage'] == '0') echo 'selected="selected"'; ?>>English</option>
							  <option value="1" <?php if($arUserDetails['szLanguage'] == '1') echo 'selected="selected"'; ?>>Spanish</option>
							</select>
						  </div>
						</div>
							
						<!-- Password Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szPassword]'))){?>has-error<?php }?>">
						  <label for="szPassword" class="col-sm-3 control-label">Password <span class="asterisk">*</span></label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
							  <input type="password" name="arUser[szPassword]" id="szPassword" class="form-control" placeholder="Password" value="">
							</div>
						  </div>
						</div>
						
						<!-- Password Confirm Field  -->
						<div class="form-group <?php if(!empty(form_error('arUser[szPasswordRepeat]'))){?>has-error<?php }?>">
						  <label for="szPasswordRepeat" class="col-sm-3 control-label">Confirm Password <span class="asterisk">*</span></label>
						  <div class="col-sm-9">
							<div class="input-group mb15">
							  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
							  <input onkeyup="($.trim($(this).val()) != '' ? $(this).attr('type', 'password') : $(this).attr('type', 'text'));" type="password" name="arUser[szPasswordRepeat]" id="szPasswordRepeat" class="form-control matches" placeholder="Password" value="">
							</div>
						  </div>
						</div>

						<!-- Profile Picture  -->
						<div class="profile-picture col-md-12">
							<div class="alert alert-danger p5 m5 upload-error hidden"></div>
							<div class="spinner">
								<div class="spinner-dot"></div>
								<div class="spinner-dot"></div>
								<div class="spinner-dot"></div>
							</div>
							<div id="avatar" class="pn mb10"></div>
							<div class="text-center">
								<?php 
								$p_avatar_data = set_input_value('p_avatar_data','');
								if(!$isDetailsSaved && $p_avatar_data != '') {
									$p_image_src = $p_avatar_data;
								} else if($idUser > 0 && $arUserDetails['szAvatarImage'] != '') {
									$p_image_src = BASE_USERS_UPLOAD_URL . $arUserDetails['szAvatarImage'];
								} else {
									$p_image_src = base_url() . 'assets/images/profile.png';
								}
								?>
								<img class="avatar avatar-preview img-circle mb20" class="mb20" src="<?php echo $p_image_src;?>" alt="Avatar Image">                                        
								<label class="btn btn-default btn-block btn-avatar-upload" for="avatar-upload">
									<input class="sr-only" name="avatar" id="avatar-upload" accept="image/*" type="file">
									<span class="fa fa-camera"></span> Change Photo 
								</label>
								<div class="row avatar-controls hidden">
									<div class="col-md-6">
										<div id="cancel-upload" class="btn btn-block btn-danger btn-cancel-upload">
											<i class="fa fa-times"></i> Cancel
										</div>
									</div>
									<div class="col-md-6">
										<button type="button" id="save-photo" class="btn btn-success btn-block btn-save-upload">
											<i class="fa fa-check"></i> Save
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- row -->
			

		<div class="col-md-3" data-tray-height="match">
			<?php 
			$szActionHtml = '<div class="panel-footer text-right">
				<div class="admin-form">
					<a href="'.base_url().'users" class="btn btn-default"> Cancel </a>
					<button type="submit" class="btn btn-primary"> '.($idUser > 0 ? 'Save' : 'Add').' </button>
					<input type="hidden" name="p_func" value="Save User Details">	
					<input type="hidden" name="arUser[id]" value="'.$idUser.'">
					<input type="hidden" name="p_avatar_data" value="" id="p_avatar_data">
				</div>			                    	
			</div>';

			if(hasPermission('manage.users') && ($idUser == 0 || ($idUser > 0 && $idUser != $arLoginUser['id']))){?>
			<div class="panel panel-system panel-border top mb15">
				<div class="panel-heading">
					<span class="panel-title">
						<i class="fa fa-tags"></i> <?php echo ($idUser > 0 ? 'Save User Details' : 'Add New User');?>
					</span>										
				</div>
				<div class="panel-body bg-light admin-form" style="padding-bottom:0;">
					<div class="section mb10">
						<label for="idRole" class="field-label text-muted fs16 mb5">Role</label>
						<label for="idRole" class="field select">
							<select name="arUser[idRole]" id="idRole" class="form-control required" placeholder="Role">
								<?php foreach($arRoles as $role){?>
								<option value="<?php echo $role['id'];?>" <?php echo (set_input_value('arUser[idRole]', (isset($arUserDetails['idRole']) ? $arUserDetails['idRole'] : 1)) == $role['id'] ? 'selected' : '');?>><?php echo $role['szDisplayName'];?></option>						
								<?php }?>
							</select>							                    
							<label for="isActive" class="field-icon">
								<i class="arrow"></i>
							</label>
						</label>
					</div>
					<div class="section mb10">
						<label for="isActive" class="field-label text-muted fs16 mb5">Is Active?</label>
						<label for="isActive" class="field select">
							<select name="arUser[isActive]" id="isActive" class="form-control required" placeholder="Suspended">
								<option value="0" <?php echo (set_input_value('arUser[isActive]', (isset($arUserDetails['isActive']) ? $arUserDetails['isActive'] : 1)) == 0 ? 'selected' : '');?>>No</option>
								<option value="1" <?php echo (set_input_value('arUser[isActive]', (isset($arUserDetails['isActive']) ? $arUserDetails['isActive'] : 1)) == 1 ? 'selected' : '');?>>Yes</option>
							</select>							                    
							<label for="isActive" class="field-icon">
								<i class="arrow"></i>
							</label>
						</label>
					</div>
				</div>
				<?php echo $szActionHtml;?>
			</div>
			<?php }?>
		</div>

	<?php if(!hasPermission('manage.users') || ($idUser > 0 && $idUser == $arLoginUser['id'])) echo $szActionHtml;?>
</form>