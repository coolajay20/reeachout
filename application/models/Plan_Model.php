<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan_Model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getAllPlans($isOnlyActive=false)
    {
        $this->db->from('tbl_plans');
        $this->db->order_by('fPlanAmount', 'ASC');
        $this->db->where(array('isDeleted'=>0));
        if($isOnlyActive)
        {
            $this->db->where(array('isActive'=>1));
        }

        $query = $this->db->get();
        return ($query->num_rows() > 0 ? $query->result_array() : array());
    }

    function getPlanByUniqueKey($szUniqueKey)
    {
        $arDetails = array();
        $szUniqueKey = trim($szUniqueKey);
        if($szUniqueKey != '')
        {
            $query = $this->db->get_where('tbl_plans', array('szUniqueKey'=>$szUniqueKey));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    }

    function getPlanByID($idPlan)
    {
        $arDetails = array();
        $idPlan = (int)$idPlan;
        if($idPlan > 0)
        {
            $query = $this->db->get_where('tbl_plans', array('id'=>$idPlan));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    }
    
    function getPlanByPlanID($szPlanID)
    {
        $arDetails = array();
        $szPlanID = trim($szPlanID);
        if($szPlanID != '')
        {
            $query = $this->db->get_where('tbl_plans', array('szPlanID'=>$szPlanID));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    }

    function deletePlan($arPlan, $iDeletedBy)
    {
        $idPlan = (int)$arPlan['id'];
        $this->db->where(array('id'=>$idPlan));
        $this->db->update('tbl_plans', array('isDeleted'=>1, 'iDeletedBy'=>$iDeletedBy, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        if($this->db->affected_rows())
        {
             // add user activity
            addUserActivity($idPlan, 'tbl_plans', "Plan {$arPlan['szPlanName']} deleted.");
            
            return true;
        }
        return false;
    }

    function savePlanDetails($data, $idLoginUser)
    {		
        $data = sanitize_array_values($data);
        $idPlan = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);

        if($idPlan > 0)
        {
            // get old details
            $arOldData = $this->getPlanByID($idPlan);
            
            // update user
            $data['iUpdatedBy'] = $idLoginUser;
            $data['dtUpdatedOn'] = date('Y-m-d H:i:s');
            $this->db->where(array('id'=>$idPlan));
            $this->db->update('tbl_plans', $data);
            
            // add user activity
            addUserActivity($idPlan, 'tbl_plans', "Plan {$data['szPlanName']} updated.", $data, $arOldData);
        }
        else
        {
            // add new user
            $data['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_plans');
            $data['iAddedBy'] = $idLoginUser;
            $data['dtAddedOn'] = date('Y-m-d H:i:s');
            $query = $this->db->insert('tbl_plans', $data);
            $idPlan = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);
            
            // add user activity
            addUserActivity($idPlan, 'tbl_plans', "New plan {$data['szPlanName']} added.", $data);
        }
        return $idPlan;
    }
    function getPlanFeatures($idPlan,$getFeatureDetails=false)
    {
        $arFeatures = array();
        $idPlan = (int)$idPlan;
        if($getFeatureDetails)
        {
            $this->db->select('pf.idFeature, f.szFeatureName');
            $this->db->from('tbl_plan_features pf');
            $this->db->join('tbl_features f', 'f.id=pf.idFeature');
            $this->db->where(array('pf.isDeleted'=>0, 'pf.idPlan'=>$idPlan));
        }
        else
        {
            $this->db->select('idFeature');
            $this->db->from('tbl_plan_features');
            $this->db->where(array('isDeleted'=>0, 'idPlan'=>$idPlan));
        }

        $query = $this->db->get();
        //echo $this->db->last_query() . "<br><br>";
        if($query->num_rows() > 0)
        {
            foreach($query->result_array() as $row)
            {
                if($getFeatureDetails)
                    $arFeatures[] = $row;
                else
                    $arFeatures[] = $row['idFeature'];
            }
        }
        return $arFeatures;
    }
    
    function addPlanFeatures($idPlan, $arFeatures, $idLoggedInUser)
    {
        $idPlan = (int)$idPlan;
        $idLoggedInUser = (int)$idLoggedInUser;
        if(!empty($arFeatures))
        {
            // add new feature
            $arPlanFeatures = $this->getPlanFeatures($idPlan);
            foreach($arFeatures as $idFeature)
            {
                if(!in_array($idFeature, $arPlanFeatures))
                {
                    // add new feature
                    $this->db->insert('tbl_plan_features', array('idPlan'=>$idPlan, 'idFeature'=>$idFeature, 'iAddedBy'=>$idLoggedInUser));
                }
            }
            
            // delete the unused features
            if(!empty($arPlanFeatures))
            {
                $arFeatiureToDelete = array_diff($arPlanFeatures, $arFeatures);
                if(!empty($arFeatiureToDelete))
                {
                    foreach($arFeatiureToDelete as $idFeature)
                    {
                        $this->db->where('idPlan',$idPlan)->where('idFeature', $idFeature)->update('tbl_plan_features', array('isDeleted'=>1, 'iDeletedBy'=>$idLoggedInUser, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
                    }
                }
            }
        }
        else
        {
            // delete all plan features
            $this->db->where('idPlan',$idPlan)->update('tbl_plan_features', array('isDeleted'=>1, 'iDeletedBy'=>$idLoggedInUser, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        }
    }
}
?>