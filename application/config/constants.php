<?php 
 if( !defined( "__APP_PATH__" ) ) 
 define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
 defined('BASEPATH') OR exit('No direct script access allowed');
 require_once(__APP_PATH__.'/environment.php');

 /*
 |--------------------------------------------------------------------------
 | Website config constants
 |--------------------------------------------------------------------------
 |
 |  Following constants are defined as per development environment
 |
 */

 if(__ENVIRONMENT__ == "AJAY_DEV")
 {
      define( "__DBC_HOST__", "localhost" );
      define( "__DBC_USER__", "root" );
      define( "__DBC_PASSWD__", "" );//abkjri3u 
      define( "__DBC_SCHEMATA__", "bitcoin"); 
      define('BASE_URL', "http://www.old.transport-eca.com/");
      define('BASE_ADMIN_URL', BASE_URL."admin/");
	 
      define( "__FACEBOOK_APP_ID__", "671570486373200" );
      define( "__FACEBOOK_APP_SECRET_KEY__", "f6b6d8588dcb4b4193321adf837ef9d2" );
	 
      define( "__BITPAY_PUBLIC_KEY_PATH__", __APP_PATH__."/application/libraries/Bitpay/keys/bitpay.pub" );
      define( "__BITPAY_PRIVATE_KEY_PATH__", __APP_PATH__."/application/libraries/Bitpay/keys/bitpay.pri" );
      define( "__BITPAY_MERCHANT_PAIRED_KEY__", "ka5Ld8D" );

     // define stripe keys
     define('STRIPE_CLIENT_ID', 'ca_9gbVPlIgSUF9OuvK4Qm6Vo0mBPauA71O');
     define('STRIPE_ACCESS_KEY', 'sk_test_q45HQG7brkGaXmV0MRSwrAmV');
     define('STRIPE_PUBLISHABLE_KEY', 'pk_test_FxzmY4rmlgUlEkUOK9tgtdKr');
	 
     define( "__BITPAY_MERCHANT_TOKEN__", "9Pei17evkvTD2ujjBBmhJgf4C4Mt1P8TavvL69FnHyAd" );
     
     define( "__WRITE_LOG__", true);
 } 
 else if(__ENVIRONMENT__=='LALIT_DEV') 
 { 
     define( "__DBC_HOST__", "localhost" ); 
     define( "__DBC_USER__", "root" ); 
     define( "__DBC_PASSWD__", "" );//abkjri3u  
     define( "__DBC_SCHEMATA__", "reeachout");  
     define('BASE_URL', "http://local.reeachout.com/"); 
     define('BASE_ADMIN_URL', BASE_URL."admin/"); 
     define( "__FACEBOOK_APP_ID__", "671570486373200" ); 
     define( "__FACEBOOK_APP_SECRET_KEY__", "f6b6d8588dcb4b4193321adf837ef9d2" );  
     define( "__BITPAY_PUBLIC_KEY_PATH__", __APP_PATH__."/application/libraries/Bitpay/keys/bitpay.pub" ); 
     define( "__BITPAY_PRIVATE_KEY_PATH__", __APP_PATH__."/application/libraries/Bitpay/keys/bitpay.pri" ); 
     define( "__BITPAY_MERCHANT_PAIRED_KEY__", "ka5Ld8D" );  

     // define stripe keys 
     define('STRIPE_CLIENT_ID', 'ca_9gbVPlIgSUF9OuvK4Qm6Vo0mBPauA71O'); 
     define('STRIPE_ACCESS_KEY', 'sk_test_q45HQG7brkGaXmV0MRSwrAmV'); 
     define('STRIPE_PUBLISHABLE_KEY', 'pk_test_FxzmY4rmlgUlEkUOK9tgtdKr');  
     define( "__BITPAY_MERCHANT_TOKEN__", "9Pei17evkvTD2ujjBBmhJgf4C4Mt1P8TavvL69FnHyAd" );
     define( "__WRITE_LOG__", true);
 }  

 else if(__ENVIRONMENT__=='OM_DEV') 
 { 
     define( "__DBC_HOST__", "localhost" ); 
     define( "__DBC_USER__", "root" ); 
     define( "__DBC_PASSWD__", "" );//abkjri3u  
     define( "__DBC_SCHEMATA__", "bitcoinl_absusi"); 
     define('BASE_URL', "http://usitech.localhost"); 
     define('BASE_ADMIN_URL', BASE_URL."admin/"); 
     define( "__FACEBOOK_APP_ID__", "671570486373200" ); 
     define( "__FACEBOOK_APP_SECRET_KEY__", "f6b6d8588dcb4b4193321adf837ef9d2" );
     define( "__BITPAY_PUBLIC_KEY_PATH__", __APP_PATH__."/application/libraries/Bitpay/keys/bitpay.pub" ); 
     define( "__BITPAY_PRIVATE_KEY_PATH__", __APP_PATH__."/application/libraries/Bitpay/keys/bitpay.pri" ); 
     define( "__BITPAY_MERCHANT_PAIRED_KEY__", "ka5Ld8D" );  

     // define stripe keys 
     define('STRIPE_CLIENT_ID', 'ca_9gbVPlIgSUF9OuvK4Qm6Vo0mBPauA71O'); 
     define('STRIPE_ACCESS_KEY', 'sk_test_q45HQG7brkGaXmV0MRSwrAmV'); 
     define('STRIPE_PUBLISHABLE_KEY', 'pk_test_FxzmY4rmlgUlEkUOK9tgtdKr');  
     define( "__BITPAY_MERCHANT_TOKEN__", "9Pei17evkvTD2ujjBBmhJgf4C4Mt1P8TavvL69FnHyAd" );     
     define( "__WRITE_LOG__", true);
 } 

 else if(__ENVIRONMENT__ == "USI_DEV") 
 { 
     define( "__DBC_HOST__", "localhost" ); 
     define( "__DBC_USER__", "bitcoinl_absusi" ); 
     define( "__DBC_PASSWD__", "VDRTe%oTQh#s" );//abkjri3u
     define( "__DBC_SCHEMATA__", "bitcoinl_absusi");
	 
     define('BASE_URL', "https://bitcoinlabs.io");
     define('BASE_ADMIN_URL', BASE_URL."admin/");

     define( "__FACEBOOK_APP_ID__", "1907373416246718" ); 
     define( "__FACEBOOK_APP_SECRET_KEY__", "eaeb793c0c407d0ce6c0a504f58bd399" ); 
	 
     define( "__BITPAY_PUBLIC_KEY_PATH__", __APP_PATH__."/application/libraries/Bitpay/keys/bitpay.pub" ); 
     define( "__BITPAY_PRIVATE_KEY_PATH__", __APP_PATH__."/application/libraries/Bitpay/keys/bitpay.pri" ); 
     define( "__BITPAY_MERCHANT_PAIRED_KEY__", "ka5Ld8D" ); 
     define( "__BITPAY_MERCHANT_TOKEN__", "9Pei17evkvTD2ujjBBmhJgf4C4Mt1P8TavvL69FnHyAd" );  

     // define stripe keys
     define('STRIPE_CLIENT_ID', 'ca_9gbVPlIgSUF9OuvK4Qm6Vo0mBPauA71O'); 

 	//Test
 	#define('STRIPE_ACCESS_KEY', 'sk_test_CT78m64DDv86v1oytbkrfqIj'); 
 	#define('STRIPE_PUBLISHABLE_KEY', 'pk_test_LVHJb4RmvG6aebhLOThZN2Di'); 
	 
 	//Live
 	define('STRIPE_ACCESS_KEY', 'sk_live_lHrWAWuFrgo108HwpFfgvE20'); 
 	define('STRIPE_PUBLISHABLE_KEY', 'pk_live_IROdLPlvb0lwLJStnKPUmBEL'); 

    define( "__WRITE_LOG__", true);
 } 

 define( "__APP_PATH_EMAIL_ATTACHMENT__", __APP_PATH__."/attachments" ); 
 define('BASE_URL_DATATABLES', BASE_URL."/theme/vendor/plugins/DataTablesV15");  

 /* 
 |-------------------------------------------------------------------------- 
 | Display Debug backtrace 
 |-------------------------------------------------------------------------- 
 | 
 | If set to TRUE, a backtrace will be displayed along with php errors. If 
 | error_reporting is disabled, the backtrace will not display, regardless 
 | of this setting 
 |
 */

 defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

 /*
 |--------------------------------------------------------------------------
 | File and Directory Modes
 |--------------------------------------------------------------------------
 |
 | These prefs are used when checking and setting modes when working
 | with the file system.  The defaults are fine on servers with proper
 | security, but you may wish (or even need) to change the values in
 | certain environments (Apache running a separate process for each
 | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
 | lways be used to set the mode correctly.
 |
 */

 defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
 defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
 defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
 defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

 /*
 |--------------------------------------------------------------------------
 | File Stream Modes
 |--------------------------------------------------------------------------
 |
 | These modes are used when working with fopen()/popen()
 |
 */

 defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
 defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
 defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care

 defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care

 defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
 defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
 defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
 defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

 /*
 |--------------------------------------------------------------------------
 | Exit Status Codes
 |--------------------------------------------------------------------------
 |
 | Used to indicate the conditions under which the script is exit()ing.
 | While there is no universal standard for error codes, there are some
 | broad conventions.  Three such conventions are mentioned below, for
 | those who wish to make use of them.  The CodeIgniter defaults were
 | chosen for the least overlap with these conventions, while still
 | leaving room for others to be defined in future versions and user
 | applications.
 |
 | The three main conventions used for determining exit status codes
 | are as follows:
 |
 |    Standard C/C++ Library (stdlibc):
 |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
 |       (This link also contains other GNU-specific conventions)
 |    BSD sysexits.h:
 |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
 |    Bash scripting:
 |       http://tldp.org/LDP/abs/html/exitcodes.html
 |
 */

 defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors 
 defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error 
 defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error 
 defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found 
 defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class 
 defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member 
 defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input 
 defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error 
 defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code 
 defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code  

 define('CUSTOMER_SUPPORT_EMAIL', 'support@bitcoinlabs.io'); //default from email
 define('CUSTOMER_SUPPORT_TITLE', 'Bitcoin Labs Support'); //default user name for emails

 define('USER_COOKIE', 'ree@c#0u#_u&#!'); 
 define('ADMIN_COOKIE', 'ree@c#0u#_@d#!n'); 
 define('ENCRYPT_KEY', 'ree@c#0u#');  

 define('WEBSITE_NAME', 'Bitcoin Labs');
 define('WEBSITE_NAME_LONG', 'Bitcoin Labs | Digital Marketing System');

 /*
  * Constants for Sendgrid email 
  */

 define('EMAIL_PROTOCOL', 'SMTP');
 define('SMTP_HOST', 'c41969.sgvps.net');
 define('SMTP_USER', 'support@bitcoinlabs.io');
 define('SMTP_PASSWORD', 'nc6xX+WWi@.e');
 define('SMTP_PORT', '465');
 define('SMTP_CRYPTO', 'SSL');

 define('BASE_ASSETS_URL', BASE_URL.'assets/');
 define('BASE_IMAGES_URL', BASE_ASSETS_URL.'images/');
 define('BASE_JS_URL', BASE_ASSETS_URL.'js/');
 define('BASE_CSS_URL', BASE_ASSETS_URL.'css/');

 // define site paths 
 define('SITE_PATH', __APP_PATH__); 
 define('DIR_UPLOADS', 'uploads/'); 
 define('DIR_USER_UPLOADS', DIR_UPLOADS.'users/'); 
 define('DIR_CONTACT_UPLOADS', DIR_UPLOADS.'/contacts/'); 

 define('__EMAIL_TEMPLATE_PATH__', SITE_PATH.'/application/views/email');

 // upload folder path
 define('UPLOAD_PATH', SITE_PATH . '/' . DIR_UPLOADS);
 define('USERS_UPLOAD_PATH', SITE_PATH . '/' . DIR_USER_UPLOADS);
 define('BASE_USERS_UPLOAD_URL', BASE_URL ."/". DIR_USER_UPLOADS);
 define( "__WEBSITE_SITE_TITLE__", "Bitcoin Labs" );

 /**
  * Client IP Constant Definitions.
  *
  */

 if(isset($_SERVER['REMOTE_ADDR'])) {
     define( "__REMOTE_ADDR__", $_SERVER['REMOTE_ADDR'] );
 }

 if(isset($_SERVER['HTTP_REFERER'])) {

     define( "__HTTP_REFERER__", $_SERVER['HTTP_REFERER'] );

 }

 if(isset($_SERVER['HTTP_USER_AGENT'])) {

     define( "__HTTP_USER_AGENT__",  $_SERVER['HTTP_USER_AGENT'] );

 }

 //Possible E-mail Status
 define( "__EMAIL_STATUS_PENDING__", "Pending" );
 define( "__EMAIL_STATUS_COMPLETE__", "Complete" ); 
 define( "__EMAIL_STATUS_UNSUBSCRIBE__", "Unsubscribe" ); 