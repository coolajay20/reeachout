        <?php if(!empty($arBreadCrumb)){$iTotal = count($arBreadCrumb);?>
        <header id="topbar" class="alt">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <?php foreach($arBreadCrumb as $i=>$crumb){?>
                    <li class="crumb-<?php echo ($i == 0 ? 'active' : (($i+1) == $iTotal ? 'trail' : 'link'));?>">
                        <?php if(($i+1) == $iTotal){?>
                        <?php echo $crumb['name'];?>
                        <?php } else {?>
              		<a href="<?php echo base_url();?><?php echo $crumb['link'];?>"><?php echo $crumb['name'];?></a>
                        <?php }?>
                    </li>
                    <?php }?>
               </ol>
            </div>        	
     	</header>
      	<!-- End: Topbar -->
        <?php }?>