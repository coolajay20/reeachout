    <!-- Start: Topbar -->
    <section id="content_wrapper">	
        <?php require_once APPPATH . 'views/breadcrumb.php';?>
      	
        <!-- Begin: Content -->
        <form method="post" action="<?php echo base_url();?>permissions/<?php echo ($idPermission > 0 ? "edit/{$arPermissionDetails['szUniqueKey']}" : "add");?>" id="profile-form" class="validate-form">	
            <section id="content" class="table-layout">
                <div class="tray tray-center">		     	
                    <div class="mw1000 center-block">    
                        <?php if($isDetailsSaved){?>
                        <div class="alert alert-success p5">
                            <i class="fa fa-check pr10"></i>
                            <strong> Congratulation!</strong>
                            Permission details has been saved successfully.
                        </div>
                        <?php } if($isFormError){?>
                        <div class="alert alert-danger p5">
                            <i class="fa fa-times pr10"></i>
                            <strong> Invalid Data!</strong>
                            Please fix the errors below and try again.
                        </div>
                        <?php }?>
	
                        <div class="panel panel-primary panel-border top mb35">
                                <div class="panel-heading">
                                    <span class="panel-title">
                                        <i class="fa fa-bookmark-o"></i>
                                        Permission Details
                                    </span>
                                </div>
                                <div class="panel-body bg-light" style="padding-bottom:0;">
                                    <div class="admin-form">
                                        <div class="section row">
                                            <label for="szPermissionName" class="field-label col-md-3 text-right-not-md">Name:<span class="text-danger">*</span></label>
                                            <?php $szPermissionNameError = form_error('arPermission[szPermissionName]');?>
                                            <div class="col-md-9">
                                                <label for="szPermissionName" class="field prepend-icon<?php if(!empty($szPermissionNameError)){?> state-error<?php }?>">
                                                    <input type="text" name="arPermission[szPermissionName]" id="szPermissionName" class="gui-input required" placeholder="Permission Name" value="<?php echo set_input_value('arPermission[szPermissionName]',(isset($arPermissionDetails['szPermissionName']) ? $arPermissionDetails['szPermissionName'] : ''));?>">
                                                    <label for="szPermissionName" class="field-icon">
                                                        <i class="fa fa-star"></i>
                                                    </label>
                                                </label>
                                                <?php if(!empty($szPermissionNameError)){?><em class="state-error" for="szPermissionName"><?php echo $szPermissionNameError;?></em><?php }?>
                                            </div>
                                        </div>
                                        
                                        <div class="section row">
                                            <label for="szDisplayName" class="field-label col-md-3 text-right-not-md">Display Name:<span class="text-danger">*</span></label>
                                            <?php $szDisplayNameError = form_error('arPermission[szDisplayName]');?>
                                            <div class="col-md-9">
                                                <label for="szDisplayName" class="field prepend-icon<?php if(!empty($szDisplayNameError)){?> state-error<?php }?>">
                                                    <input type="text" name="arPermission[szDisplayName]" id="szDisplayName" class="gui-input required" placeholder="Permission Display Name" value="<?php echo set_input_value('arPermission[szDisplayName]',(isset($arPermissionDetails['szDisplayName']) ? $arPermissionDetails['szDisplayName'] : ''));?>">
                                                    <label for="szDisplayName" class="field-icon">
                                                        <i class="fa fa-star"></i>
                                                    </label>
                                                </label>
                                                <?php if(!empty($szDisplayNameError)){?><em class="state-error" for="szDisplayName"><?php echo $szDisplayNameError;?></em><?php }?>
                                            </div>
                                        </div>

                                        <div class="section row">
                                            <label for="szDescription" class="field-label col-md-3 text-right-not-md">Description</label>
                                            <?php $szDescriptionError = form_error('arPermission[szDescription]');?>
                                            <div class="col-md-9">
                                                <label class="field prepend-icon<?php if(!empty($szDescriptionError)){?> state-error<?php }?>">
                                                    <textarea class="gui-textarea" maxlength="4000" id="szDescription" name="arPermission[szDescription]" placeholder="Description"><?php echo set_input_value('arPermission[szDescription]', (isset($arPermissionDetails['szDescription']) ? $arPermissionDetails['szDescription'] : ''));?></textarea>
                                                    <label for="szDescription" class="field-icon">
                                                      <i class="fa fa-file-text-o"></i>
                                                    </label>
                                                    <span class="input-footer">
                                                      <strong>Enter</strong> brief description about this permission type.</span>
                                                </label>
                                                <?php if(!empty($szDescriptionError)){?><em class="state-error" for="szDescription"><?php echo $szDescriptionError;?></em><?php }?>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <aside class="tray tray-right tray350" data-tray-height="match" style="height: 535px;">
                        <div class="panel panel-system panel-border top mb35">
                            <div class="panel-heading">
                                <span class="panel-title">
                                    <i class="fa fa-tags"></i> <?php echo ($idPermission > 0 ? 'Save Permission Details' : 'Add New Permission');?>
                                </span>										
                            </div>
                            <!-- <div class="panel-body bg-light" style="padding-bottom:0;">
                                <div class="admin-form">		                    
                                    <div class="section">
                                        <label for="isActive" class="field-label text-muted fs16 mb10">Is Active?</label>
                                        <label for="isActive" class="field select">
                                            <select name="arPermission[isActive]" id="isActive" class="gui-input required" placeholder="Suspended">
                                                <option value="0" <?php echo (set_input_value('arPermission[isActive]', (isset($arPermissionDetails['isActive']) ? $arPermissionDetails['isActive'] : 1)) == 0 ? 'selected' : '');?>>No</option>
                                                <option value="1" <?php echo (set_input_value('arPermission[isActive]', (isset($arPermissionDetails['isActive']) ? $arPermissionDetails['isActive'] : 1)) == 1 ? 'selected' : '');?>>Yes</option>
                                            </select>							                    
                                            <label for="isActive" class="field-icon">
                                                <i class="arrow"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
                            </div>-->
                            <div class="panel-footer text-right">
                                <div class="admin-form">
                                    <a href="<?php echo base_url();?>permissions" class="button btn-default"> Cancel </a>
                                    <button type="submit" class="button btn-primary"> <?php echo ($idPermission > 0 ? 'Save' : 'Add');?> </button>
                                    <input type="hidden" name="p_func" value="Save Permission Details">	
                                    <input type="hidden" name="arPermission[id]" value="<?php echo $idPermission;?>">
                                </div>			                    	
                            </div>
                        </div>
                    </aside>
                </section>
            </form>
	</section>