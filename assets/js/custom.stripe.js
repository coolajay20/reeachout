// service subscription stripe setup
if($('.btn-configure-subscription-setup').length > 0){	
    $('.btn-configure-subscription-setup').each(function(){
            var handler = "";
            if($.trim($(this).attr('data-monthly')) == 'true'){
                handler = configureAccessDanceStripeSetup(this);
            }	
            else 
            {
                handler = configureStripeBitcoinPaymentSetup(this);
            }
            $(this).click(function(){
                    var obj = this;
                    $(this).button('loading');
                    bootbox.confirm({
                    title: "Bitcoin Labs " + $(this).attr('data-plan-name') + " Subscription!",
                    message: '<h3 class=\'text-danger mb20\'>Are you sure you want to ' + ($.trim($(obj).attr('data-old-subscription')) != '' ? 'renew for ' : 'subscribe to ') + '"' + $(this).attr('data-plan-name') + '" plan?</h3>',
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancel'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirm'
                        }
                    },
                    callback: function (result) {
                            if(result) {
                                    if($.trim($(obj).attr('data-monthly')) == 'false'){
                                            //createBusinessSubscription(obj, '')
                                            handler.open();
                                        } else {
                                            handler.open();
                                        }
                                    }
                                    $(obj).button('reset');
                            }
                    });
            });
    });
}

function configureAccessDanceStripeSetup(obj)
{  		
    var szPublicKey = $(obj).attr('data-publishable-key');
    var szLogoImage = $(obj).attr('data-logo');
    var szEmail = $(obj).attr('data-email');
    var szDescription = $(obj).attr('data-desc');
    var szButtonLabel = $(obj).attr('data-button-label');
    var szAction = $(obj).attr('data-action');

    return StripeCheckout.configure({
        key: szPublicKey,
        image: szLogoImage,
        name: 'Bitcoin Labs Marketing System',
        email: szEmail,
        description: szDescription,
        currency: 'usd',
        allowRememberMe: false,
        panelLabel: szButtonLabel,
        token: function(token) {
            var szTokenId = token.id;
            if(szTokenId != '') {
                if(szAction == 'save-card-details')
                    saveBusinessPaymentDetails(obj, szTokenId);
                else
                    createBusinessSubscription(obj, szTokenId);
            } else {
                bootbox.alert("<h3 class='text-danger'>Something went wrong while processing your request. Please try again after some time.</h3>");
            }
        }
    });
}

function configureStripeBitcoinPaymentSetup(obj)
{  		
    var szPublicKey = $(obj).attr('data-publishable-key');
    var szLogoImage = $(obj).attr('data-logo');
    var szEmail = $(obj).attr('data-email');
    var szDescription = $(obj).attr('data-desc');
    var szButtonLabel = $(obj).attr('data-button-label');
    var szAction = $(obj).attr('data-action');
    var fAmount = $(obj).attr('data-amount');
    fAmount = parseFloat(fAmount) * 100;

    return StripeCheckout.configure({
        key: szPublicKey,
        image: szLogoImage,
        name: 'Bitcoin Labs Marketing System',
        email: szEmail,
        description: szDescription,
        currency: 'usd',
        amount: fAmount,
        allowRememberMe: false,
        bitcoin: true, 
        panelLabel: szButtonLabel,
        token: function(token) {
            var szTokenId = token.id;
            if(szTokenId != '') {
                if(szAction == 'save-card-details')
                    saveBusinessPaymentDetails(obj, szTokenId);
                else
                    createBusinessSubscription(obj, szTokenId);
            } else {
                bootbox.alert("<h3 class='text-danger'>Something went wrong while processing your request. Please try again after some time.</h3>");
            }
        }
    });
}
function saveBusinessPaymentDetails(obj, szTokenID)
{
    var url = $(obj).attr('data-url');
    show_page_loader();			
    $.ajax({
        method: 'GET',
        url: url + "?stripeToken=" + szTokenID,
        success: function(result){
            var ar_result = result.split('||||');
            if(ar_result[0] == 'SUCCESS'){
                hide_page_loader();
                bootbox.alert("<h3 class='text-success'>Congratulations ! Your card details successfully updated.</h3>");
                window.location = url;
            } else {
                hide_page_loader();
                bootbox.alert("<h3 class='text-danger'>" + ar_result[1] + "</h3>");
            }
        },
        error: function(){
            hide_page_loader();
            bootbox.alert("<h3 class='text-danger'>Something went wrong while processing your request. Please try again after some time.</h3>");
        }
    });
}

function createBusinessSubscription(obj, szTokenID)
{
    var url = $(obj).attr('data-url');
    var szPlanID = $(obj).attr('data-plan-id');
    var szPlanName = $(obj).attr('data-plan-name');
    var szOldSubscriptionID = $(obj).attr('data-old-subscription');
    var iSubscriptionType = parseInt($(obj).attr('data-subscription-type'));

    show_page_loader();			
    $.ajax({
            method: 'GET',
            url: url + "/subscription-callback?token=" + szTokenID + "&plan=" + szPlanID + "&type=" + iSubscriptionType,
            success: function(result){
                var ar_result = result.split('||||');
                if(ar_result[0] == 'SUCCESS'){
                    hide_page_loader();
                    bootbox.alert("<h3 class='text-success'>Congratulations ! You are successfully " + (szOldSubscriptionID != '' ? 'renewed for ' : 'subscribed to ') + szPlanName + " plan.</h3>");
                    $(obj).html('<a class="btn btn-success btn-md" alt="Already Subscribed"><i class="fa fa-check"></i> Already Subscribed</a>');
                    window.location = base_url + 'dashboard';
                } else {
                    hide_page_loader();
                    bootbox.alert("<h3 class='text-danger'>" + ar_result[1] + "</h3>");
                }
        },
        error: function(){
            hide_page_loader();
            bootbox.alert("<h3 class='text-danger'>Something went wrong while processing your request. Please try again after some time.</h3>");
        }
    });
}