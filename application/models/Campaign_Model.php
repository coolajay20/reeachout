<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Campaign_Model extends CI_Model
{
    function getCampaignByUniqueKey($szUniqueKey)
    {
        $arTemplateDetails = array();
        $szUniqueKey = trim($szUniqueKey);
        if($szUniqueKey != '')
        {
            $query = $this->db->get_where('tbl_campaigns',array('szUniqueKey'=>$szUniqueKey, 'isDeleted'=>0));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arTemplateDetails = $rows[0];
            }
        }
        return $arTemplateDetails;
    }

    function getCampaignByID($idTemplate)
    {
        $arTemplateDetails = array();
        $idTemplate = (int)$idTemplate;
        if($idTemplate > 0)
        {
            $query = $this->db->get_where('tbl_campaigns',array('id'=>$idTemplate, 'isDeleted'=>0));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arTemplateDetails = $rows[0];
            }
        }
        return $arTemplateDetails;
    }
    
    function saveCampaignDetails($data, $idLoginUser)
    {
        $idTemplate = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);
        
        if($idTemplate > 0)
        {
            // get old details
            $arOldData = $this->getCampaignByID($idTemplate);
                    
            // update user record
            $data['dtUpdatedOn'] = date('Y-m-d H:i:s');
            $this->db->where(array('id'=>$idTemplate));
            $this->db->update('tbl_campaigns', $data);
            
            // add user activity
            addUserActivity($idTemplate, 'tbl_campaigns', "Updated campaign details for {$data['szName']}.", $data, $arOldData);
        }
        else
        {
            // add user record
            if(empty($data['szUniqueKey']))
            {
                $data['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_campaigns');
            }
            $data['dtAddedOn'] = date('Y-m-d H:i:s');
            $query = $this->db->insert('tbl_campaigns', $data);
            $idTemplate = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);
            
            // add user activity
            addUserActivity($idTemplate, 'tbl_campaigns', "New campaign added for {$data['szName']}.", $data);
        }
        return $idTemplate;
    }

    function deleteCampaign($arTemplateDetails, $iDeletedBy)
    {
        // delete user
        $idTemplate = (int)$arTemplateDetails['id'];
        $this->db->where(array('id'=>$idTemplate));
        $this->db->update('tbl_campaigns', array('isDeleted'=>1, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        if($this->db->affected_rows() > 0)
        {
            // add user activity
            addUserActivity($idTemplate, 'tbl_campaigns', "Campaign {$arTemplateDetails['szName']} deleted.");
            
            return true;
        }
        return false;
    }
    
    function saveTemplateDetails($data, $idLoginUser)
    {		
        $data = sanitize_array_values($data,array('szMessageContent'));
        $idTemplate = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);
        
        if($idTemplate > 0)
        {
            // get old details
            $arOldData = $this->getTemplateByID($idTemplate);
                    
            // update user record
            $data['dtUpdatedOn'] = date('Y-m-d H:i:s');
            $this->db->where(array('id'=>$idTemplate));
            $this->db->update('tbl_email_templates', $data);
            
            // add user activity
            addUserActivity($idTemplate, 'tbl_email_templates', "Updated email template details for {$data['szName']}.", $data, $arOldData);
        }
        else
        {
            // add user record
            if(empty($data['szUniqueKey']))
            {
                $data['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_email_templates');
            }
            $data['dtAddedOn'] = date('Y-m-d H:i:s');
            $query = $this->db->insert('tbl_email_templates', $data);
            $idTemplate = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);
            
            // add user activity
            addUserActivity($idTemplate, 'tbl_email_templates', "New email template details added for {$data['szName']}.", $data);
        }
        return $idTemplate;
    }

    function getTemplateByUniqueKey($szUniqueKey)
    {
        $arTemplateDetails = array();
        $szUniqueKey = trim($szUniqueKey);
        if($szUniqueKey != '')
        {
            $query = $this->db->get_where('tbl_email_templates',array('szUniqueKey'=>$szUniqueKey, 'isDeleted'=>0));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arTemplateDetails = $rows[0];
            }
        }
        return $arTemplateDetails;
    }

    function getTemplateByID($idTemplate)
    {
        $arTemplateDetails = array();
        $idTemplate = (int)$idTemplate;
        if($idTemplate > 0)
        {
            $query = $this->db->get_where('tbl_email_templates',array('id'=>$idTemplate, 'isDeleted'=>0));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arTemplateDetails = $rows[0];
            }
        }
        return $arTemplateDetails;
    }

    function getTemplates($arFilters=array(), $szKeyword='', $isOnlyCount=false, $iLimit=10, $iOffset=0, $szSortField='szName', $szSortOrder='ASC')
    { 
        // set fields to select
        if($isOnlyCount)
            $this->db->select('id');
        else
            $this->db->select('*');
        
        // from table
        $this->db->from('tbl_email_templates u'); 
        
        // where
        $this->db->where('u.isDeleted',0);
                
        // set custom filters
        setCustomRecordFilters($this, $arFilters);

        // set keyword filter
        $szKeyword = trim($szKeyword);
        if($szKeyword != '')
        {
            $this->db->group_start();
            $this->db->like('szName', $szKeyword);
            $this->db->group_end();
        }
        
        // retrive records
        if($isOnlyCount)
        {
            return $this->db->get()->num_rows();
        }
        else
        {
            $query = $this->db->order_by($szSortField, $szSortOrder)->limit($iLimit, $iOffset)->get();
            //echo $this->db->last_query();die;
            return ($query->num_rows() > 0 ? $query->result_array() : array());
        }
    }

    function deleteTemplate($arTemplateDetails, $iDeletedBy)
    {
        // delete user
        $idTemplate = (int)$arTemplateDetails['id'];
        $this->db->where(array('id'=>$idTemplate));
        $this->db->update('tbl_email_templates', array('isDeleted'=>1, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        if($this->db->affected_rows() > 0)
        {
            // add user activity
            addUserActivity($idTemplate, 'tbl_email_templates', "Template {$arTemplateDetails['szName']} deleted.");
            
            return true;
        }
        return false;
    }
    
    function getCampaigns($arFilters=array(), $szKeyword='', $isOnlyCount=false, $iLimit=10, $iOffset=0, $szSortField='szName', $szSortOrder='ASC')
    {
        // set fields to select
        if($isOnlyCount)
            $this->db->select('id');
        else
            $this->db->select('*');
        
        // from table
        $this->db->from('tbl_campaigns u'); 
        
        // where
        $this->db->where('u.isDeleted',0);
                
        // set custom filters
        setCustomRecordFilters($this, $arFilters);

        // set keyword filter
        $szKeyword = trim($szKeyword);
        if($szKeyword != '')
        {
            $this->db->group_start();
            $this->db->like('szName', $szKeyword);
            $this->db->group_end();
        }
        
        // retrive records
        if($isOnlyCount)
        {
            return $this->db->get()->num_rows();
        }
        else
        {
            $query = $this->db->order_by($szSortField, $szSortOrder)->limit($iLimit, $iOffset)->get();
            //echo $this->db->last_query();die;
            return ($query->num_rows() > 0 ? $query->result_array() : array());
        }
    }
    
    function getAllActiveCampaigns()
    {
        $query = $this->db->select('id, szName, dtAddedOn')
                ->from('tbl_campaigns')->where(array('isActive'=>1,'isDeleted'=>0))->get();
        return ($query->num_rows() > 0 ? $query->result_array() : array());
    }
    
    function getCampaignTemplates($idCampaign,$iDaysDifference=false,$bCronjob=false)
    {
        $idCampaign = (int)$idCampaign;
        if($bCronjob)
        {
            $query = $this->db->where(array('idCampaign'=>$idCampaign, 'isDeleted'=>0,'iDays'=>$iDaysDifference))->order_by('iDays', 'ASC')->get('tbl_email_templates');
        }   
        else
        {
            $query = $this->db->where(array('idCampaign'=>$idCampaign, 'isDeleted'=>0))->order_by('iDays', 'ASC')->get('tbl_email_templates');
        } 
        //echo $this->db->last_query();die;
        return ($query->num_rows() > 0 ? $query->result_array() : array());
    }
    
    function updateCampaignStatus($data)
    {
        $idTemplate = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);  
        if(!empty($data) && $idTemplate>0)
        {
            // update user record 
            $this->db->where(array('id'=>$idTemplate));
            $this->db->update('tbl_campaign_delivery', $data); 
        } 
    }
    
    function addContactToCampaign($idContact,$idCampaign)
    {
        if($idContact>0 && $idCampaign>0)
        {
            $arTemplates = $this->getCampaignTemplates($idCampaign); 
             
            $arLoginUser = $this->session->userdata('login_user');
            $idLoginUser = (int)$arLoginUser['id'];
            
            if(!empty($arTemplates))
            {
                // load campaign helper
                $this->load->helper('campaign');
                
                foreach($arTemplates as $templates)
                { 
                    $query = $this->db->select('id')->from('tbl_campaign_delivery')
                            ->where(array('idContact'=>$idContact, 'idCampaign'=>$idCampaign, 'idEmailTemplate'=>$templates['id'], 'isDeleted'=>0))
                            ->get();
                    if($query->num_rows() <= 0)
                    {
                        $dtSendAt = date('Y-m-d H:i:s', strtotime("+".$templates['iDays']." DAYS"));

                        $arAddCampaignDelivery = array(); 
                        $arAddCampaignDelivery['idCampaign'] = $idCampaign;
                        $arAddCampaignDelivery['idContact'] = $idContact;
                        $arAddCampaignDelivery['idEmailTemplate'] = $templates['id'];
                        $arAddCampaignDelivery['dtSendAt'] = $dtSendAt;
                        $arAddCampaignDelivery['szStatus'] = __EMAIL_STATUS_PENDING__;
                        $arAddCampaignDelivery['dtAddedOn'] = date('Y-m-d H:i:s');
                        $arAddCampaignDelivery['iAddedBy'] = $idLoginUser;

                        $idDelivery = $this->addCampaignDelivery($arAddCampaignDelivery);
                        
                        // start campaign immediately for zero days contacts
                        if((int)$templates['iDays'] == 0 && $idDelivery > 0)
                        {
                            startCampaign($idDelivery);
                        }
                    }
                }
            }
        }
    }
    
    function addCampaignDelivery($data)
    {
        if(!empty($data))
        {
            $query = $this->db->insert('tbl_campaign_delivery', $data);
            $id = $this->db->insert_id();
            return $id;
        } 
    }
    
    function getAllCampaignsForCron($idDelivery=0)
    {
        $idDelivery = (int)$idDelivery;
        $arEmailCampaigns = array();  
        
        $this->db->select("cd.*, cd.id as idCampaignDelivery");
        $this->db->from('tbl_campaign_delivery cd');  
        $this->db->where(array('cd.isDeleted'=>0, 'cd.szStatus'=>__EMAIL_STATUS_PENDING__, 'cd.dtSendAt <='=>date('Y-m-d H:i:s')));
        if($idDelivery > 0)
        {
            $this->db->where('cd.id', $idDelivery);
        }
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            $arEmailCampaigns = $query->result_array();
        }   
        return $arEmailCampaigns;
    }
    
    function getAllCampaignsForContact($idContact,$idCampaign=false)
    {
        $arResords = array();
        $this->db->select('idCampaign')
             ->from('tbl_campaign_delivery')
             ->where('isDeleted', 0)
             ->where('idContact', $idContact);
        
        if($idCampaign>0)
        {
            $this->db->where(array('idCampaign'=>$idCampaign,'szStatus'=>__EMAIL_STATUS_PENDING__));
        }
        $query = $this->db->group_by('idCampaign')->get();
        //echo $this->db->last_query();die;
        if($query->num_rows() > 0)   
        {
            foreach($query->result_array() as $row)
            {
                $arResords[] = $row['idCampaign'];
            }
        }
        return $arResords;
    }
    
    function getAllContactsForCampaign($idCampaign)
    {
        $arResords = array();
        $query = $this->db->select('idContact')
             ->from('tbl_campaign_delivery')
             ->where('isDeleted', 0)
             ->where('idCampaign', $idCampaign)
             ->group_by('idContact')->get();
        //echo $this->db->last_query();die;
        if($query->num_rows() > 0)   
        {
            foreach($query->result_array() as $row)
            {
                $arResords[] = $row['idContact'];
            }
        }
        return $arResords;
    }
    
    function sendCampaignEmail()
    {        
        $szDeveloperNotes .= "\n > Sending E-mail to contact with Name: ".$arContactss['szFirstName']." ".$arContactss['szLastName']." E-mail: ".$arContactss['szEmail'];

        $idUser = $arContactss['idUser']; 
        $idContact = $arContactss['id'];

        //Fetching customer details
        $arUserDetails = $this->User_Model->getUserByID($idUser);  

        $arCountry = array(); 
        if((int)$arUserDetails['idCountry'] > 0) {
            $arCountry = $this->User_Model->getCountries($arUserDetails['idCountry']);                        
        }

        $szUserCountry = (!empty($arCountry) ? $arCountry[0]['szName'] : '');

        $arEmailData = array();
        //Fields for contacts
        $arEmailData['szFirstName'] = $arContactss['szFirstName'];
        $arEmailData['szLastName'] = $arContactss['szLastName'];
        $arEmailData['szFullName'] = $arContactss['szFirstName'] ." ".$arContactss['szLastName'];
        $arEmailData['szEmail'] = $arContactss['szEmail'];
        $arEmailData['szHomePhone'] = $arContactss['szPersonalPhone'];
        $arEmailData['szMobile'] = $arContactss['szPersonalMobile'];
        $arEmailData['szWorkPhone'] = $arContactss['szBusinessPhone'];

        //Fileds for customer
        $arEmailData['szUserFirstName'] = $arUserDetails['szFirstName'];
        $arEmailData['szUserLastName'] = $arUserDetails['szLastName'];
        $arEmailData['szUserFullName'] = $arUserDetails['szFirstName'] ." ".$arUserDetails['szLastName'];
        $arEmailData['szUserEmail'] = $arUserDetails['szEmail'];
        $arEmailData['szUserPhone'] = $arUserDetails['szPhone'];
        $arEmailData['szSiteUserName'] = $arUserDetails['szUserName'];
        $arEmailData['szUSIUserName'] = $arUserDetails['szUSIUserName'];
        $arEmailData['szUserAddress'] = $arUserDetails['szAddress'];
        $arEmailData['szUserCity'] = $arUserDetails['szCity'];
        $arEmailData['szUserState'] = $arUserDetails['szState'];
        $arEmailData['szUserCountry'] = $szUserCountry;

        //Social Links
        $arEmailData['szFacebookLink'] = $arUserDetails['szFacebookLink'];
        $arEmailData['szGoogleLink'] = $arUserDetails['szGoogleLink'];
        $arEmailData['szTwitterID'] = $arUserDetails['szTwitterID']; 
        $arEmailData['szLinkedInID'] = $arUserDetails['szLinkedInID'];
        $arEmailData['szDribbbleID'] = $arUserDetails['szDribbbleID'];
        $arEmailData['szSkypeID'] = $arUserDetails['szSkypeID']; 

        if (count($arEmailData) > 0)
        {
            foreach ($arEmailData as $replace_key => $replace_value)
            {
                $szEmailBody = str_replace($replace_key, $replace_value, $szEmailBody);
                $szEmailSubject= str_replace($replace_key, $replace_value, $szEmailSubject);
            }
        } 
        try
        {
            $szDeveloperNotes .= "\n > Ready to send E-mail with Subject: ".$szEmailSubject." ";

            /*
            * building block for sending E-mail
            */
            $mailBasicDetailsAry = array();
            $mailBasicDetailsAry['szEmailTo'] = $arEmailData['szEmail'];  
            $mailBasicDetailsAry['szEmailSubject'] = $szEmailSubject;
            $mailBasicDetailsAry['szEmailMessage'] = $szEmailBody; 
            $mailBasicDetailsAry['idUser'] = $idUser; 
            $mailBasicDetailsAry['idContact'] = $idContact; 
            $mailBasicDetailsAry['idEmailCampaign'] = $idCampaign; 
            $mailBasicDetailsAry['idEmailTemplate'] = $idEmailTemplate; 
            $mailBasicDetailsAry['iEmailType'] = 2; //2 means the E-mail is sent for E-mail campaign 
            $mailBasicDetailsAry['szEmailFrom'] = CUSTOMER_SUPPORT_EMAIL;
            $mailBasicDetailsAry['szFromUserName'] = CUSTOMER_SUPPORT_TITLE;
            $mailBasicDetailsAry['szEmailCC'] = '';
            $mailBasicDetailsAry['bAttachments'] = false;
            $mailBasicDetailsAry['szAttachmentFiles'] = ""; 

            $this->Email_Model->sendEmail($mailBasicDetailsAry); 
            $szDeveloperNotes .= "\n >E-mail successfully sent on: ".$arEmailData['szEmail'];
            $szDeveloperNotes .= "\n >Process completes.\n";
        } 
        catch (Exception $ex) 
        {
            $szDeveloperNotes .= "\n >Oops! Got exception while sending E-mail. Exception: ".$ex->getMessage()." ";
            $szDeveloperNotes .= "\n >Process completes.\n";
        } 
    } 
    
    function addUnsubscribeLogger($data)
    {   
        $data['dtCreatedOn'] = date('Y-m-d H:i:s');
        $query = $this->db->insert('tbl_unsubscriber_logs', $data);
        return $this->db->insert_id();
    }
    
    function unsubscribeCampaigns($data)
    {
        if(!empty($data))
        {
            $this->db->where(array('idContact'=>$data['idContact'],'idCampaign'=>$data['idCampaign'],'szStatus'=>__EMAIL_STATUS_PENDING__));
            $this->db->update('tbl_campaign_delivery', $data);  
            return true;
        } 
    }
}
?>