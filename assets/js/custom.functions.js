$(document).ready(function(){

    $('body').prepend('<div class="loading hidden"></div>');

    $('.loading').css({'background': 'url("'+base_url+'assets/images/loader.gif") no-repeat scroll center center #000', 'position': 'absolute', 'height': '100%', 'width': '100%', 'background-color': '#000', 'filter':'alpha(opacity=50)', '-moz-opacity':'0.5', '-khtml-opacity': '0.5', 'opacity': '0.5', 'z-index': '998'});

});



function show_page_loader()

{

    $('html, body').css({'overflow': 'hidden','height': '100%'});

    $('.loading').removeClass('hidden');

}



function hide_page_loader()

{

    $('html, body').css({'overflow': 'auto','height': 'auto'});

    $('.loading').addClass('hidden');

}



function change_plan_type(iPlanType,szPlanID)

{

    if(iPlanType!='')

    {

        $(".common-class-"+szPlanID).css('display','none');

        $("#type-"+iPlanType+"-"+szPlanID).css('display','block');

    }

}



$('body').on('click', function (e) {

    $('[data-toggle=popover]').each(function () {

        // hide any open popovers when the anywhere else in the body is clicked

        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {

            $(this).popover('hide');

        }

    });

});



function popoverHandler()

{

    $('[data-toggle=popover]').popover();

    $('body').on('click', function (e) {

        $('[data-toggle=popover]').each(function () {

            // hide any open popovers when the anywhere else in the body is clicked

            if (!$(this).is(e.target) && $(this).has(e.target).length === 0) {

                $(this).popover('hide');

            }

        });

    });

}



function create_slug(str)

{

    str = str.replace(/^\s+|\s+$/g, ''); // trim

    str = str.toLowerCase();



    // remove accents, swap ñ for n, etc

    var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";

    var to   = "aaaaaeeeeeiiiiooooouuuunc------";

    for (var i=0, l=from.length ; i<l ; i++) {

        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));

    }



    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars

    .replace(/\s+/g, '-') // collapse whitespace and replace by -

    .replace(/-+/g, '-'); // collapse dashes



    return str;

}



$(document).on('keyup blur', '[data-toggle=slug]', function(){

    $('#' + $(this).attr('data-slug')).val(create_slug($(this).val()));

});



$(".dataTable tbody" ).on('click', '.btn-delete-record',  function(e){

	e.preventDefault();

	var obj = this;

 	bootbox.confirm({

        title: "Delete "+$(this).attr('data-type')+"?",

        message: "<h3 class='text-danger mb20'>Are you sure you want to delete this "+$(this).attr('data-type').toLowerCase()+"?</h3>",

        buttons: {

            cancel: {

                label: '<i class="fa fa-times"></i> Cancel'

            },

            confirm: {

                label: '<i class="fa fa-check"></i> Confirm'

            }

        },

        callback: function (result) {

        	if(result) {

        		window.location = $(obj).attr('href');

        	}

        }

  	});

});



function deleteSubscription(obj)

{

    var isEnded = (parseInt($(obj).attr('data-ended')) == 1 ? true : false);

    var isUser = (parseInt($(obj).attr('data-user')) == 1 ? true : false);

    var message = (isUser ? "<h3 class='text-danger mb20'>Are you sure you want to cancel your subscription [Ending On: " + $(obj).attr('data-end') + "]?</h3>" : "<h3 class='text-danger mb20'>Are you sure you want to delete subscription [User: " + $(obj).attr('data-name') + ", " + (isEnded ? 'Cancelled Since:' : 'Current Term End:') + " " + $(obj).attr('data-end') + "]?</h3>");

    var term = (isUser ? 'canceling' : 'deleting');

    

    bootbox.confirm({

        title: "Delete "+$(this).attr('data-type')+"?",

        message: message,

        buttons: {

            cancel: {

                label: '<i class="fa fa-times"></i> Cancel'

            },

            confirm: {

                label: '<i class="fa fa-check"></i> Confirm'

            }

        },

        callback: function (result) {

            if(result) {

                show_page_loader();

                $.ajax({

                    method: 'POST',

                    url: base_url + 'deleteSubscription',

                    data: 'p_id=' + $(obj).attr('data-id'),

                    success: function(result){

                        hide_page_loader();

                        var ar_result = result.split('||||');

                        if(ar_result[0] == 'SUCCESS'){

                            bootbox.alert({

                                message: "<h3 class='text-success'>Subscription " +(isUser ? 'canceled' : 'deleted') + " successfully.</h3>",

                                callback: function () {	

                                    show_page_loader();

                                    location.reload();

                                }

                            });

                        } else {

                          bootbox.alert("<h3 class='text-danger'>" + ar_result[1] + "</h3>");

                        }

                    },

                    error: function(){

                        hide_page_loader();

                        bootbox.alert("<h3 class='text-danger'>Problem while " + (isUser ? 'canceling' : 'deleting') + " plan.</h3>");

                    }

                });

            }

        }

    });

}



function logout(obj)

{ 

    var message = "<h3 class='text-danger mb20'>Are you sure you really want to logout?</h3>"; 

    

    bootbox.confirm({

        title: "Logout?",

        message: message,

        buttons: {

            cancel: {

                label: '<i class="fa fa-times"></i> No'

            },

            confirm: {

                label: '<i class="fa fa-check"></i> Yes'

            }

        },

        callback: function (result) {

            if(result) {

                show_page_loader(); 

                var page_url = base_url + "/logout";

                $(location).attr('href',page_url); 

            }

        }

    });

}



function manageContactTags(szContactKey)

{

    $('#contactTagsModal').remove();

    show_page_loader();

    

    $.ajax({

        method: 'POST',

        url: base_url + 'contacts/tags/' + szContactKey,

        success: function(result){

            hide_page_loader();

            var ar_result = result.split('||||');

            if(ar_result[0] == 'SUCCESS'){

                $('section#content').append(ar_result[1]);

                $('#contactTagsModal').modal('show');

            } else {

              bootbox.alert("<h3 class='text-danger'>" + ar_result[1] + "</h3>");

            }

        },

        error: function(){

            hide_page_loader();

            bootbox.alert("<h3 class='text-danger'>Problem while processing your request.</h3>");

        }

    });

}



function loadTagTypeHead()

{

    if(arTypeAheadSource.length > 0){

        var tagTypeahead = $('#szTags').typeahead({

            source: arTypeAheadSource,

            displayField: 'Name',

            valueField: 'ID',

            onSelect: function(item){

                    var value = $.trim(item.value);

                    var text = $.trim(item.text);

                    selectTag(value, text);

            },

                    highlighter: function (item) {

                        if(item != undefined)

                            return item;

                        else

                            return 'No match found';

                    }

        });

    }

}



function addNewTag()

{

    var szTags = $.trim($('#szTags').val());

    if(szTags != '') {

            $('#szTags').parent().removeClass('state-error');

            var arTags = szTags.split(',');

            for(var i=0; i<arTags.length; i++) {

                    var value = $.trim(arTags[i]);

                    if(value != ''){

                            selectTag(value, value);

                    }

            }

    } else {

            $('#szTags').parent().addClass('state-error');

    }

}

function removeSelectedTag()

{

    $('.btn-remove-selected-tag').click(function(){



        var szSelected = $.trim($('#szSelectedTags').val());

        var txSelected = $.trim($('#txSelectedTags').val());

        var removed = $.trim($(this).parent().find('input').val());

        var text_removed = $.trim($(this).parent().text());



        if(szSelected.indexOf(',' + removed) != -1)			

                szSelected = szSelected.replace(',' + removed, '');

        else if(szSelected.indexOf(removed + ',') != -1)	

                szSelected = szSelected.replace(removed + ',', '');

        else

                szSelected = szSelected.replace(removed, '');



        if(txSelected.indexOf(',' + text_removed) != -1)			

                txSelected = txSelected.replace(',' + text_removed, '');

        else if(txSelected.indexOf(text_removed + ',') != -1)	

                txSelected = txSelected.replace(text_removed + ',', '');

        else

                txSelected = txSelected.replace(text_removed, '');



        if(szSelected == ''){

                $('.selected-tags').addClass('hidden');

        }



        $('#szSelectedTags').val(szSelected);

        $('#txSelectedTags').val(txSelected);

        $(this).parent().remove();

    });

}



function selectTag(value, text)

{

    if(text != 'No match found'){

        var szPostVariable = $.trim($('#szPostVariable').val());

        var szSelected = $.trim($('#szSelectedTags').val());

        var txSelected = $.trim($('#txSelectedTags').val());

        var arSelected = (txSelected != '' ? txSelected.split(',') : []);



        if(isTagAlredySelected(arSelected, text)){

                //bootbox.alert('Already exists');

        } else {

                // check if exists

                if(value == text) {

                        var arTag = isTagNameExists(arTypeAheadSource, value);

                        value = arTag['ID'];

                        text = 	arTag['Name'];

                }

                var input_name = szPostVariable + (value == text ? '[arNewTags][]' : '[arTags][]');



                var html = '<span class="selected-tag">' + 

                text + ' <a class="text-danger btn-remove-selected-tag" href="javascript:void(0);"><i class="fa fa-times-circle"></i></a>' +

                '<input type="hidden" name="'+input_name+'" value="'+value+'">'+

                '</span>';



        if(szSelected == '') $('.selected-tags').removeClass('hidden');

        $('.selected-tags').append(html);

        if(value != text) {

                $('#szSelectedTags').val(szSelected == '' ? value : szSelected + ',' + value);

        }

        $('#txSelectedTags').val(txSelected == '' ? text : txSelected + ',' + text);

        removeSelectedTag();

        }

    }

    $('.tags-typeahead').val('');

}



function isTagAlredySelected(arTags, szTag)

{

    var ret = false;

    szTag = $.trim(szTag);

    if(arTags.length > 0){			

            for(var i=0; i<arTags.length; i++){

                    var tagName = $.trim(arTags[i]);

                    if(tagName.toLowerCase() == szTag.toLowerCase()) {

                            ret = true;

                            break;

                    }

            }

    }

    return ret;

}



function isTagNameExists(arTags, szTag)

{

    szTag = $.trim(szTag);

    var arTag = {ID: szTag, Name: szTag};

    if(arTags.length > 0){			

            for(var i=0; i<arTags.length; i++){

                    var tagName = $.trim(arTags[i]['Name']);

                    if(tagName.toLowerCase() == szTag.toLowerCase()) {

                            arTag = arTags[i];

                            break;

                    }

            }

    }



    return arTag;

}



function showHide(div_id)

{

    var disp = $("#"+div_id).css('display');

    if(disp=='block')

    {

        $("#"+div_id).css('display','none'); 

    }

    else

    {

        $("#"+div_id).css('display','block'); 

    }

} 

function language_switcher(language)

{

    var page_url = base_url+"/switchLang/"+language;

    redirect_url(page_url);

}

function resendActivationLink(szUserKey)

{

//    var page_url = base_url+"/resendActivationLink/"+szUserKey;
//    redirect_url(page_url);

    $.ajax({

        method: 'POST',

        url: base_url + 'resendActivationLink/' + szUserKey,

        success: function(result){ 

            var ar_result = result.split('||||');

            if(ar_result[0] == 'SUCCESS'){

                //$("#email_confirmation_popup").css('display','none'); 
				$("#step2 p").html(ar_result[1]);
				$("#step2 btn").fadeOut(500);

                //bootbox.alert("<p>" + ar_result[1] + "</p>");

            } else {

              //bootbox.alert("<p>" + ar_result[1] + "</p>");
				$("#step2 p").html(ar_result[1]);
				$("#step2 btn").fadeOut(500);
            }

        },

        error: function(){

            //hide_page_loader();

            bootbox.alert("There was a problem processing your request.");

        }

    });

}



function redirect_url(page_url)

{

    if(page_url!='')

    {

        $(location).attr('href',page_url);

    }

    else

    {

        return false ;

    }

}



$('.qty-handler').click(function(){

    var field = $(this).attr('data-target');

    var value = $(field).val();

    value = (isNaN(value) ? 0 : parseInt(value));

    var increase = ($(this).hasClass('plus') ? true : false);

    

    if(increase){

        value++;

    } else if(value > 0) {

        value--;

    }

    $(field).val(value);

});



function checkAll(obj, selector)

{

    if($(obj).is(":checked")){

        $(selector).prop('checked', true);

    } else {

        $(selector).prop('checked', false);

    }

}

function showAddNewTag(obj)
{
    $('#new_tag').val('');
    $('.new_tag .text-danger').remove();
    $('#contact_tag').parent().find('.text-danger').remove();
    if($('.new_tag').is(":visible")){
        $('.new_tag').addClass('hidden');
    } else {
        $('.new_tag').removeClass('hidden');
    }
}

function saveAddNewTag(obj, szContactKey)
{
    $('#contact_tag').parent().find('.text-danger').remove();
    $('.new_tag .text-danger').remove();
    if($.trim($('#new_tag').val()) != ''){
        $(obj).button('loading');
        $.ajax({
            method: 'POST',
            url: base_url + 'contacts/tags/' + szContactKey,
            data: 'p_func=Add New Tag&p_tag=' + $.trim($('#new_tag').val()),
            success: function(result){
                $(obj).button('reset');
                var ar_result = result.split('||||');
                if(ar_result[0] == 'SUCCESS'){
                    $('#contact_tag').append('<option value="'+ar_result[1]+'">' + $.trim($('#new_tag').val()) + '</option');                    
                    $('.new_tag').addClass('hidden');
                    $('#contact_tag option[value="'+ar_result[1]+'"]').prop('selected', true);
                } else {
                  bootbox.alert("<h3 class='text-danger'>" + ar_result[1] + "</h3>");
                }
            },
            error: function(){
                $(obj).button('reset');
                bootbox.alert("<h3 class='text-danger'>Problem while processing your request.</h3>");
            }
        });
    } else {
        $('#new_tag').parent().append('<div class="text-danger pl20">Tag name is required.</div>');
    }
}

function saveContactTags(obj, szContactKey)
{    
    $('.new_tag .text-danger').remove();
    $('#contact_tag').parent().find('.text-danger').remove();
    $('.new_tag').addClass('hidden');
    if($.trim($('#contact_tag').val()) != ''){
        $(obj).button('loading');
        $.ajax({
            method: 'POST',
            url: base_url + 'contacts/tags/' + szContactKey,
            data: $(obj).closest('form').serialize(),
            success: function(result){
                $(obj).button('reset');
                var ar_result = result.split('||||');
                if(ar_result[0] == 'SUCCESS'){
                     bootbox.alert({
                        message: "<h3 class='text-success'>Tags saved successfully.</h3>",
                        callback: function (result) {                            
                            window.location = window.location.href;
                        }
                    });

                } else {
                  bootbox.alert("<h3 class='text-danger'>" + ar_result[1] + "</h3>");
                }
            },
            error: function(){
                $(obj).button('reset');
                bootbox.alert("<h3 class='text-danger'>Problem while processing your request.</h3>");
            }
        });
    } else {
        $('#contact_tag').parent().append('<div class="text-danger pl20">Please select a tag.</div>');
    }
}

$('.campaign-list li').click(function(e){
    if($(e.target).closest('input').length > 0){
        if($(this).find('input').is(':checked')){
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    } else {
        if($(this).find('input').is(':checked')){
            $(this).removeClass('active');
            $(this).find('input').prop('checked', false);
        } else {
            $(this).addClass('active');        
            $(this).find('input').prop('checked', true);
        }
    }
});

function updateStatus(szStatus)
{   
    show_page_loader(); 
    var serialized_form_data = $("#support_ticket_form").serialize();
    serialized_form_data = serialized_form_data + "&arTicket[szStatus]="+szStatus+"&p_func=Update ticket status";  
    $.ajax({ 
        method: 'POST', 
        url: base_url + 'support/updateStatus', 
        data: serialized_form_data, 
        success: function(result){ 
            hide_page_loader(); 
            var ar_result = result.split('||||'); 
            if(ar_result[0] == 'SUCCESS'){ 
                bootbox.alert({ 
                    message: "<h3 class='text-success'>Ticket moved successfully.</h3>", 
                    callback: function () {	 
                        show_page_loader(); 
                        location.reload(); 
                    } 
                }); 
            } else { 
              bootbox.alert("<h3 class='text-danger'>" + ar_result[1] + "</h3>"); 
            } 
        }, 
        error: function(){ 
            hide_page_loader(); 
            bootbox.alert("<h3 class='text-danger'>Problem while moving ticket.</h3>"); 
        } 
    });  
}
function isFormInputElementEmpty(formId, inputElementId,default_text) 
{	
    var field_value=$("#"+inputElementId).attr('value');
    if(($.trim(field_value) == "") || (field_value==default_text))
    {
        return true;
    }
    else 
    {
        return false;
    }
}
function validateCommentForm()
{
    var formId = "add_comment_form";
    if(isFormInputElementEmpty(formId,'szMessage')) 
    {		
       $("#szMessage").addClass('red_border');
       return false;
    } 
    else
    {
        return true;
    }
}

function addComments()
{    
    if(validateCommentForm()) {
        show_page_loader(); 
        var serialized_form_data = $("#add_comment_form").serialize(); 
        $.ajax({ 
            method: 'POST', 
            url: base_url + 'support/addComments', 
            data: serialized_form_data, 
            success: function(result){ 
                hide_page_loader(); 
                var ar_result = result.split('||||'); 
                if(ar_result[0] == 'SUCCESS'){ 
                    bootbox.alert({ 
                        message: "<h3 class='text-success'>Your comment has been submitted successfully!</h3>", 
                        callback: function () {	 
                            show_page_loader(); 
                            location.reload(); 
                        } 
                    }); 
                } else if(ar_result[0] == 'REDIRECT'){ 
                    bootbox.alert({
                        message: "<h3 class='text-danger'>" + ar_result[1] + "</h3>",
                        callback: function () {	 
                            show_page_loader(); 
                            location.reload(); 
                        }
                    }); 
                }
                else {
                  bootbox.alert("<h3 class='text-danger'>" + ar_result[1] + "</h3>"); 
                } 
            }, 
            error: function(){ 
                hide_page_loader(); 
                bootbox.alert("<h3 class='text-danger'>Problem while moving ticket.</h3>"); 
            } 
        });  
    } 
}