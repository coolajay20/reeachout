<!DOCTYPE html>
<html>

<head>
  	<!-- Meta, title, CSS, favicons, etc. -->
  	<meta charset="utf-8">
  	<title><?php if(isset($szMetaTagTitle)) echo $szMetaTagTitle; else echo "Bitcoin Labs Digital Marketing System";?></title>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">

  	<!-- Font CSS (Via CDN) -->
  	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

	<?php if(isset($isLoadAdminForm) && $isLoadAdminForm == true){?>
	<!-- Admin Forms CSS -->
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/css/admin-forms.css">
	<?php } if(isset($isLoadDataTable) && $isLoadDataTable == true){?>
        <!-- Old Methods of loading datatables -->
  	<!-- Datatables CSS -->
  	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/vendor/plugins/datatables/media/css/dataTables.bootstrap.css">-->
  	<!-- Datatables Editor Addon CSS -->
  	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/vendor/plugins/datatables/extensions/Editor/css/dataTables.editor.css">-->
  	<!-- Datatables ColReorder Addon CSS -->
  	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/vendor/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css">-->
        
        <!-- New method of datatables -->  
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL_DATATABLES ?>/media/css/dataTables.bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL_DATATABLES ?>/extensions/Buttons/css/buttons.bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/custom.datatables.css">
        
  	<?php } if(isset($isLoadCroppieScript) && $isLoadCroppieScript == true){?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/vendor/plugins/croppie/croppie.css">
        <?php } if(isset($isLoadEditorScript) && $isLoadEditorScript == true){?>
        <link href="<?php echo base_url();?>theme/vendor/plugins/trevor/sir-trevor.css" rel="stylesheet">
        <link href="<?php echo base_url();?>theme/vendor/plugins/trevor/sir-trevor-bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url();?>theme/vendor/plugins/trevor/sir-trevor-icons.css" rel="stylesheet">
        <?php } if(isset($isLoadMosaicoScript) && $isLoadMosaicoScript == true){?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/mosaico/dist/mosaico-material.min.css?v=0.10" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/mosaico/dist/vendor/notoregular/stylesheet.css" />
        <?php }?>
               
  	<!-- Theme and Custom CSS -->
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/skin/default_skin/css/theme.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/custom.style.css?<?php echo filemtime('./assets/css/custom.style.css');?>"> 

  	<!-- Favicon -->
  	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon-16x16.png">

  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->  	  
</head>

<body class="form-inputs-page admin-validation-page">
	
  	<!-- Start: Main -->
  	<div id="main">

    	<!-- Start: Header -->
    	<header class="navbar navbar-fixed-top navbar-shadow">
            <div class="navbar-branding">
                <a class="navbar-brand" href="<?php echo base_url();?>dashboard">
                    <img src="<?php echo base_url();?>assets/images/logo_bo.png" title="USI Tech Marketing System" class="center-block img-responsive" style="max-width: 225px; padding-top: 5px;">
                </a>
                <span id="toggle_sidemenu_l" class="ad ad-lines"></span>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown menu-merge">
                    <?php echo lang('header_language');?>:<br>
                    <select onchange="language_switcher(this.value)">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?>>English</option>
                        <option value="danish" <?php if($this->session->userdata('site_lang') == 'danish') echo 'selected="selected"'; ?>>Danish</option>   
                    </select>
                </li>
            <?php $arLoginActivity = getLoginUserLatestActivity(); if(!empty($arLoginActivity)) {?> 
                <li class="dropdown menu-merge">
                    <div class="navbar-btn btn-group">
                        <button data-toggle="dropdown" class="btn btn-sm dropdown-toggle">
                          <span class="fa fa-bell-o fs14 va-m"></span>
                          <span class="badge badge-danger"><?php echo count($arLoginActivity['logs']);?></span>
                        </button>
                        <div class="dropdown-menu dropdown-persist w350 animated animated-shorter fadeIn" role="menu">  
                          <div class="panel mbn">
                            <div class="panel-menu">
                                <span class="panel-icon"><i class="fa fa-clock-o"></i></span>
                                <span class="panel-title fw600"> <?php echo lang('header_recent_activity');?></span>
                                <!-- <button class="btn btn-default light btn-xs pull-right" type="button"><i class="fa fa-refresh"></i></button>-->
                            </div>
                            <div class="panel-body panel-scroller scroller-navbar scroller-overlay scroller-pn pn">
                              <ol class="timeline-list">
                                <?php foreach($arLoginActivity['logs'] as $activity){?>
                                <li class="timeline-item">
                                  <div class="timeline-icon bg-dark light">
                                    <span class="fa fa-tags"></span>
                                  </div>
                                  <div class="timeline-desc">
                                    <?php echo $activity['szActivity'];?>
                                  </div>
                                  <div class="timeline-date"><?php echo date('m/d/y', strtotime($activity['dtCreatedOn']));?><br><?php echo date('h:sa', strtotime($activity['dtCreatedOn']));?></div>
                                </li>
                                <?php }?>								
                              </ol>			   
                            </div>
                            <div class="panel-footer text-center p7">
                                <a href="<?php echo base_url();?>users/details/<?php echo $arLoginActivity['user']['szUniqueKey'];?>" class="link-unstyled"> <?php echo lang('header_view_all');?> </a>
                            </div>
                          </div>
                        </div>
                    </div>
                </li>
            <?php }?>  
                <li class="dropdown menu-merge">
                    <a href="#" class="dropdown-toggle fw600 p15" data-toggle="dropdown">
                        <span class="hidden-xs pl15"> <?php echo $arLoginUser['name'];?> </span>
                        <span class="caret caret-tp hidden-xs"></span>
                    </a>
                    
                    <ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
                        <?php #if(!hasPermission('manage.users')){ ?>
                            <li class="list-group-item">
                                <a href="<?php echo base_url();?>dashboard/" class="animated animated-short fadeInUp">
                                <span class="fa fa-dashboard"></span><?php echo lang('header_dashboard');?></a>
                            </li>
                        <?php #}?>
                        <li class="list-group-item">
                            <a href="<?php echo base_url();?>dashboard" class="animated animated-short fadeInUp">
                            <span class="fa fa-life-ring"></span> <?php echo lang('header_support_ticket');?> </a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo base_url();?>change-password" class="animated animated-short fadeInUp">
                            <span class="fa fa-wrench"></span> <?php echo lang('header_change_password');?> </a>
                        </li>
                        <li class="dropdown-footer">
                            <a href="javascript:void(0);" onclick="logout();" class="">
                            <span class="fa fa-power-off pr5"></span> <?php echo lang('header_logout');?> </a>
                        </li>
                    </ul>
                </li>
            </ul>
    	</header>
    	<!-- End: Header -->
    	
    	<!-- Start: Sidebar -->
    	<aside id="sidebar_left" class="nano nano-light affix"> 
      		<!-- Start: Sidebar Left Content -->
      		<div class="sidebar-left-content nano-content">   
                    <!-- Start: Sidebar Menu -->
                    <ul class="nav sidebar-menu">
                        <li class="sidebar-label pt20"><?php echo lang('header_menu');?></li>
                        <li<?php echo ($szPageName == 'Dashboard' ? ' class="active"' : '');?>>
                            <a href="<?php echo base_url();?>dashboard">
                                <span class="fa fa-dashboard"></span>
                                <span class="sidebar-title"><?php echo lang('header_dashboard');?></span>
                            </a>
                        </li>

                    <?php if($arLoginUser['confirmed'] == 1){?>
                    <!-- CRM Menu Cluster -->
                    <?php if(hasPermission('manage.contacts')){?>
                    <li>
                        <a class="accordion-toggle <?php echo ($szPageName == 'Contacts' || $szPageName == 'Contact Upload' || $szPageName == 'Tags' || $szPageName == "Add New Contact" ? "menu-open" : "");?>" href="#">
                                <span class="fa fa-id-card"></span>
                                <span class="sidebar-title"><?php echo lang('header_contacts');?></span>
                                <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" style="">
                            <li<?php echo ($szPageName == 'All Contacts' ? ' class="active"' : '');?>>
                                <a href="<?php echo base_url();?>contacts/all">
                                    <span class="fa fa-list"></span>
                                    <span class="sidebar-title"><?php echo lang('header_all_contact');?></span>
                                </a>
                            </li>
                            <li<?php echo ($szPageName == 'Tags' ? ' class="active"' : '');?>>
                                <a href="<?php echo base_url();?>tags">
                                    <span class="fa fa-tags"></span>
                                    <span class="sidebar-title"><?php echo lang('header_manage_tags');?></span>
                                </a>
                            </li>
                            <li<?php echo ($szPageName == 'Add New Contact' ? ' class="active"' : '');?>>
                                <a href="<?php echo base_url();?>contacts/add">
                                    <span class="fa fa-plus"></span>
                                    <span class="sidebar-title"><?php echo lang('header_add_contacts');?></span>
                                </a>
                            </li>
                            <li<?php echo ($szPageName == 'Contact Upload' ? ' class="active"' : '');?>>
                                <a href="<?php echo base_url();?>contacts/upload">
                                    <span class="fa fa-upload"></span>
                                    <span class="sidebar-title"><?php echo lang('header_upload_contacts');?></span>
                                </a>
                            </li> 
                        </ul>
                    </li>
                    <?php }?> 
                    
                     <?php if(hasPermission('manage.campaigns')){?>
                    <li>
                        <a class="accordion-toggle <?php echo ($szPageName == 'Campaigns' || $szPageName == "Add New Campaign" ? "menu-open" : "");?>" href="#">
                                <span class="fa fa-envelope"></span>
                                <span class="sidebar-title"><?php echo lang('header_campaigns');?></span>
                                <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" style="">
                            <li<?php echo ($szPageName == 'All Campaigns' ? ' class="active"' : '');?>>
                                <a href="<?php echo base_url();?>campaigns/all">
                                    <span class="fa fa-list"></span>
                                    <span class="sidebar-title"><?php echo lang('header_all_campaigns');?></span>
                                </a>
                            </li>
                            <li<?php echo ($szPageName == 'Add New Campaign' ? ' class="active"' : '');?>>
                                <a href="<?php echo base_url();?>campaigns/add">
                                    <span class="fa fa-plus"></span>
                                    <span class="sidebar-title"><?php echo lang('header_add_campaign');?></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php }?>

                    <!-- Marketing Menu Cluster -->
                    <li>
                        <a class="accordion-toggle <?php echo ($szPageName == 'Marketing' || $szPageName == "Marketing" ? "menu-open" : "");?>" href="#">
                            <span class="glyphicon glyphicon-fire"></span>
                            <span class="sidebar-title"><?php echo lang('header_marketing');?></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" style="">
                            <li<?php echo ($szPageName == 'Dashboard' ? ' class="active"' : '');?>>
                                <a href="<?php echo base_url();?>dashboard">
                                    <span class="fa fa-desktop"></span>
                                    <span class="sidebar-title"><?php echo lang('header_funnel_page');?></span>
                                </a>
                            </li>
                            <li<?php echo ($szPageName == 'Dashboard' ? ' class="active"' : '');?>>
                                <a href="<?php echo base_url();?>dashboard">
                                    <span class="fa fa-bullseye"></span>
                                    <span class="sidebar-title"><?php echo lang('header_traffic_lead');?></span>
                                </a>
                            </li>
                        </ul>
                    </li> 	 
                    <!-- Toolbox Menu Cluster -->
                    <li>
                        <a class="accordion-toggle <?php echo ($szPageName == 'Toolbox' || $szPageName == "Toolbox" ? "menu-open" : "");?>" href="#">
                            <span class="fa fa-suitcase"></span>
                            <span class="sidebar-title"><?php echo lang('header_tool_box');?></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" style="">
                            <li<?php echo ($szPageName == 'Dashboard' ? ' class="active"' : '');?>>
                                <a href="<?php echo base_url();?>dashboard">
                                    <span class="fa fa-question"></span>
                                    <span class="sidebar-title"><?php echo lang('header_coming_soon');?></span>
                                </a>
                            </li>
                        </ul>
                    </li> 	
                    <?php if(hasPermission('manage.users')){?>
                    <li>
                        <a class="accordion-toggle <?php echo ($szPageName == 'Users' || $szPageName == "New User" ? "menu-open" : "");?>" href="#">
                            <span class="fa fa-group"></span>
                            <span class="sidebar-title"><?php echo lang('header_users');?></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" style="">
                            <li<?php echo ($szPageName == 'Users' ? ' class="active"' : '');?>>
                                <a href="<?php echo base_url();?>users">
                                    <span class="fa fa-list"></span>
                                    <span class="sidebar-title"><?php echo lang('header_all_users');?></span>
                                </a>
                            </li>
                            <li <?php echo ($szPageName == 'New User' ? ' class="active"' : '');?>>
                                <a href="<?php echo base_url();?>users/add">
                                    <span class="fa fa-plus"></span>
                                    <span class="sidebar-title"><?php echo lang('add_new');?></span>
                                </a>
                            </li>
                        </ul>
                    </li> 
                    <?php } if(hasPermission('manage.membership')) { ?>  
                        <li>
                            <a class="accordion-toggle <?php echo ($szPageName == 'Membership Subscription' || $szPageName == "Membership Invoices" ? "menu-open" : "");?>" href="#">
                                <span class="fa fa-group"></span>
                                <span class="sidebar-title"><?php echo lang('header_membership');?></span>
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav" style="">
                                <li <?php echo ($szPageName == 'Membership Subscription' ? ' class="active"' : '');?>>
                                    <a href="<?php echo base_url();?>membership/subscription">
                                        <span class="fa fa-list"></span>
                                        <span class="sidebar-title"><?php echo lang('header_subscription');?></span>
                                    </a>
                                </li> 
                            </ul>
                        </li> 
                    <?php } if(hasPermission('users.activity')){?>
                        <li<?php echo ($szPageName == 'Activity Log' ? ' class="active"' : '');?>>
                            <a href="<?php echo base_url();?>activity">
                            <span class="fa fa-list"></span>
                            <span class="sidebar-title"><?php echo lang('header_activity_logs');?></span>
                            </a>
                        </li>    
                    <?php }if(hasPermission('manage.settings')){?>
                        <li>
                            <a class="accordion-toggle <?php echo ( ($szPageName == 'Plans' || $szPageName == "Subscriptions" || $szPageName=="Invoices" || $szPageName=="Features") ? "menu-open" : "");?>" href="#">
                                <span class="fa fa-gear fa-fw"></span>
                                <span class="sidebar-title"><?php echo lang('header_settings');?></span>
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav" style="">
                                <li<?php echo ($szPageName == 'Plans' ? ' class="active"' : '');?>>
                                    <a href="<?php echo base_url();?>plans">
                                        <span class="fa fa-bookmark-o"></span>
                                        <span class="sidebar-title"><?php echo lang('header_plans');?></span>
                                    </a>
                                </li>
                                <li<?php echo ($szPageName == 'Features' ? ' class="active"' : '');?>>
                                    <a href="<?php echo base_url();?>features">
                                        <span class="fa fa-bookmark-o"></span>
                                        <span class="sidebar-title"><?php echo lang('header_features');?></span>
                                    </a>
                                </li>
                                <li<?php echo ($szPageName == 'Subscriptions' ? ' class="active"' : '');?>>
                                    <a href="<?php echo base_url();?>subscriptions">
                                        <span class="fa fa-shopping-cart"></span>
                                        <span class="sidebar-title"><?php echo lang('header_subscription');?></span>
                                    </a>
                                </li>
                                <li<?php echo ($szPageName == 'Invoices' ? ' class="active"' : '');?>>
                                    <a href="<?php echo base_url();?>invoices">
                                        <span class="fa fa-shopping-cart"></span>
                                        <span class="sidebar-title"><?php echo lang('header_invoices');?></span>
                                    </a>
                                </li>
                            </ul>
                        </li> 
                        <?php } if(hasPermission('manage.roles') || hasPermission('manage.permissions')){?>
                        <li>
                            <a class="accordion-toggle <?php echo ($szPageName == 'Roles' || $szPageName == "Permissions" ? "menu-open" : "");?>" href="#">
                                <span class="fa fa-group"></span>
                                <span class="sidebar-title"><?php echo lang('header_roles_permissions');?></span>
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav" style="">
                                <?php if(hasPermission('manage.roles')){?>
                                <li<?php echo ($szPageName == 'Roles' ? ' class="active"' : '');?>>
                                    <a href="<?php echo base_url();?>roles">
                                        <span class="fa fa-user"></span>
                                        <span class="sidebar-title"><?php echo lang('header_roles');?></span>
                                    </a>
                                </li>
                                <?php } if(hasPermission('manage.permissions')){?>
                                <li<?php echo ($szPageName == 'Permissions' ? ' class="active"' : '');?>>
                                    <a href="<?php echo base_url();?>permissions">
                                        <span class="fa fa-wrench"></span>
                                        <span class="sidebar-title"><?php echo lang('header_permissions');?></span>
                                    </a>
                                </li>
                                <?php }?>
                            </ul>
                        </li>
                    <?php }}?> 
                        <li<?php echo ($szPageName == 'Change Your Password' ? ' class="active"' : '');?>>
                            <a href="<?php echo base_url();?>change-password">
                                <span class="fa fa-wrench"></span>
                                <span class="sidebar-title"><?php echo lang('header_change_password');?></span>
                            </a>
                        </li> 
                        <li>
                            <a href="javascript:void(0);" onclick="logout();">
                                <span class="glyphicon glyphicon-off"></span>
                                <span class="sidebar-title"><?php echo lang('header_logout');?></span>
                            </a>
                        </li>	          		
	            </ul>
	            <!-- End: Sidebar Menu --> 
       		</div>
       	</aside>