<!-- Begin: Content -->

<form method="post" action="<?php echo base_url();?>notifications/<?php echo ($idNotification > 0 ? "edit/{$arNotificationDetails['szUniqueKey']}" : "add");?>" id="formSirTrevor" class="validate-form">	

	<section id="content" class="table-layout">

		<div class="tray tray-center">

			<div class="mw1000 center-block">  

				<?php if($isDetailsSaved){?>

				<div class="alert alert-success p5">

                                    <i class="fa fa-check pr10"></i>

                                    <strong> Congratulations!</strong>

                                    Notification details has been saved successfully.

				</div>

				<?php } if($isFormError=== TRUE){?>

				<div class="alert alert-danger p5">

                                    <i class="fa fa-times pr10"></i>

                                    <strong> Invalid Data!</strong>

                                    Please fix the errors below and try again.

				</div>

				<?php } else if($isFormError != ''){?>

				<div class="alert alert-danger p5">

                                    <i class="fa fa-times pr10"></i>

                                    <strong> Sorry!</strong>

                                    <?php echo $isFormError;?>

				</div>

				<?php }?>



				<div class="panel panel-primary panel-border top mb15">

                                    <div class="panel-heading">

                                        <span class="panel-title">

                                            <i class="fa fa-enevelope"></i>

                                            Notification Details

                                        </span>

                                    </div> 

                                    <div class="panel-body bg-light col-lg-7" style="padding-bottom:0;">

                                        <div class="admin-form"> 

                                            <div class="row">

                                                <label for="szTitle" class="field-label text-muted fs16 mb10 col-md-12">Title<span class="text-danger">*</span></label>

                                                <?php $szTitleError = form_error('arNotification[szTitle]');?>

                                                <label for="szTitle" class="col-md-6 field prepend-icon<?php if(!empty($szTitleError)){?> state-error<?php }?>">

                                                    <div class="input-group mb15">

                                                      <span class="input-group-addon"><i class="fa fa-user"></i></span>

                                                      <input type="text" maxlength="255" name="arNotification[szTitle]" id="szTitle" class="form-control" placeholder="Title" value="<?php echo set_input_value('arNotification[szTitle]', (isset($arNotificationDetails['szTitle']) ? $arNotificationDetails['szTitle'] : ''));?>">

                                                    </div>

                                                </label>

                                                <?php if(!empty($szTitleError)){?><em class="state-error" for="szTitle"><?php echo $szTitleError;?></em><?php }?>								

                                            </div>

                                            <div class="row">

                                                <label for="szContent" class="field-label text-muted fs16 col-md-12">Message Content<span class="text-danger">*</span></label>

                                                <?php $szContentError = form_error('arNotification[szContent]');?>

                                                <div class="col-md-12">

                                                    <textarea id="wysiwyg" placeholder="Enter text here..." class="form-control" rows="10" name="arNotification[szContent]"><?php echo set_input_value('arNotification[szContent]', (isset($arNotificationDetails['szContent']) ? $arNotificationDetails['szContent'] : ''));?></textarea>									

                                                </div>

                                                <?php if(!empty($szContentError)){?><span class="text-danger" for="szContent"><?php echo $szContentError;?></em><?php }?>

                                            </div> 

                                        </div>

                                        <div class="panel-footer text-center">

                                            <div class="admin-form">

                                                <a href="<?php echo base_url().'notifications/all' ?>" class="btn btn-default"> Cancel </a>

                                                <button type="submit" class="btn btn-primary"><?php echo ($idNotification > 0 ? 'Save' : 'Add'); ?></button>

                                                <input type="hidden" name="p_func" value="Save Notification Details">

                                            </div>			                    	

                                        </div>
                                    </div>                                                                        

				</div>

			</div>

		</div> 

	</section>

</form>