<div class="panel panel-visible" id="spy3">
	<div class="panel-body pn">       
		<table class="table table-responsive table-striped table-hover dt-static" id="example" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Customer</th>
					<th>Invoice</th>
					<th>Amount</th> 
					<th>Status</th>
					<th>Date Created</th> 
				</tr>
			</thead>
			<?php if(!empty($arTransactions)){ ?>
			<tbody>
			<?php  
				foreach( $arTransactions as $Transactions )
				{    
					?>
					<tr>
						<td><?php echo $Transactions['szFirstName']." ".$Transactions['szLastName']; ?><br><small class="colorgray"><?php echo (isset($Transactions['szEmail'])? "(".$Transactions['szEmail'].")" :''); ?></small></td>
						<td><?php echo $Transactions['szTransactionID']; ?></td> 
						<td><?php echo "$".number_format((float)$Transactions['fAmount']); ?></td> 
						<td><span class="label label-success"><?php echo $Transactions['szStatus']; ?></span></td>
                                                <td><?php echo convert_date(gmdate('Y-m-d H:i:s', $Transactions['tCreated']),2); ?></td> 
					</tr> 
				<?php  } ?> 
			</tbody> 
			<?php } else {?>
				<tfoot>
					<tr>
						<td colspan="8" class="aligncenter">
							<em>no transaction found</em>
						</td>
					</tr>
				</tfoot>
			<?php }?> 
		</table> 
	</div>
</div>