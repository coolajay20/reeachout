<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Commerce_Model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getAllPlans($iLimit=0)
	{
		$iLimit = (int)$iLimit;
		$this->db->from('tbl_plans');
		$this->db->order_by('szPlanName', 'ASC');
		$this->db->where(array('isDeleted'=>0));
		if($iLimit > 0)
			$this->db->limit($iLimit);
			
		$query = $this->db->get();
		return ($query->num_rows() > 0 ? $query->result_array() : array());
	}
	
	function getPlanByUniqueKey($szUniqueKey)
	{
		$arDetails = array();
		$szUniqueKey = trim($szUniqueKey);
		if($szUniqueKey != '')
		{
			$query = $this->db->get_where('tbl_plans', array('szUniqueKey'=>$szUniqueKey));
			if($query->num_rows() > 0)
			{
				$rows = $query->result_array();
				$arDetails = $rows[0];
			}
		}
		return $arDetails;
	}
	
	function getPlanByID($idPlan)
	{
		$arDetails = array();
		$idPlan = (int)$idPlan;
		if($idPlan > 0)
		{
			$query = $this->db->get_where('tbl_plans', array('id'=>$idPlan));
			if($query->num_rows() > 0)
			{
				$rows = $query->result_array();
				$arDetails = $rows[0];
			}
		}
		return $arDetails;
	}
	
	function getUniqueKeyForPlan()
	{
		$characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 30; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    if($this->isPlanUniqueKeyExists($randomString))
	    {
	    	$randomString = $this->getUniqueKeyForPlan();
	    }
	    return $randomString;
	}
	
	function isPlanUniqueKeyExists($szKey)
	{
		$szKey = trim($szKey);
		
		$query = $this->db->get_where('tbl_plans', array('szUniqueKey' => $szKey));
		return $query->num_rows() > 0;
	}
	
	function deletePlan($id, $iDeletedBy)
	{
		$this->db->where(array('id'=>$id));
		$this->db->update('tbl_plans', array('isDeleted'=>1, 'iDeletedBy'=>$iDeletedBy, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
		return ($this->db->affected_rows() > 0 ? true : false);
	}
	
	function savePlanDetails($data, $idLoginUser)
	{		
		$data = sanitize_array_values($data);
		$id = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);
		
		if($id > 0)
		{
			$data['iUpdatedBy'] = $idLoginUser;
			$data['dtUpdatedOn'] = date('Y-m-d H:i:s');
			$this->db->where(array('id'=>$id));
			$this->db->update('tbl_plans', $data);
		}
		else
		{
			$data['szUniqueKey'] = $this->getUniqueKeyForPlan();
			$data['iAddedBy'] = $idLoginUser;
			$data['dtAddedOn'] = date('Y-m-d H:i:s');
			$query = $this->db->insert('tbl_plans', $data);
			$id = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);
		}
		return $id;
	}
}
?>