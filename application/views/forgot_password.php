<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>Forgot Password</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/skin/default_skin/css/theme.css">
  
  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon-16x16.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body class="admin-validation-page external-page external-alt sb-l-c sb-r-c" style="background-color: #1d2939">
  <!-- Start: Main -->
  <div id="" class="animated fadeIn">

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- begin canvas animation bg -->
      <div id="canvas-wrapper">
        <canvas id="demo-canvas"></canvas>
      </div>

      <!-- Begin: Content -->
      <section id="content" class="animated fadeIn">

        <div class="admin-form theme-info mw500" style="margin-top: 10%;" id="login">
          <div class="row mb15 table-layout">

            <div class="col-xs-6 pln">
              	<img src="<?php echo base_url();?>assets/images/logo_beta.png" title="Forgot Password | Bitcoin Labs" class="img-responsive" style="max-width: 275px;">            	
            </div>

            <div class="col-xs-6 va-b">
              <h3 class="text-right">
                Forgot Password
              </h3>
            </div>
          </div>
		  
		  <?php if($isLinkSent){?>
		  <div class="alert alert-success" style="margin-top: 3%;">
          	<i class="fa fa-check pr10"></i>
           	<strong> Congratulations!</strong><br><br>
           	An email from support@bitcoinlabs.io containing your reset password link has been sent to <?php echo $szEmail?>. If you do not receive it in a few minutes please check your spam or junk folder.<br><br>
			Please follow the link to reset your password and remember that this link will be valid only for next 24 hours.
     	  </div>
		  <?php } else {?>
          <div class="panel">

            <form method="post" action="<?php echo base_url();?>forgot-password" id="forgot-password-form" class="validate-form">
              <div class="panel-body p15">

                <div class="alert alert-micro alert-border-left alert-info pastel alert-dismissable mn">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <i class="fa fa-info pr10"></i> Enter your
                  <b>Email address</b> and instructions will be sent to you!
                </div>

              </div>
              <!-- end .form-body section -->
              <div class="panel-footer p25 pv15">

                <div class="section mn">
				
				  <?php $szEmailError = form_error('szEmail');?>
                  <div class="smart-widget sm-right smr-80">
                    <label for="szEmail" class="field prepend-icon<?php if(!empty($szEmailError)){?> state-error<?php }?>">
                      <input type="text" name="szEmail" id="szEmail" class="gui-input required email" placeholder="Your email address" value="<?php echo set_input_value('szEmail');?>">
                      <label for="szEmail" class="field-icon">
                        <i class="fa fa-user"></i>
                      </label>
                    </label>
                    <?php if(!empty($szEmailError)){?><em class="state-error" for="szEmail"><?php echo $szEmailError;?></em><?php }?>
                    <button type="submit" for="szEmail" class="button">Reset</button>
                  </div>
                  <!-- end .smart-widget section -->

                </div>
                <!-- end section -->
        
              </div>
              <!-- end .form-footer section -->
			  <input type="hidden" name="p_func" value="Forgot Password">
            </form>

          </div>
          <?php }?>

        </div>

      </section>
      <!-- End: Content -->

    </section>
    <!-- End: Content-Wrapper -->

  </div>
  <!-- End: Main -->


  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  
  <!-- jQuery Validate Plugin-->
  <script src="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Validate Addon -->
  <script src="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="<?php echo base_url();?>theme/assets/js/utility/utility.js"></script>
  <script src="<?php echo base_url();?>theme/assets/js/demo/demo.js"></script>
  <script src="<?php echo base_url();?>theme/assets/js/main.js"></script>

  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core      
    Core.init();

    // Init Demo JS
    Demo.init();
  });
  
  // Init Base URL
  var base_url = '<?php echo base_url();?>';
  </script>
  
  <!-- Custom Validation Script -->
  <script src="<?php echo base_url();?>assets/js/custom.validate.js?<?php echo filemtime('./assets/js/custom.validate.js');?>"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {
 	validate_form();
  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>

</html>