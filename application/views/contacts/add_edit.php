<!-- Begin: Content -->
<style>
    .campaign-list{list-style: none;border:1px solid #ccc;max-height:200px;overflow-x:hidden;overflow-y: scroll;padding: 10px;}
    .campaign-list li{padding:10px 5px;margin-bottom: 5px;background:#f7f7f7;cursor: pointer;}
    .campaign-list li.active{background:#428bca;color:#fff;}
</style>
<form method="post" action="<?php echo base_url();?>contacts/<?php echo ($idContact > 0 ? "edit/{$arContactDetails['szUniqueKey']}" : "add");?>" id="profile-form" class="validate-form">	
	<section id="content" class="table-layout">
		<div class="tray tray-center">		     	
			<div class="mw1000 center-block">    
				<?php if($isDetailsSaved){?>
				<div class="alert alert-success p5">
					<i class="fa fa-check pr10"></i>
					<strong> <?php echo lang('common_congratulation'); ?>!</strong>
					<?php echo lang('header_contact')." ".lang('common_details_saved'); ?>.
				</div>
				<?php } if($isFormError=== TRUE){?>
				<div class="alert alert-danger p5">
					<i class="fa fa-times pr10"></i>
					<strong> <?php echo lang('common_invalid_data'); ?>!</strong>
					<?php echo lang('common_fix_errors_below'); ?>.
				</div>
				<?php } else if($isFormError != ''){?>
				<div class="alert alert-danger p5">
					<i class="fa fa-times pr10"></i>
					<strong> <?php echo lang('common_sorry'); ?>!</strong>
					<?php echo $isFormError;?>
				</div>
				<?php }?>

				<div class="panel panel-primary panel-border top mb15">
					<div class="panel-heading">
						<span class="panel-title"><i class="fa fa-user"></i>&nbsp;&nbsp;<?php echo lang('contacts_basic_information'); ?></span>
					</div>
					<div class="panel-body bg-light" style="padding-bottom:0;">
						<div class="admin-form">

							<div class="section row mb5">
								<label for="szFirstName" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_first_name'); ?><span class="text-danger">*</span></label>
								<?php $szFirstNameError = form_error('arContact[szFirstName]');?>
								<div class="col-md-4">
									<label for="szFirstName" class="field prepend-icon<?php if(!empty($szFirstNameError)){?> state-error<?php }?>">
										<div class="input-group mb15">
										  <span class="input-group-addon"><i class="fa fa-user"></i></span>
										  <input type="text" name="arContact[szFirstName]" id="szFirstName" class="form-control" placeholder="<?php echo lang('contacts_first_name'); ?>" value="<?php echo set_input_value('arContact[szFirstName]', (isset($arContactDetails['szFirstName']) ? $arContactDetails['szFirstName'] : ''));?>">
										</div>
									</label>
									<?php if(!empty($szFirstNameError)){?><em class="state-error" for="szFirstName"><?php echo $szFirstNameError;?></em><?php }?>
								</div>

								<label for="szLastName" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_last_name'); ?><span class="text-danger">*</span></label>
								<?php $szLastNameError = form_error('arContact[szLastName]');?>
								<div class="col-md-4">
									<label for="szLastName" class="field prepend-icon<?php if(!empty($szLastNameError)){?> state-error<?php }?>">
										<div class="input-group mb15">
											<span class="input-group-addon"><i class="fa fa-user"></i></span>
											<input type="text" name="arContact[szLastName]" id="szLastName" class="form-control" placeholder="<?php echo lang('contacts_last_name'); ?>" value="<?php echo set_input_value('arContact[szLastName]', (isset($arContactDetails['szLastName']) ? $arContactDetails['szLastName'] : ''));?>">
										</div>
										</label>
									</label>
									<?php if(!empty($szLastNameError)){?><em class="state-error" for="szLastName"><?php echo $szLastNameError;?></em><?php }?>
								</div>
							</div> 

							<div class="section row mb5">
								<label for="szEmail" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_email'); ?><span class="text-danger">*</span></label>
								<?php $szEmailError = form_error('arContact[szEmail]');?>
								<div class="col-md-4">
									<label for="szEmail" class="field prepend-icon<?php if(!empty($szEmailError)){?> state-error<?php }?>">
										<div class="input-group mb15">
										<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
											<input type="email" name="arContact[szEmail]" id="szEmail" class="form-control" placeholder="<?php echo lang('contacts_email'); ?>" value="<?php echo set_input_value('arContact[szEmail]', (isset($arContactDetails['szEmail']) ? $arContactDetails['szEmail'] : ''));?>">
										</div>
									</label>
									<?php if(!empty($szEmailError)){?><em class="state-error" for="szEmail"><?php echo $szEmailError;?></em><?php }?>
								</div>

								<label for="idType" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_type'); ?></label>
								<div class="col-md-4">
									<label for="idType" class="field select">
										<select name="arContact[idType]" id="idType" class="form-control" placeholder="<?php echo lang('contacts_type'); ?>">                 
											<?php foreach($arTypes as $type){?>
												<option value="<?php echo $type['id'];?>" <?php echo (set_input_value('arContact[idType]', (isset($arContactDetails['idType']) ? $arContactDetails['idType'] : 1)) == $type['id'] ? 'selected' : '');?>><?php echo $type['szType'];?></option>						
											<?php }?>
										</select>							                    
										<label for="idType" class="field-icon">
											<i class="arrow"></i>
										</label>
									</label>
								</div> 
							</div>                                                                                                        
						</div>
					</div>
				</div> 
                            
                                <?php if(!empty($arCampaigns)){?>
                                <div class="panel panel-primary panel-border top mb15">  
                                    <div class="panel-heading">
                                        <span class="panel-title">
                                            <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;<?php echo lang('contacts_attach_campaign'); ?>
                                        </span> 
                                    </div>
                                    <div class="panel-body bg-light" id="campaigns">
                                        <ul class="campaign-list">
                                            <?php foreach($arCampaigns as $i=>$campaign){?>
                                            <li for="campaign_<?php echo $i;?>"<?php echo (set_input_value("arCampaigns[{$i}]") == $campaign['id'] ? ' class="active"' : (!empty($arContactCampaigns) && in_array($campaign['id'], $arContactCampaigns) ? ' class="active"' : ''));?>><input type="checkbox" id="campaign_<?php echo $i;?>" name="arCampaigns[<?php echo $i;?>]" value="<?php echo $campaign['id'];?>" <?php echo (set_input_value("arCampaigns[{$i}]") == $campaign['id'] ? 'checked' : (!empty($arContactCampaigns) && in_array($campaign['id'], $arContactCampaigns) ? 'checked' : ''));?>> <?php echo $campaign['szName'];?></li>
                                            <?php }?>
                                        </ul>
                                    </div>
                                </div>
                                <?php }?>

				<div class="panel panel-primary panel-border top mb15">  
					<div class="panel-heading">
						<span class="panel-title">
							<i class="fa fa-home"></i>&nbsp;&nbsp;<?php echo lang('contacts_personal_information'); ?>
						</span> 
					</div>
					<div class="panel-body bg-light" id="personal_information">
						<div class="admin-form">
							<div class="section row mb5">
								<label for="szPersonalAddress" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_address'); ?></label> 
								<div class="col-md-4">
									<label for="szPersonalAddress" class="field prepend-icon<?php if(!empty($szPersonalAddressError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szPersonalAddress]" id="szPersonalAddress" class="form-control" placeholder="<?php echo lang('contacts_address'); ?>" value="<?php echo set_input_value('arContact[szPersonalAddress]', (isset($arContactDetails['szPersonalAddress']) ? $arContactDetails['szPersonalAddress'] : ''));?>">
										<label for="szPersonalAddress" class="field-icon">
											<i class="fa fa-address-book"></i>
										</label>
									</label>
									<?php if(!empty($szPersonalAddressError)){?><em class="state-error" for="szPersonalAddress"><?php echo $szPersonalAddressError;?></em><?php }?>
								</div>

								<label for="szPersonalAddress2" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_address'); ?> 2</label> 
								<div class="col-md-4">
									<label for="szPersonalAddress2" class="field prepend-icon<?php if(!empty($szPersonalAddress2Error)){?> state-error<?php }?>">
										<input type="text" name="arContact[szPersonalAddress2]" id="szPersonalAddress2" class="form-control" placeholder="<?php echo lang('contacts_address'); ?> 2" value="<?php echo set_input_value('arContact[szPersonalAddress2]', (isset($arContactDetails['szPersonalAddress2']) ? $arContactDetails['szPersonalAddress2'] : ''));?>">
										<label for="szPersonalAddress2" class="field-icon">
											<i class="fa fa-address-book"></i>
										</label>
									</label>
									<?php if(!empty($szPersonalAddress2Error)){?><em class="state-error" for="szPersonalAddress2"><?php echo $szPersonalAddress2Error;?></em><?php }?>
								</div>
							</div> 

							<div class="section row mb5">
								<label for="szPersonalPostcode" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_postcode'); ?></label> 
								<div class="col-md-4">
									<label for="szPersonalPostcode" class="field prepend-icon<?php if(!empty($szPersonalPostcodeError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szPersonalPostcode]" id="szPersonalPostcode" class="form-control" placeholder="<?php echo lang('contacts_postcode'); ?>" value="<?php echo set_input_value('arContact[szPersonalPostcode]', (isset($arContactDetails['szPersonalPostcode']) ? $arContactDetails['szPersonalPostcode'] : ''));?>">
										<label for="szPersonalPostcode" class="field-icon">
											<i class="fa fa-address-book"></i>
										</label>
									</label>
									<?php if(!empty($szPersonalPostcodeError)){?><em class="state-error" for="szPersonalPostcode"><?php echo $szPersonalPostcodeError;?></em><?php }?>
								</div>

								<label for="szPersonalCity" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_city'); ?></label> 
								<div class="col-md-4">
									<label for="szPersonalCity" class="field prepend-icon<?php if(!empty($szPersonalCityError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szPersonalCity]" id="szPersonalCity" class="form-control" placeholder="<?php echo lang('contacts_city'); ?>" value="<?php echo set_input_value('arContact[szPersonalCity]', (isset($arContactDetails['szPersonalCity']) ? $arContactDetails['szPersonalCity'] : ''));?>">
										<label for="szPersonalCity" class="field-icon">
											<i class="fa fa-address-book"></i>
										</label>
									</label>
									<?php if(!empty($szPersonalCityError)){?><em class="state-error" for="szPersonalCity"><?php echo $szPersonalCityError;?></em><?php }?>
								</div> 
							</div>

							<div class="section row mb5">
								<label for="szPersonalState" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_state'); ?></label> 
								<div class="col-md-4">
									<label for="szPersonalState" class="field prepend-icon<?php if(!empty($szPersonalStateError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szPersonalState]" id="szPersonalState" class="form-control" placeholder="<?php echo lang('contacts_state'); ?>" value="<?php echo set_input_value('arContact[szPersonalState]', (isset($arContactDetails['szPersonalState']) ? $arContactDetails['szPersonalState'] : ''));?>">
										<label for="szPersonalState" class="field-icon">
											<i class="fa fa-address-book"></i>
										</label>
									</label>
									<?php if(!empty($szPersonalStateError)){?><em class="state-error" for="szPersonalState"><?php echo $szPersonalStateError;?></em><?php }?>
								</div>

								<label for="idPersonalCountry" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_country'); ?></label>
								<?php $idPersonalCountryError = form_error('arContact[idPersonalCountry]');?>
								<div class="col-md-4">
									<label for="idPersonalCountry" class="field select<?php if(!empty($idPersonalCountryError)){?> state-error<?php }?>">
										<select name="arContact[idPersonalCountry]" id="idPersonalCountry" class="form-control" placeholder="<?php echo lang('contacts_country'); ?>" onchange="changeStateFieldForCRM(this, 'state-field');">
											<option value="">Select Country...</option>
											<?php foreach($arCountries as $country){?>
											<option value="<?php echo $country['id'];?>" <?php echo (set_input_value('arContact[idPersonalCountry]', (isset($arContactDetails['idPersonalCountry']) ? $arContactDetails['idPersonalCountry'] : 0)) == $country['id'] ? 'selected' : '');?>><?php echo $country['szName'];?></option>
											<?php }?>
										</select>							                    
										<label for="idPersonalCountry" class="field-icon">
											<i class="arrow"></i>
										</label>
									</label>
									<?php if(!empty($idPersonalCountryError)){?><em class="state-error" for="idPersonalCountry"><?php echo $idPersonalCountryError;?></em><?php }?>
								</div> 
							</div> 

							<div class="section row mb5">
								<label for="szPersonalPhone" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_phone'); ?></label> 
								<div class="col-md-4">
									<label for="szPersonalPhone" class="field prepend-icon<?php if(!empty($szPersonalPhoneError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szPersonalPhone]" id="szPersonalPhone" class="form-control" placeholder="<?php echo lang('contacts_phone'); ?>" value="<?php echo set_input_value('arContact[szPersonalPhone]', (isset($arContactDetails['szPersonalPhone']) ? $arContactDetails['szPersonalPhone'] : ''));?>">
										<label for="szPersonalPhone" class="field-icon">
											<i class="fa fa-phone"></i>
										</label>
									</label>
									<?php if(!empty($szPersonalPhoneError)){?><em class="state-error" for="szPersonalPhone"><?php echo $szPersonalPhoneError;?></em><?php }?>
								</div>
								<label for="szPersonalMobile" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_mobile'); ?></label> 
								<div class="col-md-4">
									<label for="szPersonalMobile" class="field prepend-icon<?php if(!empty($szPersonalMobileError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szPersonalMobile]" id="szPersonalMobile" class="form-control" placeholder="<?php echo lang('contacts_mobile'); ?>" value="<?php echo set_input_value('arContact[szPersonalMobile]', (isset($arContactDetails['szPersonalMobile']) ? $arContactDetails['szPersonalMobile'] : ''));?>">
										<label for="szPersonalMobile" class="field-icon">
											<i class="fa fa-phone"></i>
										</label>
									</label>
									<?php if(!empty($szPersonalMobileError)){?><em class="state-error" for="szPersonalMobile"><?php echo $szPersonalMobileError;?></em><?php }?>
								</div> 
							</div> 
						</div>
					</div>
				</div>

				<div class="panel panel-primary panel-border top mb15">
					<div class="panel-heading">
						<span class="panel-title"><i class="fa fa-briefcase"></i>&nbsp;&nbsp;<?php echo lang('contacts_business')." ".lang('contacts_information'); ?></span>
					</div>
					<div class="panel-body bg-light" style="padding-bottom:0;">
						<div class="admin-form">
							<div class="section row mb5">
								<label for="szBusinessCompanyName" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_company'); ?></label> 
								<div class="col-md-4">
									<label for="szBusinessCompanyName" class="field prepend-icon<?php if(!empty($szBusinessCompanyNameError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szBusinessCompanyName]" id="szBusinessCompanyName" class="form-control" placeholder="<?php echo lang('contacts_company'); ?>" value="<?php echo set_input_value('arContact[szBusinessCompanyName]', (isset($arContactDetails['szBusinessCompanyName']) ? $arContactDetails['szBusinessCompanyName'] : ''));?>">
										<label for="szBusinessCompanyName" class="field-icon">
											<i class="fa fa-briefcase"></i>
										</label>
									</label>
									<?php if(!empty($szBusinessCompanyNameError)){?><em class="state-error" for="szBusinessCompanyName"><?php echo $szBusinessAddressError;?></em><?php }?>
								</div> 
								<label for="szBusinessTitle" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_title'); ?></label> 
								<div class="col-md-4">
									<label for="szBusinessTitle" class="field prepend-icon<?php if(!empty($szBusinessTitleError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szBusinessTitle]" id="szBusinessTitle" class="form-control" placeholder="<?php echo lang('contacts_title'); ?>" value="<?php echo set_input_value('arContact[szBusinessTitle]', (isset($arContactDetails['szBusinessTitle']) ? $arContactDetails['szBusinessTitle'] : ''));?>">
										<label for="szBusinessTitle" class="field-icon">
											<i class="fa fa-briefcase"></i>
										</label>
									</label>
									<?php if(!empty($szBusinessTitleError)){?><em class="state-error" for="szBusinessTitle"><?php echo $szBusinessTitleError;?></em><?php }?>
								</div>
							</div>
							<div class="section row mb5">
								<label for="szBusinessAddress" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_address'); ?></label> 
								<div class="col-md-4">
									<label for="szBusinessAddress" class="field prepend-icon<?php if(!empty($szBusinessAddressError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szBusinessAddress]" id="szBusinessAddress" class="form-control" placeholder="<?php echo lang('contacts_address'); ?>" value="<?php echo set_input_value('arContact[szBusinessAddress]', (isset($arContactDetails['szBusinessAddress']) ? $arContactDetails['szBusinessAddress'] : ''));?>">
										<label for="szBusinessAddress" class="field-icon">
											<i class="fa fa-address-book"></i>
										</label>
									</label>
									<?php if(!empty($szBusinessAddressError)){?><em class="state-error" for="szBusinessAddress"><?php echo $szBusinessAddressError;?></em><?php }?>
								</div>

								<label for="szBusinessAddress2" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_address'); ?> 2</label> 
								<div class="col-md-4">
									<label for="szBusinessAddress2" class="field prepend-icon<?php if(!empty($szBusinessAddress2Error)){?> state-error<?php }?>">
										<input type="text" name="arContact[szBusinessAddress2]" id="szBusinessAddress2" class="form-control" placeholder="<?php echo lang('contacts_address'); ?> 2" value="<?php echo set_input_value('arContact[szBusinessAddress2]', (isset($arContactDetails['szBusinessAddress2']) ? $arContactDetails['szBusinessAddress2'] : ''));?>">
										<label for="szBusinessAddress2" class="field-icon">
											<i class="fa fa-address-book"></i>
										</label>
									</label>
									<?php if(!empty($szBusinessAddress2Error)){?><em class="state-error" for="szBusinessAddress2"><?php echo $szBusinessAddress2Error;?></em><?php }?>
								</div>
							</div> 

							<div class="section row mb5">
								<label for="szBusinessPostcode" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_postcode'); ?></label> 
								<div class="col-md-4">
									<label for="szBusinessPostcode" class="field prepend-icon<?php if(!empty($szBusinessPostcodeError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szBusinessPostcode]" id="szBusinessPostcode" class="form-control" placeholder="<?php echo lang('contacts_postcode'); ?>" value="<?php echo set_input_value('arContact[szBusinessPostcode]', (isset($arContactDetails['szBusinessPostcode']) ? $arContactDetails['szBusinessPostcode'] : ''));?>">
										<label for="szBusinessPostcode" class="field-icon">
											<i class="fa fa-address-book"></i>
										</label>
									</label>
									<?php if(!empty($szBusinessPostcodeError)){?><em class="state-error" for="szBusinessPostcode"><?php echo $szBusinessPostcodeError;?></em><?php }?>
								</div>
								<label for="szBusinessCity" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_city'); ?></label> 
								<div class="col-md-4">
									<label for="szBusinessCity" class="field prepend-icon<?php if(!empty($szBusinessCityError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szBusinessCity]" id="szBusinessCity" class="form-control" placeholder="<?php echo lang('contacts_city'); ?>" value="<?php echo set_input_value('arContact[szBusinessCity]', (isset($arContactDetails['szBusinessCity']) ? $arContactDetails['szBusinessCity'] : ''));?>">
										<label for="szBusinessCity" class="field-icon">
											<i class="fa fa-address-book"></i>
										</label>
									</label>
									<?php if(!empty($szBusinessCityError)){?><em class="state-error" for="szBusinessCity"><?php echo $szBusinessCityError;?></em><?php }?>
								</div> 
							</div>

							<div class="section row mb5">
								<label for="szBusinessState" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_state'); ?></label> 
								<div class="col-md-4">
									<label for="szBusinessState" class="field prepend-icon<?php if(!empty($szBusinessStateError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szBusinessState]" id="szBusinessState" class="form-control" placeholder="<?php echo lang('contacts_state'); ?>" value="<?php echo set_input_value('arContact[szBusinessState]', (isset($arContactDetails['szBusinessState']) ? $arContactDetails['szBusinessState'] : ''));?>">
										<label for="szBusinessState" class="field-icon">
											<i class="fa fa-address-book"></i>
										</label>
									</label>
									<?php if(!empty($szBusinessStateError)){?><em class="state-error" for="szBusinessState"><?php echo $szBusinessStateError;?></em><?php }?>
								</div>
								<label for="idBusinessCountry" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_country'); ?></label>
								<?php $idBusinessCountryError = form_error('arContact[idBusinessCountry]');?>
								<div class="col-md-4">
									<label for="idBusinessCountry" class="field select<?php if(!empty($idBusinessCountryError)){?> state-error<?php }?>">
										<select name="arContact[idBusinessCountry]" id="idBusinessCountry" class="form-control" placeholder="<?php echo lang('contacts_country'); ?>" onchange="changeStateFieldForCRM(this, 'state-field');">
											<option value="">Select Country...</option>
											<?php foreach($arCountries as $country){?>
											<option value="<?php echo $country['id'];?>" <?php echo (set_input_value('arContact[idBusinessCountry]', (isset($arContactDetails['idBusinessCountry']) ? $arContactDetails['idBusinessCountry'] : 0)) == $country['id'] ? 'selected' : '');?>><?php echo $country['szName'];?></option>
											<?php }?>
										</select>							                    
										<label for="idBusinessCountry" class="field-icon">
											<i class="arrow"></i>
										</label>
									</label>
									<?php if(!empty($idBusinessCountryError)){?><em class="state-error" for="idBusinessCountry"><?php echo $idBusinessCountryError;?></em><?php }?>
								</div> 
							</div>
							
							<div class="section row mb5">
								<label for="szBusinessPhone" class="field-label text-muted fs16 mb10 col-md-2"><?php echo lang('contacts_work_phone'); ?></label> 
								<div class="col-md-4">
									<label for="szBusinessPhone" class="field prepend-icon<?php if(!empty($szBusinessPhoneError)){?> state-error<?php }?>">
										<input type="text" name="arContact[szBusinessPhone]" id="szBusinessPhone" class="form-control" placeholder="<?php echo lang('contacts_work_phone'); ?>" value="<?php echo set_input_value('arContact[szBusinessPhone]', (isset($arContactDetails['szBusinessPhone']) ? $arContactDetails['szBusinessPhone'] : ''));?>">
										<label for="szBusinessPhone" class="field-icon">
											<i class="fa fa-phone"></i>
										</label>
									</label>
									<?php if(!empty($szBusinessPhoneError)){?><em class="state-error" for="szBusinessPhone"><?php echo $szBusinessPhoneError;?></em><?php }?>
								</div>  
							</div> 
						</div>
					</div>
				</div> 

				<div class="panel-footer text-center">
					<div class="admin-form">
						<a href="<?php echo base_url().'contacts/all' ?>" class="btn btn-default"><?php echo lang('common_cancel'); ?></a>
						<button type="submit" class="btn btn-primary"><?php echo ($idContact > 0 ? lang('common_save') : lang('common_add')); ?></button>
						<input type="hidden" name="p_func" value="Save Contact Details">
					</div>			                    	
				</div>
			</div>
		</div> 
	</section>
</form>