	<section id="content_wrapper">		
		<!-- Start: Topbar -->
      	<header id="topbar" class="alt">
        	<div class="topbar-left">
          		<ol class="breadcrumb">
            		<li class="crumb-active">
              			<a href="<?php echo base_url();?>dashboard">Dashboard</a>
            		</li>
            		<li class="crumb-trail">Not Permitted</li>
          		</ol>
        	</div>        	
     	</header>
      	<!-- End: Topbar -->

      	<section id="content" class="table-layout">

        	<!-- begin: .tray-center -->
        	<div class="tray tray-center" style="height: 621px;">

	            <!-- Begin: Content Header -->
	            <div class="content-header">
	              	<h2>Not Permitted</h2>
	              	<p class="lead text-danger">You are not permitted to access this feature.</p>
	            </div>
         	</div>
		</section>
	</section>