<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        else if(!hasPermission('manage.roles'))
        {
            redirect(base_url() . 'dashboard', 'refresh');
        }
        else
        {
            /* Load Role Model */
            $this->load->model('Role_Model');

            /* Load form helper */ 
            $this->load->helper('form');

            /* Load form validation library */ 
            $this->load->library('form_validation');

            /* Changing error delimiters Globally */
            $this->form_validation->set_error_delimiters('', '');
        }            
    }

    public function index($arParams=array())
    {
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        else
        {
            $data['szMetaTagTitle'] = $data['szPageName'] = "Access";
			$data['szMetaTagTitle'] = "Roles";
            $data['arLoginUser'] = $this->session->userdata('login_user');
            $data['isLoadDataTable'] = true;
            $data['arRoles'] = $this->Role_Model->getAllRoles();

            $data = array_merge($data, $arParams);

            // set breadcrumb
            #$data['arBreadCrumb'][] = array('name'=>'Dashboard', 'link'=>'dashboard');
            #$data['arBreadCrumb'][] = array('name'=>'Roles', 'link'=>'roles');

            $this->load->view('layout/header', $data);
            $this->load->view('roles/list');
            $this->load->view('layout/footer');
        }
    }

    public function edit($szRoleKey='')
    {
        $this->add($szRoleKey);		
    }

    public function add($szRoleKey='')
    {
        if(!is_user_login())
        {
                redirect(base_url().'login', 'refresh');
        }
        else
        {
            $data['idRole'] = 0;
            $data['isFormError'] = false;
            $data['isDetailsSaved'] = false;
            $data['arRoleDetails'] = array();
            $data['arLoginUser'] = $this->session->userdata('login_user');	

            if($this->input->post('p_func') == 'Save Role Details')
            {			
                $idRole = (int)$this->input->post('arRole[id]');

                //Set validation rules
                $this->form_validation->set_rules('arRole[szRoleName]', 'Role name', "required|is_unique[tbl_roles:szRoleName:id:{$idRole}]");
                $this->form_validation->set_rules('arRole[szDisplayName]', 'Role display name', "required|is_unique[tbl_roles:szDisplayName:id:{$idRole}]");

                if($this->form_validation->run() == TRUE) 
                {				
                    $arRoleDetails = $this->input->post('arRole');
                    if($data['idRole'] = $this->Role_Model->saveRoleDetails($arRoleDetails, $data['arLoginUser']['id']))
                    {
                        $data['isDetailsSaved'] = true;
                        
                        // save role permissions
                        $this->Role_Model->addRolePermission($data['idRole'], $this->input->post('arRolePermissions', true), $data['arLoginUser']['id']);
                    }
                }
                else
                {
                    $data['isFormError'] = true;
                }
            }

            if($szRoleKey)
            {
                $data['arRoleDetails'] = $this->Role_Model->getRoleByUniqueKey($szRoleKey);
                $data['idRole'] = (isset($data['arRoleDetails']['id']) ? (int)$data['arRoleDetails']['id'] : 0);
            }
            else if($data['idRole'] > 0)
            {
                $data['arRoleDetails'] = $this->Role_Model->getRoleByID($data['idRole']);
            }

            $data['szPageName'] = "Roles";
            $data['szMetaTagTitle'] = ($data['idRole'] > 0 ? 'Edit Role' : 'Add New Role');	
            $data['isLoadAdminForm'] = true;
            $data['isLoadCMSScripts'] = true;
            $data['isLoadValidationScript'] = true;
            
            // set breadcrumb
            $data['arRolePermissions'] = array();
            $data['arBreadCrumb'][] = array('name'=>'Dashboard', 'link'=>'dashboard');
            $data['arBreadCrumb'][] = array('name'=>'Roles', 'link'=>'roles');
            if(!empty($data['arRoleDetails']))
            {
                $data['arBreadCrumb'][] = array('name'=>'Edit Role');
                $data['arRolePermissions'] = $this->Role_Model->getRolePermissions($data['arRoleDetails']['id']);
            }
            else
                $data['arBreadCrumb'][] = array('name'=>'Add New Role');
            
            // get permissions
            $this->load->model('Permission_Model');
            $data['arPermissions'] = $this->Permission_Model->getAllPermissions();

            $this->load->view('layout/header', $data);
            $this->load->view('roles/add_edit');
            $this->load->view('layout/footer');
        }
    }

    function delete($szRoleKey='')
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }	

        $message = array();
        $szRoleKey = trim($szRoleKey);
        if($szRoleKey != '')
        {
            $arRoleDetails = $this->Role_Model->getRoleByUniqueKey($szRoleKey);
            if(!empty($arRoleDetails))
            {
                if($arRoleDetails['isRemovable'] == 1)
                {
                    if($arRoleDetails['isDeleted'] == 0)
                    {
                        $arLoginUser = $this->session->userdata('login_user');
                        $this->Role_Model->deleteRole($arRoleDetails, $arLoginUser['id']);
                        $message['szSuccessMessage'] = "Role {$arRoleDetails['szDisplayName']} has been deleted successfully";
                    }
                    else
                    {
                        $message['szErrorMessage'] = "Role {$arRoleDetails['szDisplayName']} is already deleted.";
                    }
                }
                else
                {
                    $message['szErrorMessage'] = "Role {$arRoleDetails['szDisplayName']} is not removable.";
                }
            }
            else
            {
                $message['szErrorMessage'] = "Role not found.";
            }
        }
        else
        {
            $message['szErrorMessage'] = "Invalid Role delete request.";
        }

        $this->index($message);
    }
}
?>