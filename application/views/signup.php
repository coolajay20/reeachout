<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	#if (!isset($_GET['bypass'])) { exit('The system is currently in maintance mode.'); }

	$remember = array(
		'name'	=> 'remember',
		'id'	=> 'remember',
		'value'	=> 1,
		'checked' => set_value('remember'),
	);

	/* Fetch error message */
	$szFirstNameError = form_error('arUser[szFirstName]');
	$szLastNameError = form_error('arUser[szLastName]');
	$szEmailError = form_error('arUser[szEmail]'); 
	$szUserNameError = form_error('arUser[szUserName]'); 
	$szUSIUserNameError = form_error('arUser[szUSIUserName]'); 
	$szPasswordError = form_error('arUser[szPassword]');

#var_dump($_SESSION['sponsor']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Done-For-You Marketing System to put you on the fast track to generating your own leads and nurturing them into potential clients and business partners.">
  <meta name="keywords" content="usi tech marketing system signup">
  <meta name="author" content="">
  
	<meta property="og:title" content="<?php echo WEBSITE_NAME_LONG; ?>" />
	<meta property="og:description" content="Done-For-You Marketing System to put you on the fast track to generating your own leads and nurturing them into potential clients and business partners." />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo base_url();?>signup" />
	<meta property="og:image" content="<?php echo base_url();?>assets/images/fbshare-signup.jpg" />
  
  <link rel="shortcut icon" href="images/favicon.png" type="image/png">
 
  <title>Signup | <?php echo WEBSITE_NAME; ?></title>

  <link href="<?php echo base_url();?>assets/backend/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body>

<!-- Custom Styles -->
<style>
.signuppanel form {
	color: #fff;
}
	
.signuppanel label.checkboxclass {
    display: inline;
	padding-left: 0;
	position: static;
}
	
.signuppanel checkbox {
	margin-left: 5px;
}
	
.signuppanel .help-block {
	font-style: italic;
	font-size: 12px;
	color: #ccc;
	line-height: 1.3em;
}
	
.signuppanel a {
	color: #bbb;
}

.signup-footer {
    border-top: none;
    margin-top: 0;
}
	
@media screen and (max-width: 640px) {
	.pull-right, .logopanel {
		display: block !important;
		float: none !important;
		text-align: center;
	}
	
	@media screen and (max-width: 640px)
	.signuppanel .form-control {
		margin-bottom: 0px;
	}
}
	
em.state-error {
    color: white;
    background-color: darkred;
    padding: 5px 8px 5px 4px;
	font-size: 10px;
}
</style>
<!-- END Custom Styles -->

<section>

    <div class="signuppanel">
        
        <div class="row">

            <div class="col-md-12">
               
                <div class="signup-info">
                    <div class="logopanel">
                        <a href="/"><img class="mb10" src="<?php echo base_url();?>assets/images/logo_beta.png" alt=""></a>
                    	<p class="mt10 pull-right"><?php echo lang('signup_already_member');?> <a style="color: #428bca" href="/login/"><?php echo lang('signup_sign_in');?></a></p>
                    </div><!-- logopanel -->
                </div><!-- signup-info -->
                
				<form id="signup" name="signup" class="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>signup">

					<h4><?php echo lang('signup_for_account');?></h4>
			   
				   <!-- First and Last Name Inputs -->
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo lang('signup_name');?></label>
						<div class="col-md-4">
							<input type="text" class="form-control required" name="arUser[szFirstName]" id="szFirstName" placeholder="<?php echo lang('signup_name_first');?>" value="<?php echo set_input_value('arUser[szFirstName]');?>">
							<?php if(!empty($szFirstNameError)){?><em class="state-error" for="szFirstName"><?php echo $szFirstNameError;?></em><?php }?>
						</div>
						<div class="col-md-5">
							<input type="text" class="form-control required" name="arUser[szLastName]" id="szLastName" placeholder="<?php echo lang('signup_name_last');?>" value="<?php echo set_input_value('arUser[szLastName]');?>">
							<?php if(!empty($szLastNameError)){?><em class="state-error" for="szLastName"><?php echo $szLastNameError;?></em><?php }?>
						</div>
						<span class="help-block col-md-9 col-md-offset-3"><?php echo lang('signup_name_tip');?></span>
					</div>

				   <!-- E-mail Input -->
					<div class="form-group">
						<label for="szEmail" class="col-md-3 control-label"><?php echo lang('signup_email');?></label>
						<div class="col-md-9">
							<input type="email" class="form-control required" name="arUser[szEmail]" id="szEmail" value="<?php echo set_input_value('arUser[szEmail]');?>">
							<?php if(!empty($szEmailError)){?><em class="state-error" for="szEmail"><?php echo $szEmailError;?></em><?php }?>
							<span class="help-block"><?php echo lang('signup_email_tip');?></span>
						</div>
					</div>

				   <!-- USI Username Input -->
					<div class="form-group">
						<label for="szUSIUserName" class="col-md-3 control-label"><?php echo lang('signup_usi_username');?></label>
						<div class="col-md-9">
							<input type="text" class="form-control required" name="arUser[szUSIUserName]" id="szUSIUserName" value="<?php echo set_input_value('arUser[szUSIUserName]');?>">
							<?php if(!empty($szUSIUserNameError)){?><em class="state-error" for="szUSIUserName"><?php echo $szUSIUserNameError;?></em><?php }?>
							<span class="help-block"><?php echo lang('signup_usi_username_tip');?></span>
						</div>
					</div>

				   <!-- System Username Input -->
					<div class="form-group">
						<label for="szUserName" class="col-md-3 control-label"><?php echo lang('signup_system_id');?></label>
						<div class="col-md-9">
							<input type="text" class="form-control required" name="arUser[szUserName]" id="szUserName" value="<?php echo set_input_value('arUser[szUserName]');?>">
							<?php if(!empty($szUserNameError)){?><em class="state-error" for="szUserName"><?php echo $szUserNameError;?></em><?php }?>
							<span class="help-block"><?php echo lang('signup_system_id_tip');?></span>
						</div>
					</div>

					<!-- Password Input -->
					<div class="form-group">
						<label for="szPassword" class="col-md-3 control-label"><?php echo lang('signup_password');?></label>
						<div class="col-md-9">
							<input type="password" class="form-control required" name="arUser[szPassword]" id="szPassword">
							<?php if(!empty($szPasswordError)){?><em class="state-error" for="szPassword"><?php echo $szPasswordError;?></em><?php }?>
							<span class="help-block"><?php echo lang('signup_password_tip');?></span>
						</div>
					</div>
					
					<div class="form-group">
						<div class="ckbox ckbox-default col-md-9 col-md-offset-3">
							<input class="font-control required" type="checkbox" value="1" id="checkboxDefault">
							<label class="checkboxclass" for="checkboxDefault"><?php echo lang('signup_terms_privacy')?></label>
						</div>
						
						<div class="ckbox ckbox-default col-md-9 col-md-offset-3">
							<?php
							if ($_SESSION['sponsor'] != '') {
								echo '<small>Your Sponsor is: '.$_SESSION['sponsor']['fname'].' '.$_SESSION['sponsor']['lname'].'</small>';
							}
							?>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-9 col-md-offset-3">
							<button id="submitbtn" type="submit" class="btn btn-primary pull-right"><?php echo lang('signup_next'); ?>&nbsp;&nbsp;<i style="font-size: 11px;" class="fa fa-chevron-right"></i></button>
						</div>
					</div>
					
					<input type="hidden" name="szSponsor" value="<?php echo $_SESSION['sponsor']['id'] ?>">
					<input type="hidden" name="p_func" value="Register">
				</form>
           Language: <a href="?lang=en">English</a> | <a href="?lang=sp">Spanish</a>
            </div><!-- col-sm-6 -->
            
        </div><!-- row -->
        
        <div class="signup-footer">
            <div class="text-center">
                &copy; 2017 <?php echo WEBSITE_NAME; ?>. All Rights Reserved.
            </div>
            <!--
            <div class="pull-right">
                Created By: <a href="#">X</a>
            </div>
            -->
        </div>
        
    </div><!-- signuppanel -->
    
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"><?php echo lang('signup_terms_privacy_title'); ?></h4>
		  </div>
		  <div class="modal-body">
			<p></p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('signup_terms_privacy_close'); ?></button>
		  </div>
		</div>

	  </div>
	</div>
  
</section>

<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/bootstrap.min.js"></script>

<script>
// Event for checkboxes
$('#submitbtn').attr('disabled', true);
$("#checkboxDefault").on('change', function() {
  if(this.checked) {
    // You custom logic will come here
    $('#submitbtn').attr('disabled', false);
  } else {
    $("#submitbtn").attr('disabled', true);
  }
});
</script>

</body>
</html>