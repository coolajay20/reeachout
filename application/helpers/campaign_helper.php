<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function startCampaign($idDelivery=0)
{
    $CI = get_instance();
    
    /* Load Campaign Model */ 
    $CI->load->model('Campaign_Model');

    /* Load Email Model */ 
    $CI->load->model('Email_Model');

    /* Load Contact Model */ 
    $CI->load->model('Contact_Model');

    /* Load User Model */ 
    $CI->load->model('User_Model');

    //Fetching all active campaign from the system 
    $arCampaign = $CI->Campaign_Model->getAllCampaignsForCron($idDelivery);  
    $szDeveloperNotes = "\n\n************************** ".date('Y-m-d H:i:s')."************************** ";
    setLog($szDeveloperNotes);

    if(!empty($arCampaign))
    {
        foreach ($arCampaign as $arCampaigns)
        {  
            if((int)$idDelivery == 0)
            {
                print_R($arCampaigns);
                echo "<br><br>";
            }
            
            $idEmailTemplate = $arCampaigns['idEmailTemplate']; 
            $idCampaignDelivery = $arCampaigns['idCampaignDelivery']; 
            $idContact = $arCampaigns['idContact'];
            $idCampaign = $arCampaigns['idCampaign'];

            $szDeveloperNotes .= "\n > Working on Contact ID: ".$idContact.", Template ID: ".$idEmailTemplate.", Campaign ID: ".$idCampaign.", Campaign Delivery ID: ".$idCampaignDelivery;

            $arEmailTemplate = $CI->Campaign_Model->getTemplateByID($idEmailTemplate);
            if(empty($arEmailTemplate))
            {
                $szDeveloperNotes .= "\n > No E-mail template found with Template ID: ".$idEmailTemplate;
                $szDeveloperNotes .= "\n > Quit - Process completes.\n";
            } 
            $arContacts = $CI->Contact_Model->getContactByID($idContact);
            if(empty($arContacts))
            {
                $szDeveloperNotes .= "\n > No contact found with Contact ID: ".$idContact;
                $szDeveloperNotes .= "\n > Quit - Process completes.\n";
            }
            
            $arCampaignDetails = $CI->Campaign_Model->getCampaignByID($idCampaign);
            if(empty($arCampaignDetails))
            {
                $szDeveloperNotes .= "\n > No Campaign found with Campaign ID: ".$idContact;
                $szDeveloperNotes .= "\n > Quit - Process completes.\n";
            } 

            $szDeveloperNotes .= "\n > Working on E-mail Template: ".$arEmailTemplate['szName']."";

            $szEmailSubject = $arEmailTemplate['szSubject'];
            $szEmailBody = $arEmailTemplate['szMessageContent'];


            $szDeveloperNotes .= "\n > Sending E-mail to contact with Name: ".$arContacts['szFirstName']." ".$arContacts['szLastName']." E-mail: ".$arContacts['szEmail'];

            $idUser = $arContacts['idUser'];  
            $szContactUniqueKey = $arContacts['szUniqueKey'];  
            $szCampaignUniqueKey = $arCampaignDetails['szUniqueKey'];  
            
            $szUnsubscribeLink = base_url()."unsubscribe/".$szCampaignUniqueKey."/".$szContactUniqueKey."/";
            $szUnsubscribeAnchorTag = "<a href='".$szUnsubscribeLink."'>unsubscribe</a>";

            //Fetching customer details
            $arUserDetails = $CI->User_Model->getUserByID($idUser);  

            $arCountry = array(); 
            if((int)$arUserDetails['idCountry'] > 0) {
                $arCountry = $CI->User_Model->getCountries($arUserDetails['idCountry']);                        
            }

            $szUserCountry = (!empty($arCountry) ? $arCountry[0]['szName'] : '');

            $arEmailData = array();
            //Fields for contacts
            $arEmailData['szFirstName'] = $arContacts['szFirstName'];
            $arEmailData['szLastName'] = $arContacts['szLastName'];
            $arEmailData['szFullName'] = $arContacts['szFirstName'] ." ".$arContacts['szLastName'];
            $arEmailData['szEmail'] = $arContacts['szEmail'];
            $arEmailData['szHomePhone'] = $arContacts['szPersonalPhone'];
            $arEmailData['szMobile'] = $arContacts['szPersonalMobile'];
            $arEmailData['szWorkPhone'] = $arContacts['szBusinessPhone'];
            $arEmailData['szUnsubscribe'] = $szUnsubscribeAnchorTag;

            //Fileds for customer
            $arEmailData['szUserFirstName'] = $arUserDetails['szFirstName'];
            $arEmailData['szUserLastName'] = $arUserDetails['szLastName'];
            $arEmailData['szUserFullName'] = $arUserDetails['szFirstName'] ." ".$arUserDetails['szLastName'];
            $arEmailData['szUserEmail'] = $arUserDetails['szEmail'];
            $arEmailData['szUserPhone'] = $arUserDetails['szPhone'];
            $arEmailData['szSiteUserName'] = $arUserDetails['szUserName'];
            $arEmailData['szUSIUserName'] = $arUserDetails['szUSIUserName'];
            $arEmailData['szUserAddress'] = $arUserDetails['szAddress'];
            $arEmailData['szUserCity'] = $arUserDetails['szCity'];
            $arEmailData['szUserState'] = $arUserDetails['szState'];
            $arEmailData['szUserCountry'] = $szUserCountry;

            //Social Links
            $arEmailData['szFacebookLink'] = $arUserDetails['szFacebookLink'];
            $arEmailData['szGoogleLink'] = $arUserDetails['szGoogleLink'];
            $arEmailData['szTwitterID'] = $arUserDetails['szTwitterID']; 
            $arEmailData['szLinkedInID'] = $arUserDetails['szLinkedInID'];
            $arEmailData['szDribbbleID'] = $arUserDetails['szDribbbleID'];
            $arEmailData['szSkypeID'] = $arUserDetails['szSkypeID']; 

            if (count($arEmailData) > 0)
            {
                foreach ($arEmailData as $replace_key => $replace_value)
                {
                    $szEmailBody = str_replace($replace_key, $replace_value, $szEmailBody);
                    $szEmailSubject= str_replace($replace_key, $replace_value, $szEmailSubject);
                }
            } 
            try
            {
                $szDeveloperNotes .= "\n > Ready to send E-mail with Subject: ".$szEmailSubject." ";

                /*
                * building block for sending E-mail
                */
                $mailBasicDetailsAry = array();
                $mailBasicDetailsAry['szEmailTo'] = $arEmailData['szEmail'];  
                $mailBasicDetailsAry['szEmailSubject'] = $szEmailSubject;
                $mailBasicDetailsAry['szEmailMessage'] = $szEmailBody; 
                $mailBasicDetailsAry['idUser'] = $idUser; 
                $mailBasicDetailsAry['idContact'] = $idContact; 
                $mailBasicDetailsAry['idEmailCampaign'] = $idCampaign; 
                $mailBasicDetailsAry['idEmailTemplate'] = $idEmailTemplate; 
                $mailBasicDetailsAry['idCampaignDelivery'] = $idCampaignDelivery; 
                $mailBasicDetailsAry['iEmailType'] = 2; //2 means the E-mail is sent for E-mail campaign 
                $mailBasicDetailsAry['szEmailFrom'] = $arUserDetails['szUserName'].'@usitech.io';
                $mailBasicDetailsAry['szFromUserName'] = $arUserDetails['szFirstName'] ." ".$arUserDetails['szLastName'];
                $mailBasicDetailsAry['szEmailCC'] = '';
                $mailBasicDetailsAry['bAttachments'] = false;
                $mailBasicDetailsAry['szAttachmentFiles'] = ""; 
 
                $CI->Email_Model->sendEmail($mailBasicDetailsAry); 
                $szDeveloperNotes .= "\n >E-mail successfully sent on: ".$arEmailData['szEmail'];
                $szDeveloperNotes .= "\n >Process completes.\n";
            }
            catch (Exception $ex) 
            {
                $szDeveloperNotes .= "\n >Oops! Got exception while sending E-mail. Exception: ".$ex->getMessage()." ";
                $szDeveloperNotes .= "\n >Process completes.\n";
            }  

            //updating szStatus to completed for the email template
            $arUpdateTemplate = array();
            $arUpdateTemplate['id'] = $idCampaignDelivery;
            $arUpdateTemplate['dtSentOn '] = date('Y-m-d H:i:s');
            $arUpdateTemplate['szStatus  '] = __EMAIL_STATUS_COMPLETE__;
            $arUpdateTemplate['dtStatusUpdated '] = date('Y-m-d H:i:s');

            $CI->Campaign_Model->updateCampaignStatus($arUpdateTemplate);  
        }
    }
    else
    {
        $szDeveloperNotes .= "\n > We don't have any Active Campaign to send.";
        $szDeveloperNotes .= "\n >Process completes.\n";
    }
    setLog($szDeveloperNotes);
}
    
function setLog($szLogMessage)
{
    if(!empty($szLogMessage))
    {
        $szLogFilePath = FCPATH."/logs/campaignLogs".date('Y-m-d').".log";
        $f = fopen($szLogFilePath, "a");
        fwrite($f,$szLogMessage); 
        fclose($f);
    }        
}
?>