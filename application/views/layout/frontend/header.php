<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Done-For-You Marketing System to put you on the fast track to generating your own leads and nurturing them into potential clients and business partners.">
    <meta name="keywords" content="usi tech marketing system">
    <meta name="author" content="">

    <title><?php if(isset($szMetaTagTitle)) echo $szMetaTagTitle; else echo "Bitcoin Labs | Digital Marketing System";?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/home.css" rel="stylesheet">
    
    <!-- FontAwesome core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    
    <style>
		footer small a {color: #666;}
			footer small a:hover {color: #555;}
		
		/* Responsive Video Frame Styles */
		.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; border: 3px #333 solid; }

		/* New Custom Button Gold */
		.btn {
			padding: 14px 24px;
			border: 0 none;
			font-weight: 700;
			letter-spacing: 1px;
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			text-transform: uppercase;}
		.btn:focus, .btn:active:focus, .btn.active:focus {
			outline: 0 none;}
		.btn-cta {
			color: rgb(0, 0, 0);
			background: linear-gradient(135deg, rgb(255, 204, 51) 20%, rgb(255, 179, 71) 80%);}
		.btn-cta:hover, .btn-cta:focus, .btn-cta:active, .btn-cta.active, .open > .dropdown-toggle.btn-cta {
			color: #444444;}
		.btn-cta:active, .btn-cta.active {
			box-shadow: none;}
		
		/* Custom Class' for Elements */
		.fa-check {color: green;}
		.fa-times {color: red;}
		.sticky {position: fixed; bottom: 0; width: 100%; z-index: 9999;}
		.op-9 {opacity: .9;}
		.bg-grey {background-color: #eeeeee;}
		.bg-gold {background-color: #f6a91a;}
		.text-white {color: #ffffff;}
		.text-gold {color: #f6a91a;}
			.text-gold strong {text-decoration:  underline;}
		.mask-dark {
			background-color: rgba(0,0,0,0.6);
			background-size: cover;
			width: 100%;
		}
		.mask-xdark {
			background-color: rgba(0,0,0,0.9);
			background-size: cover;
			width: 100%;
		}
		.mask-light {
			background-color: rgba(255,255,255,255.6);
			background-size: cover;
			width: 100%;
		}
		img {max-width: 100%;}
		
		/* Styles for Overview Section */
		section#overview {
			background: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/H8WuRINimqur8ud/the-growth-of-the-virtual-currency-bitcoin-bitcoin-growth-chart-candle-chart-on-the-online-forex-monitor_syyjyb1z__F0000.png') bottom center no-repeat fixed;
			background-size: cover;
		}
		
		/* Styles for Strategy Section */
		section#strategy {
			
		}
		
		/* Styles for Features Section */
		section#features {
			
		}
		section#features h4 {
			margin-top: 5px;
		}
		
		/* Sticky CTA Style */
		section#cta {
			border-top: 2px #f6a91a solid;
		}
		
		/* Custom Blockquote Styling */
		blockquote{
		  display:block;
		  background: #fff;
		  padding: 15px 20px 15px 45px;
		  margin: 0 0 20px;
		  position: relative;

		  /*Font*/
		  font-family: Georgia, serif;
		  font-size: 16px;
		  line-height: 1.2;
		  color: #666;
		  text-align: justify;

		  /*Borders - (Optional)*/
		  border-left: 15px solid #f6a91a;
		  border-right: 2px solid #f6a91a;

		  /*Box Shadow - (Optional)*/
		  -moz-box-shadow: 2px 2px 15px #ccc;
		  -webkit-box-shadow: 2px 2px 15px #ccc;
		  box-shadow: 2px 2px 15px #ccc;
		}

		blockquote::before{
		  content: "\201C"; /*Unicode for Left Double Quote*/

		  /*Font*/
		  font-family: Georgia, serif;
		  font-size: 60px;
		  font-weight: bold;
		  color: #999;

		  /*Positioning*/
		  position: absolute;
		  left: 10px;
		  top:5px;
		}

		blockquote::after{
		  /*Reset to make sure*/
		  content: "";
		}

		blockquote a{
		  text-decoration: none;
		  background: #eee;
		  cursor: pointer;
		  padding: 0 3px;
		  color: #c76c0c;
		}

		blockquote a:hover{
		 color: #666;
		}

		blockquote em{
		  font-style: italic;
		}
		
		/* Price Table Styles */
		.panel
		{
			text-align: center;
		}
		.panel:hover { box-shadow: 0 1px 5px rgba(0, 0, 0, 0.4), 0 1px 5px rgba(130, 130, 130, 0.35); }
		.panel-body
		{
			padding: 0px;
			text-align: center;
		}

		.the-price
		{
			background-color: rgba(220,220,220,.17);
			box-shadow: 0 1px 0 #dcdcdc, inset 0 1px 0 #fff;
			padding: 20px;
			margin: 0;
		}

		.the-price h1
		{
			line-height: 1em;
			padding: 0;
			margin: 0;
		}

		.subscript
		{
			font-size: 25px;
		}

		/* CSS-only ribbon styles    */
		.cnrflash
		{
			/*Position correctly within container*/
			position: absolute;
			top: -9px;
			right: 4px;
			z-index: 1; /*Set overflow to hidden, to mask inner square*/
			overflow: hidden; /*Set size and add subtle rounding  		to soften edges*/
			width: 100px;
			height: 100px;
			border-radius: 3px 5px 3px 0;
		}
		.cnrflash-inner
		{
			/*Set position, make larger then 			container and rotate 45 degrees*/
			position: absolute;
			bottom: 0;
			right: 0;
			width: 145px;
			height: 145px;
			-ms-transform: rotate(45deg); /* IE 9 */
			-o-transform: rotate(45deg); /* Opera */
			-moz-transform: rotate(45deg); /* Firefox */
			-webkit-transform: rotate(45deg); /* Safari and Chrome */
			-webkit-transform-origin: 100% 100%; /*Purely decorative effects to add texture and stuff*/ /* Safari and Chrome */
			-ms-transform-origin: 100% 100%;  /* IE 9 */
			-o-transform-origin: 100% 100%; /* Opera */
			-moz-transform-origin: 100% 100%; /* Firefox */
			background-image: linear-gradient(90deg, transparent 50%, rgba(255,255,255,.1) 50%), linear-gradient(0deg, transparent 0%, rgba(1,1,1,.2) 50%);
			background-size: 4px,auto, auto,auto;
			background-color: #aa0101;
			box-shadow: 0 3px 3px 0 rgba(1,1,1,.5), 0 1px 0 0 rgba(1,1,1,.5), inset 0 -1px 8px 0 rgba(255,255,255,.3), inset 0 -1px 0 0 rgba(255,255,255,.2);
		}
		.cnrflash-inner:before, .cnrflash-inner:after
		{
			/*Use the border triangle trick to make  				it look like the ribbon wraps round it's 				container*/
			content: " ";
			display: block;
			position: absolute;
			bottom: -16px;
			width: 0;
			height: 0;
			border: 8px solid #800000;
		}
		.cnrflash-inner:before
		{
			left: 1px;
			border-bottom-color: transparent;
			border-right-color: transparent;
		}
		.cnrflash-inner:after
		{
			right: 0;
			border-bottom-color: transparent;
			border-left-color: transparent;
		}
		.cnrflash-label
		{
			/*Make the label look nice*/
			position: absolute;
			bottom: 0;
			left: 0;
			display: block;
			width: 100%;
			padding-bottom: 5px;
			color: #fff;
			text-shadow: 0 1px 1px rgba(1,1,1,.8);
			font-size: 0.95em;
			font-weight: bold;
			text-align: center;
		}
		
		/* Responsive Styling */
		@media screen and (max-width: 480px) {
			p {font-size: .8rem;}
			h1 {font-size: 1.7rem;}
			h2 {font-size: 1.5rem;}
			h3 {font-size: 1.3rem;}
			h4 {font-size: 1.2rem;}
			blockquote {font-size: .5rem;}
			
			section#cta.sticky p {font-size: .5em; display: none;}
		}
	</style>

  </head>

  <body>
