		<!-- Begin CSS -->
		<link href="http://5elementsofmarketing.com/order/assets/css/vendor/datepicker3.css" rel="stylesheet">
		<link href="http://5elementsofmarketing.com/order/assets/css/vendor/sweet-alert.css" rel="stylesheet">
		<link href="http://5elementsofmarketing.com/order/assets/css/helpers.css" rel="stylesheet">
		<link href="http://5elementsofmarketing.com/order/assets/css/app.css" rel="stylesheet">
		<link href="http://5elementsofmarketing.com/order/assets/css/custom.css" rel="stylesheet">

		<!-- Begin JS -->
		<script type="text/javascript">
			var checkNotification = true;
		</script>
		<script src="http://5elementsofmarketing.com/order/assets/js/vendor/stripe.min.js"></script>
		<script src="http://5elementsofmarketing.com/order/assets/js/vendor/sweet-alert.min.js"></script>
		<script src="http://5elementsofmarketing.com/order/assets/js/vendor/jquery.form.min.js"></script>
		<script src="http://5elementsofmarketing.com/order/assets/js/vendor/jquery.jGet.js"></script>
		<script src="http://5elementsofmarketing.com/order/assets/js/vendor/jquery.validate.min.js"></script>
		<script src="http://5elementsofmarketing.com/order/assets/js/vendor/jquery.validate.additional-methods.min.js"></script>
		<script src="http://5elementsofmarketing.com/order/assets/js/app.js"></script>
        <script src="http://5elementsofmarketing.com/order/assets/js/functions.js"></script>

		<!--[if lt IE 9]>
			<script src="http://5elementsofmarketing.com/js/html5shiv.min.js"></script>
		<![endif]-->
	</head>
    <body>

		<noscript>
	    	<div class="alert alert-danger mt20neg">
	    		<div class="container aligncenter">
	    			<strong>Oops!</strong> It looks like your browser doesn't have Javascript enabled.  Please enable Javascript to use this website.
	    		</div>
	    	</div>
	    </noscript>
    			
		
	

		<div class="container">
   		
    		
			

	    	<form action="#" method="post" class="validate form-horizontal " id="order_form">
	    		<input type="hidden" name="csrf" value="b9c67518140d5d3a5a994fb65cad42ca">
	    		<input type="hidden" name="action" value="process_payment">
	    						<input type="hidden" class="enable-subscriptions" value="stripe_only">
				<input type="hidden" class="publishable-key" value="pk_live_15suy6d2pAJeSjdHV7oiyGtx">

				<div class="row">
					<div class="col-md-6">

						<h3 class="colorgray mb30">Payment Details</h3>

						
							
								<!-- <div class="form-group">
									<label class="col-md-3 control-label"><span class="colordanger">*</span>Package(s)</label>
									<div class="col-md-9">
										<select name="item_id" class="form-control" data-rule-required="true">
											<option value="">-- Select Item --</option>
																						<option value="" data-name="" data-price="" > ()</option>
																					</select>
									</div>
								</div>-->
								
								<div class="form-group" id="package_options">
									<label class="col-md-3 control-label"><span class="colordanger">*</span>Package(s)</label>
									<div class="col-md-9">
										<label><input type="checkbox" name="item_id[]" class="order_purchase_items" value="1" id="order_items_1" data-name="Facebook Ad Marketing" data-price="599.00"  onclick="validate_selection(this.id,this.value);"> Facebook Ad Marketing ($599.00)</label>
										<label><input type="checkbox" name="item_id[]" class="order_purchase_items" value="2" id="order_items_2" data-name="Solo Ad Banner Marketing" data-price="599.00"  onclick="validate_selection(this.id,this.value);"> Solo Ad Banner Marketing ($599.00)</label>
										<label><input type="checkbox" name="item_id[]" class="order_purchase_items" value="3" id="order_items_3" data-name="Phone Marketing" data-price="599.00"  onclick="validate_selection(this.id,this.value);"> Phone Marketing ($599.00)</label>
										<label><input type="checkbox" name="item_id[]" class="order_purchase_items" value="4" id="order_items_4" data-name="AI Text Marketing" data-price="599.00"  onclick="validate_selection(this.id,this.value);"> AI Text Marketing ($599.00)</label>
										<label><input type="checkbox" name="item_id[]" class="order_purchase_items" value="5" id="order_items_5" data-name="YouTube Marketing" data-price="599.00"  onclick="validate_selection(this.id,this.value);"> YouTube Marketing ($599.00)</label>
										<label><input type="checkbox" name="item_id[]" class="order_purchase_items" value="6" id="order_items_6" data-name="5 Elements of Marketing Complete Package" data-price="2499.00" checked onclick="validate_selection(this.id,this.value);"> 5 Elements of Marketing Complete Package ($2,499.00)</label>
									</div>
								</div>	

								<div class="form-group mt10neg">
									<label class="col-md-3 control-label"><span class="colordanger">*</span>Subscription</label>
									<div class="col-md-9">
										<input type="hidden" name="payment_type" value="one_time"> <!-- recurring -->
										<div class="alert alert-info recurring-alert mt10 font13">
											<strong>No Contract Service Plan: </strong> $33 Monthly<br>
											<span class="paypal-length-text displaynone">No end date</span>

											<p class="mt10 font12">
												By activiting your subscription, you authorize Bitcoin Labs to charge your credit card or debit card. You will NOT be charged for the first 7 days, after which you will be automatically charged $33 each billing cycle until your subscription is canceled.
											</p>

										</div>
									</div>
								</div>	
							
						
					</div>
					<div class="col-md-6">
						
						<h3 class="colorgray mb30">Your Information</h3>
						<div class="form-group">
							<label class="control-label col-md-3"><span class="colordanger">*</span>Name</label>
							<div class="col-md-9">
								<input type="text" name="name" class="form-control" placeholder="Name" value="" data-rule-required="true">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><span class="colordanger">*</span>Email</label>
							<div class="col-md-9">
								<input type="text" name="email" class="form-control" placeholder="Email" value="" data-rule-required="true" data-rule-email="true">
							</div>
						</div>

													<hr class="visible-xs visible-sm">
							<h3 class="colorgray mt40 mb30">Billing Address</h3>
							<div class="form-group">
								<label class="control-label col-md-3"><span class="colordanger">*</span>Address</label>
								<div class="col-md-9">
									<input type="text" name="address" class="form-control" placeholder="Address" value="" data-rule-required="true">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><span class="colordanger">*</span>City</label>
								<div class="col-md-9">
									<input type="text" name="city" class="form-control" placeholder="City" value="" data-rule-required="true">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><span class="colordanger">*</span>State/Zip</label>
								<div class="col-md-9">
									<div class="row">
										<div class="col-md-8 col-xs-8 pr5">
											<select name="state" class="form-control" data-rule-required="true">
												<option value="">-- Select State --</option>
																								<optgroup label="US States">
																										<option value="AL">Alabama</option>
																										<option value="AK">Alaska</option>
																										<option value="AZ">Arizona</option>
																										<option value="AR">Arkansas</option>
																										<option value="BVI">British Virgin Islands</option>
																										<option value="CA">California</option>
																										<option value="CO">Colorado</option>
																										<option value="CT">Connecticut</option>
																										<option value="DE">Delaware</option>
																										<option value="FL">Florida</option>
																										<option value="GA">Georgia</option>
																										<option value="GU">Guam</option>
																										<option value="HI">Hawaii</option>
																										<option value="ID">Idaho</option>
																										<option value="IL">Illinois</option>
																										<option value="IN">Indiana</option>
																										<option value="IA">Iowa</option>
																										<option value="KS">Kansas</option>
																										<option value="KY">Kentucky</option>
																										<option value="LA">Louisiana</option>
																										<option value="ME">Maine</option>
																										<option value="MP">Mariana Islands</option>
																										<option value="MPI">Mariana Islands (Pacific)</option>
																										<option value="MD">Maryland</option>
																										<option value="MA">Massachusetts</option>
																										<option value="MI">Michigan</option>
																										<option value="MN">Minnesota</option>
																										<option value="MS">Mississippi</option>
																										<option value="MO">Missouri</option>
																										<option value="MT">Montana</option>
																										<option value="NE">Nebraska</option>
																										<option value="NV">Nevada</option>
																										<option value="NH">New Hampshire</option>
																										<option value="NJ">New Jersey</option>
																										<option value="NM">New Mexico</option>
																										<option value="NY">New York</option>
																										<option value="NC">North Carolina</option>
																										<option value="ND">North Dakota</option>
																										<option value="OH">Ohio</option>
																										<option value="OK">Oklahoma</option>
																										<option value="OR">Oregon</option>
																										<option value="PA">Pennsylvania</option>
																										<option value="PR">Puerto Rico</option>
																										<option value="RI">Rhode Island</option>
																										<option value="SC">South Carolina</option>
																										<option value="SD">South Dakota</option>
																										<option value="TN">Tennessee</option>
																										<option value="TX">Texas</option>
																										<option value="UT">Utah</option>
																										<option value="VT">Vermont</option>
																										<option value="USVI">VI  U.S. Virgin Islands</option>
																										<option value="VA">Virginia</option>
																										<option value="WA">Washington</option>
																										<option value="DC">Washington, D.C.</option>
																										<option value="WV">West Virginia</option>
																										<option value="WI">Wisconsin</option>
																										<option value="WY">Wyoming</option>
																									</optgroup>
																								<optgroup label="Canadian Provinces">
																										<option value="AB">Alberta</option>
																										<option value="BC">British Columbia</option>
																										<option value="MB">Manitoba</option>
																										<option value="NB">New Brunswick</option>
																										<option value="NF">Newfoundland</option>
																										<option value="NT">Northwest Territories</option>
																										<option value="NS">Nova Scotia</option>
																										<option value="NVT">Nunavut</option>
																										<option value="ON">Ontario</option>
																										<option value="PE">Prince Edward Island</option>
																										<option value="QC">Quebec</option>
																										<option value="SK">Saskatchewan</option>
																										<option value="YK">Yukon</option>
																									</optgroup>
																								<optgroup label="Australian Provinces">
																										<option value="AU-NSW">New South Wales</option>
																										<option value="AU-QLD">Queensland</option>
																										<option value="AU-SA">South Australia</option>
																										<option value="AU-TAS">Tasmania</option>
																										<option value="AU-VIC">Victoria</option>
																										<option value="AU-WA">Western Australia</option>
																										<option value="AU-ACT">Australian Capital Territory</option>
																										<option value="AU-NT">Northern Territory</option>
																									</optgroup>
																								<option value="N/A">Other</option>
											</select>
										</div>
										<div class="col-md-4 col-xs-4 pl5">
											<input type="text" name="zip" class="form-control" placeholder="Zip" value="" data-rule-required="true">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group mb40">
								<label class="control-label col-md-3"><span class="colordanger">*</span>Country</label>
								<div class="col-md-9">
									<select name="country" class="form-control" data-rule-required="true">
										<option value="">-- Select Country --</option>
																				<option value="US" selected>United States</option>
																				<option value="CA" >Canada</option>
																				<option value="UK" >United Kingdom</option>
																				<option value="AU" >Australia</option>
																				<option value="AF" >Afghanistan</option>
																				<option value="AL" >Albania</option>
																				<option value="DZ" >Algeria</option>
																				<option value="AS" >American Samoa</option>
																				<option value="AD" >Andorra</option>
																				<option value="AO" >Angola</option>
																				<option value="AI" >Anguilla</option>
																				<option value="AQ" >Antarctica</option>
																				<option value="AG" >Antigua and Barbuda</option>
																				<option value="AR" >Argentina</option>
																				<option value="AM" >Armenia</option>
																				<option value="AW" >Aruba</option>
																				<option value="AT" >Austria</option>
																				<option value="AZ" >Azerbaijan</option>
																				<option value="BS" >Bahamas</option>
																				<option value="BH" >Bahrain</option>
																				<option value="BD" >Bangladesh</option>
																				<option value="BB" >Barbados</option>
																				<option value="BY" >Belarus</option>
																				<option value="BE" >Belgium</option>
																				<option value="BZ" >Belize</option>
																				<option value="BJ" >Benin</option>
																				<option value="BM" >Bermuda</option>
																				<option value="BT" >Bhutan</option>
																				<option value="BO" >Bolivia</option>
																				<option value="BA" >Bosnia and Herzegovina</option>
																				<option value="BW" >Botswana</option>
																				<option value="BR" >Brazil</option>
																				<option value="BN" >Brunei Darussalam</option>
																				<option value="BG" >Bulgaria</option>
																				<option value="BF" >Burkina Faso</option>
																				<option value="BI" >Burundi</option>
																				<option value="KH" >Cambodia</option>
																				<option value="CM" >Cameroon</option>
																				<option value="CV" >Cape Verde</option>
																				<option value="KY" >Cayman Islands</option>
																				<option value="CF" >Central African Republic</option>
																				<option value="TD" >Chad</option>
																				<option value="CL" >Chile</option>
																				<option value="CN" >China</option>
																				<option value="CX" >Christmas Island</option>
																				<option value="CC" >Cocos (Keeling) Islands</option>
																				<option value="CO" >Colombia</option>
																				<option value="KM" >Comoros</option>
																				<option value="CG" >Congo</option>
																				<option value="CD" >Congo, The Democratic Republic of the</option>
																				<option value="CK" >Cook Islands</option>
																				<option value="CR" >Costa Rica</option>
																				<option value="CI" >Cote D`Ivoire</option>
																				<option value="HR" >Croatia</option>
																				<option value="CY" >Cyprus</option>
																				<option value="CZ" >Czech Republic</option>
																				<option value="DK" >Denmark</option>
																				<option value="DJ" >Djibouti</option>
																				<option value="DM" >Dominica</option>
																				<option value="DO" >Dominican Republic</option>
																				<option value="EC" >Ecuador</option>
																				<option value="EG" >Egypt</option>
																				<option value="SV" >El Salvador</option>
																				<option value="GQ" >Equatorial Guinea</option>
																				<option value="ER" >Eritrea</option>
																				<option value="EE" >Estonia</option>
																				<option value="ET" >Ethiopia</option>
																				<option value="FK" >Falkland Islands (Malvinas)</option>
																				<option value="FO" >Faroe Islands</option>
																				<option value="FJ" >Fiji</option>
																				<option value="FI" >Finland</option>
																				<option value="FR" >France</option>
																				<option value="GF" >French Guiana</option>
																				<option value="PF" >French Polynesia</option>
																				<option value="GA" >Gabon</option>
																				<option value="GM" >Gambia</option>
																				<option value="GE" >Georgia</option>
																				<option value="DE" >Germany</option>
																				<option value="GH" >Ghana</option>
																				<option value="GI" >Gibraltar</option>
																				<option value="GR" >Greece</option>
																				<option value="GL" >Greenland</option>
																				<option value="GD" >Grenada</option>
																				<option value="GP" >Guadeloupe</option>
																				<option value="GU" >Guam</option>
																				<option value="GT" >Guatemala</option>
																				<option value="GN" >Guinea</option>
																				<option value="GW" >Guinea-Bissau</option>
																				<option value="GY" >Guyana</option>
																				<option value="HT" >Haiti</option>
																				<option value="HN" >Honduras</option>
																				<option value="HK" >Hong Kong</option>
																				<option value="HU" >Hungary</option>
																				<option value="IS" >Iceland</option>
																				<option value="IN" >India</option>
																				<option value="ID" >Indonesia</option>
																				<option value="IR" >Iran (Islamic Republic Of)</option>
																				<option value="IQ" >Iraq</option>
																				<option value="IE" >Ireland</option>
																				<option value="IL" >Israel</option>
																				<option value="IT" >Italy</option>
																				<option value="JM" >Jamaica</option>
																				<option value="JP" >Japan</option>
																				<option value="JO" >Jordan</option>
																				<option value="KZ" >Kazakhstan</option>
																				<option value="KE" >Kenya</option>
																				<option value="KI" >Kiribati</option>
																				<option value="KP" >Korea North</option>
																				<option value="KR" >Korea South</option>
																				<option value="KW" >Kuwait</option>
																				<option value="KG" >Kyrgyzstan</option>
																				<option value="LA" >Laos</option>
																				<option value="LV" >Latvia</option>
																				<option value="LB" >Lebanon</option>
																				<option value="LS" >Lesotho</option>
																				<option value="LR" >Liberia</option>
																				<option value="LI" >Liechtenstein</option>
																				<option value="LT" >Lithuania</option>
																				<option value="LU" >Luxembourg</option>
																				<option value="MO" >Macau</option>
																				<option value="MK" >Macedonia</option>
																				<option value="MG" >Madagascar</option>
																				<option value="MW" >Malawi</option>
																				<option value="MY" >Malaysia</option>
																				<option value="MV" >Maldives</option>
																				<option value="ML" >Mali</option>
																				<option value="MT" >Malta</option>
																				<option value="MH" >Marshall Islands</option>
																				<option value="MQ" >Martinique</option>
																				<option value="MR" >Mauritania</option>
																				<option value="MU" >Mauritius</option>
																				<option value="MX" >Mexico</option>
																				<option value="FM" >Micronesia</option>
																				<option value="MD" >Moldova</option>
																				<option value="MC" >Monaco</option>
																				<option value="MN" >Mongolia</option>
																				<option value="MS" >Montserrat</option>
																				<option value="MA" >Morocco</option>
																				<option value="MZ" >Mozambique</option>
																				<option value="NA" >Namibia</option>
																				<option value="NP" >Nepal</option>
																				<option value="NL" >Netherlands</option>
																				<option value="AN" >Netherlands Antilles</option>
																				<option value="NC" >New Caledonia</option>
																				<option value="NZ" >New Zealand</option>
																				<option value="NI" >Nicaragua</option>
																				<option value="NE" >Niger</option>
																				<option value="NG" >Nigeria</option>
																				<option value="NO" >Norway</option>
																				<option value="OM" >Oman</option>
																				<option value="PK" >Pakistan</option>
																				<option value="PW" >Palau</option>
																				<option value="PS" >Palestine Autonomous</option>
																				<option value="PA" >Panama</option>
																				<option value="PG" >Papua New Guinea</option>
																				<option value="PY" >Paraguay</option>
																				<option value="PE" >Peru</option>
																				<option value="PH" >Philippines</option>
																				<option value="PL" >Poland</option>
																				<option value="PT" >Portugal</option>
																				<option value="PR" >Puerto Rico</option>
																				<option value="QA" >Qatar</option>
																				<option value="RE" >Reunion</option>
																				<option value="RO" >Romania</option>
																				<option value="RU" >Russian Federation</option>
																				<option value="RW" >Rwanda</option>
																				<option value="VC" >Saint Vincent and the Grenadines</option>
																				<option value="MP" >Saipan</option>
																				<option value="SM" >San Marino</option>
																				<option value="SA" >Saudi Arabia</option>
																				<option value="SN" >Senegal</option>
																				<option value="SC" >Seychelles</option>
																				<option value="SL" >Sierra Leone</option>
																				<option value="SG" >Singapore</option>
																				<option value="SK" >Slovak Republic</option>
																				<option value="SI" >Slovenia</option>
																				<option value="SO" >Somalia</option>
																				<option value="ZA" >South Africa</option>
																				<option value="ES" >Spain</option>
																				<option value="LK" >Sri Lanka</option>
																				<option value="KN" >St. Kitts/Nevis</option>
																				<option value="LC" >St. Lucia</option>
																				<option value="SD" >Sudan</option>
																				<option value="SR" >Suriname</option>
																				<option value="SZ" >Swaziland</option>
																				<option value="SE" >Sweden</option>
																				<option value="CH" >Switzerland</option>
																				<option value="SY" >Syria</option>
																				<option value="TW" >Taiwan</option>
																				<option value="TI" >Tajikistan</option>
																				<option value="TZ" >Tanzania</option>
																				<option value="TH" >Thailand</option>
																				<option value="TG" >Togo</option>
																				<option value="TK" >Tokelau</option>
																				<option value="TO" >Tonga</option>
																				<option value="TT" >Trinidad and Tobago</option>
																				<option value="TN" >Tunisia</option>
																				<option value="TR" >Turkey</option>
																				<option value="TM" >Turkmenistan</option>
																				<option value="TC" >Turks and Caicos Islands</option>
																				<option value="TV" >Tuvalu</option>
																				<option value="UG" >Uganda</option>
																				<option value="UA" >Ukraine</option>
																				<option value="AE" >United Arab Emirates</option>
																				<option value="UY" >Uruguay</option>
																				<option value="UZ" >Uzbekistan</option>
																				<option value="VU" >Vanuatu</option>
																				<option value="VE" >Venezuela</option>
																				<option value="VN" >Viet Nam</option>
																				<option value="VG" >Virgin Islands (British)</option>
																				<option value="VI" >Virgin Islands (U.S.)</option>
																				<option value="WF" >Wallis and Futuna Islands</option>
																				<option value="YE" >Yemen</option>
																				<option value="YU" >Yugoslavia</option>
																				<option value="ZM" >Zambia</option>
																				<option value="ZW" >Zimbabwe</option>
																			</select>
								</div>
							</div>
												
						<hr class="visible-xs visible-sm">
						<h3 class="colorgray mb30">
							Payment Method
							<div class="floatright">
																	<img src="http://5elementsofmarketing.com/order/assets/images/credit-cards.jpg" class="">
															</div>
						</h3>

						<div class="creditcard-content">

							<div class="form-group">
								<label class="control-label col-md-3"><span class="colordanger">*</span>Name on Card</label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" data-stripe="name" name="cardholder_name" class="form-control" placeholder="Name on Card" value="" data-rule-required="true">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									</div> 
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><span class="colordanger">*</span>Card Number</label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" data-stripe="number" class="form-control card-number" placeholder="Card Number" value="" data-rule-required="true" data-rule-creditcard="true">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									</div> 
									<div class="card-type-image none"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><span class="colordanger">*</span>Expiration/CVC</label>
								<div class="col-md-9">
									<div class="row">
										<div class="col-md-4 col-xs-4 pr5">
											<select data-stripe="exp-month" class="form-control" data-rule-required="true">
							                    							                    <option value="1" >01</option>
							                    							                    <option value="2" >02</option>
							                    							                    <option value="3" >03</option>
							                    							                    <option value="4" >04</option>
							                    							                    <option value="5" >05</option>
							                    							                    <option value="6" >06</option>
							                    							                    <option value="7" >07</option>
							                    							                    <option value="8" >08</option>
							                    							                    <option value="9" >09</option>
							                    							                    <option value="10" >10</option>
							                    							                    <option value="11" selected="selected">11</option>
							                    							                    <option value="12" >12</option>
							                    							                </select> 	
										</div>
										<div class="col-md-4 col-xs-4 pl5 pr5">
											<select data-stripe="exp-year" class="form-control" data-rule-required="true">
							                    							                    <option value="2017">2017</option>
							                    							                    <option value="2018">2018</option>
							                    							                    <option value="2019">2019</option>
							                    							                    <option value="2020">2020</option>
							                    							                    <option value="2021">2021</option>
							                    							                    <option value="2022">2022</option>
							                    							                    <option value="2023">2023</option>
							                    							                    <option value="2024">2024</option>
							                    							                    <option value="2025">2025</option>
							                    							                    <option value="2026">2026</option>
							                    							                    <option value="2027">2027</option>
							                    							                </select>
										</div>
										<div class="col-md-4 col-xs-4 pl5">
											<div class="input-group">
												<input type="text" data-stripe="cvc" name="cvc" class="form-control" placeholder="CVC" value="" data-rule-required="true">
												<span class="input-group-addon"><i class="fa fa-lock"></i></span>
											</div> 
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row mt50">
							
							<div class="col-md-12 alignright">
								<div class="creditcard-content">

									<button type="submit" class="btn btn-lg btn-primary submit-button mb20" data-loading-text='<i class="fa fa-spinner fa-spin"></i> Submitting...' data-complete-text='<i class="fa fa-check"></i> Payment Complete!' >
										<span class="total ">Total: $<span></span> <small></small></span>
										<i class="fa fa-check"></i> Submit Payment
									</button>

								</div>
								<div class="paypal-content displaynone">
									<a href="#" class="btn btn-lg btn-primary submit-button paypal-button" data-loading-text='<i class="fa fa-spinner fa-spin"></i> Sending to PayPal...' >
										<span class="total ">Total: $<span></span> <small></small></span>
										Pay with Bitcoin <i class="fa fa-angle-double-right"></i>
									</a>
								</div>
							</div>
						</div>

					</div>
				</div>

	    	</form>
                    
                                                        <script type="text/javascript">
                                    validate_selection('order_items_'+'6','6'); 
                                </script>
                                                    

	    	

    	</div>