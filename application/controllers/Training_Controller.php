<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Training_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /* Load Marketing Model */
        #$this->load->model('Marketing_Model');

        /* Load User Model */ 
        #$this->load->model('User_Model');

        /* Load form helper */ 
        #$this->load->helper('form');

        /* Load form validation library */ 
        #$this->load->library('form_validation');

        /* Changing error delimiters Globally */
        #$this->form_validation->set_error_delimiters('', '');     	     

    }

    public function index($arParams = array())
    {
		if (is_user_login()) {
			$data['arLoginUser'] = $this->session->userdata('login_user');
			$data['szFavIcon'] = "fa-graduation-cap";
			$data['szPageName'] = "Training";
			$data['szMetaTagTitle'] = "Getting Started";

			$data = array_merge($data, $arParams);

			$this->load->view('layout/header', $data);
			$this->load->view('training/index', $data);
			$this->load->view('layout/footer');
			
		} else {
			
			redirect(base_url() . 'dashboard', 'refresh');
		}
    }

    public function gst($arParams = array())
    { 		
		if (is_user_login()) {
			$data['arLoginUser'] = $this->session->userdata('login_user');
			$data['szFavIcon'] = "fa-graduation-cap";
			$data['szPageName'] = "Training";
			$data['szMetaTagTitle'] = "Getting Started";

			$data = array_merge($data, $arParams);

			$this->load->view('layout/header', $data);
			$this->load->view('training/gst', $data);
			$this->load->view('layout/footer');
			
		} else {
			
			redirect(base_url() . 'dashboard', 'refresh');
		}
    }
}
?>