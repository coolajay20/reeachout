<div class="row text-center">
	<form method="post" action="<?php echo base_url();?>membership/subscription" id="profile-form" class="validate-form">	
		<?php if(!empty($arPlans)){ ?>   
		
			<?php foreach($arPlans as $plan) { ?> 
				<div class="col-sm-6">
				  <div class="panel panel-primary panel-alt">
				  	<div class="panel-heading">
				  	
				  		<div class="row">
				  			<div class="col-sm-3"></div>
							<div class="col-sm-6">
								<h2 class="panel-title"><?php echo $plan['szPlanName']?></h2>
								<p></p>
								<div class="input-group center-block">
									<div class="section row mb5" style="margin-top:10px;"> 
										<select id="idSubscriptionType" class="form-control input-lg" name="idSubscriptionType" onchange="change_plan_type(this.value,'<?php echo $plan['szPlanID']?>');">
											<option value="MONTHLY">Monthly</option>
											<option value="YEARLY">Yearly</option>
											<option value="LIFE_TIME">Life Time</option>
										</select> 
									</div> 
								</div>
							</div>
							<div class="col-sm-3"></div>
					  	</div>
					  	
					</div>
					
					<div class="panel-body">
						<?php
						$szPlanFeatures = '<hr>';
						if(!empty($plan['arFeatures']))
						{
							foreach($plan['arFeatures'] as $feature)
							{
								$szPlanFeatures .= "<h5>{$feature['szFeatureName']}</h5><hr>\n\t\t\t\t\t\t";
							}
						}
						?>
						<div class="plan-pricing common-class-<?php echo $plan['szPlanID']?>" id="<?php echo "type-MONTHLY-".$plan['szPlanID']; ?>" style="height:auto;">
							<h2>$<?php echo str_replace(".00","",$plan['fPlanAmount'])?></h2>
							<h5>Billing: Monthly</h5>
							<h5>Validity: 1 Month</h5>
							<?php echo $szPlanFeatures;?>
							<button type="button" id="btn-renew-subscription-<?php echo $plan['szPlanID']?>" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..." data-plan-id="<?php echo $plan['szPlanID']?>" data-url="<?php echo base_url();?>membership/subscribe" data-action="subscribe" data-button-label="Subscribe Now" data-publishable-key="<?php echo STRIPE_PUBLISHABLE_KEY;?>" data-logo="<?php echo base_url() ?>assets/images/stripe_logo.jpg" data-email="<?php echo (isset($arLoginUser['email'])  && trim($arLoginUser['email']) != '' ? trim($arLoginUser['email']) : '');?>" data-desc=" Monthly Subscription for <?php echo $plan['szPlanName'];?> Plan" data-plan-name="<?php echo $plan['szPlanName'];?>" data-subscription-type="1" data-monthly="true" data-old-subscription="" class="btn btn-primary btn-lg btn-configure-subscription-setup">Subscribe Now</button>
						</div>  
						<div class="plan-pricing common-class-<?php echo $plan['szPlanID']?>" id="<?php echo "type-YEARLY-".$plan['szPlanID']; ?>" style="height:auto;display:none;">
							<h2>$<?php echo str_replace(".00","",$plan['fPlanAmountYearly'])?></h2>
							<h5>Billing: Yearly</h5> 
							<h5>Validity: 1 Year</h5>
							<?php echo $szPlanFeatures;?>
							<button type="button" id="btn-renew-subscription-<?php echo $plan['szPlanID']?>" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..." data-plan-id="<?php echo $plan['szPlanID']?>" data-amount="<?php echo $plan['fPlanAmountYearly']; ?>" data-url="<?php echo base_url();?>membership/subscribe" data-action="subscribe" data-button-label="Buy Now" data-publishable-key="<?php echo STRIPE_PUBLISHABLE_KEY;?>" data-logo="<?php echo base_url();?>assets/images/logo.png" data-email="<?php echo (isset($arLoginUser['email'])  && trim($arLoginUser['email']) != '' ? trim($arLoginUser['email']) : '');?>" data-desc="<?php echo $arLoginUser['name'];?> subscription for <?php echo $plan['szPlanName'];?>" data-plan-name="<?php echo $plan['szPlanName'];?>" data-subscription-type="2" data-monthly="false" data-old-subscription="" class="btn btn-primary btn-lg btn-configure-subscription-setup">Buy Now</button>
						</div> 
						<div class="plan-pricing common-class-<?php echo $plan['szPlanID']?>" id="<?php echo "type-LIFE_TIME-".$plan['szPlanID']; ?>" style="height:auto;display:none;">
							<h2>$<?php echo str_replace(".00","",$plan['fPlanAmountLifeTime'])?></h2>
							<h5>Billing: One time</h5> 
							<h5>Validity: Life Time</h5>
							<?php echo $szPlanFeatures;?>
							<button type="button" id="btn-renew-subscription-<?php echo $plan['szPlanID']?>" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..." data-plan-id="<?php echo $plan['szPlanID']?>" data-amount="<?php echo $plan['fPlanAmountLifeTime']; ?>" data-url="<?php echo base_url();?>membership/subscribe" data-action="subscribe" data-button-label="Buy Now" data-publishable-key="<?php echo STRIPE_PUBLISHABLE_KEY;?>" data-logo="<?php echo base_url();?>assets/images/logo.png" data-email="<?php echo (isset($arLoginUser['email'])  && trim($arLoginUser['email']) != '' ? trim($arLoginUser['email']) : '');?>" data-desc="<?php echo $arLoginUser['name'];?> subscription for <?php echo $plan['szPlanName'];?>" data-plan-name="<?php echo $plan['szPlanName'];?>" data-subscription-type="3" data-monthly="false" data-old-subscription="" class="btn btn-primary btn-lg btn-configure-subscription-setup">Buy Now</button>
						</div>

					</div>
				  </div><!-- panel -->
				</div>
			<?php } ?>  			
		<?php } ?>
	</form>
</div>
