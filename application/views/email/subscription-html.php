<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Subscription Confirmation from szSiteName!</title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
Dear szFirstName,
<br /><br />
Thank you for doing business with us. Your subscription has been activated successfully!<br/><br/>
Here are the details: <br/><br/>
szSubscriptionDetails
<br />
<br />
Best regards, <br />
~ Team @ szSiteName
</td>
</tr>
</table>
</div>
</body>
</html>