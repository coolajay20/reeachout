<?php 
class Custom404Ctrl extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct(); 
    } 

    public function index()
    {
		$data['content'] = 'error_404';
		$this->output->set_status_header('404'); 
		$this->load->view('404', $data);
    } 
} 
?>