<?php if(isset($szSuccessMessage) && $szSuccessMessage != ''){?>
<div class="alert alert-success p5"><?php echo $szSuccessMessage;?></div>
<?php } if(isset($szErrorMessage) && $szErrorMessage != ''){?>
<div class="alert alert-danger p5"><?php echo $szErrorMessage;?></div>
<?php }?>

<div class="panel panel-visible" id="spy3">
    <div class="panel-heading">
        <p class="text-left">
            <a href="<?php echo base_url();?>campaigns/add" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New Campaign</a>
        </p>
    </div>
    <div class="panel-body pn">
        <table class="table table-responsive table-striped table-hover dt-dynamic" id="customer_data_listing" cellspacing="0" width="100%" data-url="<?php echo base_url();?>campaigns" data-func="Get_Campaigns">
            <thead>
                <tr>             	
                    <th data-sort="szName">Name</th>
                    <th data-sort="iContacts">Contact(s)</th> 
                    <th data-sort="isActive">Is Active?</th>
                    <th data-sort="dtAddedOn">Date Added</th>
                    <th data-class="action-links">Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>