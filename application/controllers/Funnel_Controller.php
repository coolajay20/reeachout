<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Funnel_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /* Load Funnel Model */
        $this->load->model('Funnel_Model');

        /* Load User Model */ 
        $this->load->model('User_Model');

        /* Load form helper */ 
        #$this->load->helper('form');

        /* Load form validation library */ 
        #$this->load->library('form_validation');

        /* Changing error delimiters Globally */
        #$this->form_validation->set_error_delimiters('', '');     	
		
		/* Load tracking library */ 
		$this->load->library('Tracking');

    }

    public function index()
    {		
			$host = str_replace(".", "_", $_SERVER['HTTP_HOST']);
			$this->cp($host);
    }

    public function cp($arParams = array())
    { 
			$data['sponsor'] = $this->session->userdata('sponsor');
            $this->load->view('funnels/'.$arParams, $data);
    }
	
    public function setRef($arParams = array())
    { 
			$userdata = $this->User_Model->validateUser($arParams);
		
			$idCountry = $this->User_Model->getCountries($userdata["idCountry"]);
			$setsponsor = array(
				'isactive' => $userdata["isActive"],
				'fname' => $userdata["szFirstName"],
				'lname' => $userdata["szLastName"],
				'phone' => $userdata["szPhone"],
				'email' => $userdata['szEmail'],
				'avatar' => $userdata["szAvatarImage"],
				'username' => $userdata["szUserName"],
				'usilink' => $userdata["szUSIUserName"],
				'city' => $userdata["szCity"],
				'country' => $idCountry[0]["szName"],
				'fb' => $userdata["szFacebookLink"],
				'google' => $userdata["szGoogleLink"],
				'twitter' => $userdata["szTwitterID"],
				'linkedin' => $userdata["szLinkedInID"],
				'dribble' => $userdata["szDribbbleID"],
				'skype' => $userdata["szSkypeID"],
				'id' => $userdata["id"]
			);

			#$this->load->view('funnels/fbshare');
			$this->session->set_userdata('sponsor', $setsponsor);
			redirect('http://'.$_SERVER['HTTP_HOST'], 'refresh');
    }
	
}
?>