<form method="post" action="<?php echo base_url();?>features/<?php echo ($idFeatures > 0 ? "edit/{$arFeaturesDetails['szUniqueKey']}" : "add");?>" id="profile-form" class="validate-form">	
	<section id="content" class="table-layout">
		<div class="tray tray-center">		     	
			<div class="mw1000 center-block">    
				<?php if($isDetailsSaved){?>
				<div class="alert alert-success p5">
					<i class="fa fa-check pr10"></i>
					<strong> Congratulation!</strong>
					Feature details has been saved successfully.
				</div>
				<?php } if($isFormError){?>
				<div class="alert alert-danger p5">
					<i class="fa fa-times pr10"></i>
					<strong> Invalid Data!</strong>
					Please fix the errors below and try again.
				</div>
				<?php }?>

							
					<div class="col-sm-12">
					  <div class="panel panel-primary panel-alt">
						<div class="panel-heading">
						  <p>Features are displayed on membership plans.</p>
						</div>
						<div class="panel-body">

							<div class="form-group">
							  <label for="szFeatureName" class="col-sm-3 control-label">Name:<span class="text-danger">*</span></label>
							  <?php $szFeatureNameError = form_error('arFeatures[szFeatureName]');?>
							  <div class="col-sm-6">
								<div class="input-group mb15">
									<span class="input-group-addon"><i class="fa fa-star"></i></span>
									<input type="text" name="arFeatures[szFeatureName]" data-slug="szFeatureName" id="szFeatureName" class="form-control" placeholder="Feature Name" value="<?php echo set_input_value('arFeatures[szFeatureName]',(isset($arFeaturesDetails['szFeatureName']) ? $arFeaturesDetails['szFeatureName'] : ''));?>">
								</div>
								<?php if(!empty($szFeatureNameError)){?><em class="text-danger" for="szFeatureName"><?php echo $szFeatureNameError;?></em><?php }?>
							  </div>
							</div>
						              
							<div class="form-group">
							  <label for="isActive" class="col-sm-3 control-label">Is Active?</label>
							  <div class="col-sm-5">
								<select name="arFeatures[isActive]" id="isActive" class="form-control input-lg" placeholder="Suspended">
									<option value="0" <?php echo (set_input_value('arFeatures[isActive]', (isset($arFeaturesDetails['isActive']) ? $arFeaturesDetails['isActive'] : 1)) == 0 ? 'selected' : '');?>>No</option>
									<option value="1" <?php echo (set_input_value('arFeatures[isActive]', (isset($arFeaturesDetails['isActive']) ? $arFeaturesDetails['isActive'] : 1)) == 1 ? 'selected' : '');?>>Yes</option>
								</select>	
							  </div>
							</div>
							
							<div class="panel-footer">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<a href="<?php echo base_url();?>features" class="btn btn-default"> Cancel </a>
										<button type="submit" class="btn btn-primary"> <?php echo ($idFeatures > 0 ? 'Save' : 'Add');?> </button>
										<input type="hidden" name="p_func" value="Save Features Details">	
										<input type="hidden" name="arFeatures[id]" value="<?php echo $idFeatures;?>">
									</div>
								</div>
							</div>
							
						</div> <!-- panel body -->
					</div><!-- panel -->
				</div>
				
			</div>
		</div>
	</section>
</form>