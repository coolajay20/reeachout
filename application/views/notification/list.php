<?php if(isset($szSuccessMessage) && $szSuccessMessage != ''){?>
<div class="alert alert-success p5"><?php echo $szSuccessMessage;?></div>
<?php } if(isset($szErrorMessage) && $szErrorMessage != ''){?>
<div class="alert alert-danger p5"><?php echo $szErrorMessage;?></div>
<?php }?>

<div class="panel panel-visible" id="spy3">
	<div class="panel-heading">
		<p class="text-left">
			<a href="<?php echo base_url();?>notifications/add" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New Notification</a>
		</p>
	</div>
	<div class="panel-body pn">
		<table class="table table-responsive table-striped table-hover dt-dynamic" id="customer_data_listing" cellspacing="0" width="100%" data-url="<?php echo base_url();?>notifications/all" data-func="Get_Notifications">
			<thead>
				<tr>             	
                                    <th data-sort="n.szTitle">Title</th>
                                    <th data-sort="iUserCount">Users</th>
                                    <th data-sort="n.dtAddedOn">Date Added</th>
                                    <th>Action</th>
				</tr>
			</thead>
		</table>
	</div>
</div>