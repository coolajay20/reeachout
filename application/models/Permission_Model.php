<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission_Model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getAllPermissions()
    {
        $this->db->from('tbl_permissions');
        $this->db->order_by('szPermissionName', 'ASC');
        $this->db->where(array('isDeleted'=>0));

        $query = $this->db->get();
        return ($query->num_rows() > 0 ? $query->result_array() : array());
    }
	
    function getPermissionByUniqueKey($szUniqueKey)
    {
        $arDetails = array();
        $szUniqueKey = trim($szUniqueKey);
        if($szUniqueKey != '')
        {
            $query = $this->db->get_where('tbl_permissions', array('szUniqueKey'=>$szUniqueKey));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    }
	
    function getPermissionByID($idPermission)
    {
        $arDetails = array();
        $idPermission = (int)$idPermission;
        if($idPermission > 0)
        {
            $query = $this->db->get_where('tbl_permissions', array('id'=>$idPermission));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    }

    function deletePermission($arPermission, $iDeletedBy)
    {
        $idPermission = (int)$arPermission['id'];
        $this->db->where(array('id'=>$idPermission));
        $this->db->update('tbl_permissions', array('isDeleted'=>1, 'iDeletedBy'=>$iDeletedBy, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        if($this->db->affected_rows())
        {
             // add user activity
            addUserActivity($idPermission, 'tbl_permissions', "Permission {$arPermission['szDisplayName']} deleted.");
            
            return true;
        }
        return false;
    }

    function savePermissionDetails($data, $idLoginUser)
    {		
        $data = sanitize_array_values($data);
        $idPermission = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);

        if($idPermission > 0)
        {
            // get old details
            $arOldData = $this->getPermissionByID($idPermission);

            $data['iUpdatedBy'] = $idLoginUser;
            $data['dtUpdatedOn'] = date('Y-m-d H:i:s');
            $this->db->where(array('id'=>$idPermission));
            $this->db->update('tbl_permissions', $data);

            // add user activity
            addUserActivity($idPermission, 'tbl_permissions', "Permission {$data['szDisplayName']} updated.", $data, $arOldData);
        }
        else
        {
            $data['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_permissions');
            $data['iAddedBy'] = $idLoginUser;
            $data['dtAddedOn'] = date('Y-m-d H:i:s');
            $query = $this->db->insert('tbl_permissions', $data);
            $idPermission = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);

            // add user activity
            addUserActivity($idPermission, 'tbl_permissions', "New permission {$data['szDisplayName']} added.", $data);
        }
        return $idPermission;
    }
}
?>