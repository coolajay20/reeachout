<!-- Start: Topbar -->
<section id="content_wrapper">	
    <?php require_once APPPATH . 'views/breadcrumb.php';?> 
    <!-- Begin: Content -->
    <form method="post" action="<?php echo base_url();?>tags/<?php echo ($idTags > 0 ? "edit/{$arTagDetails['szUniqueKey']}" : "add");?>" id="profile-form" class="validate-form">	
        <section id="content" class="table-layout">
            <div class="tray tray-center">		     	
                <div class="mw1000 center-block">    
                    <?php if($isDetailsSaved){?>
                    <div class="alert alert-success p5">
                        <i class="fa fa-check pr10"></i>
                        <strong> Congratulation!</strong>
                        Tag has been saved successfully.
                    </div>
                    <?php } if($isFormError){?>
                    <div class="alert alert-danger p5">
                        <i class="fa fa-times pr10"></i>
                        <strong> Invalid Data!</strong>
                        Please fix the errors below and try again.
                    </div>
                    <?php }?>

                    <div class="panel panel-primary panel-border top mb35">
                            <div class="panel-heading">
                                <span class="panel-title">
                                    <i class="fa fa-bookmark-o"></i>
                                    Tag Details
                                </span>
                            </div>
                            <div class="panel-body bg-light" style="padding-bottom:0;">
                                <div class="admin-form">
                                    <div class="section row">
                                        <label for="szTagName" class="field-label col-md-2 text-right-not-md">Name:<span class="text-danger">*</span></label>
                                        <?php $szTagNameError = form_error('arTag[szTagName]');?>
                                        <div class="col-md-10">
                                            <label for="szTagName" class="field prepend-icon<?php if(!empty($szTagNameError)){?> state-error<?php }?>">
                                                <input type="text" name="arTag[szTagName]" data-toggle="slug" data-slug="szPlanID" id="szTagName" class="gui-input required" placeholder="Tag Name" value="<?php echo set_input_value('arTag[szTagName]',(isset($arTagDetails['szTagName']) ? $arTagDetails['szTagName'] : ''));?>">
                                                <label for="szTagName" class="field-icon">
                                                    <i class="fa fa-star"></i>
                                                </label>
                                            </label>
                                            <?php if(!empty($szTagNameError)){?><em class="state-error" for="szTagName"><?php echo $szTagNameError;?></em><?php }?>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div class="panel-footer text-center">
                                <div class="admin-form">
                                    <a href="<?php echo base_url();?>tags" class="button btn-default"> Cancel </a>
                                    <button type="submit" class="button btn-primary"> <?php echo ($idTags > 0 ? 'Save' : 'Add');?> </button>
                                    <input type="hidden" name="p_func" value="Save Tag Details">	
                                    <input type="hidden" name="arTag[id]" value="<?php echo $idTags;?>"> 
                                </div>			                    	
                            </div>
                        </div>
                    </div>
                </div> 
            </section>
        </form>
    </section>