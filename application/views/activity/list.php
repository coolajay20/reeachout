	<section id="content_wrapper">
            <!-- Start: Topbar -->
            <header id="topbar" class="alt">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="<?php echo base_url();?>dashboard">Dashboard</a>
                        </li>
                        <li class="crumb-trail">Activity Log</li>
                    </ol>
                </div>        	
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">
                <?php if(isset($szSuccessMessage) && $szSuccessMessage != ''){?>
                <div class="alert alert-success p5"><?php echo $szSuccessMessage;?></div>
                <?php } if(isset($szErrorMessage) && $szErrorMessage != ''){?>
                <div class="alert alert-danger p5"><?php echo $szErrorMessage;?></div>
                <?php }?>
                <div class="clearfix mb10"> 
                    <?php if($idUser > 0){?>
                    <h1 class="mtn mb20 pull-left"><?php echo "{$arUserDetails['szFirstName']} {$arUserDetails['szLastName']}";?></h1>
                    <span class="pl10 pt15 pull-left fs15">activity log.</span>
                    <?php } else {?>
                    <h1 class="mtn mb20 pull-left">Activity Log</h1>
                    <span class="pl10 pt15 pull-left fs15">activity log for all users.</span>
                    <?php }?>
                </div>

                <div class="panel panel-visible" id="spy3">
                    <div class="panel-body pn">
                        <table class="table table-responsive table-striped table-hover" id="example" cellspacing="0" width="100%" data-url="<?php echo base_url();?>activity<?php echo ($idUser > 0 ? "/user/{$arUserDetails['szUniqueKey']}" : '');?>" data-func="Get Activity">                    
                            <thead>
                                <tr>
                                    <th data-sort="a.dtCreatedOn" class="hidden" data-class="hidden">Log Time</th>
                                    <th data-sort="u.szFirstName">User</th>             	
                                    <th data-sort="a.szIPAddress">IP Address</th>
                                    <th data-sort="a.szActivity">Message</th>
                                    <th data-sort="a.dtCreatedOn">Log Time</th>
                                    <th>More</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>                 	
                                    <th data-sort="a.dtCreatedOn" class="hidden" data-class="hidden">Log Time</th>            	
                                    <th data-sort="u.szFirsName">User</th>   
                                    <th data-sort="a.szIPAddress">IP Address</th>
                                    <th data-sort="a.szActivity">Message</th>
                                    <th data-sort="a.dtCreatedOn">Log Time</th>
                                    <th>More</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </section>
        </section>