<?php

namespace Stripe;

class Authorize
{
	public static function getAuthorizeURL($state)
    {
    	return "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=" . STRIPE_CLIENT_ID . "&scope=read_write&state={$state}";
    }
    
    public static function getAccessToken($code)
    {
    	try{
		  	$post = "client_secret=" . STRIPE_ACCESS_KEY . "&grant_type=authorization_code&code={$code}";
		  	
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL, 'https://connect.stripe.com/oauth/token');
			curl_setopt($ch,CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
			$result = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			$errno = curl_errno($ch);
			
			curl_close($ch);
			
			if($curl_info['http_code'] == 200)
				return array("ACK"=>"SUCCESS", "RESPONSE"=>json_decode($result, true));
			else
				return array("ACK"=>"ERROR", "RESPONSE"=>json_decode($result, true));
			 
		} catch (Exception $e){
			return array("ACK"=>"ERROR", "RESPONSE"=>array('error_description'=>$e->getMessage()));
		}
    }
    
    public static function createCharge($params=array(), $options=array())
    {
    	try{
		  	$post = http_build_query($params);
		  	
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL, 'https://api.stripe.com/v1/charges');
			curl_setopt($ch,CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			if(!empty($options))
				curl_setopt($ch, CURLOPT_HTTPHEADER, $options);
			
			$result = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			$errno = curl_errno($ch);
			print_r($result);die;
			
			curl_close($ch);
			
			if($curl_info['http_code'] == 200)
				return array("ACK"=>"SUCCESS", "RESPONSE"=>json_decode($result, true));
			else
				return array("ACK"=>"ERROR", "RESPONSE"=>json_decode($result, true));
			 
		} catch (Exception $e){
			return array("ACK"=>"ERROR", "RESPONSE"=>array('error_description'=>$e->getMessage()));
		}
    }
}