   <?php /*
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <div class="logo"><a href="/"><img src="<?php echo base_url();?>assets/images/logo_beta.png" alt=""></a></div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#overview">Overview</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#strategy">Strategy</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#features">Features</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#pricing">Pricing</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/login/">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/signup/">Sign Up</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

   
    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/H8WuRINimqur8ud/the-growth-of-the-virtual-currency-bitcoin-bitcoin-growth-chart-candle-chart-on-the-online-forex-monitor_syyjyb1z__F0000.png')">
            <div class="carousel-caption d-none d-md-block">
              <h3>Build Your Business</h3>
              <p>Using The Most Powerful, Done-For-You, System Online!</p>
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('http://placehold.it/1900x1080')">
            <div class="carousel-caption d-none d-md-block">
              <h3>Second Slide</h3>
              <p>This is a description for the second slide.</p>
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('http://placehold.it/1900x1080')">
            <div class="carousel-caption d-none d-md-block">
              <h3>Third Slide</h3>
              <p>This is a description for the third slide.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>
	*/ ?>
    
	<!-- Promo Video
	====================================================== -->
	<section id="overview">
		<div class="mask-dark">
			<div class="container text-center text-white pt-4 pb-5">
				<div class="text-center"><small><a style="color: white;" href="/login/">Member Login</a></small></div>
				<img src="<?php echo base_url();?>assets/images/logo_beta-highres.png" />
				<h3 class="text-gold">How would you like to <strong>increase revenue</strong> and <strong>boost productivity?</strong></h3>
				<h1>All-in-One Marketing Solution for Clients &amp; Partners</h1>
				<div class='embed-container'><iframe src='https://player.vimeo.com/video/242656033?autoplay=0' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
			</div>
		</div> 
	</section>
	<!-- =================================================
	Promo Video End -->
	
	<!-- Divider Section ========================= -->
	<section id="divider" class="bg-gold py-2"></section>

	<!-- About Content Area  Start 
	====================================================== -->
	<section class="bg-grey" id="strategy">
		<!-- Start:About Awesome content area-->
		<div class="container">
			<div class="row py-5">
				<!-- Start:Left area-->
				<div class="col-lg-7">
					<div class="main-title left-align">
						<h3><span>Tools &amp; Effective Strategies</span></h5>
						<h1><strong>Automate Your Business</strong></h1>
					</div>
					<div class="text-box">
						<p>The secret to achieving massive results in any business is taking massive action and being persistant. With the right tools and an effective duplication strategy, you greatly improve your chances of building a powerful team.</p>

						<h5><strong>Do You Have an Effective Duplication Strategy?</strong></h5>

						<p>Tools "bridge the gap" between experience and inexperience, giving the new person leverage in order to have a much better chance of achieving sucess.</p>
						
						<div style="text-align: center; margin: 50px 0;">
							<img src="<?php echo base_url();?>assets/images/marketing/usi_marketing_sites_header.png" alt="<?php echo WEBSITE_NAME_LONG ?>">
						</div>
						
						<blockquote><i>"Our strategy is centered around being efficient to help you achieve maximum productivity with minimum wasted effort."</i></blockquote>
					</div>
				</div>
				<!-- End:Left area-->

				<!-- Start:Right area-->
				<div class="col-lg-5" style="text-align: center;">
					<div>
						<img src="<?php echo base_url();?>assets/images/new_app.png" alt="<?php echo WEBSITE_NAME ?>"> 
					</div>                	
				</div>
				<!-- End:Right area-->

			</div>
		</div>
		<!-- End:Awesome Structure content area-->
	</section>
	<!-- =================================================
	About Content Area  End -->


	<!-- Features sec Start 
	====================================================== -->
	<section class="" id="features">
		<!-- Start:Services area-->
		<div class="container py-5">
			<div class="row text-center">
				
				<div class="col-lg-12 pb-4">
					<h2><strong>Build Your Business To It's Full Potential!</strong></h2>
					<p>All of your funnels, tools, leads, traffic plus everything you could want to build your business all in one beautiful, easy to use, backoffice.</p> 
				</div>				

				<div class="col-lg-4 pb-4">
					<div class="features-box">
						<div class="features-icon"><img src="<?php echo base_url();?>assets/images/build-for-you-funnels.png" /></div>
						<div class="features-text">
							<h4>Built-For-You “Funnels”</h5>
							<p>We've designed these marketing funnels help identify those who qualify to become potential clients and business partners.</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 pb-4">
					<div class="features-box">
						<div class="features-icon"><img src="<?php echo base_url();?>assets/images/capture-leads.png" /></div>
						<div class="features-text">
							<h4>Capture &amp; Manage Leads</h5>
							<p>Manage and Follow-up Easily. Our contact manager captures all information from any forms filled out on your pages.</p>
						</div>
					</div>
				</div> 

				<div class="col-lg-4 pb-4">
					<div class="features-box">
						<div class="features-icon"><img src="<?php echo base_url();?>assets/images/tools-to-drive-traffic.png" /></div>
						<div class="features-text">
							<h4>Plug-n-Play Ready</h5>
							<p>Virtually everything provided for you within the system is ready to use the moment you sign in and setup your account.</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 pb-4">
					<div class="features-box">
						<div class="features-icon"><img src="<?php echo base_url();?>assets/images/nurture-prospects.png" /></div>
						<div class="features-text">
							<h4>Nurture Your Prospects</h5>
							<p>The moment your contact submits their information via one of the lead capture pages, they are added to an email campaign which automatically nutures them to taking action.</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 pb-4">
					<div class="features-box">
						<div class="features-icon"><img src="<?php echo base_url();?>assets/images/tracking-scoring.png" /></div>
						<div class="features-text">
							<h4>Tracking &amp; Analytics</h5>
							<p>Gain detailed intelligence on each contact, including page views, repeat visits, length of visit, email opens, videos watched, scores, files downloaded, and more.</p>
						</div>
					</div>
				</div> 	

				<div class="col-lg-4 pb-4">
					<div class="features-box">
						<div class="features-icon"><img src="<?php echo base_url();?>assets/images/instant-alerts.png" /></div>
						<div class="features-text">
							<h4>Instant Notifications</h5>
							<p>You automatically receive an email alert that notifies you in real-time. No waiting.</p>
						</div>
					</div>
				</div> 

			</div>
		</div>
		<!-- End:Services area -->
	</section>    
	<!-- =================================================
	Services sec End -->


	<!-- Section Divider Start 
	====================================================== -->
	<section id="pricing" class="bg-grey">
		<div class="container text-center py-5">
			<div class="row">
				<div class="col-lg-12 pb-5">
					<h2><strong>Cancel Anytime, No Monthly Commitment</strong></h2>
					<p>All Plans Include a Personalized Blog, Marketing Funnels, Capture Pages, Powerful Email Campaigns, Training &amp; 24/7 Online Support.</p>
				</div>

				<div class="col-xs-12 col-md-6">
					<div class="panel panel-primary bg-white pt-2">
						<div class="panel-heading">
							<h3 class="panel-title">
								Basic</h3>
						</div>
						<div class="panel-body">
							<div class="the-price">
								<h1>
									$21<span class="subscript">/mo</span></h1>
								<small>7 Day FREE Trial</small>
							</div>
							<table class="table">
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Basic Lead Capture Pages &amp; Funnels</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Email Autoresponder Campaigns</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Contact Management System</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Analytics &amp; Tracking</td></tr>
								<tr><td><i class="fa fa-times" aria-hidden="true"></i> Lead Scoring Technology</td></tr>
								<tr><td><i class="fa fa-times" aria-hidden="true"></i> Replicated Blogging Platform</td></tr>
								<tr><td><i class="fa fa-times" aria-hidden="true"></i> Facebook Pixel Integration</td></tr>
								<tr><td><i class="fa fa-times" aria-hidden="true"></i> Google Retargeting Integration</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> 24/7 Online Support</td></tr>
							</table>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="panel panel-success bg-white pt-2">
						<div class="cnrflash">
							<div class="cnrflash-inner">
								<span class="cnrflash-label">MOST
									<br>
									POPULAR</span>
							</div>
						</div>
						<div class="panel-heading">
							<h3 class="panel-title">
								Pro</h3>
						</div>
						<div class="panel-body">
							<div class="the-price">
								<h1>
									$33<span class="subscript">/mo</span></h1>
								<small>7 Day FREE Trial</small>
							</div>
							<table class="table">
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Premium Capture Pages &amp; Funnels</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Email Autoresponder Campaigns</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Contact Management System</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Analytics &amp; Tracking</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Lead Scoring Technology</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Replicated Blogging Platform</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Facebook Pixel Integration</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> Google Retargeting Integration</td></tr>
								<tr><td><i class="fa fa-check" aria-hidden="true"></i> 24/7 Online Support</td></tr>
							</table>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- Section Divider End 
    ====================================================== --> 
    
    
	<!-- Section FAQ Start 
	====================================================== -->
	<section id="faq" class="">
		<div class="container text-center pt-5">
			<div class="row">
				<div class="col-lg-12 pb-5">
					<h2><strong>Frequently Asked Questions</strong></h2>
				</div>
				
				<div class="col-lg-6 pb-5">
					<h4><i class="fa fa-question-circle"></i> Why are there 2 membership options?</h4>
					<p>We have a wide variety of members. Some want to use this as a side hobby while others want to run a full on enterprise level business. The more you use the system, the higher our server bill goes each month.</p>
				</div>

				<div class="col-lg-6 pb-5">
					<h4><i class="fa fa-question-circle"></i> Do I need any experience?</h4>
					<p>Having previous marketing experience is always great, however it is not required. We have several recorded video trainings to help you in case you are brand new. Anyone can use this system if you follow instructions.</p>
				</div>
				
				<div class="col-lg-6 pb-5">
					<h4><i class="fa fa-question-circle"></i> Will this system really work for me?</h4>
					<p>Yes. Our system serves as a tool to provide leverage, ultimately making it much easier to build your business. We'll explain how to use it and show you real-world examples on the inside of the members area.</p>
				</div>
				
				<div class="col-lg-6 pb-5">
					<h4><i class="fa fa-question-circle"></i> Can I get a refund if i don't like it?</h4>
					<p>We provide you with a 7 day trial the moment you activate your membership. If it doesn't work for you, simply cancel within the trial period and you won't be charged. If you were charged after the trial because you forgot to cancel and didn't use the system, simply send us an email requesting a refund and we'll be glad to assist.</p>
				</div>
			</div>
		</div>
	</section>    
	<!-- End: Section area -->

    
    <!-- Section CTA Start-->
    <section id="cta" class="sticky">
		<div class="mask-xdark">
		  <div class="container pt-2 text-white text-center">
		  	<div class="row">
				<div class="col-lg-10 pb-0">
					<p>The moment you sign up, you’ll have access to an innovative marketing platform that enables you to generate leads through professionally designed capture pages, trigger email campaigns automatically and measure your prospect’s engagement level.</p>
				</div>
				<div class="col-lg-2 pt-1 pb-2">
					<a class="btn btn-cta" href="/signup">Get Started <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
				</div>
			</div>
		  </div>
		</div>
    </section>
    <!-- Secion CTA End 
    ====================================================== --> 