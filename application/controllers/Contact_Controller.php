<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Contact_Controller extends CI_Controller {



    function __construct()

    {

        parent::__construct();



        /* Load Contact Model */

        $this->load->model('Contact_Model');
        $this->load->model('Email_Model');



        /* Load form helper */ 

        $this->load->helper('form');



        /* Load form validation library */ 

        $this->load->library('form_validation');



        /* Changing error delimiters Globally */

        $this->form_validation->set_error_delimiters('', '');     	     

    }

    

    public function all()

    {

        $this->index();

    }



    public function index($arParams = array())

    { 

        if(!is_user_login())

        {

            redirect(base_url() . 'login', 'refresh');

        }

        else if(!hasPermission('manage.contacts'))

        {

            redirect(base_url() . 'dashboard', 'refresh');

        }

        else if(trim($this->input->get('p_func')) == 'Get_Contacts')
        {
            $arLoginUser = $this->session->userdata('login_user');

            list($iLimit, $iOffset, $iDrawPage, $szKeyword, $arFilters, $szSortField, $szSortOrder) = parseDTRequest($this);

            $iTotalRecords = $this->Contact_Model->getContacts($arLoginUser['id'], $arFilters, $szKeyword, true);

            $arUsers = $this->Contact_Model->getContacts($arLoginUser['id'], $arFilters, $szKeyword, false, $iLimit, $iOffset, $szSortField, $szSortOrder); 

            $response = array("draw"=>$iDrawPage, "recordsTotal"=>$iTotalRecords, "recordsFiltered"=>$iTotalRecords, "data"=>array()); 

            if(!empty($arUsers))
            {
                $i = 0;

                foreach($arUsers as $contact)
                {					

                    $szActionLinks = '<td class="table-action">';
                    $szActionLinks .= "<a href='".base_url()."contacts/edit/".$contact['szUniqueKey']."' data-toggle='tooltip' title='Edit Contact'><i class='fa fa-pencil'></i></a>";
                    $szActionLinks .= "<a href='".base_url()."contacts/delete/".$contact['szUniqueKey']."' data-toggle='tooltip' title='Delete Contact'><i class='fa fa-trash-o'></i></a>";
                    $szActionLinks .= "<a href='javascript:void(0);' class='text-info' onclick=manageContactTags('{$contact['szUniqueKey']}'); data-toggle='tooltip' title='Manage Tags'><i class='fa fa-tags'></i></a></li>";
                    $szActionLinks .= '</td>';

                    $response['data'][$i][0] = "{$contact['szFirstName']} {$contact['szLastName']}";
                    $response['data'][$i][1] = $contact['szEmail'];
                    $response['data'][$i][2] = $this->Contact_Model->getContactTypeById($contact['idType']);
                    $response['data'][$i][3] = convert_date($contact['dtAddedOn'],2);
                    $response['data'][$i][4] = $szActionLinks;

                    $i++;				
                }
            }

            echo json_encode($response,true);
        }
        else
        {

			$data['szFavIcon'] = "fa-users";
            $data['szPageName'] = "Contacts";
            $data['szMetaTagTitle'] = "All Contacts";		

            $data['isLoadDataTable'] = true;
            $data['isLoadAdminForm'] = true;
            $data['isLoadTagsTypeAhead'] = true;
            $data['arLoginUser'] = $this->session->userdata('login_user');

            $data = array_merge($data, $arParams);
                        
            // check contact view
            $szActivityAction = trim($this->input->get('view-contact'));
            if($szActivityAction == 'all')
            {
                $this->db->where('idUser', $data['arLoginUser']['id'])->update('tbl_contacts', array('isNew'=>0));                
            }

            $this->load->view('layout/header', $data);
            $this->load->view('contacts/list');
            $this->load->view('layout/footer');
        }
    }

    function edit($szUserKey='')
    {
        $this->add($szUserKey);
    }

    function add($szUserKey='')
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }

        else if(!hasPermission('manage.contacts'))
        {
            redirect(base_url().'dashboard', 'refresh');
        }

        else
        {
            /* Load Contact Model */
            $this->load->model('User_Model'); 

            // set/initialize varibales
            $data['idContact'] = 0;

            $data['isFormError'] = false;
            $data['arContactDetails'] = array();
            $data['isDetailsSaved'] = false;
            $data['arContactCampaigns'] = array();
            $szUserKey = trim($szUserKey);		
            $data['arLoginUser'] = $this->session->userdata('login_user');
            
            // load campaign model
            $this->load->model('Campaign_Model');
            
            if($szUserKey != '')
            {
                $arUserDetails = $this->Contact_Model->getContactByUniqueKey($szUserKey);
                if(!empty($arUserDetails))
                {
                    $data['idContact'] = $arUserDetails['id'];
                    $data['arContactDetails'] = $arUserDetails;
                    $data['arContactCampaigns'] = $this->Campaign_Model->getAllCampaignsForContact($data['idContact']);
                }
            }

            if(($data['idContact'] > 0 && $data['arContactDetails']['idUser'] == $data['arLoginUser']['id']) || ($data['idContact'] == 0))
            {                                
                if($this->input->post('p_func') == 'Save Contact Details')
                {
                    $this->form_validation->set_rules('arContact[szFirstName]', 'First name', 'trim|required');
                    $this->form_validation->set_rules('arContact[szLastName]', 'Last name', 'trim|required');				
                    $this->form_validation->set_rules('arContact[szEmail]', 'Email address', "trim|required|valid_email|is_unique[tbl_contacts:szEmail:id:{$data['idContact']}]");         

                    if ($this->form_validation->run() == true)
                    { 
                        $arUserDetails = $this->input->post('arContact', true);
                        $arUserDetails['idUser'] = $data['arLoginUser']['id'];
                        $bAddNewContact = false;
                        if($data['idContact'] > 0)
                        {
                            $arUserDetails['id'] = $data['idContact'];
                        }
                        else
                        {
                            $bAddNewContact = true;
                        }

                        if($data['idContact'] = $this->Contact_Model->saveUserDetails($arUserDetails, $data['arLoginUser']['id']))
                        {
                            $data['isDetailsSaved'] = true;
                            $data['arContactDetails'] = $this->Contact_Model->getContactByID($data['idContact']);
                            
                            // save the campaigns
                            $arCampaigns = $this->input->post('arCampaigns', true);
                            if(empty($arCampaigns)) $arCampaigns = array();
                            $arOldCampaigns = $data['arContactCampaigns'] = $this->Campaign_Model->getAllCampaignsForContact($data['idContact']);
                            
                            // add the new campaigns
                            if(!empty($arCampaigns))
                            {
                                foreach($arCampaigns as $idCampaign)
                                {
                                    if(!in_array($idCampaign, $arOldCampaigns))
                                    {
                                        $this->Campaign_Model->addContactToCampaign($data['idContact'], $idCampaign);
                                    }
                                }
                            }
                            
                            // remove the campaigns
                            if(!empty($arOldCampaigns))
                            {
                                foreach($arOldCampaigns as $idCampaign)
                                {
                                    if(!in_array($idCampaign, $arCampaigns))
                                    {
                                        $this->db->where(array('idContact'=>$data['idContact'], 'idCampaign'=>$idCampaign))->update('tbl_campaign_delivery', array('isDeleted'=>1, 'dtDeletedOn'=>date('Y-m-d H:i:s'), 'iDeletedBy'=>$data['arLoginUser']['id']));
                                    }
                                }
                                $data['arContactCampaigns'] = $this->Campaign_Model->getAllCampaignsForContact($data['idContact']);
                            }
                            
                            if($bAddNewContact)
                            {
                                $arUserDetails = $this->User_Model->getUserByID($data['arLoginUser']['id']);
                                
                                //Sending confirmation email to customer
                                $arEmailData = array(); 
                                $arEmailData['szFirstName'] = $arUserDetails['szFirstName'];
                                $arEmailData['szLastName'] = $arUserDetails['szLastName']; 
                                $arEmailData['szEmail'] = $arUserDetails['szEmail'];  
                                $arEmailData['szContactFirstName'] = $data['arContactDetails']['szFirstName'];
                                $arEmailData['szContactLastName'] = $data['arContactDetails']['szLastName']; 
                                $arEmailData['szContactEmail'] = $data['arContactDetails']['szEmail']; 
                                $arEmailData['szContactPhone'] = $data['arContactDetails']['szPersonalPhone'];  
                                $arEmailData['szSiteName'] = WEBSITE_NAME; 
                                $arEmailData['szLoginPageUrl'] = base_url().'login';
 
                                $this->Email_Model->sendSystemEmails($arEmailData,'NEW_SUBSCRIBER_ADDED');
                            }
                        }
                    }
                    else
                    {
                        $data['isFormError'] = true;
                    }
                }            

                $data['szPageName'] = "Contacts";
                $data['szMetaTagTitle'] = ($data['idContact'] > 0 ? "Edit Contact Details" : "Add New Contact");	

                #$data['szMetaTagTitle'] = ($data['idContact'] > 0 ? "Edit Contact Details" : "Add New Contact");	

                $data['isLoadBootBox'] = true;

                $data['isLoadAdminForm'] = true;

                $data['isLoadValidationScript'] = true;

                

                $data['arTypes'] = $this->Contact_Model->getAllContactTypes();

                $data['arCountries'] = $this->User_Model->getCountries(); 
                
                // load all active campaigns
                $data['arCampaigns'] = $this->Campaign_Model->getAllActiveCampaigns();
                
                // check contact view
                $szActivityAction = trim($this->input->get('view-contact'));
                if((int)$szActivityAction == 1 && $data['idContact'] > 0)
                {
                    $this->db->where('idUser', $data['arLoginUser']['id'])->where('szUniqueKey', $data['arContactDetails']['szUniqueKey'])->update('tbl_contacts', array('isNew'=>0));                
                }

                $this->load->view('layout/header', $data);

                $this->load->view('contacts/add_edit');

                $this->load->view('layout/footer');

            }

            else

            {

                redirect(base_url() . 'dashboard');

            }

        }

    }

    

    function delete($szUserKey='')

    {  

        if(!is_user_login())

        {

            redirect(base_url().'login', 'refresh');

        }

        else if(!hasPermission('manage.contacts'))

        {

            redirect(base_url().'dashboard', 'refresh');

        }

        else

        {

            $data = array();

            $szUserKey = trim($szUserKey);

            if($szUserKey != '')

            {

                $arUserDetails = $this->Contact_Model->getContactByUniqueKey($szUserKey);

                if(!empty($arUserDetails))

                {

                    if(1)

                    {

                        if((int)$arUserDetails['isDeleted'] == 0)

                        {

                            // get login user details

                            $arLoginUser = $this->session->userdata('login_user');

                            $idUser = (int)$arUserDetails['id'];

                            

                            // delete the record

                            if($this->Contact_Model->deleteContact($arUserDetails, $arLoginUser['id']))

                            {

                                $data['szSuccessMessage'] = "Contact {$arUserDetails['szFirstName']} {$arUserDetails['szLastName']} has been deleted successfully.";                                

                            }

                            else

                            {

                                $data['szErrorMessage'] = "Problem while deleting user {$arUserDetails['szFirstName']} {$arUserDetails['szLastName']}.";

                            }

                        }

                        else

                        {

                            $data['szErrorMessage'] = "Contact {$arUserDetails['szFirstName']} {$arUserDetails['szLastName']} is already deleted.";

                        }

                    }

                    else

                    {

                        $data['szErrorMessage'] = "Contact {$arUserDetails['szFirstName']} {$arUserDetails['szLastName']} is not removable.";

                    }

                }

                else

                {

                    $data['szErrorMessage'] = 'Contact record not exists.';

                }

            }

            else

            {

                $data['szErrorMessage'] = 'Invalid contact delete request.';

            }

            $this->index($data);

        }

    }

    

    function details($szUserKey='')

    {

        if(!is_user_login())

        {

            redirect(base_url().'login', 'refresh');

        }

        else

        {

            $data = array();

            $szUserKey = trim($szUserKey);

            if($szUserKey != '')

            {

                $arUserDetails = $this->Contact_Model->getUserByUniqueKey($szUserKey);

                if(!empty($arUserDetails))

                {

                    $data['arUserDetails'] = $arUserDetails;

                    $data['arUserDetails']['dtLastLogin'] = $this->Contact_Model->getUserLastLogin($arUserDetails['id']);

                    

                    $arCountry = array();

                    if((int)$arUserDetails['idCountry'] > 0)

                    {

                        $arCountry = $this->Contact_Model->getCountries($arUserDetails['idCountry']);                        

                    }

                    $data['arUserDetails']['szCountry'] = (!empty($arCountry) ? $arCountry[0]['szName'] : '');

                    

                    // get activities

                    $data['arUserDetails']['activities'] = $this->Contact_Model->getUserActivity($arUserDetails['id']);                    

                    

                    $data['szPageName'] = "Users";

                    $data['szMetaTagTitle'] = "User Details";		

                    $data['isLoadDataTable'] = true;

                    $data['arLoginUser'] = $this->session->userdata('login_user');



                    $this->load->view('layout/header', $data);

                    $this->load->view('users/details');

                    $this->load->view('layout/footer');

                }

                else

                {

                    $data['szErrorMessage'] = 'User record not exists.';

                }

            }

            else

            {

                $data['szErrorMessage'] = 'Invalid user delete request.';

            }

        }   

    } 

    

    function upload()

    {

        if(!is_user_login())

        {

            redirect(base_url().'login', 'refresh');

        }

        else if(!hasPermission('manage.contacts'))

        {

            redirect(base_url().'dashboard', 'refresh');

        }

        else

        { 

            $this->load->model('Tags_Model');

            $arLoginUser = $this->session->userdata('login_user');  

            // set/initialize varibales

            $data['idContact'] = 0;

            $data['isFormError'] = false;

            $data['arContactDetails'] = array();

            $data['isDetailsSaved'] = false; 	

            $data['arLoginUser'] = $arLoginUser; 

            if($this->input->post('p_func') == 'UPLOAD_CONTACTS')

            {  

                $arUploadContacts = $this->input->post('arUploadContact', true);

                

                $szFileName = date('dmY')."-USI-".md5(mt_rand(0, 9999999)."_".time());

                $config['upload_path'] = DIR_CONTACT_UPLOADS;

                $config['allowed_types'] = 'csv';

                $config['max_size'] = '2096'; //2 MB

                $config['file_name'] = $szFileName; 

                $this->load->library('upload', $config); 

                if ($this->upload->do_upload('szUploadFileName'))

                {  

                    $arUploadedFileInfo = $this->upload->data();

                    $szCSVFilePath = $arUploadedFileInfo['full_path'];

                    $szCSVFileName = $arUploadedFileInfo['file_name'];

                    

                    if(file_exists($szCSVFilePath))

                    { 

                        /*

                        * Adding file upload logs just to have historical data so in future any point of time we can see who and when uploaded the csv file.

                        */

                        $arFileUploadLogs = array();

                        $arFileUploadLogs['szFileName'] = $szCSVFileName;

                        $arFileUploadLogs['iUploadedBy'] = $arLoginUser['id'];

                        $arFileUploadLogs['szDeveloperNotes'] = "Contact bulk upload";

                        $arFileUploadLogs['szIPAddress'] = __REMOTE_ADDR__;

                        $arFileUploadLogs['szBrowser'] = __HTTP_REFERER__;

                        $arFileUploadLogs['szReferrer'] = __HTTP_USER_AGENT__;

                        $this->Contact_Model->saveUploadedFileLogs($arFileUploadLogs);

                                

                        

                        $this->load->library('Csvimport'); 

                        $arCsvData = $this->csvimport->get_array($szCSVFilePath); 

                        

                        /* Load Contact Model */

                        $this->load->model('User_Model'); 

                        

                        $idContactType = $arUploadContacts['idType'];

                        if(!empty($arCsvData))

                        {

                            $ctr = 1;

                            $iSuccess = 0;

                            $iError = 0;

                            $arMessage = array();

                            foreach ($arCsvData as $arCsvDatas)

                            {

                                /*

                                * We need to add 2 validations here,

                                * a. IF E-mail is valid

                                * b. If E-mail is unique

                                * 

                                * If we don't find valid country name in csv file then we use NULL as default value for the country.

                                */

                                $idPersonalCountry = $this->User_Model->getCountryByName($arCsvDatas['Country']);

                                $idBusinessCountry = $this->User_Model->getCountryByName($arCsvDatas['Business Country']); 

                                  

                                $arContactData = array();

                                //Basic details

                                $arContactData['szFirstName'] = $arCsvDatas['First Name'];

                                $arContactData['szLastName'] = $arCsvDatas['Last Name'];

                                $arContactData['szEmail'] = $arCsvDatas['Email Address'];

                                $arContactData['idType'] = $idContactType;

                                

                                //Personal details

                                $arContactData['szPersonalAddress'] = $arCsvDatas['Address'];

                                $arContactData['szPersonalAddress2'] = $arCsvDatas['Address 2'];

                                $arContactData['szPersonalPostcode'] = $arCsvDatas['Zip'];

                                $arContactData['szPersonalCity'] = $arCsvDatas['City'];

                                $arContactData['szPersonalState'] = $arCsvDatas['State'];

                                $arContactData['idPersonalCountry'] = $idPersonalCountry; 

                                $arContactData['szPersonalPhone'] = $arCsvDatas['Home Phone'];

                                $arContactData['szPersonalMobile'] = $arCsvDatas['Mobile Phone'];

                                

                                //Business details

                                $arContactData['szBusinessCompanyName'] = $arCsvDatas['Company'];

                                $arContactData['szBusinessTitle'] = $arCsvDatas['Title'];

                                $arContactData['szBusinessAddress'] = $arCsvDatas['Business Address'];

                                $arContactData['szBusinessAddress2'] = $arCsvDatas['Business Address 2'];

                                $arContactData['szBusinessPostcode'] = $arCsvDatas['Business Zip'];

                                $arContactData['szBusinessCity'] = $arCsvDatas['Business City'];

                                $arContactData['szBusinessState'] = $arCsvDatas['Business State'];

                                $arContactData['idBusinessCountry'] = $idBusinessCountry; 

                                $arContactData['szBusinessPhone'] = $arCsvDatas['Work Phone'];

                                $arContactData['idUser'] = $arLoginUser['id'];

                                

                                if($this->Contact_Model->saveUserDetails($arContactData, $arLoginUser['id']))

                                {

                                    $arMessage[] = "<li class='green_text'>Row: ".$ctr." - E-mail: ".$arContactData['szEmail']." is successfully added as contact.</li>";

                                    $iSuccess++;

                                } 

                                else

                                {

                                    $arMessage[] = "<li class='red_text'>Row: ".$ctr." - E-mail: ".$arContactData['szEmail']." is not successfully added.</li>";

                                    $iError++;

                                }

                                $ctr++;

                            }

                            

                            $data['isDetailsSaved'] = true;

                            $data['arMessage'] = $arMessage; 

                            $data['iTotalRowsUploaded'] = $ctr-1; 

                            $data['iTotalSuccessRowsUploaded'] = $iSuccess; 

                            $data['iTotalErrorRowsUploaded'] = $iError; 

                        }

                        else

                        {

                            $data['szFileUploadError'] = "Csv file must have atleast 1 record.";

                        }

                    }

                    else

                    {

                        $data['szFileUploadError'] = "Something went wrong! Please try again later.";

                    } 

                }

                else

                {

                    $data['szFileUploadError'] = $this->upload->display_errors(); 

                } 

            }                            

                            
			$data['szFavIcon'] = "fa-upload";
            $data['szPageName'] = "Contacts";
            $data['szMetaTagTitle'] = "Import Contacts";	
            $data['isLoadBootBox'] = true;

            $data['isLoadAdminForm'] = true;

            $data['isLoadValidationScript'] = true;

            $data['isLoadUploadScript'] = true;

            $data['isLoadTagsScript'] = true;



            $data['arTypes'] = $this->Contact_Model->getAllContactTypes(); 

            $data['arContactTags'] = $this->Tags_Model->getAllTags($arLoginUser['id']);



            $this->load->view('layout/header', $data);

            $this->load->view('contacts/upload');

            $this->load->view('layout/footer'); 

        }

    }

    

    function tags($szUniqueKey='')
    {
        if(!is_user_login())
        {
            echo "ERROR||||Your last login session has been expired. <a href='".base_url()."login'>Click here to login again</a>.";
        }
        else if(!hasPermission('manage.contacts'))
        {
            echo "ERROR||||You don't have permission to perform this action.";
        }
        else
        {
            $arContactDetails = $this->Contact_Model->getContactByUniqueKey($szUniqueKey);
            if(!empty($arContactDetails))
            {
                $this->load->model('Tags_Model');

                $arLoginUser = $this->session->userdata('login_user'); 

                $data['szContactKey'] = $szUniqueKey;

                $data['arContactTags'] = $this->Tags_Model->getAllTags($arLoginUser['id']);

                $data['arSelectedTagIDs'] = array();
                $data['arSelectedTags'] = $this->Contact_Model->getContactTags($arContactDetails['id']);
                if(!empty($data['arSelectedTags']))
                {
                    foreach($data['arSelectedTags'] as $tag)
                    {
                        $data['arSelectedTagIDs'][] = $tag['idTag'];
                    }
                }

                if(trim($this->input->post('p_func',true)) == 'Add New Tag')
                {
                    $szTagName = trim($this->input->post('p_tag',true));
                    if($szTagName != '')
                    {
                        $arTag['idUser'] = $arLoginUser['id'];
                        $arTag['szTagName'] = $szTagName;
                        $arTag['isActive'] = 1;
                        if($idTag = $this->Tags_Model->saveTagDetails($arTag, $arLoginUser['id']))
                        {
                            echo "SUCCESS||||{$idTag}";
                        }
                        else
                        {
                            echo 'ERROR||||Something went wrong please try again after sometime.';
                        }
                    }
                    else
                    {
                        echo 'ERROR||||Tag name is required.';
                    }
                }

                else if(trim($this->input->post('p_func',true)) == 'Add Contact Tags')

                {

                    $arNewTags = array();

                    $arOldTags = array();

                    $arTags = $this->input->post('arContact');

                    

                    $arPreviousTag = array();      	

                    if(!empty($data['arSelectedTags']))

                    {

	        	foreach($data['arSelectedTags'] as $tag)

	        	{

                            $arPreviousTag[] = $tag['idTag'];

	        	}

                    }



                    if(!empty($arTags))

                    {

                        if(!empty($arTags['arTags']))

                        {

                            $arOldTags=$arTags['arTags'];

                        }

                        

                        if(!empty($arTags['arNewTags']))

                        {

                            foreach($arTags['arNewTags'] as $tag)

                            {
                                if($tag != '')
                                {
                                    $arTagDetails = $this->Tags_Model->isTagExists($arLoginUser['id'], $tag);

                                    if(!empty($arTagDetails))

                                    {

                                        if((int)$arTagDetails[0]['iActive'] == 1)

                                        {

                                            $idTag = $this->Contact_Model->isContactTagAlreadyExists($arContactDetails['id'], $arTagDetails[0]['id']);

                                            if(empty($idTag))

                                            {

                                                $arOldTags[] = $this->Contact_Model->addContactTag($arContactDetails['id'], $idTag);

                                            }

                                            else

                                            {

                                                $arOldTags[] = $idTag;

                                            }

                                        }

                                    }

                                    else

                                    {

                                        // save tag to miscellaneous category

                                        $arTag['idUser'] = $arLoginUser['id'];

                                        $arTag['szTagName'] = $tag;

                                        $arTag['isActive'] = 1;

                                        $arOldTags[] = $this->Tags_Model->saveTagDetails($arTag, $arLoginUser['id']);

                                    }
                                }
                            }

                        }



                        $arDeleteTag=array();

                        $arAddTag=array();

                        if(!empty($arPreviousTag))

                        {

                            $arDeleteTag=array_diff($arPreviousTag,$arOldTags);

                            $arAddTag=array_diff($arOldTags,$arPreviousTag);

                        }

                        else

                        {

                            $arAddTag=$arOldTags;

                        }



                        if(!empty($arDeleteTag))

                        {

                            foreach($arDeleteTag as $key=>$idTag)

                            {

                                $this->Contact_Model->deleteContactTag($arContactDetails['id'], $idTag);

                            }

                        }

                        

                        if(!empty($arAddTag))

                        {

                            foreach($arAddTag as $key=>$idTag)

                            {

                                $this->Contact_Model->addContactTag($arContactDetails['id'], $idTag);			

                            }

                        }

                    }

                    else

                    {

                        $this->Contact_Model->deleteContactTag($arContactDetails['id']);

                    }

                    echo "SUCCESS||||Done";

                }

                else

                {

                    echo "SUCCESS||||";

                    $this->load->view('contacts/tags',$data);

                }

            }

            else

            {

                echo "ERROR||||Contact not found.";

            }

        }

    }

}

?>