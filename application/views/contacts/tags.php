<!-- Image editing feature -->
<div id="contactTagsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form id="frmManageContactTags" action="" method="post">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><?php echo lang('contacts_manage_contact_tags'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="admin-form">
                        <div class="section row">
                            <div class="col-xs-9">  
                                <select name="arContact[arTags][0]" id="contact_tag" class="form-control gui-input wp-100">
                                    <option value=""><?php echo lang('contacts_select'); ?>...</option>
                                    <?php $iTotal = count($arContactTags); if(!empty($arContactTags)){ foreach($arContactTags as $key=>$tag){?> 
                                    <option value="<?php echo $tag['id'];?>" <?php echo (in_array($tag['id'], $arSelectedTagIDs) ? 'selected' : '');?>><?php echo $tag['szTagName'];?></option>
                                    <?php }}?>
                                </select>                                
                            </div> 
                            <div class="col-xs-3 text-right"> 
                                <button type="button" class="btn btn-primary wp-100" onclick="showAddNewTag(this);"><?php echo lang('common_add_new'); ?></button>
                            </div> 
                        </div>
                        <div class="section row new_tag mt10 hidden">
                            <div class="col-xs-9">                              
                                <input type="text" class="form-control wp-100" name="arContact[arNewTags][0]" id="new_tag" placeholder="<?php echo lang('common_add_new'); ?>">
                            </div> 
                            <div class="col-xs-3 text-right"> 
                                <button type="button" class="btn btn-info wp-100" data-loading-text="<i class='fa fa-spin fa-spinner'></i> Adding..." onclick="saveAddNewTag(this, '<?php echo $szContactKey;?>');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo lang('common_add'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                            </div> 
                        </div>
                        <?php if(isset($szNewTagsError) && $szNewTagsError != ''){?><em class="state-error mb20" for="szNewTags"><?php echo $szNewTagsError;?></em><?php }?> 
                        <input type="hidden" id="szSelectedTags" value="<?php echo $szSelectedTags;?>"> 
                        <input type="hidden" id="txSelectedTags" value="<?php echo $txSelectedTags;?>"> 
                        <input type="hidden" id="szPostVariable" value="arContact"> 
                    </div>
                </div> 
                <div class="modal-footer"> 
                  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('common_close'); ?></button> 
                   <button type="button" class="btn btn-primary" onclick="saveContactTags(this, '<?php echo $szContactKey;?>');" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Saving..."> <?php echo lang('common_update'); ?> </button>

                </div> 
                <input type="hidden" name="p_func" value="Add Contact Tags"> 
            </form> 
        </div> 
    </div> 
</div> 