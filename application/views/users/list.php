<?php if(isset($szSuccessMessage) && $szSuccessMessage != ''){?>
<div class="alert alert-success p5"><?php echo $szSuccessMessage;?></div>
<?php } if(isset($szErrorMessage) && $szErrorMessage != ''){?>
<div class="alert alert-danger p5"><?php echo $szErrorMessage;?></div>
<?php }?>

<div class="col-md-12">
	<div class="table-responsive">
		<p class="text-left">
			<a href="<?php echo base_url();?>users/add" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New User</a>
		</p>
		<table class="table mb30 table-striped dt-dynamic" id="customer_data_listing" data-url="<?php echo base_url();?>users" data-func="Get_Users">
			<thead>
				<tr>
					<th data-sort="szUserName">Username</th>             	
					<th data-sort="szFirstName">Full Name</th>
					<th data-sort="szEmail">Email</th>
					<th data-sort="dtCreatedOn">Registration Date</th>
					<th data-sort="szPlanName">Subscription</th>
					<th data-sort="szSubStatus">Membership</th>
					<th data-class="table-action"></th>
				</tr>
			</thead>
		</table>
	</div><!-- table-responsive -->
</div>