<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked' => set_value('remember'),
);

/* Fetching error message */
$szEmailError = form_error('szEmail');
$szPasswordError = form_error('szPassword'); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Done-For-You Marketing System to put you on the fast track to generating your own leads and nurturing them into potential clients and business partners.">
  <meta name="author" content="">
  <link rel="shortcut icon" href="images/favicon.png" type="image/png">

  <title>Login | <?php echo WEBSITE_NAME; ?></title>

  <link href="<?php echo base_url();?>assets/backend/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body>

<section>
    <div class="signinpanel">
       
        <div class="row">
            <div class="col-md-12">
                <div class="signin-info text-center">
                    <div class="logopanel">
                        <a href="/"><img src="<?php echo base_url();?>assets/images/logo_beta.png" alt=""></a>
                    </div><!-- logopanel -->

                    <div class="mb20"></div>
                    <?php echo lang('signin_not_member');?> <a href="/signup/"><?php echo lang('signin_sign_up');?></a>
                </div><!-- signin0-info -->
               
                <form method="post" action="<?php echo base_url();?>login">
                    <h4 class="nomargin"><?php echo lang('signin_title');?></h4>
                    <p class="mt5 mb20"><?php echo lang('signin_access_account');?></p>
                	
                    <!-- Email Address / Username Input --> 
                    <input name="szEmail" id="szEmail" type="text" class="form-control uname" placeholder="<?php echo lang('signin_username');?>" value="<?php echo set_input_value('szEmail');?>" />
                    <?php if(!empty($szEmailError)){?><em class="has-error" for="szEmail"><?php echo $szEmailError;?></em><?php }?>
                    
                    <!-- Password Input --> 
                    <input name="szPassword" id="szPassword" type="password" class="form-control pword" placeholder="<?php echo lang('signin_password');?>" />
                    <a href="/forgot-password"><small><?php echo lang('signin_forgot_password');?></small></a>
                    <?php if(!empty($szPasswordError)){?><em class="has-error" for="szPassword"><?php echo $szPasswordError;?></em><?php }?>
                    
                    <button id="btn-login" class="btn btn-success btn-block"><?php echo lang('signin_submit_btn');?></button>
                    
                    <?php /*
                    <a id="btn-fblogin" href="/auth_oa2/session/facebook" class="btn btn-primary btn-block"><i class="fa fa-facebook"></i>&nbsp;&nbsp;<?php echo lang('signin_login_fb');?></a>
					*/ ?>
                    
                    <input type="hidden" name="p_func" value="Log In">
                </form>
                
                <?php #lang_selection(); ?>
                
            </div><!-- col-sm-5 -->
        </div><!-- row -->
        
        <div class="signup-footer">
            <div class="text-center">
                &copy; 2017 All Rights Reserved. Bitcoin Labs Digital Marketing System
            </div>
            <!--
            <div class="pull-right">
                Created By: <a href="#">X</a>
            </div>
            -->
        </div>
        
    </div><!-- signin -->
</section>

</body>
</html>
