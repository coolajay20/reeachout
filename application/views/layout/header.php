<?php

if($arLoginUser['avatar'] != '') {

	$avatar_src = BASE_USERS_UPLOAD_URL . $arLoginUser['avatar'];

} else {

	$avatar_src = base_url() . 'assets/images/profile.png';

}

$globalStats = get_global_stats();

?>

<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <meta name="description" content="">

  <meta name="author" content="">

  <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.png" type="image/png">



  <title><?php if(isset($szMetaTagTitle)) echo $szMetaTagTitle; else echo WEBSITE_NAME_LONG;?></title>


  <link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/style.default.css?v=<?php echo time(); ?>">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/style.inverse.css">

  <link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/bootstrap-wysihtml5.css" />

  <link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/jquery.datatables.css">
  
	<style>
		.headerbar .topnav > ul > li > a:hover,
		.headerbar .topnav > ul > li > a:focus,
		.headerbar .topnav > ul > li.open > a,
		.headerbar .topnav > ul > li.active > a {
			box-shadow: none;
			color: #f4aa3b;
			background: none;
		}	
	</style>

  <?php if(isset($isLoadCroppieScript) && $isLoadCroppieScript == true){?>

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/vendor/plugins/croppie/croppie.css">

  <?php } if(isset($isLoadDTPickerScript) && $isLoadDTPickerScript == true){?>

  <script src="<?php echo base_url();?>theme/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css"></script>

  <?php }?>



  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!--[if lt IE 9]>

  <script src="js/html5shiv.js"></script>

  <script src="js/respond.min.js"></script>

  <![endif]-->

</head>



<body class="horizontal-menu">

<!-- Preloader -->

<div id="preloader">

    <div id="status"><img src="<?php echo base_url();?>/assets/backend/images/loaders/custom/loader.gif"></div>

</div>



<section>

  

  <div class="leftpanel">

    

    <div class="logopanel">

       	<a href="<?php echo base_url() ?>dashboard">

        <img src="<?php echo base_url() ?>assets/images/logo_beta.png" title="Bitcoin Labs | Digital Marketing System" class="center-block img-responsive" />

		</a>

    </div><!-- logopanel -->

        

    <div class="leftpanelinner">

        

        <!-- This is only visible to small devices -->

        <div class="visible-xs hidden-sm hidden-md hidden-lg">   

            <div class="media userlogged">

                <img alt="" src="<?php echo $avatar_src ?>" class="media-object">

                <div class="media-body">

                    <h4><?php echo $arLoginUser['name'] ?></h4>

                    <span>@<?php echo $arLoginUser['user_name'];?>&nbsp;&nbsp;<span class="text-<?php echo ($arLoginUser['confirmed'] == 1 ? 'success' : 'danger');?>"><i class="fa fa-<?php echo ($arLoginUser['confirmed'] == 1 ? 'check' : 'times');?>-circle"></i> <?php echo ($arLoginUser['confirmed'] == 1 ? '' : 'Not ');?><?php echo lang('header_verified');?></span></span>

                </div>

            </div>

          

            <h5 class="sidebartitle actitle"><?php echo lang('header_account');?></h5>

            <ul class="nav nav-pills nav-stacked nav-bracket mb30">

              <li><a href="<?php echo base_url();?>users/profile"><i class="fa fa-user"></i> <span><?php echo lang('header_profile');?></span></a></li>

              <!-- <li><a href=""><i class="fa fa-cog"></i> <span>Account Settings</span></a></li> -->

               <li><a href="<?php echo base_url() ?>support/" target="_blank"><i class="fa fa-question-circle"></i> <span>Help Desk</span></a></li> 
               
               <li><a href="<?php echo base_url() ?>inbox"><i class="glyphicon glyphicon-envelope"></i> Inbox</a></li>

              <li><a href="<?php echo base_url() ?>logout"><i class="fa fa-sign-out"></i> <span><?php echo lang('header_logout');?></span></a></li>

            </ul>

        </div>

      <?php if(hasPermission('manage.users')){?>

      <h5 class="sidebartitle"><?php echo lang('header_administration');?></h5>

      <ul class="nav nav-pills nav-stacked nav-bracket">

        <li class="nav-parent <?php echo ($szPageName == 'Users' ? "nav-active" : "");?>"><a href=""><i class="fa fa-group"></i> <span><?php echo lang('header_users');?></span></a>

          <ul class="children" style="<?php echo ($szPageName == 'Users' ? "display: block" : "");?>">

            <li class="<?php echo ($szMetaTagTitle == 'Users Administration' ? "active" : "");?>"><a href="<?php echo base_url();?>users"><span class="pull-right badge badge-success"></span><i class="fa fa-list"></i> <?php echo lang('header_all_users');?></a></li>

            <li class="<?php echo ($szMetaTagTitle == 'Add New User' ? "active" : "");?>"><a href="<?php echo base_url();?>users/add"><i class="fa fa-plus"></i> <?php echo lang('header_add_new');?></a></li>

          </ul>

        </li>

        <li class="nav-parent <?php echo ($szPageName == 'Access' ? "nav-active" : "");?>"><a href=""><i class="fa fa-lock"></i> <span><?php echo lang('header_roles_permissions');?></span></a>

          <ul class="children" style="<?php echo ($szPageName == 'Access' ? "display: block" : "") ?>">

            <li class="<?php echo ($szMetaTagTitle == 'Roles' ? "active" : "");?>"><a href="<?php echo base_url();?>roles"><i class="fa fa-user"></i> <?php echo lang('header_roles');?></a></li>

            <li class="<?php echo ($szMetaTagTitle == 'Permissions' ? "active" : "");?>"><a href="<?php echo base_url() ?>permissions"><i class="fa fa-gear"></i> <?php echo lang('header_permissions');?></a></li>

          </ul>

        </li>

        <li class="nav-parent <?php echo ($szPageName == 'Membership (Admin)' ? "nav-active" : "");?>"><a href=""><i class="fa fa-gear"></i> <span><?php echo lang('header_membership');?></span></a>

          <ul class="children" style="<?php echo ($szPageName == 'Membership (Admin)' ? "display: block" : "");?>">

            <li class="<?php echo ($szMetaTagTitle == 'Plans' ? "active" : "");?>"><a href="<?php echo base_url();?>plans"><i class="fa fa-list"></i> <?php echo lang('header_plans');?></a></li>

            <li class="<?php echo ($szMetaTagTitle == 'Features' ? "active" : "");?>"><a href="<?php echo base_url();?>features"><i class="fa fa-star"></i> <?php echo lang('header_features');?></a></li>

            <li class="<?php echo ($szMetaTagTitle == 'Subscriptions' ? "active" : "");?>"><a href="<?php echo base_url();?>subscriptions"><i class="fa fa-list"></i> <?php echo lang('header_subscription');?></a></li>

            <li class="<?php echo ($szMetaTagTitle == 'Invoices' ? "active" : "");?>"><a href="<?php echo base_url();?>invoices"><i class="fa fa-usd"></i> <?php echo lang('header_invoices');?></a></li>

          </ul>

        </li>

        <li class="nav-parent <?php echo ($szPageName == 'Autoresponder' ? "nav-active" : "");?>"><a href=""><i class="fa fa-envelope"></i> <span><?php echo lang('header_campaigns');?></span></a>

          <ul class="children" style="<?php echo ($szPageName == 'Autoresponder' ? "display: block" : "");?>">

            <li class="<?php echo ($szMetaTagTitle == 'All Emails' ? "active" : "");?>"><a href="<?php echo base_url();?>campaigns/templates/all"><i class="fa fa-list"></i> <?php echo lang('header_all_emails');?></a></li>

            <li class="<?php echo ($szMetaTagTitle == 'Add New Email' ? "active" : "");?>"><a href="<?php echo base_url();?>campaigns/templates/add"><i class="fa fa-plus"></i> <?php echo lang('header_add_email');?></a></li>

            <li class="<?php echo ($szMetaTagTitle == 'All Campaigns' ? "active" : "");?>"><a href="<?php echo base_url();?>campaigns/all"><i class="fa fa-list"></i> <?php echo lang('header_all_campaigns');?></a></li>

            <li class="<?php echo ($szMetaTagTitle == 'Add New Campaign' ? "active" : "");?>"><a href="<?php echo base_url();?>campaigns/add"><i class="fa fa-plus"></i> <?php echo lang('header_add_campaign');?></a></li>

          </ul>

        </li>
        
        <li class="nav-parent <?php echo ($szPageName == 'System Settings' ? "nav-active" : "");?>"><a href=""><i class="fa fa-gears"></i> <span><?php echo lang('header_settings');?></span></a>

          <ul class="children" style="<?php echo ($szPageName == 'System Settings' ? "display: block" : "");?>">

            <li class="<?php echo ($szMetaTagTitle == 'Translator' ? "active" : "");?>"><a href="<?php echo base_url();?>translator"><i class="fa fa-globe"></i> <?php echo lang('header_settings_translator');?></a></li>

          </ul>

        </li>
        
        <!-- Help Desk Menu -->
        <li class="nav-parent <?php echo ($szPageName == 'Help Desk' ? "nav-active" : "");?>"><a href=""><i class="fa fa-support"></i> <span><?php echo lang('header_help_desk');?></span></a>

          <ul class="children" style="<?php echo ($szPageName == 'Help Desk' ? "display: block" : "");?>">

            <li class="<?php echo ($szMetaTagTitle == 'All Tickets' ? "active" : "");?>"><a href="<?php echo base_url();?>support/all"><i class="fa fa-list"></i> <?php echo lang('header_help_desk_all_tickets');?></a></li>

            <li class="<?php echo ($szMetaTagTitle == 'New Tickets' ? "active" : "");?>"><a href="<?php echo base_url();?>"><i class="fa fa-list"></i> <?php echo lang('header_help_desk_new_tickets');?></a></li>
            
            <li class="<?php echo ($szMetaTagTitle == 'Pending Tickets' ? "active" : "");?>"><a href="<?php echo base_url();?>"><i class="fa fa-list"></i> <?php echo lang('header_help_desk_pending_tickets');?></a></li>
            
            <li class="<?php echo ($szMetaTagTitle == 'Solved Tickets' ? "active" : "");?>"><a href="<?php echo base_url();?>"><i class="fa fa-list"></i> <?php echo lang('header_help_desk_solved_tickets');?></a></li>
         
          </ul>

        </li>
        
        <!-- Notification Menu -->
        <li class="nav-parent <?php echo ($szPageName == 'Notifications' ? "nav-active" : "");?>"><a href=""><i class="fa fa-enevelope"></i> <span><?php echo lang('header_notifications');?></span></a>

          <ul class="children" style="<?php echo ($szPageName == 'Notifications' ? "display: block" : "");?>">

            <li class="<?php echo ($szMetaTagTitle == 'All Notifications' ? "active" : "");?>"><a href="<?php echo base_url();?>notifications/all"><i class="fa fa-list"></i> <?php echo lang('header_all_notifications');?></a></li>

            <li class="<?php echo ($szMetaTagTitle == 'New Notification' ? "active" : "");?>"><a href="<?php echo base_url();?>notifications/new"><i class="fa fa-list"></i> <?php echo lang('header_new_notification');?></a></li>
         
          </ul>

        </li>

      </ul>

      <?php }?>

    </div><!-- leftpanelinner -->

  </div><!-- leftpanel -->

  

  <div class="mainpanel">

    <div class="headerbar">

	<div class="header-left">
        
		<div class="logopanel">
			<a href="<?php echo base_url() ?>dashboard">
			<img src="<?php echo base_url() ?>assets/images/logo_beta.png" title="<?php echo WEBSITE_NAME; ?>" class="center-block img-responsive" />
			</a>
		</div><!-- logopanel -->
        
        <div class="topnav">
            <a class="menutoggle"><i class="fa fa-bars"></i></a>
            
			<ul class="nav nav-horizontal">
				<li class="<?php echo ($szPageName == 'Home' ? "active" : "");?>"><a href="<?php echo base_url() ?>dashboard/"><i class="fa fa-dashboard"></i> <span><?php echo lang('header_dashboard');?></span></a></li>

				<li class="<?php echo ($szPageName == 'Training' ? "active" : "");?>"><a href="<?php echo base_url() ?>training/"><i class="fa fa-graduation-cap"></i> <span><?php echo lang('header_training');?></span></a></li>

				<li class="nav-parent <?php echo ($szPageName == 'Contacts' ? "active" : "");?>"><a class="dropdown-toggle" data-toggle="dropdown"  href=""><i class="fa fa-group"></i> <?php echo lang('header_contacts');?> <span class="caret"></a>
				  <ul class="dropdown-menu children" style="<?php #echo ($szPageName == 'Contacts' ? "display: block" : "");?>">
					<li class="<?php echo ($szMetaTagTitle == 'All Contacts' ? "active" : "");?>"><a href="<?php echo base_url();?>contacts/all"><i class="fa fa-list"></i> <?php echo lang('header_all_contact');?> <span class="badge badge-notice"><? echo $globalStats['subscribers']['total']; ?></span></a></li>
					<li class="<?php echo ($szMetaTagTitle == 'Add New Contact' ? "active" : "");?>"><a href="<?php echo base_url();?>contacts/add"><i class="fa fa-plus"></i> <?php echo lang('header_add_contacts');?></a></li>
					<?php if(hasPermission('manage.users')){?>
					<li class="<?php echo ($szMetaTagTitle == 'Manage Tags' ? "active" : "");?>"><a href="<?php echo base_url();?>tags"><i class="fa fa-tags"></i> <?php echo lang('header_manage_tags');?></a></li>
					<li class="<?php echo ($szMetaTagTitle == 'Import Contacts' ? "active" : "");?>"><a href="<?php echo base_url();?>contacts/upload"><i class="fa fa-upload"></i> <?php echo lang('header_upload_contacts');?></a></li>
					<?php } ?>
				  </ul>
				</li>

				<li class="nav-parent <?php echo ($szPageName == 'Marketing' ? "active" : "");?>"><a class="dropdown-toggle" data-toggle="dropdown" href=""><i class="fa fa-suitcase"></i> <?php echo lang('header_marketing');?> <span class="caret"></a>
				  <ul class="dropdown-menu children" style="<?php #echo ($szPageName == 'Marketing' ? "display: block" : "");?>">
					<li class="<?php echo ($szMetaTagTitle == 'Sales Funnels' ? "active" : "");?>"><a href="<?php echo base_url();?>marketing/funnels"><i class="fa fa-desktop"></i> <?php echo lang('header_funnels');?></a></li>
					<li class="<?php echo ($szMetaTagTitle == 'Lead Packs' ? "active" : "");?>"><a href="<?php echo base_url();?>marketing/leads"><i class="fa fa-bullseye"></i> <?php echo lang('header_lead_acks');?></a></li>
					<li class="<?php echo ($szMetaTagTitle == 'Campaigns' ? "active" : "");?>"><a href="<?php echo base_url();?>marketing/campaigns"><i class="fa fa-envelope"></i> <?php echo lang('header_campaigns');?></a></li>
				  </ul>
				</li>

				<li class="nav-parent <?php echo ($szPageName == 'Membership' ? "active" : "");?>"><a class="dropdown-toggle" data-toggle="dropdown" href=""><i class="fa fa-gear"></i> <?php echo lang('header_membership'); ?> <span class="caret"></a>
				  <ul class="dropdown-menu children" style="<?php #echo ($szPageName == 'Membership' ? "display: block" : "");?>">
					<li class="<?php echo ($szMetaTagTitle == 'Subscriptions' ? "active" : "");?>"><a href="<?php echo base_url();?>membership/subscription"><i class="fa fa-ticket"></i> <?php echo lang('header_subscriptions');?></a></li>
					<li class="<?php echo ($szMetaTagTitle == 'Payment History' ? "active" : "");?>"><a href="<?php echo base_url();?>membership/history"><i class="fa fa-history"></i> <?php echo lang('header_payment_istory');?></a></li>
					<li class="<?php echo ($szMetaTagTitle == 'Referrals' ? "active" : "");?>"><a href="<?php echo base_url();?>membership/referrals"><i class="fa fa-ticket"></i> <?php echo lang('header_referrals');?></a></li>
				  </ul>
				</li>
			
			  <?php if(hasPermission('manage.users')){?>
			  
				<li class="nav-parent <?php echo ($szPageName == 'Administration' ? "nav-active" : "");?>"><a class="dropdown-toggle" data-toggle="dropdown" href=""><i class="fa fa-group"></i><?php echo lang('header_administration');?> <span class="caret"></a>

				  <ul class="dropdown-menu children" style="<?php #echo ($szPageName == 'Users' ? "display: block" : "");?>">

					<li class="nav-parent <?php echo ($szPageName == 'Users' ? "nav-active" : "");?>"><a href=""><i class="fa fa-group"></i> <span><?php echo lang('header_users');?></span></a>

					  <ul class="children" style="<?php echo ($szPageName == 'Users' ? "display: block" : "");?>">

						<li class="<?php echo ($szMetaTagTitle == 'Users Administration' ? "active" : "");?>"><a href="<?php echo base_url();?>users"><span class="pull-right badge badge-success"><?php echo $globalStats['users']['total']; ?> / <?php echo $globalStats['users']['active']; ?></span><i class="fa fa-list"></i> <?php echo lang('header_all_users');?></a></li>

						<li class="<?php echo ($szMetaTagTitle == 'Add New User' ? "active" : "");?>"><a href="<?php echo base_url();?>users/add"><i class="fa fa-plus"></i> <?php echo lang('header_add_new');?></a></li>

					  </ul>

					</li>

					<li class="nav-parent <?php echo ($szPageName == 'Access' ? "nav-active" : "");?>"><a href=""><i class="fa fa-lock"></i> <span><?php echo lang('header_roles_permissions');?></span></a>

					  <ul class="children" style="<?php echo ($szPageName == 'Access' ? "display: block" : "") ?>">

						<li class="<?php echo ($szMetaTagTitle == 'Roles' ? "active" : "");?>"><a href="<?php echo base_url();?>roles"><i class="fa fa-user"></i> <?php echo lang('header_roles');?></a></li>

						<li class="<?php echo ($szMetaTagTitle == 'Permissions' ? "active" : "");?>"><a href="<?php echo base_url() ?>permissions"><i class="fa fa-gear"></i> <?php echo lang('header_permissions');?></a></li>

					  </ul>

					</li>

					<li class="nav-parent <?php echo ($szPageName == 'Membership (Admin)' ? "nav-active" : "");?>"><a href=""><i class="fa fa-gear"></i> <span><?php echo lang('header_membership');?></span></a>

					  <ul class="children" style="<?php echo ($szPageName == 'Membership (Admin)' ? "display: block" : "");?>">

						<li class="<?php echo ($szMetaTagTitle == 'Plans' ? "active" : "");?>"><a href="<?php echo base_url();?>plans"><i class="fa fa-list"></i> <?php echo lang('header_plans');?></a></li>

						<li class="<?php echo ($szMetaTagTitle == 'Features' ? "active" : "");?>"><a href="<?php echo base_url();?>features"><i class="fa fa-star"></i> <?php echo lang('header_features');?></a></li>

						<li class="<?php echo ($szMetaTagTitle == 'Subscriptions' ? "active" : "");?>"><a href="<?php echo base_url();?>subscriptions"><i class="fa fa-list"></i> <?php echo lang('header_subscription');?></a></li>

						<li class="<?php echo ($szMetaTagTitle == 'Invoices' ? "active" : "");?>"><a href="<?php echo base_url();?>invoices"><i class="fa fa-usd"></i> <?php echo lang('header_invoices');?></a></li>

					  </ul>

					</li>

					<li class="nav-parent <?php echo ($szPageName == 'Autoresponder' ? "nav-active" : "");?>"><a href=""><i class="fa fa-envelope"></i> <span><?php echo lang('header_campaigns');?></span></a>

					  <ul class="children" style="<?php echo ($szPageName == 'Autoresponder' ? "display: block" : "");?>">

						<li class="<?php echo ($szMetaTagTitle == 'All Emails' ? "active" : "");?>"><a href="<?php echo base_url();?>campaigns/templates/all"><i class="fa fa-list"></i> <?php echo lang('header_all_emails');?></a></li>

						<li class="<?php echo ($szMetaTagTitle == 'Add New Email' ? "active" : "");?>"><a href="<?php echo base_url();?>campaigns/templates/add"><i class="fa fa-plus"></i> <?php echo lang('header_add_email');?></a></li>

						<li class="<?php echo ($szMetaTagTitle == 'All Campaigns' ? "active" : "");?>"><a href="<?php echo base_url();?>campaigns/all"><i class="fa fa-list"></i> <?php echo lang('header_all_campaigns');?></a></li>

						<li class="<?php echo ($szMetaTagTitle == 'Add New Campaign' ? "active" : "");?>"><a href="<?php echo base_url();?>campaigns/add"><i class="fa fa-plus"></i> <?php echo lang('header_add_campaign');?></a></li>

					  </ul>

					</li>

					<li class="nav-parent <?php echo ($szPageName == 'System Settings' ? "nav-active" : "");?>"><a href=""><i class="fa fa-gears"></i> <span><?php echo lang('header_settings');?></span></a>

					  <ul class="children" style="<?php echo ($szPageName == 'System Settings' ? "display: block" : "");?>">

						<li class="<?php echo ($szMetaTagTitle == 'Translator' ? "active" : "");?>"><a href="<?php echo base_url();?>translator"><i class="fa fa-globe"></i> <?php echo lang('header_settings_translator');?></a></li>

					  </ul>

					</li>

					<!-- Help Desk Menu -->
					<li class="nav-parent <?php echo ($szPageName == 'Help Desk' ? "nav-active" : "");?>"><a href=""><i class="fa fa-support"></i> <span><?php echo lang('header_help_desk');?></span></a>

					  <ul class="children" style="<?php echo ($szPageName == 'Help Desk' ? "display: block" : "");?>">  
                                                <li class="<?php echo ($szMetaTagTitle == 'New Tickets' ? "active" : "");?>"><a href="<?php echo base_url();?>support/all/new"><i class="fa fa-list"></i> <?php echo lang('header_help_desk_new_tickets');?></a></li> 
                                                <li class="<?php echo ($szMetaTagTitle == 'Pending Tickets' ? "active" : "");?>"><a href="<?php echo base_url();?>support/all/pending"><i class="fa fa-list"></i> <?php echo lang('header_help_desk_pending_tickets');?></a></li> 
                                                <li class="<?php echo ($szMetaTagTitle == 'Closed Tickets' ? "active" : "");?>"><a href="<?php echo base_url();?>support/all/closed"><i class="fa fa-list"></i> <?php echo lang('header_help_desk_solved_tickets');?></a></li> 
                                                <li class="<?php echo ($szMetaTagTitle == 'Trashed Tickets' ? "active" : "");?>"><a href="<?php echo base_url();?>support/all/trash"><i class="fa fa-list"></i> <?php echo lang('header_help_desk_trashed_tickets');?></a></li> 
					  </ul>

					</li>
                                        
                                        <!-- Notification Menu -->
                                        <li class="nav-parent <?php echo ($szPageName == 'Notifications' ? "nav-active" : "");?>"><a href=""><i class="fa fa-enevelope"></i> <span><?php echo lang('header_notifications');?></span></a>

                                          <ul class="children" style="<?php echo ($szPageName == 'Notifications' ? "display: block" : "");?>">

                                            <li class="<?php echo ($szMetaTagTitle == 'All Notifications' ? "active" : "");?>"><a href="<?php echo base_url();?>notifications/all"><i class="fa fa-list"></i> <?php echo lang('header_all_notifications');?></a></li>

                                            <li class="<?php echo ($szMetaTagTitle == 'New Notification' ? "active" : "");?>"><a href="<?php echo base_url();?>notifications/new"><i class="fa fa-list"></i> <?php echo lang('header_new_notification');?></a></li>

                                          </ul>

                                        </li>

				  </ul>
				  
				</li>
				
			  <?php }?>
			  

				<li class="nav-parent"><a class="dropdown-toggle" data-toggle="dropdown" href=""><span class="pull-right badge badge-<?php
				switch ($globalStats['subscription']['szStatus']) {
					case '':
						echo "notice";
						break;
					case 'trialing':
						echo "warning";
						break;
					case 'canceled':
						echo "danger";
						break;
					case 'active':
						echo "success";
						break;
				}
				?>"><?php echo ucfirst($globalStats['subscription']['szStatus']); ?></span></a>
				</li>
				

			  
			  </ul>
            
        </div><!-- topnav -->
          
      </div><!-- header-left -->

      <div class="header-right">

        <ul class="headermenu">

         <!--

		<li class="dropdown menu-merge">

			<select id="lanuage" onchange="language_switcher(this.value)">

				<option style="background-image:url(https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/US_flag_51_stars.svg/1200px-US_flag_51_stars.svg.png);" value="english" <?php #if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?>>English</option>

				<option value="spanish" <?php #if($this->session->userdata('site_lang') == 'spanish') echo 'selected="selected"'; ?>>Spanish</option>   

			</select>

		</li>

        

        <style>

		#language {

			border: none !important;

		}

		</style>

         

         

          <li>

            <div class="btn-group">

              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">

                <img style="border-radius: 0;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/US_flag_51_stars.svg/1200px-US_flag_51_stars.svg.png" alt="English" />

                    English

                <span class="caret"></span>

              </button>

              <ul class="dropdown-menu dropdown-menu-usermenu pull-right">

                <li><a href="?lang=english"><img style="border-radius: 0;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/US_flag_51_stars.svg/1200px-US_flag_51_stars.svg.png" width="35px" alt="English" /> English</a></li>

              </ul>

            </div>

          </li>

       -->
          <?php $arLoginContacts = getLoginUserLatestContacts(); $contactcount = count($arLoginContacts);?> 
          <li>

            <div class="btn-group">

              <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">

                <i class="glyphicon glyphicon-user"></i>

                <span class="badge"><?php echo ($contactcount > 0 ? $contactcount : ''); ?></span>

              </button>

              <div class="dropdown-menu dropdown-menu-head pull-right">                

                <h5 class="title"><?php echo $contactcount ?> New Contacts</h5>

                <ul class="dropdown-list user-list">

                  <?php if(!empty($arLoginContacts)){foreach($arLoginContacts as $contact){?>

                   <li class="new">

                    <a href="<?php echo base_url();?>contacts/edit/<?php echo $contact['szUniqueKey'];?>?view-contact=1">

                    <span class="thumb"><img src="" alt="" /></span>

                    <span class="desc">

                      <span class="name"><?php echo "{$contact['szFirstName']} {$contact['szLastName']}";?> <span class="badge badge-success"><?php echo lang('header_view');?></span></span>

                      <span class="msg"><?php echo date('m/d/y', strtotime($contact['dtAddedOn']));?> <?php echo date('h:ia', strtotime($contact['dtAddedOn']));?></span>

                    </span>

                    </a>

                  </li>

                  <?php }}?>

                  <li class="new"><a href="<?php echo base_url();?>contacts/all?view-contact=all"><?php echo lang('header_see_all_subscribers');?></a></li>

                </ul>
              </div>

            </div>

          </li>

          <?php $arLoginMessages = getLoginUserLatestMessages(); $messagecount = count($arLoginMessages);?> 
          <li>

            <div class="btn-group">

              <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">

                <i class="glyphicon glyphicon-envelope"></i>

                <span class="badge"><?php echo ($messagecount > 0 ? $messagecount : ''); ?></span>

              </button>

              <div class="dropdown-menu dropdown-menu-head pull-right">

                <h5 class="title">You Have <?php echo $messagecount;?> New Messages</h5>

                <ul class="dropdown-list gen-list">

                  <?php if(!empty($arLoginMessages)){foreach($arLoginMessages as $message){?>

                   <li class="new">

                    <a href="<?php echo base_url();?>inbox/<?php echo $message['szUniqueKey'];?>?view-message=1">

                    <span class="thumb"><img src="" alt="" /></span>

                    <span class="desc">

                      <span class="name"><?php echo "{$message['szTitle']}";?> <span class="badge badge-success"><?php echo lang('header_view');?></span></span>

                      <span class="msg"><?php echo date('m/d/y', strtotime($message['dtAddedOn']));?> <?php echo date('h:ia', strtotime($message['dtAddedOn']));?></span>

                    </span>

                    </a>

                  </li>

                  <?php }}?>
                  
                  <li class="new"><a href="<?php echo base_url();?>inbox?view-message=all"><?php echo lang('header_read_all_messages');?></a></li>

                </ul>

              </div>

            </div>

          </li>

          <?php $arLoginActivity = getLoginUserLatestActivity(); $activitycount = count($arLoginActivity['logs']); if(!empty($arLoginActivity)) {?> 

          <li>

            <div class="btn-group">

              <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">

                <i class="glyphicon glyphicon-globe"></i>

                <span class="badge"><?php if ($activitycount != 0 ) { echo count($arLoginActivity['logs']); } ?></span>

              </button>

              <div class="dropdown-menu dropdown-menu-head pull-right">

                <h5 class="title"><?php #echo lang('header_recent_activity');?>You Have <?php echo count($arLoginActivity['logs']) ?> New Notifications</h5>

                <ul class="dropdown-list gen-list">

                  <?php foreach($arLoginActivity['logs'] as $activity){?>

                   <li class="new">

                    <a href="<?php echo base_url();?>users/profile?view-activity=<?php echo $activity['id'];?>">

                    <span class="thumb"><img src="" alt="" /></span>

                    <span class="desc">

                      <span class="name"><?php echo $activity['szActivity'];?> <span class="badge badge-success"><?php echo lang('header_view');?></span></span>

                      <span class="msg"><?php echo convert_date($activity['dtCreatedOn'],1);?></span>

                    </span>

                    </a>

                  </li>

                  <?php }?>

                  <li class="new"><a href="<?php echo base_url();?>users/profile?view-activity=all"><?php echo lang('header_view_all');?></a></li>

                </ul>

              </div>

            </div>

          </li>

          <?php }?> 

          <li>

            <div class="btn-group">

              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">

                <img src="<?php echo $avatar_src;?>" />

                <?php echo $arLoginUser['name'];?>

                <span class="caret"></span>

              </button>

              <ul class="dropdown-menu dropdown-menu-usermenu pull-right">

                <li><a href="<?php echo base_url();?>users/profile"><i class="glyphicon glyphicon-user"></i> <?php echo lang('header_my_profile');?></a></li>

                <!-- <li><a href=""><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li> -->

                <li><a href="<?php echo base_url() ?>support"><i class="glyphicon glyphicon-question-sign"></i> Help</a></li>
                
                <li><a href="<?php echo base_url() ?>inbox"><i class="glyphicon glyphicon-envelope"></i> Inbox</a></li>

                <li><a href="<?php echo base_url() ?>logout"><i class="glyphicon glyphicon-log-out"></i> <?php echo lang('header_logout');?></a></li>

              </ul>

            </div>

          </li>

        </ul>

      </div><!-- header-right -->

      

    </div><!-- headerbar -->

    

    <div class="pageheader">

      <h2><i class="fa <?php echo $szFavIcon ?>"></i> <?php echo $szMetaTagTitle ?> <span><?php echo $szPageName ?></span></h2>

      <div class="breadcrumb-wrapper">

        <span class="label">You are here:</span>

        <ol class="breadcrumb">

          <li><a href="/"><?php echo $szPageName ?></a></li>

          <li class="active"><?php echo $szMetaTagTitle ?></li>

        </ol>

      </div>

    </div>

	<?php /*
	<div class="alert alert-warning">
		<strong><?php #echo lang('dashboard_notice');?>:</strong> <?php #echo lang('dashboard_notice_text');?>
	</div>
	*/ ?>

	<div class="contentpanel">