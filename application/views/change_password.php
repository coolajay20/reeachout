	<section id="content_wrapper">		
		<!-- Start: Topbar -->
      	<header id="topbar" class="alt">
        	<div class="topbar-left">
          		<ol class="breadcrumb">
            		<li class="crumb-active">
              			<a href="<?php echo base_url();?>dashboard">Dashboard</a>
            		</li>
            		<li class="crumb-trail">Change Password</li>
          		</ol>
        	</div>        	
     	</header>
      	<!-- End: Topbar -->

      	<section id="content" class="table-layout">

        	<!-- begin: .tray-center -->
        	<div class="tray tray-center" style="height: 621px;">

	            <!-- Begin: Content Header -->
	            <div class="content-header">
	            	<?php if(!$passwordChanged){?>
	              	<h2> Change Password</h2>
	              	<p class="lead">Please enter your old and new password.</p>
	              	<?php } else {?>
	              	<h2> Password Changed</h2>
	              	<?php }?>
	            </div>

            	<!-- Validation Example -->
            	<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
					<div class="row">
						<div class="col-sm-6 col-sm-offset-3">
							<?php if($passwordChanged){?>
							<div class="alert alert-success" style="margin-top: 3%;">
				       			<i class="fa fa-check pr10"></i>
								<strong>Cogratulations!</strong>
								Your account password has been changed successfully.
				     	  	</div>
							<?php } else {?>
		              		<div class="panel heading-border panel-primary">		
		                		<form method="post" action="<?php echo base_url();?>change-password" id="change-pwd-form" class="validate-form">		
		                  			<div class="panel-body bg-light">
		                  				<?php $szOldPasswordError = form_error('arChange[szOldPassword]');?>
			                    		<div class="section">
		                        			<label for="szOldPassword" class="field prepend-icon<?php if(!empty($szOldPasswordError)){?> state-error<?php }?>">
		                          				<input name="arChange[szOldPassword]" id="szOldPassword" class="gui-input required" placeholder="Old password" type="password" value="<?php echo set_input_value('arChange[szOldPassword]');?>" autocomplete="off">
		                          				<label for="szOldPassword" class="field-icon">
		                            				<i class="fa fa-user"></i>
		                          				</label>
		                        			</label>
		                        			<?php if(!empty($szOldPasswordError)){?><em class="state-error" for="szOldPassword"><?php echo $szOldPasswordError;?></em><?php }?>
			                    		</div>
			                    		
			                    		<?php $szNewPasswordError = form_error('arChange[szNewPassword]');?>
			                    		<div class="section">
		                        			<label for="szNewPassword" class="field prepend-icon<?php if(!empty($szNewPasswordError)){?> state-error<?php }?>">
		                          				<input name="arChange[szNewPassword]" id="szNewPassword" class="gui-input required minlength maxlength" data-max-len="16" data-min-len="6" placeholder="New password" type="password" value="<?php echo set_input_value('arChange[szNewPassword]');?>" autocomplete="off">
		                          				<label for="szNewPassword" class="field-icon">
		                            				<i class="fa fa-user"></i>
		                          				</label>
		                        			</label>
		                        			<?php if(!empty($szNewPasswordError)){?><em class="state-error" for="szNewPassword"><?php echo $szNewPasswordError;?></em><?php }?>
			                    		</div>
			                    		
			                    		<?php $szRepeatPasswordError = form_error('arChange[szRepeatNewPassword]');?>
			                    		<div class="section">
		                        			<label for="szRepeatNewPassword" class="field prepend-icon<?php if(!empty($szRepeatPasswordError)){?> state-error<?php }?>">
		                          				<input name="arChange[szRepeatNewPassword]" id="szRepeatNewPassword" class="gui-input required matches" data-match="szPassword" placeholder="Confirm new password" type="password" value="<?php echo set_input_value('arChange[szRepeatNewPassword]');?>" autocomplete="off">
		                          				<label for="szRepeatNewPassword" class="field-icon">
		                            				<i class="fa fa-user"></i>
		                          				</label>
		                        			</label>
		                        			<?php if(!empty($szRepeatPasswordError)){?><em class="state-error" for="szRepeatNewPassword"><?php echo $szRepeatPasswordError;?></em><?php }?>
			                    		</div>
		              				</div>
		              				<div class="panel-footer text-right">
		              					<button type="button" onclick="window.history.go(-1); return false;" class="button btn-default"> Cancel </button>
				                    	<button type="submit" class="button btn-primary"> Change Password </button>				                    	
				                  	</div>
				                  	<input type="hidden" name="p_func" value="Change Password">
		             			</form>
		         			</div>
		         			<?php }?>
		         		</div>
		         	</div>
         		</div>
         	</div>
		</section>
	</section>