var myc;
$(document).ready(function(){
	// Import image
	var $inputImage = $('#avatar-upload'),
	URL = window.URL || window.webkitURL,
	fileType,
	fileData;

	if (URL) {
		$inputImage.change(function() {
			$('.upload-error').text('').addClass('hidden');
			var files = this.files,
			file;
	
			if (files && files.length) {
				fileData = file = files[0];
				fileType = file.type;
	
				if (/^image\/\w+$/.test(file.type)) {					
					$inputImage.val('');
					$('.btn-avatar-upload').addClass('hidden');
					$('.avatar-preview').addClass('hidden');
					$('.avatar-controls').removeClass('hidden');
					fileData.convertToBase64(function(base64){
                                            myc = $('#avatar').croppie({
                                                viewport: {
                                                        width: 180,
                                                        height: 180,
                                                        type: 'circle'
                                                },
                                                boundary: {
                                                        width: 230,
                                                        height: 230
                                                },
                                                url: base64,
                                                enforceBoundary: false
                                                // mouseWheelZoom: false
                                            });

                                            myc.croppie('bind', {
                                                url: base64
                                            });
					});
				} else {
					$('.upload-error').text('Please choose a valid image file.').removeClass('hidden');
				}
			}
		});		 	
	} else {
		$inputImage.parent().remove();
	}
});

$('.btn-cancel-upload').click(function(){
    $('.btn-avatar-upload').removeClass('hidden');
    $('.avatar-preview').removeClass('hidden');
    $('.avatar-controls').addClass('hidden');
    $('#avatar').html('').removeClass('croppie-container');
    $('#p_avatar_data').val('');
});

$('.btn-save-upload').click(function(){
    myc.croppie('result', {
        type: 'canvas',
        size: 'viewport',
        format: 'png',
        circle: true
    }).then(function (resp) {
        $('.btn-avatar-upload').removeClass('hidden');
        $('.avatar-preview').removeClass('hidden').attr('src', resp);
        $('.avatar-controls').addClass('hidden');
        $('#avatar').html('').removeClass('croppie-container');
        $('#p_avatar_data').val(resp);
    });
});

File.prototype.convertToBase64 = function(callback){
	var reader = new FileReader();
  	reader.onload = function(e) {
   		callback(e.target.result)
  	};
  	reader.onerror = function(e) {
    	callback(null);
  	};        
 	reader.readAsDataURL(this);
};