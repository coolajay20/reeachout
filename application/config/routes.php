<?php

defined('BASEPATH') OR exit('No direct script access allowed');



/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	https://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There are three reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router which controller/method to use if those

| provided in the URL cannot be matched to a valid route.

|

|	$route['translate_uri_dashes'] = FALSE;

|

| This is not exactly a route, but allows you to automatically route

| controller and method names that contain dashes. '-' isn't a valid

| class or method name character, so it requires translation.

| When you set this option to TRUE, it will replace ALL dashes in the

| controller and method URI segments.

|

| Examples:	my-controller/index	-> my_controller/index

|		my-controller/my-method	-> my_controller/my_method

*/


//define each domain and it's route
$sites_route = array();
$sites_route['multiplymybtc.com'] = 'multiplymybtc';

//get domain name
$host = $_SERVER['HTTP_HOST'];
preg_match("/[^\.\/]+\.[^\.\/]+$/", $host, $matches);

//build the routes
if(isset($sites_route[$matches[0]]))
{
    $route['default_controller'] = 'Funnel_Controller';
}
else
{
	$route['default_controller'] = 'Home_Controller';
}

	$route['ref/(:any)'] = 'Funnel_Controller/setRef/$1';

	$route['api'] = 'Api_Controller';
	$route['api/(:any)'] = 'Api_Controller/$1';

	$route['pages/(:any)'] = 'Page_Controller/getPages/$1';

	$route['attachments'] = "Home_Controller/upload";

	$route['tags'] = 'Tags_Controller';
	$route['tags/(:any)'] = 'Tags_Controller/$1';
	$route['tags/(:any)/(:any)'] = 'Tags_Controller/$1/$2';

	$route['plans'] = 'Plan_Controller';
	$route['plans/(:any)'] = 'Plan_Controller/$1';
	$route['plans/(:any)/(:any)'] = 'Plan_Controller/$1/$2';

	$route['features'] = 'Features_Controller';
	$route['features/(:any)'] = 'Features_Controller/$1';
	$route['features/(:any)/(:any)'] = 'Features_Controller/$1/$2';

	$route['roles'] = 'Role_Controller';
	$route['roles/(:any)'] = 'Role_Controller/$1';
	$route['roles/(:any)/(:any)'] = 'Role_Controller/$1/$2'; 

	$route['permissions'] = 'Permission_Controller';
	$route['permissions/(:any)'] = 'Permission_Controller/$1';
	$route['permissions/(:any)/(:any)'] = 'Permission_Controller/$1/$2';

	$route['users'] = 'User_Controller';
	$route['users/(:any)'] = 'User_Controller/$1';
	$route['users/(:any)/(:any)'] = 'User_Controller/$1/$2';

	$route['contacts'] = 'Contact_Controller';
	$route['contacts/(:any)'] = 'Contact_Controller/$1';
	$route['contacts/(:any)/(:any)'] = 'Contact_Controller/$1/$2';

	$route['campaigns'] = 'Campaign_Controller';
	$route['campaigns/(:any)'] = 'Campaign_Controller/$1';
	$route['campaigns/(:any)/(:any)'] = 'Campaign_Controller/$1/$2';
	$route['campaigns/(:any)/(:any)/(:any)'] = 'Campaign_Controller/$1/$2/$3';

	$route['membership'] = 'Membership_Controller'; 

	$route['membership/(:any)'] = 'Membership_Controller/$1';
	$route['membership/(:any)/(:any)'] = 'Membership_Controller/$1/$2';

	$route['marketing'] = 'Marketing_Controller';
	$route['marketing/(:any)'] = 'Marketing_Controller/$1';
	$route['marketing/(:any)/(:any)'] = 'Marketing_Controller/$1/$2';

	$route['support'] = 'Support_Controller';
	$route['support/(:any)'] = 'Support_Controller/$1';
	$route['support/(:any)/(:any)'] = 'Support_Controller/$1/$2';
        
        $route['notifications'] = 'Notification_Controller';
	$route['notifications/(:any)'] = 'Notification_Controller/$1';
	$route['notifications/(:any)/(:any)'] = 'Notification_Controller/$1/$2';

	$route['funnel'] = 'Funnel_Controller';
	$route['funnel/(:any)'] = 'Funnel_Controller/$1';
	$route['funnel/(:any)/(:any)'] = 'Funnel_Controller/$1/$2';

	$route['training'] = 'Training_Controller';
	$route['training/(:any)'] = 'Training_Controller/$1';
	$route['training/(:any)/(:any)'] = 'Training_Controller/$1/$2';

	$route['webhook'] = 'Webhook_Controller';
	$route['webhook/(:any)'] = 'Webhook_Controller/$1';

	$route['cronjob'] = 'Cronjob_Controller';
	$route['cronjob/(:any)'] = 'Cronjob_Controller/$1';

	$route['translator'] = 'Translator_Controller';
	$route['translator/(:any)'] = 'Translator_Controller/$1';

	$route['auth_oa2/session/(:any)'] = 'Auth_oa2/session/$1';
	$route['bitpay/(:any)'] = 'Bitpay_Controller/$1';

	$route['change-password'] = 'Home_Controller/changePassword';
	$route['forgot-password'] = 'Home_Controller/forgotPassword';

	$route['reset-password'] = 'Home_Controller/resetPassword';
	$route['reset-password/(:any)'] = 'Home_Controller/resetPassword/$1';

	$route['(:any)'] = 'Home_Controller/$1';
	$route['(:any)/(:any)'] = 'Home_Controller/$1/$2';
	$route['(:any)/(:any)/(:any)'] = 'Home_Controller/$1/$2/$3';
	$route['(:any)/(:any)/(:any)/(:any)'] = 'Home_Controller/$1/$2/$3/$4';
	$route['translate_uri_dashes'] = FALSE;

$route['404_override'] = 'Custom404Ctrl';