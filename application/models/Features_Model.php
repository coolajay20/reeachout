<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Features_Model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getAllFeatures($isOnlyActive=false)
    {
        $this->db->from('tbl_features');
        $this->db->order_by('szFeatureName', 'ASC');
        $this->db->where(array('isDeleted'=>0));
        if($isOnlyActive)
        {
            $this->db->where(array('isActive'=>1));
        }

        $query = $this->db->get();
        return ($query->num_rows() > 0 ? $query->result_array() : array());
    }

    function getFeaturesByUniqueKey($szUniqueKey)
    {
        $arDetails = array();
        $szUniqueKey = trim($szUniqueKey);
        if($szUniqueKey != '')
        {
            $query = $this->db->get_where('tbl_features', array('szUniqueKey'=>$szUniqueKey));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    }

    function getFeaturesByID($idFeature)
    {
        $arDetails = array();
        $idFeature = (int)$idFeature;
        if($idFeature > 0)
        {
            $query = $this->db->get_where('tbl_features', array('id'=>$idFeature));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    } 

    function deleteFeatures($arFeatures, $iDeletedBy)
    {
        $idFeature = (int)$arFeatures['id'];
        $this->db->where(array('id'=>$idFeature));
        $this->db->update('tbl_features', array('isDeleted'=>1, 'iDeletedBy'=>$iDeletedBy, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        if($this->db->affected_rows())
        {
             // add user activity
            addUserActivity($idFeature, 'tbl_features', "Feature {$arFeatures['szFeatureName']} deleted.");
            
            return true;
        }
        return false;
    }

    function saveFeaturesDetails($data, $idLoginUser)
    {		
        $data = sanitize_array_values($data);
        $idFeature = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);

        if($idFeature > 0)
        {
            // get old details
            $arOldData = $this->getFeaturesByID($idFeature);
            
            // update user
            $data['iUpdatedBy'] = $idLoginUser;
            $data['dtUpdatedOn'] = date('Y-m-d H:i:s');
            $this->db->where(array('id'=>$idFeature));
            $this->db->update('tbl_features', $data);
            
            // add user activity
            addUserActivity($idFeature, 'tbl_features', "Feature {$data['szFeatureName']} updated.", $data, $arOldData);
        }
        else
        {
            // add new user
            $data['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_features');
            $data['iAddedBy'] = $idLoginUser;
            $data['dtAddedOn'] = date('Y-m-d H:i:s');
            $query = $this->db->insert('tbl_features', $data);
            $idFeature = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);
            
            // add user activity
            addUserActivity($idFeature, 'tbl_features', "New Feature {$data['szFeatureName']} added.", $data);
        }
        return $idFeature;
    }
}
?>