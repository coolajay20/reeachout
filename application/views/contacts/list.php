<?php if(isset($szSuccessMessage) && $szSuccessMessage != ''){?>
<div class="alert alert-success p5"><?php echo $szSuccessMessage;?></div>
<?php } if(isset($szErrorMessage) && $szErrorMessage != ''){?>
<div class="alert alert-danger p5"><?php echo $szErrorMessage;?></div>
<?php }?>
<p class="text-right">
	<a href="<?php echo base_url();?>contacts/add" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New Contact</a>
</p>
<section id="content">
<div class="panel panel-visible" id="spy3">
	<div class="panel-body pn">
		<table class="table table-responsive table-striped table-hover dt-dynamic" id="customer_data_listing" cellspacing="0" width="100%" data-url="<?php echo base_url();?>contacts" data-func="Get_Contacts">                    
			<thead>
				<tr>             	
					<th data-sort="szFirstName"><?php echo lang('contacts_name'); ?></th>
					<th data-sort="szEmail"><?php echo lang('contacts_email'); ?></th>
					<th data-sort="idType"><?php echo lang('contacts_type'); ?></th>
					<th data-sort="dtAddedOn"><?php echo lang('contacts_date_added'); ?></th>
					<th data-class="table-action"><?php echo lang('common_actions'); ?></th>
				</tr>
			</thead>
		</table>
	</div>
</div>
</section>