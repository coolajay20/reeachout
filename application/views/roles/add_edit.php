    <!-- Start: Topbar -->
    <section id="content_wrapper">	
        <?php require_once APPPATH . 'views/breadcrumb.php';?>
      	
        <!-- Begin: Content -->
        <form method="post" action="<?php echo base_url();?>roles/<?php echo ($idRole > 0 ? "edit/{$arRoleDetails['szUniqueKey']}" : "add");?>" id="profile-form" class="validate-form">	
            <section id="content" class="table-layout">
                <div class="tray tray-center">		     	
                    <div class="mw1000 center-block">    
                        <?php if($isDetailsSaved){?>
                        <div class="alert alert-success p5">
                            <i class="fa fa-check pr10"></i>
                            <strong> Congratulation!</strong>
                            Role details has been saved successfully.
                        </div>
                        <?php } if($isFormError){?>
                        <div class="alert alert-danger p5">
                            <i class="fa fa-times pr10"></i>
                            <strong> Invalid Data!</strong>
                            Please fix the errors below and try again.
                        </div>
                        <?php }?>
	
                        <div class="panel panel-primary panel-border top mb35">
                                <div class="panel-heading">
                                    <span class="panel-title">
                                        <i class="fa fa-bookmark-o"></i>
                                        Role Details
                                    </span>
                                </div>
                                <div class="panel-body bg-light" style="padding-bottom:0;">
                                    <div class="admin-form">
                                        <div class="section row">
                                            <label for="szRoleName" class="field-label col-md-3 text-right-not-md">Name:<span class="text-danger">*</span></label>
                                            <?php $szRoleNameError = form_error('arRole[szRoleName]');?>
                                            <div class="col-md-9">
                                                <label for="szRoleName" class="field prepend-icon<?php if(!empty($szRoleNameError)){?> state-error<?php }?>">
                                                    <input type="text" name="arRole[szRoleName]" id="szRoleName" class="gui-input required" placeholder="Role Name" value="<?php echo set_input_value('arRole[szRoleName]',(isset($arRoleDetails['szRoleName']) ? $arRoleDetails['szRoleName'] : ''));?>">
                                                    <label for="szRoleName" class="field-icon">
                                                        <i class="fa fa-star"></i>
                                                    </label>
                                                </label>
                                                <?php if(!empty($szRoleNameError)){?><em class="state-error" for="szRoleName"><?php echo $szRoleNameError;?></em><?php }?>
                                            </div>
                                        </div>
                                        
                                        <div class="section row">
                                            <label for="szDisplayName" class="field-label col-md-3 text-right-not-md">Display Name:<span class="text-danger">*</span></label>
                                            <?php $szDisplayNameError = form_error('arRole[szDisplayName]');?>
                                            <div class="col-md-9">
                                                <label for="szDisplayName" class="field prepend-icon<?php if(!empty($szDisplayNameError)){?> state-error<?php }?>">
                                                    <input type="text" name="arRole[szDisplayName]" id="szDisplayName" class="gui-input required" placeholder="Role Display Name" value="<?php echo set_input_value('arRole[szDisplayName]',(isset($arRoleDetails['szDisplayName']) ? $arRoleDetails['szDisplayName'] : ''));?>">
                                                    <label for="szDisplayName" class="field-icon">
                                                        <i class="fa fa-star"></i>
                                                    </label>
                                                </label>
                                                <?php if(!empty($szDisplayNameError)){?><em class="state-error" for="szDisplayName"><?php echo $szDisplayNameError;?></em><?php }?>
                                            </div>
                                        </div>

                                        <div class="section row">
                                            <label for="szDescription" class="field-label col-md-3 text-right-not-md">Description</label>
                                            <?php $szDescriptionError = form_error('arRole[szDescription]');?>
                                            <div class="col-md-9">
                                                <label class="field prepend-icon<?php if(!empty($szDescriptionError)){?> state-error<?php }?>">
                                                    <textarea class="gui-textarea" maxlength="4000" id="szDescription" name="arRole[szDescription]" placeholder="Description"><?php echo set_input_value('arRole[szDescription]', (isset($arRoleDetails['szDescription']) ? $arRoleDetails['szDescription'] : ''));?></textarea>
                                                    <label for="szDescription" class="field-icon">
                                                      <i class="fa fa-file-text-o"></i>
                                                    </label>
                                                    <span class="input-footer">
                                                      <strong>Enter</strong> brief description about this role type.</span>
                                                </label>
                                                <?php if(!empty($szDescriptionError)){?><em class="state-error" for="szDescription"><?php echo $szDescriptionError;?></em><?php }?>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <aside class="tray tray-right tray350" data-tray-height="match" style="height: 535px;">
                        <div class="panel panel-system panel-border top mb35">
                            <div class="panel-heading">
                                <span class="panel-title">
                                    <i class="fa fa-tags"></i> <?php echo ($idRole > 0 ? 'Save Role Details' : 'Add New Role');?>
                                </span>										
                            </div>
                            <?php if(!empty($arPermissions)){?>
                            <div class="panel-body bg-light" style="padding-bottom:0;">
                                <div class="admin-form">		                    
                                    <div class="section mb10">
                                        <label class="field-label text-muted fs16 mb10">Permissions</label>
                                        <div class="option-group field" id="iSellOnline">
                                            <?php foreach ($arPermissions as $i=>$permission){?>
                                            <label class="option option-primary mb10">
                                                <input name="arRolePermissions[<?php echo $i;?>]" value="<?php echo $permission['id'];?>" type="checkbox" <?php echo ((int)set_input_value("arRolePermissions[$i]", 0) == $permission['id'] ? 'checked' : (in_array($permission['id'], $arRolePermissions) ? 'checked' : ''));?>>
                                                <span class="checkbox"></span> <?php echo $permission['szDisplayName'];?>
                                            </label><br>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
                            <div class="panel-footer text-right">
                                <div class="admin-form">
                                    <a href="<?php echo base_url();?>roles" class="button btn-default"> Cancel </a>
                                    <button type="submit" class="button btn-primary"> <?php echo ($idRole > 0 ? 'Save' : 'Add');?> </button>
                                    <input type="hidden" name="p_func" value="Save Role Details">	
                                    <input type="hidden" name="arRole[id]" value="<?php echo $idRole;?>">
                                </div>			                    	
                            </div>
                        </div>
                    </aside>
                </section>
            </form>
	</section>