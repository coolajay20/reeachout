<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funnel_Model extends CI_Model
{

    function getVisitors($idUser=NULL, $selector)
    {
        // set fields to select
        $this->db->select('id');
        $this->db->from('tbl_visitors u');
        $this->db->where('u.isDeleted',0)->where('idUser',(int)$idUser);
		
		switch ($selector) {
			case 'today':
				$this->db->group_start();
				$this->db->like('dtAddedOn', date("Y-m-d"));
				$this->db->group_end();
				break;
				
			case 'week':
				$this->db->group_start();
				$this->db->where('dtAddedOn >=', date("Y-m-d", strtotime('-7 days')));
				$this->db->group_end();
				break;

			case 'month':
				$this->db->group_start();
				$this->db->where('dtAddedOn >=', date("Y-m-d", strtotime('-30 days')));
				$this->db->group_end();
				break;
				
			case 'total':
				break;
				
			default: break;
		}
        
        // retrive records
         return $this->db->get()->num_rows();
    }
}

?>