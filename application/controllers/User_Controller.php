<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /* Load User Model */
        $this->load->model('User_Model');

        /* Load Membership Model */
        $this->load->model('Membership_Model');

        /* Load form helper */ 
        $this->load->helper('form');

        /* Load form validation library */ 
        $this->load->library('form_validation');

        /* Changing error delimiters Globally */
        $this->form_validation->set_error_delimiters('', '');     	     
    }

    public function index($arParams = array())
    { 
        if(!is_user_login()) {
            redirect(base_url() . 'login', 'refresh');
			
        } else if(!hasPermission('manage.users')) {
			
            redirect(base_url() . 'dashboard', 'refresh');
			
        } else if(trim($this->input->get('p_func')) == 'Get_Users') { 
			
            list($iLimit, $iOffset, $iDrawPage, $szKeyword, $arFilters, $szSortField, $szSortOrder) = parseDTRequest($this);
            $iTotalRecords = $this->User_Model->getUsers($arFilters, $szKeyword, true);
            $arUsers = $this->User_Model->getUsers($arFilters, $szKeyword, false, $iLimit, $iOffset, $szSortField, $szSortOrder); 
            $response = array("draw"=>$iDrawPage, "recordsTotal"=>$iTotalRecords, "recordsFiltered"=>$iTotalRecords, "data"=>array()); 

            if(!empty($arUsers)) {

                $i = 0;

                foreach($arUsers as $user)
                {
					
					$szActionLinks = '<li><a href="'.base_url()."users/details/".$user['szUniqueKey'].'">View User</a></li>';
					$szActionLinks .= '<li><a href="'.base_url()."users/edit/".$user['szUniqueKey'].'">Edit User</a></li>';
					
                    if($user['isRemovable'] == 1) {
                        $szActionLinks .= '<li><a href="'.base_url()."users/delete/".$user['szUniqueKey'].'">Delete User</a></li>';
                    }

					
                    $idUser = $user['id'];

                    $arSubscription = array();
                    $arSubscription = $this->Membership_Model->getUserSubscription($idUser);

                    if(isset($arSubscription['id']) && $arSubscription['id']>0)

                    {

                        if($arSubscription['iType']==1)

                        {

                            $szInterval = "Monthly";

                        }

                        else if($arSubscription['iType']==2)

                        {

                            $szInterval = "Yearly"; 

                        } 

                        else if($arSubscription['iType']==3)

                        {

                            $szInterval = "Lifetime"; 

                        }

                        $szSubscriptionDetails = $arSubscription['szPlanName']." (".$szInterval.")";

                        $szTitle = "";

                        if($arSubscription['tCurrentTermEnd'] > time()) 

                        {

                            if($arSubscription['tTrialEnd'] > time())

                            {

                                $szTitle = 'On trial till ' . date('d. M, Y', $arSubscription['tTrialEnd']);

                                $szStatus = '<span class="label label-warning">Trial</span>';

                            }

                            else

                            {

                                $szStatus = '<span class="label label-success">Active</span>';

                            }

                        }

                        else

                        {

                            $szTitle = 'Cancelled since ' . date('d. M, Y', $arSubscription['tCurrentTermEnd']);

                            $szStatus = '<span class="label label-danger">Cancelled</span>';  

                        }

                    }

                    else

                    {

                        $szSubscriptionDetails = "N/A";

                        $szStatus = "--";

                    }

                    

                    /*

                     * @Ajay - We are no longer displaying Active column on user listing page as we are now assuming all the user in the system is Active

                     * ($user['isActive'] == 1 ? '<span class="text-success"><i class="fa fa-check-circle"></i> Yes</span>' : '<span class="text-danger"><i class="fa fa-times-circle"></i> No</span>');                    

                     */

                    $response['data'][$i][0] = $user['szUserName'];

                    $response['data'][$i][1] = "{$user['szFirstName']} {$user['szLastName']}";

                    $response['data'][$i][2] = $user['szEmail'] . ' ' . ($user['isConfirmed'] ? '<i class="fa fa-check-circle text-success"></i>' : '<i class="fa fa-times-circle text-danger"></i>');

                    $response['data'][$i][3] = convert_date($user['dtCreatedOn'],3);

                    $response['data'][$i][4] = $szSubscriptionDetails;

                    if(!empty($szTitle))

                    {

                        $response['data'][$i][5] = '<a href="#" data-toggle="tooltip" data-title="'.$szTitle.'" style="text-decoration: none;">'.$szStatus.'</a>';

                    }

                    else

                    {

                        $response['data'][$i][5] = $szStatus;

                    } 

                    $response['data'][$i][6] = '<td class="table-action"><div class="btn-group"><a data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-cog"></i></a><ul role="menu" class="dropdown-menu pull-right">'.$szActionLinks.'</ul></div></td>';

                    $i++;				

                }

            }

            echo json_encode($response,true);

        }

        else

        {
			
			$data['szFavIcon'] = "fa-users";
            $data['szPageName'] = "Users";

            $data['szMetaTagTitle'] = "Users Administration";		

            $data['isLoadDataTable'] = true;

            $data['arLoginUser'] = $this->session->userdata('login_user');

            

            $data = array_merge($data, $arParams);



            $this->load->view('layout/header', $data);

            $this->load->view('users/list');

            $this->load->view('layout/footer');

        }

    }

    

    function edit($szUserKey='')

    {

        $this->add($szUserKey);

    }



    function add($szUserKey='')

    {

        if(!is_user_login())

        {

            redirect(base_url().'login', 'refresh');

        }

        else

        {

            // set/initialize varibales

            $data['idUser'] = 0;

            $data['isFormError'] = false;

            $data['arUserDetails'] = array();

            $data['isDetailsSaved'] = false;

            $szUserKey = trim($szUserKey);		

            $data['arLoginUser'] = $this->session->userdata('login_user');



            if($szUserKey != '')

            {

                $arUserDetails = $this->User_Model->getUserByUniqueKey($szUserKey);

                if(!empty($arUserDetails))

                {

                    $data['idUser'] = $arUserDetails['id'];

                    $data['arUserDetails'] = $arUserDetails;

                }

            }

               

            if($data['idUser'] > 0 || ($data['idUser'] == 0 && hasPermission('manage.users')))

            {

                if($this->input->post('p_func') == 'Save User Details')

                {

                    $idPostUser = (int)$this->input->post('arUser[id]');



                    if($idPostUser == 0 || ($idPostUser > 0 && $idPostUser == $data['idUser']))

                    {

                        // check role field only if logged in user has permiision to manage users

                        if(hasPermission('manage.users') && ($idPostUser == 0 || ($idPostUser > 0 && $idPostUser != $data['arLoginUser']['id'])))

                        {

                            $this->form_validation->set_rules('arUser[idRole]', 'User role', 'trim|required|integer|greater_than[0]');

                        }



                        $this->form_validation->set_rules('arUser[szFirstName]', 'First name', 'trim|required');

                        $this->form_validation->set_rules('arUser[szLastName]', 'Last name', 'trim|required');				

                        $this->form_validation->set_rules('arUser[szEmail]', 'Email address', "trim|required|valid_email|is_unique[tbl_users:szEmail:id:{$data['idUser']}]");

                        $this->form_validation->set_rules('arUser[szUSIUserName]', 'USI Username', 'trim|required|alpha_numeric');		

                        

                        if($data['idUser'] == 0)

                        {

                            $this->form_validation->set_rules('arUser[szUserName]', 'Username', "trim|required|alpha_numeric|is_unique[tbl_users:szUserName:id:{$data['idUser']}]");

                            $this->form_validation->set_rules('arUser[szPassword]', 'Password', "trim|required|min_length[6]|max_length[25]");

                            if(trim($this->input->post('arUser[szPassword]', true)) != '')

                            $this->form_validation->set_rules('arUser[szPasswordRepeat]', 'Confirm password', 'trim|required|min_length[6]|max_length[25]|matches[arUser[szPassword]]');

                        }

                        else

                        {

                            if($data['arLoginUser']['id'] != $data['idUser'])

                            {

                                $this->form_validation->set_rules('arUser[szUserName]', 'Username', "trim|required|alpha_numeric|is_unique[tbl_users:szUserName:id:{$data['idUser']}]");

                            }

                            $this->form_validation->set_rules('arUser[szPassword]', 'Password', "trim|min_length[6]|max_length[25]");

                            if(trim($this->input->post('arUser[szPassword]', true)) != '')

                            $this->form_validation->set_rules('arUser[szPasswordRepeat]', 'Confirm password', 'trim|min_length[6]|max_length[25]|matches[arUser[szPassword]]');

                        }



                        if ($this->form_validation->run() == true)

                        { 

                            $arUserDetails = $this->input->post('arUser', true);

                            // manage DOB

                            if(isset($arUserDetails['dtDOB']) && $arUserDetails['dtDOB'] != '')

                            {

                                $arDT = explode('/', $arUserDetails['dtDOB']);

                                $arUserDetails['dtDOB'] = "{$arDT[2]}-{$arDT[0]}-{$arDT[1]}";

                            }



                            // check if logged in user has permission to change the user role

                            if(!hasPermission('manage.users') || ($idPostUser > 0 && $idPostUser == $data['arLoginUser']['id']))

                            {

                                unset($arUserDetails['idRole']);

                                unset($arUserDetails['iActive']);

                            }



                            if(trim($arUserDetails['szPassword']) != '') 

                                $arUserDetails['szPassword'] = encrypt(trim($arUserDetails['szPassword']));

                            else

                                unset($arUserDetails['szPassword']);

                            unset($arUserDetails['szPasswordRepeat']);



                            if($data['idUser'] = $this->User_Model->saveUserDetails($arUserDetails, $data['arLoginUser']['id']))

                            {

                                $data['isDetailsSaved'] = true;

                                $data['arUserDetails'] = $this->User_Model->getUserByID($data['idUser']);



                                // check avatar image

                                $photo_data = trim($this->input->post('p_avatar_data'));

                                if($photo_data)

                                {

                                    list($type, $photo_data) = explode(';', $photo_data);

                                    list(, $photo_data)      = explode(',', $photo_data);

                                    list(, $photo_ext)		 = explode('/', $type);

                                    $photo_data = base64_decode($photo_data);

                                    $photo_ext = strtolower($photo_ext);



                                    $photo_image = "{$data['arUserDetails']['szFirstName']}-{$data['arUserDetails']['szLastName']}-".time().".{$photo_ext}";

                                    file_put_contents(USERS_UPLOAD_PATH . $photo_image, $photo_data);



                                    // delete the old file if exists

                                    if($data['arUserDetails']['szAvatarImage'] != '')

                                    {

                                        @unlink(USERS_UPLOAD_PATH . $data['arUserDetails']['szAvatarImage']);

                                    }



                                    // update the new image name

                                    $data['arUserDetails']['szAvatarImage'] = $photo_image;

                                    $this->User_Model->db->where('id', $data['idUser'])->update('tbl_users', array('szAvatarImage'=>$photo_image));

                                 }

                            }
							
							//unset any lang sessions and use profile default language
							$CI =& get_instance();
							$CI->session->unset_userdata('site_lang');
                        }

                        else

                        {

                            $data['isFormError'] = true;

                        }

                    }

                    else

                    {

                        $data['isFormError'] = "You can't edit details of another user while editing other.";

                    }

                }


				$data['szFavIcon'] = ($data['idUser'] > 0 ? "fa-edit" : "fa-plus");
                $data['szPageName'] = "Users";

                $data['szMetaTagTitle'] = ($data['idUser'] > 0 ? "Edit User Details" : "Add New User");	

                $data['isLoadBootBox'] = true;

                $data['isLoadAdminForm'] = true;

                $data['isLoadValidationScript'] = true;

                $data['isLoadCroppieScript'] = true;

                $data['isLoadDTPickerScript'] = true;



                // get avaible roles

                // get permissions

                $this->load->model('Role_Model');

                $data['arRoles'] = $this->Role_Model->getAllRoles();

                $data['arCountries'] = $this->User_Model->getCountries();



                $this->load->view('layout/header', $data);

                $this->load->view('users/add_edit');

                $this->load->view('layout/footer');

            }

            else

            {

                redirect(base_url() . 'dashboard');

            }

        }

    }

    

    function delete($szUserKey='')

    {  

        if(!is_user_login())

        {

            redirect(base_url().'login', 'refresh');

        }

        else if(!hasPermission('manage.users'))

        {

            redirect(base_url().'dashboard', 'refresh');

        }

        else

        {

            $data = array();

            $szUserKey = trim($szUserKey);

            if($szUserKey != '')

            {

                $arUserDetails = $this->User_Model->getUserByUniqueKey($szUserKey);

                if(!empty($arUserDetails))

                {

                    if((int)$arUserDetails['isRemovable'] == 1)

                    {

                        if((int)$arUserDetails['isDeleted'] == 0)

                        {

                            // get login user details

                            $arLoginUser = $this->session->userdata('login_user');

                            $idUser = (int)$arUserDetails['id'];

                            

                            /*

                            * Fetching subscription of the user

                            */

                            $arSubscription = array();

                            $arSubscription = $this->Membership_Model->getUserSubscription($idUser);

                              

                            $arTransactions = array();

                            $arTransactions = $this->Membership_Model->getUserTransactions($idUser);

                            

                            // delete the record

                            if($this->User_Model->deleteUser($arUserDetails, $arLoginUser['id']))

                            {

                                $data['szSuccessMessage'] = "User {$arUserDetails['szFirstName']} {$arUserDetails['szLastName']} has been deleted successfully.";

                                

                                if(!empty($arSubscription))

                                {

                                    //Deleting subscription data

                                    $this->Membership_Model->deleteCustomerSubscription($arSubscription);

                                } 

                                

                                if(!empty($arTransactions))

                                {

                                    /*

                                    * Deleting all the transactions of  the user

                                    */

                                    $arUpdate['idUser'] = $idUser; 

                                    $arUpdate['isDeleted'] = 1; 

                                    $this->Membership_Model->deleteUsereTransaction($arUpdate);

                                } 

                            

                                // delete the user profile picture as well

                                if($arUserDetails['szAvatarImage'] != '')

                                {

                                    @unlink(USERS_UPLOAD_PATH . $arUserDetails['szAvatarImage']);

                                }

                            }

                            else

                            {

                                $data['szErrorMessage'] = "Problem while deleting user {$arUserDetails['szFirstName']} {$arUserDetails['szLastName']}.";

                            }

                        }

                        else

                        {

                            $data['szErrorMessage'] = "User {$arUserDetails['szFirstName']} {$arUserDetails['szLastName']} is already deleted.";

                        }

                    }

                    else

                    {

                        $data['szErrorMessage'] = "User {$arUserDetails['szFirstName']} {$arUserDetails['szLastName']} is not removable.";

                    }

                }

                else
                {
                    $data['szErrorMessage'] = 'User record not exists.';
                }
            }

            else
            {
                $data['szErrorMessage'] = 'Invalid user delete request.';
            }
            $this->index($data);
        }
    }
	
    function details($szUserKey='')
    {
        if(!is_user_login()) {
            redirect(base_url().'login', 'refresh');
			
        } else {
			
            $data = array();
            $szUserKey = trim($szUserKey);
			
            if($szUserKey != '') {
                $arUserDetails = $this->User_Model->getUserByUniqueKey($szUserKey);
				
                if(!empty($arUserDetails)) {
                    $data['arUserDetails'] = $arUserDetails;
                    $data['arUserDetails']['dtLastLogin'] = $this->User_Model->getUserLastLogin($arUserDetails['id']);
                    
                    $arCountry = array();
					
                    if((int)$arUserDetails['idCountry'] > 0) {
                        $arCountry = $this->User_Model->getCountries($arUserDetails['idCountry']);                        
                    }
					
                    $data['arUserDetails']['szCountry'] = (!empty($arCountry) ? $arCountry[0]['szName'] : '');

                    // get activities
                    $data['arUserDetails']['activities'] = $this->User_Model->getUserActivity($arUserDetails['id']);
					$data['szFavIcon'] = "fa-user";
                    $data['szPageName'] = "Users";
                    $data['szMetaTagTitle'] = "User Details";		
                    $data['isLoadDataTable'] = true;
                    $data['arLoginUser'] = $this->session->userdata('login_user');

                    $this->load->view('layout/header', $data);
                    $this->load->view('users/details');
                    $this->load->view('layout/footer');
					
                } else {
					
                    $data['szErrorMessage'] = 'User record not exists.';
                }
				
            } else {
				
                $data['szErrorMessage'] = 'Invalid user delete request.';
				
            }
        }   
    }
	
    function profile()
    {
        if(!is_user_login()) {
            redirect(base_url().'login', 'refresh');
			
        } else {
			
            $data = array();
            $szUserID = $this->session->userdata('login_user');
			
			$this->load->model('Membership_Model');

			$data['arSubscription'] = $this->Membership_Model->getUserSubscription($szUserID['id']);
			$data['arTransactions'] = $this->Membership_Model->getUserTransactions($szUserID['id']);
			
			$arUserDetails = $this->User_Model->getUserByID($szUserID['id']);

			$data['arUserDetails'] = $arUserDetails;
			$data['arUserDetails']['dtLastLogin'] = $this->User_Model->getUserLastLogin($arUserDetails['id']);

			$arCountry = array();

			if((int)$arUserDetails['idCountry'] > 0) {
				$arCountry = $this->User_Model->getCountries($arUserDetails['idCountry']);                        
			}

			$data['arUserDetails']['szCountry'] = (!empty($arCountry) ? $arCountry[0]['szName'] : '');

			// Get Activities
			$data['arUserDetails']['activities'] = $this->User_Model->getUserActivity($arUserDetails['id']);
			
			// Define Page Details
			$data['szFavIcon'] = "fa-user";
			$data['szPageName'] = "Users";
			$data['szMetaTagTitle'] = "User Details";		
			$data['isLoadDataTable'] = true;
			$data['arLoginUser'] = $this->session->userdata('login_user');
                        
                        // check activity view
                        $szActivityAction = trim($this->input->get('view-activity'));
                        if($szActivityAction != '')
                        {
                            if($szActivityAction == 'all')
                            {
                                $this->db->where('idUser', $data['arLoginUser']['id'])->update('tbl_user_activity', array('isNew'=>0));
                            }
                            else if((int)$szActivityAction > 0)
                            {
                                $this->db->where('idUser', $data['arLoginUser']['id'])->where('id', (int)$szActivityAction)->update('tbl_user_activity', array('isNew'=>0));
                            }
                        }

			$this->load->view('layout/header', $data);
			$this->load->view('users/details', $data);
			$this->load->view('layout/footer');
				
		}
    }
}
?>