
<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="WHAT'S INSIDE: The System and Strategy Our Team is Using To Make More Money With Bitcoin.">
    <meta name="keywords" content="multiply bitcoin, bitcoin multiplier">
    <meta name="author" content="">

    <title>Grow Your Bitcoin Up To 1% Per Day</title>

    <!-- Bootstrap core CSS -->
    <link href="https://bitcoinlabs.io/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://bitcoinlabs.io/assets/css/home.css" rel="stylesheet">
    
    <!-- FontAwesome core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    
    <style>
		footer small a {color: #666;}
			footer small a:hover {color: #555;}
		
		/* Responsive Video Frame Styles */
		.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; border: 3px #333 solid; }

		/* New Custom Button Gold */
		.btn {
			padding: 14px 24px;
			border: 0 none;
			font-weight: 700;
			letter-spacing: 1px;
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			text-transform: uppercase;}
		.btn:focus, .btn:active:focus, .btn.active:focus {
			outline: 0 none;}
		.btn-cta {
			color: rgb(0, 0, 0);
			background: linear-gradient(135deg, rgb(255, 204, 51) 20%, rgb(255, 179, 71) 80%);}
		.btn-cta:hover, .btn-cta:focus, .btn-cta:active, .btn-cta.active, .open > .dropdown-toggle.btn-cta {
			color: #444444;}
		.btn-cta:active, .btn-cta.active {
			box-shadow: none;}
		
		/* Custom Class' for Elements */
		.fa-check {color: green;}
		.fa-times {color: red;}
		.sticky {position: fixed; bottom: 0; width: 100%; z-index: 9999;}
		.op-9 {opacity: .9;}
		.bg-grey {background-color: #eeeeee;}
		.bg-gold {background-color: #f6a91a;}
		.text-white {color: #ffffff;}
		.text-gold {color: #f6a91a;}
			.text-gold strong {text-decoration:  underline;}
		a:hover {color: #F8B73C;}
		.mask-dark {
			background-color: rgba(0,0,0,0.6);
			background-size: cover;
			width: 100%;
		}
		.mask-xdark {
			background-color: rgba(0,0,0,0.9);
			background-size: cover;
			width: 100%;
		}
		.mask-light {
			background-color: rgba(255,255,255,255.6);
			background-size: cover;
			width: 100%;
		}
		img {max-width: 100%;}
		
		/* Styles for Overview Section */
		section#overview {
			background: url('https://bitcoinlabs.io/assets/images/bitcoin_multiplier.jpg') bottom center no-repeat fixed;
			background-size: cover;
		}
		
		/* Styles for Strategy Section */
		section#strategy {
			
		}
		
		/* Styles for Features Section */
		section#features {
			
		}
		section#features h4 {
			margin-top: 5px;
		}
		
		/* Sticky CTA Style */
		section#cta {
			border-top: 2px #f6a91a solid;
		}
		
		/* Custom Blockquote Styling */
		blockquote{
		  display:block;
		  background: #fff;
		  padding: 15px 20px 15px 45px;
		  margin: 0 0 20px;
		  position: relative;

		  /*Font*/
		  font-family: Georgia, serif;
		  font-size: 16px;
		  line-height: 1.2;
		  color: #666;
		  text-align: justify;

		  /*Borders - (Optional)*/
		  border-left: 15px solid #f6a91a;
		  border-right: 2px solid #f6a91a;

		  /*Box Shadow - (Optional)*/
		  -moz-box-shadow: 2px 2px 15px #ccc;
		  -webkit-box-shadow: 2px 2px 15px #ccc;
		  box-shadow: 2px 2px 15px #ccc;
		}

		blockquote::before{
		  content: "\201C"; /*Unicode for Left Double Quote*/

		  /*Font*/
		  font-family: Georgia, serif;
		  font-size: 60px;
		  font-weight: bold;
		  color: #999;

		  /*Positioning*/
		  position: absolute;
		  left: 10px;
		  top:5px;
		}

		blockquote::after{
		  /*Reset to make sure*/
		  content: "";
		}

		blockquote a{
		  text-decoration: none;
		  background: #eee;
		  cursor: pointer;
		  padding: 0 3px;
		  color: #c76c0c;
		}

		blockquote a:hover{
		 color: #666;
		}

		blockquote em{
		  font-style: italic;
		}
		
		/* Price Table Styles */
		.panel
		{
			text-align: center;
		}
		.panel:hover { box-shadow: 0 1px 5px rgba(0, 0, 0, 0.4), 0 1px 5px rgba(130, 130, 130, 0.35); }
		.panel-body
		{
			padding: 0px;
			text-align: center;
		}

		.the-price
		{
			background-color: rgba(220,220,220,.17);
			box-shadow: 0 1px 0 #dcdcdc, inset 0 1px 0 #fff;
			padding: 20px;
			margin: 0;
		}

		.the-price h1
		{
			line-height: 1em;
			padding: 0;
			margin: 0;
		}

		.subscript
		{
			font-size: 25px;
		}

		/* CSS-only ribbon styles    */
		.cnrflash
		{
			/*Position correctly within container*/
			position: absolute;
			top: -9px;
			right: 4px;
			z-index: 1; /*Set overflow to hidden, to mask inner square*/
			overflow: hidden; /*Set size and add subtle rounding  		to soften edges*/
			width: 100px;
			height: 100px;
			border-radius: 3px 5px 3px 0;
		}
		.cnrflash-inner
		{
			/*Set position, make larger then 			container and rotate 45 degrees*/
			position: absolute;
			bottom: 0;
			right: 0;
			width: 145px;
			height: 145px;
			-ms-transform: rotate(45deg); /* IE 9 */
			-o-transform: rotate(45deg); /* Opera */
			-moz-transform: rotate(45deg); /* Firefox */
			-webkit-transform: rotate(45deg); /* Safari and Chrome */
			-webkit-transform-origin: 100% 100%; /*Purely decorative effects to add texture and stuff*/ /* Safari and Chrome */
			-ms-transform-origin: 100% 100%;  /* IE 9 */
			-o-transform-origin: 100% 100%; /* Opera */
			-moz-transform-origin: 100% 100%; /* Firefox */
			background-image: linear-gradient(90deg, transparent 50%, rgba(255,255,255,.1) 50%), linear-gradient(0deg, transparent 0%, rgba(1,1,1,.2) 50%);
			background-size: 4px,auto, auto,auto;
			background-color: #aa0101;
			box-shadow: 0 3px 3px 0 rgba(1,1,1,.5), 0 1px 0 0 rgba(1,1,1,.5), inset 0 -1px 8px 0 rgba(255,255,255,.3), inset 0 -1px 0 0 rgba(255,255,255,.2);
		}
		.cnrflash-inner:before, .cnrflash-inner:after
		{
			/*Use the border triangle trick to make  				it look like the ribbon wraps round it's 				container*/
			content: " ";
			display: block;
			position: absolute;
			bottom: -16px;
			width: 0;
			height: 0;
			border: 8px solid #800000;
		}
		.cnrflash-inner:before
		{
			left: 1px;
			border-bottom-color: transparent;
			border-right-color: transparent;
		}
		.cnrflash-inner:after
		{
			right: 0;
			border-bottom-color: transparent;
			border-left-color: transparent;
		}
		.cnrflash-label
		{
			/*Make the label look nice*/
			position: absolute;
			bottom: 0;
			left: 0;
			display: block;
			width: 100%;
			padding-bottom: 5px;
			color: #fff;
			text-shadow: 0 1px 1px rgba(1,1,1,.8);
			font-size: 0.95em;
			font-weight: bold;
			text-align: center;
		}
		
		/* Responsive Styling */
		@media screen and (max-width: 480px) {
			p {font-size: .8rem;}
			h1 {font-size: 1.5rem;}
			h2 {font-size: 1.3rem;}
			h3 {font-size: 1.1rem;}
			h4 {font-size: .9rem;}
			blockquote {font-size: .5rem;}
			small {font-size: 65%;}
			section#cta.sticky p {font-size: .5em; display: none;}
		}
		
		.modal {
		  text-align: center;
		}

		@media screen and (min-width: 768px) { 
		  .modal:before {
			display: inline-block;
			vertical-align: middle;
			content: " ";
			height: 100%;
		  }
		}

		.modal-dialog {
		  display: inline-block;
		  text-align: left;
		  vertical-align: middle;
		}
	</style>

  </head>

  <body>
       
	<!-- Promo Video
	====================================================== -->
	<section id="overview">
		<div class="mask-dark">
			<div class="container text-center text-white pt-4 pb-4">
				<h3><strong class="text-gold" style="text-decoration: underline;">ATTENTION</strong>: For Those Who Are Serious About Crypto Mining &amp; Trading</h3>
				<h1>What You Need To Know About <strong class="text-gold">Multiplying Your Bitcoin</strong></h1>
				<div class='embed-container mt-4'><iframe src='https://player.vimeo.com/video/243732903?autoplay=0' frameborder='0'></iframe></div>
			</div>
		</div> 
	</section>
	<!-- =================================================
	Promo Video End -->
	
	<!-- Divider Section ========================= -->
	<section id="divider" class="bg-gold py-2"></section>
    
    <!-- Section CTA Start-->
    <section id="cta">
		<div class="mask-xdark">
		  <div class="container pt-3 pb-3 text-white text-center">
		  	<div class="row">
				<div class="col-lg-7 pb-0">
					<h4><strong class="text-gold">WHAT'S INSIDE</strong>: The System and Strategy My Team is Using To Make More Money With Bitcoin</h4>
					<small><a class="text-gold" href="#">Wait a Minute.. What's Bitcoin?</a></small>
				</div>
				
				<div class="col-lg-5 pt-1">
					<a class="btn btn-cta" data-toggle="modal" data-target=".bs-example-modal-lg" href="javascript:void(0);"><i class="fa fa-line-chart" aria-hidden="true"></i> YES! Please Teach Me Now!</a><br>
					<small style="font-size: 60%;">You will be asked to enter your contact details in order to gain access.</small>
				</div>
			</div>
			<small class="text-center" style="color: #999; font-size: 65%; display: none;"><strong>Earnings Disclaimer</strong>: <em>Any income or earnings statements are estimates of income potential only, and there is no assurance that your earnings will match the figures presented, which are given as examples.</em></small>
		  </div>
		</div>
    </section>
    
	<!-- START: CTA Modal Set-->
	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-3 text-center">
						<img alt="" src="http://bitcoinlabs.io/<?php echo $sponsor['avatar'] == '' ? "assets/images/profile.png" : "uploads/users/".$sponsor['avatar']; ?>" class="thumbnail media-object" style="border: 2px #666 solid; border-radius: 50%; padding: 2px;">
						<small><strong><?php echo $sponsor['fname'] ?> <?php echo $sponsor['lname'] ?></strong></small>
					</div>

					<div class="col-lg-9 text-center">
						<h2>Discover What You Need To Know To Crush It with Cryptocurrency</h2>
						<h4>Enter your details below to get instant access:</h4>
						<form name="apiform" id="apiform" method="POST" data-toggle="validator" role="form" class="form-horizontal">
							<div class="form-group">
								<div class="row row-pad-5">
									<div class="col-lg-5 col-sm-12">
									  <input type="input" name="fname" placeholder="First Name" class="form-control input-lg">
									</div>
									<div class="col-lg-7 col-sm-12">
									  <input type="input" name="lname" placeholder="Last Name" class="form-control input-lg">
									</div>
									<div class="col-lg-12">
										<input class="form-control input-lg" type="email" name="email" placeholder="Email Address" style="" data-error="Bruh, that email address is invalid" required>
									</div>

									<div class="col-lg-12">
										<input class="form-control input-lg" type="phone" name="phone" placeholder="Phone Number" style="" required>
									</div>
								</div>
							</div>
							<input type="hidden" name="tag[]" value="multiplymybtc">
							<input type="hidden" name="campaign" value="OrQemsr9zLKP5KaGYOuIOST3hZ8zMD">
							<input type="hidden" name="userID" value="<?php echo $sponsor['username']; ?>">
							<button id="submit" type="submit" class="btn btn-primary btn-lg text-center" style="margin-top: 15px; padding: 15px 40px"><strong>Accept Invitation</strong> <i class="fa fa-arrow-right"></i></button>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	  </div>
	</div>
	<!-- END: CTA Modal Set-->
   
    <!-- Secion CTA End 
    ====================================================== -->
    
    <!-- Footer -->
    <footer id="footer" class="py-5 bg-dark">
      <div class="container">
        <div class="logo text-center mb-2"><img src="https://bitcoinlabs.io/assets/images/logo_beta.png" alt=""></div>
        <p class="m-0 text-center text-white">&copy; 2017 Bitcoin Labs. All Rights Reserved</p>
        <div class="text-center"><small><a href="/terms/">Terms</a> - <a href="/privacy/">Privacy</a> - <a target="_blank" href="https://bitcoinlabs.io/ref/<?php echo $sponsor['username'] ?>">Get a Site Like This!</a></small>
        </div>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
	<script src="https://bitcoinlabs.io/assets/vendor/jquery/jquery.min.js"></script>
    <script src="https://bitcoinlabs.io/assets/vendor/popper/popper.min.js"></script>
    <script src="https://bitcoinlabs.io/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    
	<script>
		
	/* Disable Text Selection */
	(function($){

	  $.fn.ctrlCmd = function(key) {

		var allowDefault = true;

		if (!$.isArray(key)) {
		   key = [key];
		}

		return this.keydown(function(e) {
			for (var i = 0, l = key.length; i < l; i++) {
				if(e.keyCode === key[i].toUpperCase().charCodeAt(0) && e.metaKey) {
					allowDefault = false;
				}
			};
			return allowDefault;
		});
	};


	$.fn.disableSelection = function() {

		this.ctrlCmd(['a', 'c']);

		return this.attr('unselectable', 'on')
				   .css({'-moz-user-select':'-moz-none',
						 '-moz-user-select':'none',
						 '-o-user-select':'none',
						 '-khtml-user-select':'none',
						 '-webkit-user-select':'none',
						 '-ms-user-select':'none',
						 'user-select':'none'})
				   .bind('selectstart', false);
	};

	})(jQuery); $(':not(input,select,textarea)').disableSelection();

	//form control
	$(document).ready(function(){
		$(function(){
			$('#apiform').on('submit',function(event){
			   event.preventDefault();
			   event.stopPropagation();

				var values = {};
				$.each($('#apiform').serializeArray(), function(i, field) {

					values[field.name] = field.value;
					//alert(field.name);
					//alert(field.value);
				});

				var dataString = "fname=" + values['fname'] + "&lname=" + values['lname'] + "&email=" + values['email'] + "&phone=" + values['phone'] + "&campaign=" + values['campaign'] + "&tag[]=" + values['tag[]'] + "&userID=" + values['userID'];

				//var dataString = JSON.stringify(values);
				//alert(dataString);

				$.ajax({
					//contentType: "application/json",
					//dataType: "json",
					type: "POST",
					url: "/api/lead",
					data: dataString,
					beforeSend: function(data) {
						$("#submit").attr("disabled", "disabled");
						$("#submit").text("Processing your request..");
					},
					success: function(data) {
						console.log(data);
						data = JSON.parse(data);

						if (data["result"] == "success") {
							window.location = "https://<?php echo $sponsor['usilink'] ?>.usitech-int.com/";
							//$('#cta').fadeOut(0);
							//$('#video-group').removeClass('hidden');
						} else {
							alert(data['message']);
							$("#submit").text('Try Again');
						}

						//$('.cover-heading').removeAttr('style', 'background-color');
						//alert('success' + data);
						//player_1.api('play');
						//$("span.fname").html(values['fname']);
					},
					error: function (jqXHR, textStatus, errorThrown) {
						//alert('error' + textStatus).delay(1000);
						$("#submit").text("Oops, please try again..");
					},
					complete: function(){
						$("#submit").removeAttr("disabled", "disabled");
					}
				});
			});
		});
	});
	</script>

  </body>
</html>