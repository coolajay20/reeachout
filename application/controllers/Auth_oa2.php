<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_oa2 extends CI_Controller
{
    public function session($provider_name)
    { 
        $this->load->library('session');
        $this->load->helper('url_helper'); 
        $this->load->library('oauth2/OAuth2');
        $this->load->library('tank_auth');
        $this->load->model('User_Model');
        $this->load->model('Email_Model');
        $this->load->config('oauth2', TRUE);

        $provider = $this->oauth2->provider($provider_name, array(
            'id' => $this->config->item($provider_name.'_id', 'oauth2'),
            'secret' => $this->config->item($provider_name.'_secret', 'oauth2'),
        ));


        if ( ! $this->input->get('code'))
        {
            // By sending no options it'll come back here
            $provider->authorize();
        }
        else
        { 
            try
            { 
                $token = $provider->access($this->input->get('code'));
                
                $arFacebookDetails = array();
                $arFacebookDetails = $provider->get_user_info($token);
 
                if(!empty($arFacebookDetails['email']))
                { 
                    $arUserDetails = array();
                    $arUserDetails = $this->User_Model->getUserByLogin($arFacebookDetails['email']);
                    
                    if(!empty($arUserDetails))
                    {
                        //User already exists. Autologin user into our system
                        $this->User_Model->loginUserBySocialMedia($arUserDetails);
                        redirect(base_url().'dashboard', 'refresh');
                    }
                    else
                    {
                        $szPassword = generateAlphaNumericString(8);
                        //User email doesn't exists in our system. Try creating new profile for customer
                        $arAddUserAry = array();
                        $arAddUserAry['szFirstName'] = $arFacebookDetails['first_name'];
                        $arAddUserAry['szLastName'] = $arFacebookDetails['last_name'];
                        $arAddUserAry['szEmail'] = $arFacebookDetails['email'];
                        $arAddUserAry['szUserName'] = $arFacebookDetails['email'];
                        $arAddUserAry['szPassword'] = encrypt($szPassword);
                        $arAddUserAry['iShortSignup'] = 1;
                        $arAddUserAry['isActive'] = 1;
                        
                        if(strtolower($provider_name)=='facebook')
                        {
                            $arAddUserAry['iFacebookLogin'] = 1;
                            if(isset($arUserDetails['urls']['Facebook']) && !empty($arUserDetails['urls']['Facebook']))
                            {
                                $arAddUserAry['szFacebookLink'] = $arUserDetails['urls']['Facebook'];
                            }
                        }
                        else if(strtolower($provider_name)=='google')
                        {
                            $arAddUserAry['iGoogleLogin'] = 1;
                        } 
                        if(!empty($arFacebookDetails['dtDateofBirth']))
                        {
                           $arAddUserAry['dtDOB'] = $arFacebookDetails['dtDateofBirth'];
                        }
                        
                        //Adding customer details into database
                        $this->User_Model->saveUserDetails($arAddUserAry,1);
                        
                        //Sending welcome email to customer
                        $arEmailData = array();
                        $arEmailData['szFirstName'] = $arFacebookDetails['first_name'];
                        $arEmailData['szEmail'] = $arFacebookDetails['email'];
                        $arEmailData['szPassword'] = $szPassword;
                        $arEmailData['szSiteName'] = WEBSITE_NAME; 
                        $arEmailData['szLoginPageUrl'] = base_url().'login';
                        
                        //Sending welcome E-mail to customer
                        $this->Email_Model->sendSystemEmails($arEmailData,'WELCOM_EMAIL');
                        
                        //Fetching newly added user details by email address
                        $arUserDetails = array();
                        $arUserDetails = $this->User_Model->getUserByLogin($arFacebookDetails['email']); 
                        if(!empty($arUserDetails))
                        {
                            //Autologin user into our system
                            $this->User_Model->loginUserBySocialMedia($arUserDetails); 
                            if(is_user_login())
                            {
                                redirect(base_url().'dashboard', 'refresh');
                            }
                        }
                        else
                        { 
                            redirect(base_url().'login', 'refresh');
                        }
                    }
                }
                else
                { 
                    redirect(base_url().'login', 'refresh');
                }
            } 
            catch (OAuth2_Exception $e)
            {
                show_error('That didnt work: '.$e);
            } 
        }
    } 

    /**
     * Send email message of given type (activate, forgot_password, etc.)
     *
     * @param	string
     * @param	string
     * @param	array
     * @return	void
     */
    function _send_email($type, $email, &$data)
    {
        $this->load->library('email');
        $this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
        $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
        $this->email->to($email);
        $this->email->subject(sprintf($this->lang->line('auth_subject_'.$type), $this->config->item('website_name', 'tank_auth')));
        $this->email->message($this->load->view('email/'.$type.'-html', $data, TRUE));
        $this->email->set_alt_message($this->load->view('email/'.$type.'-txt', $data, TRUE));
        $this->email->send();
    } 
}

/* End of file auth_oa2.php */
/* Location: ./application/controllers/auth_oa2.php */