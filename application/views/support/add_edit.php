<?php if($isDetailsSaved){?>

<div class="alert alert-success fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h2><strong>Your ticket has been submitted successfully!</strong></h2>
	<p>Our agents typically repond within 24 hours, please be patient if it takes longer than expected.</p>
</div>

<?php } if($isFormError=== TRUE){?>

<div class="alert alert-danger fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>Oops!</h4>
	<p>Please fix the errors and try again..</p>
</div>

<?php } else if($isFormError != ''){?>

<div class="alert alert-danger fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>Oops!</h4>
	<p><?php echo $isFormError;?></p>
</div>

<?php } ?>

<div class="contentpanel">	       
    <div class="row"> 
        <div class="col-sm-12 col-lg-5">
          <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Submit a Ticket</h3>
                <p>Use the form below to request assistance:</p>
            </div><!-- panel-heading -->
            
             <div class="panel-body">
                <form method="post" action="<?php echo base_url();?>support/<?php #echo ($idTicket > 0 ? "edit/{$arTicketDetails['szUniqueKey']}" : "add");?>" id="formSirTrevor" class="validate-form">
                    <?php  if($isAdmin){ ?>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <?php $idCustomer = set_input_value('arTicket[idUser]', (isset($arTicketDetails['idUser']) ? $arTicketDetails['idUser'] : ''));?>
                                    <label for="idCustomer" class="field-label text-muted fs16 mb10 col-md-12">Customer:</label>
                                    <select name="arTicket[idUser]" id="idUser" class="form-control mb15" placeholder="Customer Name">
                                        <option value="">Select</option>
                                        <?php
                                            if(!empty($arUserDetails))
                                            {
                                                foreach($arUserDetails as $arUserDetailss)
                                                {
                                                    ?>
                                                    <option value="<?php echo $arUserDetailss['id']; ?>" <?php echo ($arUserDetailss['id'] == $idCustomer ? 'selected' : '');?>><?php echo $arUserDetailss['szFirstName']." ".$arUserDetailss['szLastName']." (".$arUserDetailss['szEmail'].")"?></option> 
                                                    <?php
                                                }
                                            }
                                        ?> 
                                    </select> 
                                </div>
                            </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <?php $szType = set_input_value('arTicket[szType]', (isset($arTicketDetails['szType']) ? $arTicketDetails['szType'] : ''));?>
                            <label for="szType" class="field-label text-muted fs16 mb10 col-md-12">Type:</label>
                            <select name="arTicket[szType]" id="szType" class="form-control mb15" placeholder="Status">
                                <option value="0" <?php echo ($szType == 0 || $szType == '' ? 'selected' : '');?>>General Inquiries</option>
                                <option value="1" <?php echo ($szType == 1 && $szType != '' ? 'selected' : '');?>>Technical Assistance</option>
                                <option value="2" <?php echo ($szType == 2 && $szType != '' ? 'selected' : '');?>>Billing</option>
                                <option value="3" <?php echo ($szType == 3 && $szType != '' ? 'selected' : '');?>>Improvements</option>
                                <option value="4" <?php echo ($szType == 4 && $szType != '' ? 'selected' : '');?>>Bugs</option>
                            </select> 
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <?php $szPriority = set_input_value('arTicket[szPriority]', (isset($arTicketDetails['szPriority']) ? $arTicketDetails['szPriority'] : ''));?>
                            <label for="szPriority" class="field-label text-muted fs16 mb10 col-md-12">Priority:</label>
                            <select name="arTicket[szPriority]" id="szType" class="form-control mb15" placeholder="Priority">
                                <option value="0" <?php echo ($szPriority == 0 || $szPriority == '' ? 'selected' : '');?>>Normal</option>
                                <option value="1" <?php echo ($szPriority == 1 && $szPriority != '' ? 'selected' : '');?>>Medium</option>
                                <option value="2" <?php echo ($szPriority == 2 && $szPriority != '' ? 'selected' : '');?>>High</option>
                                <option value="3" <?php echo ($szPriority == 3 && $szPriority != '' ? 'selected' : '');?>>Urgent</option>
                            </select> 
                        </div>
                    </div>								

                    <div class="row">
                        <label for="szMessage" class="field-label text-muted fs16 mb10 col-md-12">How can we help?</label>
                        <?php $szMessageError = form_error('arTicket[szMessage]');?>
                        <label for="szMessage" class="field prepend-icon col-md-12<?php if(!empty($szMessageError)){?> state-error<?php }?>">
                          <textarea type="text-area" name="arTicket[szMessage]" id="szMessage" class="form-control" placeholder="Please be detailed.." value="<?php echo set_input_value('arTicket[szMessage]', (isset($arTicketDetails['szMessage']) ? $arTicketDetails['szMessage'] : ''));?>" required></textarea>
                        </label>
                        <?php if(!empty($szMessageError)){?><span class="text-danger" for="szMessage"><?php echo $szMessageError;?></span><?php }?>
                    </div> 
                    <div class="admin-form">
                        <button type="submit" class="btn btn-primary">Submit Ticket</button>
                        <input type="hidden" name="p_func" value="Save Ticket Details">
                    </div>
                </form> 
            </div><!-- panel-body -->
          </div><!-- panel -->
        </div>
        <?php  if(!$isAdmin){ ?>
        <div class="col-sm-12 col-lg-7">
          <div class="panel panel-default">
            <div class="panel-heading">
                <h3>My Tickets:</h3>
            </div><!-- panel-heading -->
            
             <div class="panel-body">
				<div class="table-responsive">
					<table class="table table-primary table-tickets">
                                            <thead>
                                                <tr>
                                                    <th>Type</th>
                                                    <th>Message</th>
                                                    <th>Date</th>
                                                    <th>Priority</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <?php 
                                                    if(!empty($arTickets)){
                                                                           
                                                        foreach($arTickets as $ticket){
                                                            $szActionLinks = '<li><a href="'.base_url()."support/ticket/".$ticket['szUniqueKey'].'">View/Reply</a></li>';   
                                                            $szLinks = '<div class="btn-group"><a data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-cog"></i></a><ul role="menu" class="dropdown-menu pull-right">'.$szActionLinks.'</ul></div>'; 
                                                    ?>
							<tr>
								<td><?php				 
                                                                        switch ($ticket['szType']) {
                                                                           case "0" :
                                                                              $ticket['szType'] = "General Inquiry";
                                                                              $fa = "fa-info";
                                                                              break;
                                                                           case "1" :
                                                                              $ticket['szType'] = "Technical Assistance";
                                                                              $fa = "fa-cogs";
                                                                              break;
                                                                           case "2":
                                                                              $ticket['szType'] = "Billing / Refunds";
                                                                              $fa = "fa-dollar";
                                                                              break;
                                                                           case "3":
                                                                              $ticket['szType'] = "Improvements";
                                                                              $fa = "fa-magic";
                                                                              break;
                                                                           case "4":
                                                                              $ticket['szType'] = "Bug";
                                                                              $fa = "fa-bug";
                                                                              break;
                                                                        } 
									echo '<i class="fa '.$fa.' tooltips" data-toggle="tooltip" title="" data-original-title="'.$ticket['szType'].'"></i>';
                                                                    ?>
								</td>
								<td><?php echo $string = substr($ticket['szMessage'],0); ?></td>
								<td><?php echo convert_date($ticket['dtAddedOn'],4);?></td>
								<td><?php					 
									 switch ($ticket['szPriority']) {
										 case "0" :
											 echo "Normal";
											 break;
										 case "1" :
											 echo "Medium";
											 break;
										 case "2":
											 echo "High";
											 break;
										 case "3":
											 echo "Urgent";
											 break;												 
									 } 
									?>
								</td>
								<td><?php					 
									 switch ($ticket['szStatus']) {
										 case "0" :
											 echo "Closed";
											 break;
										 case "1" :
											 echo "New";
											 break;
										 case "2":
											 echo "Pending";
											 break;
									 } 
									?>
								</td>
                                                                <td><?php echo $szLinks; ?></td>
							</tr>
                                                    <?php $i++; } } ?>
						</tbody>
					</table>
				</div><!-- table-responsive -->    
				             
            </div><!-- panel-body -->
          </div><!-- panel -->
        </div><!-- col-sm-3 -->
        <?php } ?>
	</div><!-- row -->
</div> 