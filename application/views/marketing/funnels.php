		<div class="row">
			<!-- Personal Replicated Website & Blog -->
			<div class="panel panel-alt panel-border top mb15">
				<div class="panel-heading">
					
				</div>
				<div class="panel-body">
					<div class="col-md-5 text-center">
						<img style="width: 100%" src="<?php echo base_url()?>assets/images/marketing/funnels_preview-multiplymybtc.png" alt="">
					</div>
					<div class="col-md-7 text-center">
						<h2>Client/Partner Acquisition Funnel</h2>
						<p>Attract your prospects and peak their interest using this sales funnel which captures their contact details, automatically subscribes them to a lead nurturing email campaign and sends them to watch an opportunity presentation on USI Tech.</p>
						<p><b>Your Link: </b><a target="_blank" class="block nowrap" href="http://multiplymybtc.com/ref/<?php echo $arLoginUser['user_name'] ?>">http://multiplymybtc.com/ref/<?php echo $arLoginUser['user_name'] ?></a> </p>
						<p>
						<a href="#" class="btn btn-primary btn-sm disabled"><i class="fa fa-gear"></i>&nbsp;&nbsp;Edit</a>
						<a target="_blank" href="http://multiplymybtc.com/ref/<?php echo $arLoginUser['user_name'] ?>" class="btn btn-success btn-sm">Preview Site &nbsp;<i class="fa fa-eye"></i></a>
						</p>
						<small>Visitor Analytics <strong>Enabled</strong></small>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<!-- Personal Replicated Website & Blog -->
			<div class="panel panel-alt panel-border top mb15">
				<div class="panel-heading">
					
				</div>
				<div class="panel-body">
					<div class="col-md-5 text-center">
						<img style="width: 100%" src="<?php echo base_url()?>assets/images/marketing/funnels_preview-usitechio.png" alt="">
					</div>
					<div class="col-md-7 text-center">
						<h2>Your Replicated Website &amp; Blog</h2>
						<p>This is a professionally built content marketing blog that is personalized to you! &nbsp; The website will display your Name and Telephone number at the top of every page with a call to action that will take them to a registration page for them to sign up. The blog articles all display your full name, picture and phone number. &nbsp;You can share the main URL or any article with your special link.</p>
						<p><b>Your Link: </b><a target="_blank" class="block nowrap" href="http://usitech.io/?id=<?php echo $arLoginUser['user_name'] ?>">http://usitech.io/?id=<?php echo $arLoginUser['user_name'] ?></a> </p>
						<p>
						<a target="_blank" href="http://usitech.io/?id=<?php echo $arLoginUser['user_name'] ?>" class="btn btn-success btn-sm">Preview Site &nbsp;<i class="fa fa-eye"></i></a>
						</p>
						<small>Visitor Analytics <strong>Disabled</strong></small>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<!-- Video Presentaiton Capture Page -->
			<div class="col-md-12 text-center">
				<div class="panel panel-alt panel-border top mb15">
					<div class="panel-heading">
						
					</div>
					<div class="panel-body">
						<div class="col-md-5 text-center">
							<img style="width: 100%" src="<?php echo base_url()?>assets/images/marketing/funnels_preview-usitechio-invite.png" alt="">
						</div>
					<div class="col-md-7 text-center">
						<h2>USI Tech Invitation w/Capture Page</h2>
						<p>This will peak your prospects intrest by exposing them to a video presentation containing a form to capture their name, email address and phone number which will be added to your contact manager and redirect them to the GST page.</p>
						<p><b>Your Link: </b><a target="_blank" class="block nowrap" href="http://usitech.io/invite/?id=<?php echo $arLoginUser['user_name'] ?>">http://usitech.io/invite/?id=<?php echo $arLoginUser['user_name'] ?></a> </p>
						<p><a target="_blank" href="http://usitech.io/invite/?id=<?php echo $arLoginUser['user_name'] ?>" class="btn btn-success btn-sm">Preview Site &nbsp;<i class="fa fa-eye"></i></a></p>
						<small>Visitor Analytics <strong>Disabled</strong></small>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<!-- Getting Started Training -->
			<div class="col-md-12 text-center">
				<div class="panel panel-alt panel-border top mb15">
					<div class="panel-heading">
						
					</div>
					<div class="panel-body">
						<div class="col-md-5 text-center">
							<img style="width: 100%" src="<?php echo base_url()?>assets/images/marketing/funnels_preview-usitechio-gst.png" alt="">
						</div>
						<div class="col-md-7 text-center">
							<h2>USI Getting Started Training</h2>
							<p>This will educate and motivate your new prospects as they are presented with a step-by-step guide on how to get started.</p>
							<p><b>Your Link: </b><a target="_blank" class="block nowrap" href="http://usitech.io/gst/?id=<?php echo $arLoginUser['user_name'] ?>">http://usitech.io/gst/?id=<?php echo $arLoginUser['user_name'] ?></a> </p>
							<p><a target="_blank" href="http://usitech.io/gst/?id=<?php echo $arLoginUser['user_name'] ?>" class="btn btn-success btn-sm">Preview Site &nbsp;<i class="fa fa-eye"></i></a></p>
							<small>Visitor Analytics <strong>Disabled</strong></small>
						</div>
					</div>
					
				</div>
			</div>
		</div><!-- row -->