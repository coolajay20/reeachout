<!DOCTYPE html>

<html>



<head>

  <!-- Meta, title, CSS, favicons, etc. -->

  <meta charset="utf-8">

  <title><?php if(isset($szMetaTagTitle)) echo $szMetaTagTitle; else echo "Bitcoin Labs";?></title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">



  <!-- Font CSS (Via CDN) -->

  <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>



  <!-- Theme CSS -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/skin/default_skin/css/theme.css">

  

  <!-- Admin Forms CSS -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/css/admin-forms.css">



  <!-- Favicon -->

  <<link rel="shortcut icon" href="<?php echo base_url();?>assets/frontend/img/favicon.png">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!--[if lt IE 9]>

    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

    <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->

</head>



<body class="admin-validation-page external-page external-alt sb-l-c sb-r-c">

  <!-- Start: Main -->

  <div id="main" class="animated fadeIn">



    <!-- Start: Content-Wrapper -->

    <section id="content_wrapper">



      <!-- Begin: Content -->

      <section id="content">



        <div class="admin-form theme-primary mw600" style="margin-top: 3%;" id="register">



          <!-- Registration Logo -->

          <div class="row table-layout">

            <img src="<?php echo base_url();?>assets/images/logo.png" title="<?php echo WEBSITE_NAME; ?>" class="center-block img-responsive" style="max-width: 275px;">

          </div>

          

          <?php if(isset($isConfirmLinkExpired) && $isConfirmLinkExpired == true){?>

		  <div class="alert alert-danger" style="margin-top: 3%;">

          	<i class="fa fa-remove pr10"></i>

           	<strong> Oops, something didn't go right..</strong>

           	Your last activation link has expired. Please <a href="<?php echo base_url();?>resendActivationLink/<?php echo $szUserKey?>">click here</a> to get the activation link again. Please remember that this link will be valid only for next 24 hours.

     	  </div>

          <?php } else if(isset($isConfirmationLinkAlreadySent) && $isConfirmationLinkAlreadySent == true){?>

          <div class="alert alert-danger" style="margin-top: 3%;">

          	<i class="fa fa-remove pr10"></i>

           	<strong> Oops, something didn't go right..</strong>

           	An activation link has already been sent to your registered email address which is valid for 24 hours from the time when it was sent.

     	  </div>

          <?php } else if(isset($isConfirmationLinkResend) && $isConfirmationLinkResend == true){?>

          <div class="alert alert-success" style="margin-top: 3%;">

          	<i class="fa fa-check pr10"></i>

           	<strong> Congratulations!</strong>

           	We have resend a activation link to your registered email address. Please follow the link to activate your account and remember that this link will be valid only for next 24 hours.

     	  </div>

          <?php } else if(isset($isRegistrationDone) && $isRegistrationDone == true){?>

          <div class="alert alert-success" style="margin-top: 3%;">

          	<i class="fa fa-check pr10"></i>

           	<strong> Congratulations!</strong>

           	Your registration was successful. The activation link will be sent shortly on you registered email address which will be valid for next 24 hours.

     	  </div>

          <?php } else if(isset($isEmailConfirmed) && $isEmailConfirmed == true){?>

          <div class="alert alert-success" style="margin-top: 3%;">

          	<i class="fa fa-check pr10"></i>

           	<strong> Congratulations!</strong>

           	Your email address has been verified successfully. <a href="<?php echo base_url();?>login">Click here to log in.</a>

     	  </div>

     	  <?php } else if(isset($isAlreadyConfirmed) && $isAlreadyConfirmed == true){?>

     	  <div class="alert alert-danger" style="margin-top: 3%;">

          	<i class="fa fa-remove pr10"></i>

           	<strong> Oops, something didn't go right..</strong>

           	Your email address has already been verified. <a href="<?php echo base_url();?>login">Click here to log in.</a>

     	  </div>

          <?php } else if(isset($isPasswordLinkExpired) && $isPasswordLinkExpired == true){?>

		  <div class="alert alert-danger" style="margin-top: 3%;">

          	<i class="fa fa-remove pr10"></i>

           	<strong> Oops, something didn't go right..</strong>

           	Your last reset password link has been expired. Please <a href="<?php echo base_url();?>forgot-password">click here</a> to re-send the confirmation link. Please remember that this link will be valid only for next 24 hours.

     	  </div>

          <?php } else if(isset($isPasswordLinkAlreadySent) && $isPasswordLinkAlreadySent == true){?>

          <div class="alert alert-danger" style="margin-top: 3%;">

          	<i class="fa fa-remove pr10"></i>

           	<strong> Oops, something didn't go right..</strong>

           	Reset password link has already been send to your registered email address which is valid for 24 hours from the time when it was sent.

     	  </div>

          <?php } else if(isset($isNotValidUser) && $isNotValidUser == true){?>

          <div class="alert alert-danger" style="margin-top: 3%;">

          	<i class="fa fa-remove pr10"></i>

           	<strong> Oops, something didn't go right..</strong>

           	You are not a registered valid user.

     	  </div>

          <?php }?>

          

   	  </section>

      <!-- End: Content -->



    </section>

    <!-- End: Content-Wrapper -->



  </div>

  <!-- End: Main -->





  <!-- BEGIN: PAGE SCRIPTS -->



  <!-- jQuery -->

  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery-1.11.1.min.js"></script>

  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>



  <!-- Theme Javascript -->

  <script src="<?php echo base_url();?>theme/assets/js/utility/utility.js"></script>

  <script src="<?php echo base_url();?>theme/assets/js/demo/demo.js"></script>

  <script src="<?php echo base_url();?>theme/assets/js/main.js"></script>



  <!-- Page Javascript -->

  <script type="text/javascript">

  jQuery(document).ready(function() {



    "use strict";



    // Init Theme Core      

    Core.init();



    // Init Demo JS

    Demo.init();



  });

  </script>



  <!-- END: PAGE SCRIPTS -->



</body>



</html>