<?php 

if(isset($szFileUploadError) && !empty($szFileUploadError))

{

    $szUploadFileNameError = $szFileUploadError;

}

else

{

    $szUploadFileNameError = form_error('szUploadFileName');

} 

?>    

<!-- Start: Topbar -->

    <section id="content_wrapper">	

        <?php require_once APPPATH . 'views/breadcrumb.php';?> 

      	

        <!-- Begin: Content -->

        <form method="post" action="<?php echo base_url();?>contacts/upload" id="profile-form" class="validate-form" enctype="multipart/form-data">	

            <section id="content" class="table-layout">

                <div class="tray tray-center">		     	

                    <div class="mw1000 center-block">    

                        <?php if($isDetailsSaved){?>

                        <div class="alert alert-success p5">

                            <i class="fa fa-check pr10"></i>

                            <strong> Congratulations!</strong>

                            Your contacts have been successfully imported. Please find the summery below.

                        </div>

                        <div class="upload-summery"> 

                            <div class="summery-total">

                                <ul class="list-group">

                                    <li class="list-group-item justify-content-between">

                                      Total rows uploaded:

                                      <span class="badge badge-default badge-pill"><?php echo (isset($iTotalRowsUploaded)?$iTotalRowsUploaded:0);?></span>

                                    </li>

                                    <li class="list-group-item justify-content-between">

                                      Successfully uploaded rows:

                                      <span class="badge badge-default badge-pill"><?php echo (isset($iTotalSuccessRowsUploaded)?$iTotalSuccessRowsUploaded:0);?></span>

                                    </li>

                                    <li class="list-group-item justify-content-between">

                                      Rows uploaded with error:

                                      <span class="badge badge-default badge-pill"><?php echo (isset($iTotalErrorRowsUploaded)?$iTotalErrorRowsUploaded:0);?></span>

                                    </li> 

                                </ul> 

                            </div>

                            <div class="summery-details">

                                <h4>Upload Details</h4>

                                <?php if(isset($arMessage) && !empty($arMessage)) {?>

                                    <ul>

                                        <?php echo implode("",$arMessage); ?>

                                    </ul>

                                <?php } ?>

                            </div>

                        </div> 

			

			<?php } if($isFormError=== TRUE){?>

			<div class="alert alert-danger p5">

                            <i class="fa fa-times pr10"></i>

                            <strong> Invalid Data!</strong>

                            Please fix the errors below and try again.

                        </div>

			<?php } else if($isFormError != ''){?>

                        <div class="alert alert-danger p5">

                            <i class="fa fa-times pr10"></i>

                            <strong> Sorry!</strong>

                            <?php echo $isFormError;?>

                        </div>

                        <?php }?> 

			<div class="panel panel-primary panel-border top mb15">

                            <div class="panel-body bg-light" style="padding-bottom:0;">

                                <div class="admin-form">

                                    <div class="section row mb5">

                                        <label for="szUploadFileName" class="field-label col-md-2 text-right-not-md">File<span class="text-danger">*</span></label> 

                                        <div class="col-md-3"> 

                                            <div class="fileupload fileupload-new" data-provides="fileupload">

                                                <span class="">

                                                <input type="file" name="szUploadFileName" class="required" accept=".csv," placeholder="Upload file" id="szUploadFileName" /></span>

                                                <span class="fileupload-preview"></span>

                                                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>

                                            </div> 

                                            <?php if(!empty($szUploadFileNameError)){?><em class="state-error margin-base-top" for="szUploadFileName"><?php echo $szUploadFileNameError;?></em><?php }?>

                                        </div> 

                                    </div>

                                    <div class="section row mb5">

                                        <label for="idType" class="field-label col-md-2 text-right-not-md">Contact Type</label>

                                        <?php $idTypeError = form_error('arUploadContact[idType]');?>

                                        <div class="col-md-3">

                                            <label for="idType" class="field select">

                                                <select name="arUploadContact[idType]" id="idType" class="gui-input required" placeholder="Role">                                        

                                                    <?php foreach($arTypes as $type){?>

                                                    <option value="<?php echo $type['id'];?>" <?php echo (set_input_value('arUploadContact[idType]', (isset($arUploadContactDetails['idType']) ? $arUploadContactDetails['idType'] : 1)) == $type['id'] ? 'selected' : '');?>><?php echo $type['szType'];?></option>						

                                                    <?php }?>

                                                </select>							                    

                                                <label for="idType" class="field-icon">

                                                    <i class="arrow"></i>

                                                </label>

                                            </label>

                                            <?php if(!empty($idTypeError)){?><em class="state-error" for="$idTypeError"><?php echo $idTypeError;?></em><?php }?>

                                        </div>

                                    </div>

                                    <div class="section row mb5">

                                        <div class="col-md-7"> 

                                            <h5 class="text-red margin-base-top">

                                                <i>**You must use the bulk upload template (CSV files only). <a class="text-light-green" href="<?php echo base_url();?>files/template/usitech-contact-template.csv"> Download template</a></i>

                                            </h5>

                                        </div>

                                    </div>

<!--                                    <div class="section row">

                                        <label for="szTagName" class="field-label col-md-2 text-right-not-md">Tags:</label>

                                        <?php $szTagNameError = form_error('arTag[szTagName]');?>

                                        <div class="col-md-6">

                                            <label for="szTagName" class="field prepend-icon<?php if(!empty($szTagNameError)){?> state-error<?php }?>">

                                                <input type="text" name="arTag[szTagName]" id="szTagName" class="gui-input tags-typeahead" placeholder="Tag Name" value="<?php echo set_input_value('arTag[szTagName]',(isset($arTagDetails['szTagName']) ? $arTagDetails['szTagName'] : ''));?>">

                                                <label for="szTagName" class="field-icon">

                                                    <i class="fa fa-tags"></i>

                                                </label>

                                            </label>

                                            <?php if(!empty($szTagNameError)){?><em class="state-error" for="szTagName"><?php echo $szTagNameError;?></em><?php }?>

                                        </div>

                                    </div>-->

                                </div> 

                            </div>

                            <div class="panel-footer">

                                <div class="admin-form text-center"> 

                                    <a href="<?php echo base_url().'contacts/all'; ?>" class="btn btn-default">Cancel</a>

                                    <button type="submit" class="btn btn-primary">Import</button>

                                    <input type="hidden" name="p_func" value="UPLOAD_CONTACTS"> 

                                </div>

                            </div>

                        </div>

                    </div>

                </div>  

            </section>

        </form>

    </section>