<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>New Invoice from szSiteName!</title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
Dear szFirstName,
<br /><br />
Thank you for doing business with us. This is a general payment reminder.<br /><br />
Following are the invoice details-<br />
Invoice #: szInvoiceNumber<br />
Invoice Amount: fInvoiceAmount<br />
Plan Details: szPlanDetails<br />
<br />
<br />
We will also notify you as we receive the payment for this invoice.
<br />
<br />
Best regards, <br />
The szSiteName Team
</td>
</tr>
</table>
</div>
</body>
</html>