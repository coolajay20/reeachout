  	
  	</div>
  	<!-- End: Main --> 
  	
  	<!-- jQuery -->
  	<script src="<?php echo base_url();?>theme/vendor/jquery/jquery-1.11.1.min.js"></script>
  	<script src="<?php echo base_url();?>theme/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>  

	<?php if(isset($isLoadValidationScript) && $isLoadValidationScript == true){?>
  	<!-- jQuery Validate Plugin-->
  	<script src="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
  
  	<!-- jQuery Validate Addon -->
  	<script src="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>
  	
	<?php } if(isset($isLoadDataTable) && $isLoadDataTable == true){?>
	<!-- Old methods of loading Datatables -->
  	<!--<script src="<?php echo base_url();?>theme/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>--> 
  	<!-- Datatables Tabletools addon -->
  	<!--<script src="<?php echo base_url();?>theme/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>--> 
  	<!-- Datatables ColReorder addon -->
  	<!--<script src="<?php echo base_url();?>theme/vendor/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>--> 
  	<!-- Datatables Bootstrap Modifications  -->
  	<!--<script src="<?php echo base_url();?>theme/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>-->
        
        
        <!-- New methods of loading Datatables --> 
        <script type="text/javascript" language="javascript" src="<?php echo BASE_URL_DATATABLES ?>/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo BASE_URL_DATATABLES ?>/media/js/dataTables.bootstrap.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo BASE_URL_DATATABLES ?>/extensions/Buttons/js/dataTables.buttons.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo BASE_URL_DATATABLES ?>/extensions/Buttons/js/buttons.bootstrap.js"></script>
        <script type="text/javascript" language="javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" language="javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
	<script type="text/javascript" language="javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
        
        <script type="text/javascript" language="javascript" src="<?php echo BASE_URL_DATATABLES ?>/extensions/Buttons/js/buttons.html5.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo BASE_URL_DATATABLES ?>/extensions/Buttons/js/buttons.print.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo BASE_URL_DATATABLES ?>/extensions/Buttons/js/buttons.colVis.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo BASE_URL_DATATABLES ?>/shCore.js"></script>
        <script src="<?php echo base_url();?>assets/js/custom.datatablev15.js?v=<?php echo time();?>"></script>
        
  	
	<?php if(!isset($isLoadBootBox)){?>
  	<!-- Bootbox Plugin -->
  	<script src="<?php echo base_url();?>theme/assets/js/utility/bootbox/bootbox.js"></script>  	  	
	<?php }} if(isset($isLoadBootBox) && $isLoadBootBox == true){?>

  	<!-- Bootbox Plugin -->
  	<script src="<?php echo base_url();?>theme/assets/js/utility/bootbox/bootbox.js"></script>  	  	
	<?php } if(isset($isLoadCroppieScript) && $isLoadCroppieScript == true){?>
        
        <!-- Croppie Plugin -->
        <script src="<?php echo base_url();?>theme/vendor/plugins/croppie/croppie.js"></script>
        <?php } if((isset($isLoadDTPickerScript) && $isLoadDTPickerScript == true)){?>
        
  	<!-- Time/Date Plugin Dependencies -->
  	<script src="<?php echo base_url();?>theme/vendor/plugins/globalize/globalize.min.js"></script>
  	<script src="<?php echo base_url();?>theme/vendor/plugins/moment/moment.min.js"></script>  	
  	
  	<!-- DateTime Plugin -->
  	<script src="<?php echo base_url();?>theme/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>
  	<?php }if(isset($isLoadTagsTypeAhead) && $isLoadTagsTypeAhead == true){?>
  	<script src="<?php echo base_url();?>theme/vendor/plugins/typeahead/bootstrap-typeahead.min.js"></script>
  	<?php }?>
        
  	<!-- Theme Javascript -->
  	<script src="<?php echo base_url();?>theme/assets/js/utility/utility.js"></script>
  	<script src="<?php echo base_url();?>theme/assets/js/demo/demo.js"></script>
  	<script src="<?php echo base_url();?>theme/assets/js/main.js"></script>
  	
  	<?php if(isset($isLoadCMSScripts) && $isLoadCMSScripts == true) {?>
  	<script src="<?php echo base_url();?>theme/vendor/plugins/tinymce/tinymce.min.js"></script>

	<script type="text/javascript">
        tinymce.init({ 
            selector:'.mce-editor', 
            height:'400',
            theme: 'modern',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"            
        });
	</script>
	<?php }if(isset($isLoadEditorScript) && $isLoadEditorScript == true){?>
        <script src="<?php echo base_url();?>theme/vendor/plugins/trevor/underscore.js"></script>
        <script src="<?php echo base_url();?>theme/vendor/plugins/trevor/eventable.js"></script>
        <script src="<?php echo base_url();?>theme/vendor/plugins/trevor/sortable.min.js"></script>
        <script src="<?php echo base_url();?>theme/vendor/plugins/trevor/sir-trevor.js"></script>
        <script src="<?php echo base_url();?>theme/vendor/plugins/trevor/sir-trevor-bootstrap.js"></script>
        <script type="text/javascript">
            new SirTrevor.Editor({ el: $('.js-st-instance'),
            //blockTypes: ["Columns", "Heading", "Text", "ImageExtended", "Quote", "Accordion", "Button", "Video", "List", "Iframe"]
            blockTypes: ["Columns", "Heading", "Text", "ImageExtended", "Accordion", "Button", "List"]
            });
            SirTrevor.onBeforeSubmit();
            
            function formSirTrevorSubmit(){
                SirTrevor.onBeforeSubmit();
                document.getElementById("formSirTrevor").submit();
            }
        </script>
        <?php } if(isset($isLoadMosaicoScript) && $isLoadMosaicoScript == true){?>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/knockout.js"></script>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/jquery-ui.min.js"></script>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/jquery.ui.touch-punch.min.js"></script>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/load-image.all.min.js"></script>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/canvas-to-blob.min.js"></script>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/jquery.iframe-transport.js"></script>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/jquery.fileupload.js"></script>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/jquery.fileupload-process.js"></script>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/jquery.fileupload-image.js"></script>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/jquery.fileupload-validate.js"></script>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/knockout-jqueryui.min.js"></script>
        <script src="<?php echo base_url();?>assets/mosaico/dist/vendor/tinymce.min.js"></script>

        <script src="<?php echo base_url();?>assets/mosaico/dist/mosaico.min.js?v=0.16"></script>
        <script>
        $(function() {
          if (!Mosaico.isCompatible()) {
            alert('Update your browser!');
            return;
          }
          // var basePath = window.location.href.substr(0, window.location.href.lastIndexOf('/')).substr(window.location.href.indexOf('/','https://'.length));
          var basePath = '<?php echo base_url();?>';
          if (basePath.lastIndexOf('#') > 0) basePath = basePath.substr(0, basePath.lastIndexOf('#'));
          if (basePath.lastIndexOf('?') > 0) basePath = basePath.substr(0, basePath.lastIndexOf('?'));
          if (basePath.lastIndexOf('/') > 0) basePath = basePath.substr(0, basePath.lastIndexOf('/'));
          var plugins;
          // A basic plugin that expose the "viewModel" object as a global variable.
          // plugins = [function(vm) {window.viewModel = vm;}];
          //alert(basePath);
          var ok = Mosaico.init({
            imgProcessorBackend: basePath+'/uploads/mosaico/img/',
            emailProcessorBackend: basePath+'/uploads/mosaico/dl/',
            titleToken: "MOSAICO Responsive Email Designer",
            fileuploadConfig: {
              url: basePath+'/uploads/mosaico/',
              // messages??
            }
          }, plugins);
          if (!ok) {
            console.log("Missing initialization hash, redirecting to main entrypoint");
            //document.location = ".";
          }
        });
        </script>
        <?php }?>

	<!-- Initialization -->
  	<script type="text/javascript">
  	jQuery(document).ready(function() {

    	"use strict";

    	// Init Theme Core    
    	Core.init();

    	// Init Demo JS    
    	Demo.init();
    	    	
  	});
        <?php if(isset($isLoadDTPickerScript) && $isLoadDTPickerScript == true){?>
	$('.datepicker').datepicker({
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '1970:<?php date('Y');?>'
      	});
      	$('.timepicker').datetimepicker({
            pickDate: false
      	});
	<?php }?>
  	// Init Base URL
	var base_url = '<?php echo base_url();?>';

  	</script>

	<?php if(isset($isLoadValidationScript) && $isLoadValidationScript == true){?>
	<script src="<?php echo base_url();?>assets/js/custom.validate.js?<?php echo filemtime('./assets/js/custom.validate.js');?>"></script>
	<?php } if(isset($isLoadDataTable) && $isLoadDataTable == true){?>
	<!-- Custom Datatables -->
	<script src="<?php echo base_url();?>assets/js/custom.datatable.js?<?php echo filemtime('./assets/js/custom.datatable.js');?>"></script>
	<?php } if(isset($isLoadCroppieScript) && $isLoadCroppieScript == true){?>
  	<script src="<?php echo base_url();?>assets/js/cropper.function.js?<?php echo filemtime('./assets/js/cropper.function.js');?>"></script>
  	<?php } if(isset($isSubscriptionScriptRequired) && $isSubscriptionScriptRequired == true){?>
	<script src="https://checkout.stripe.com/checkout.js"></script>    
        <script src="<?php echo base_url();?>assets/js/custom.stripe.js?<?php echo filemtime('./assets/js/custom.stripe.js');?>"></script>
	<?php } if(isset($isLoadUploadScript) && $isLoadUploadScript===true){ ?>
            <script src="<?php echo base_url();?>assets/js/custom.fileupload.js?<?php echo filemtime('./assets/js/custom.fileupload.js');?>"></script>
        <?php }?> 
	<!-- Custom Admin Functions -->
	<script src="<?php echo base_url();?>assets/js/custom.functions.js?<?php echo filemtime('./assets/js/custom.functions.js');?>"></script>
</body>

</html>