<?php
/**
 * Facebook OAuth2 Provider
 *
 * @package    CodeIgniter/OAuth2
 * @category   Provider
 * @author     Phil Sturgeon
 * @copyright  (c) 2012 HappyNinjas Ltd
 * @license    http://philsturgeon.co.uk/code/dbad-license
 */

class OAuth2_Provider_Facebook extends OAuth2_Provider
{
    protected $scope = array('email','public_profile','user_birthday','user_hometown','user_location');

    public function url_authorize()
    {
        return 'https://www.facebook.com/dialog/oauth';
    }

    public function url_access_token()
    {
        return 'https://graph.facebook.com/oauth/access_token';
    }

    public function get_user_info(OAuth2_Token_Access $token)
    {
        $iDebug = false;
        $url = 'https://graph.facebook.com/me?'.http_build_query(array(
            'access_token' => $token->access_token,
            'fields'=>'name,email,first_name,last_name,link,gender,picture,age_range,birthday',
        )); 
        $user = json_decode(file_get_contents($url));
        
        if($iDebug)
        {
            $log_file = "fb_login.log";
            $handle = fopen($log_file, "a+");
            fwrite($handle, print_R($user,true));
            fclose($handle); 
        }
        
        if(isset($user->name) && !empty($user->name) && !isset($user->first_name))
        {
            $nameAry = explode(" ", $user->name);
            $user->first_name = $nameAry[0];
            $user->last_name = $nameAry[1];
        }
        $dtDateofBirth = "";
        if(!empty($user->birthday))
        {
            $dobAry = explode('/', $user->birthday);
            $dtDateofBirth = "{$dobAry[2]}-{$dobAry[0]}-{$dobAry[1]}";
        }
        
        // Create a response from the request
        return array(
            'uid' => $user->id,
            'nickname' => isset($user->username) ? $user->username : null,
            'name' => isset($user->name) ? $user->name : null,
            'first_name' => isset($user->first_name) ? $user->first_name : null,
            'last_name' => isset($user->last_name) ? $user->last_name : null,
            'email' => isset($user->email) ? $user->email : null,
            'location' => isset($user->hometown->name) ? $user->hometown->name : null,
            'description' => isset($user->bio) ? $user->bio : null,
            'image' => 'https://graph.facebook.com/me/picture?type=normal&access_token='.$token->access_token,
            'gender' => isset($user->gender) ? $user->gender : null,
            'dtDateofBirth' => $dtDateofBirth,
            'urls' => array(
              'Facebook' => isset($user->link) ? $user->link : null,
            ),
        );
    }
}
