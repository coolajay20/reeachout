<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Membership_Controller extends CI_Controller {



    function __construct()

    {

        parent::__construct();

        

        if(!is_user_login())

        {

            redirect(base_url() . 'login', 'refresh');

        }

        else if(!hasPermission('manage.membership'))

        {

            redirect(base_url() . 'dashboard', 'refresh');

        }

        else

        {

            /* Load Membership Model */

            $this->load->model('Membership_Model');



            /* Load Plan Model */

            $this->load->model('Plan_Model');



            /* Load Plan Model */

            $this->load->model('Plan_Model');



            /* Load form helper */ 

            $this->load->helper('form');



            /* Load form validation library */ 

            $this->load->library('form_validation');



            /* Changing error delimiters Globally */

            $this->form_validation->set_error_delimiters('', '');  

        }        

    }



    public function index($arParams=array())

    {

        if(!is_user_login())

        {

            redirect(base_url() . 'login', 'refresh');

        }

        else

        {

            redirect(base_url() . 'dashboard', 'refresh');

        }

    }  
	
	
    public function referrals($arParams=array())
    {
		if (is_user_login()) {
			$data['arLoginUser'] = $this->session->userdata('login_user');
			$data['szFavIcon'] = "fa-paper-plane";
			$data['szPageName'] = "Membership";
			$data['szMetaTagTitle'] = "Referrals";

			$data = array_merge($data, $arParams);

			$this->load->view('layout/header', $data);
			$this->load->view('membership/referrals', $data);
			$this->load->view('layout/footer');
			
		} else {
			
			redirect(base_url() . 'dashboard', 'refresh');
		}
    }  

    public function subscription($arParams=array())

    {

        if(!is_user_login())

        {

            redirect(base_url() . 'login', 'refresh');

        }

        else

        {

            $isOldSubscriptionActive = false;

            $data['arLoginUser'] = $this->session->userdata('login_user');

            $arOldSubscription = $this->Membership_Model->getUserSubscription($data['arLoginUser']['id']);

            if(!empty($arOldSubscription))

            {

                if($arOldSubscription['tCurrentTermEnd'] > time())

                {

                    $isOldSubscriptionActive = true;

                }

            }

            

            if($isOldSubscriptionActive)

            {

                redirect(base_url() . 'dashboard', 'refresh');

            }

            else

            {

				$data['szFavIcon'] = "fa-ticket";
                $data['szPageName'] = "Membership";

                $data['szMetaTagTitle'] = "Subscriptions";

                $data['isLoadDataTable'] = true;

                $data['isSubscriptionScriptRequired'] = true;

                $data['arPlans'] = $this->Plan_Model->getAllPlans(true);

                $data['arProfileDetails'] = $this->User_Model->getUserByID($data['arLoginUser']['id']); 



                $data = array_merge($data, $arParams);

                

                // set plan features

                if(!empty($data['arPlans']))

                {

                    foreach($data['arPlans'] as $key=>$plan)

                    {

                        $data['arPlans'][$key]['arFeatures'] = $this->Plan_Model->getPlanFeatures($plan['id'],true);

                    }

                }



                // set breadcrumb

                $data['arBreadCrumb'][] = array('name'=>'Dashboard', 'link'=>'dashboard');

                $data['arBreadCrumb'][] = array('name'=>'Membership Subscriptin', 'link'=>'membership');



                $this->load->view('layout/header', $data);

                $this->load->view('membership/createSubscription');

                $this->load->view('layout/footer');

            }

        }

    }

    
	
    public function subscription2($arParams=array())

    {

        if(!is_user_login())

        {

            redirect(base_url() . 'login', 'refresh');

        }

        else

        {

            $isOldSubscriptionActive = false;

            $data['arLoginUser'] = $this->session->userdata('login_user');

            $arOldSubscription = $this->Membership_Model->getUserSubscription($data['arLoginUser']['id']);

            if(!empty($arOldSubscription))

            {

                if($arOldSubscription['tCurrentTermEnd'] > time())

                {

                    $isOldSubscriptionActive = true;

                }

            }

            

            if($isOldSubscriptionActive)

            {

                redirect(base_url() . 'dashboard', 'refresh');

            }

            else

            {

				$data['szFavIcon'] = "fa-ticket";
                $data['szPageName'] = "Membership";

                $data['szMetaTagTitle'] = "Subscriptions";

                $data['isLoadDataTable'] = true;

                $data['isSubscriptionScriptRequired'] = true;

                $data['arPlans'] = $this->Plan_Model->getAllPlans(true);

                $data['arProfileDetails'] = $this->User_Model->getUserByID($data['arLoginUser']['id']); 



                $data = array_merge($data, $arParams);

                

                // set plan features

                if(!empty($data['arPlans']))

                {

                    foreach($data['arPlans'] as $key=>$plan)

                    {

                        $data['arPlans'][$key]['arFeatures'] = $this->Plan_Model->getPlanFeatures($plan['id'],true);

                    }

                }



                // set breadcrumb

                $data['arBreadCrumb'][] = array('name'=>'Dashboard', 'link'=>'dashboard');

                $data['arBreadCrumb'][] = array('name'=>'Membership Subscriptin', 'link'=>'membership');



                $this->load->view('layout/header', $data);

                $this->load->view('membership/createSubscription2');

                $this->load->view('layout/footer');

            }

        }

    }

	
	

    public function subscribe($arParams=array())

    {        

        if(!is_user_login())

        {

            echo "ERROR||||Your login session has expired. Please <a href=\"".base_url()."login\">login</a> again.";           

        }

        else

        {

            // get login user details

            $arLoginUser = $this->session->userdata('login_user');

            

            // write log for checkout process

            write_log("subscriptions/subscribe", "\nNew subscription request by {$arLoginUser['name']} for plan " . urldecode(trim($this->input->get('plan', true))) . " at " . date('Y-m-d h:i:s A') . "\n");        

        

            $szStripeToken = trim($this->input->get('token', true));

            $iSubscriptionType = (int)$this->input->get('type', true);            

            $szPlanID = urldecode(trim($this->input->get('plan', true)));

            $arUserDetails = $this->User_Model->getUserByID($arLoginUser['id']);



            if(!empty($szPlanID))

            {

                // Check Plan

                $this->load->model('Plan_Model');							

                $arPlanDetails = $this->Plan_Model->getPlanByPlanID($szPlanID);

                if(!empty($arPlanDetails))

                {

                    $isOldSubscriptionActive = false;

                    $arOldSubscription = $this->Membership_Model->getUserSubscription($arLoginUser['id']);

                    if(!empty($arOldSubscription))

                    {

                        if($arOldSubscription['tCurrentTermEnd'] > time())

                        {

                            $isOldSubscriptionActive = true;

                        }

                    }

                    

                    if($isOldSubscriptionActive)

                    {

                        write_log("subscriptions/subscribe", "User old subscription is still active.\n");

                        echo "ERROR||||Your old subscription is still active.";

                    }

                    else

                    {                        

                        $isValidRequest = true;

                        $arNewSubscriptionDetails = array();

                        $arPlanDetails['iTrialPeriod'] = (int)$arPlanDetails['iTrialPeriod'];

                        $tTrialPeriod = ($arPlanDetails['iTrialPeriod'] > 0 ? strtotime("+{$arPlanDetails['iTrialPeriod']} day") : 0);

                        

                        // load stripe library

                        $this->load->library('CI_Stripe');

                        \Stripe\Stripe::setApiKey(STRIPE_ACCESS_KEY);

                            

                        // validate the stripe token

                        if($iSubscriptionType == 1 && $szStripeToken != '')

                        { 

                            // Validate Token

                            try {				

                                $token = \Stripe\Token::retrieve($szStripeToken);

                                $szCustomerEmail = $token->email;

                                write_log("subscriptions/subscribe", "Token get validated for {$szCustomerEmail}\n");

                            } catch (Exception $e) {

                                // error here

                                $isValidRequest = false;

                                write_log("subscriptions/subscribe", "Token Error (".$e->getMessage().").\n");

                            }

                        } 

                        

                        if($isValidRequest)

                        {

                            if($iSubscriptionType == 1)

                            {

                                $isValidPlan = true;

                                try {

                                    $plan = \Stripe\Plan::retrieve($szPlanID);

                                    write_log("subscriptions/subscribe", "Plan {$szPlanID} exists on stripe.\n");

                                } catch (Exception $e) {

                                    // error here

                                    $isValidPlan = false;

                                    write_log("subscriptions/subscribe", "Plan Error (".$e->getMessage().").\n");

                                }

                                

                                if(!$isValidPlan)

                                {

                                    try	{

                                        $plan = \Stripe\Plan::create(array(

                                            "name" => $arPlanDetails['szPlanName'],

                                            "id" => $arPlanDetails['szPlanID'],

                                            "interval" => 'month',

                                            "currency" => "usd",

                                            "amount" => round($arPlanDetails['fPlanAmount']*100),

                                            "metadata[yearly_amount]" => round($arPlanDetails['fPlanAmountYearly']*100),

                                            "metadata[lifetime_amount]" => round($arPlanDetails['fPlanAmountLifeTime']*100),

                                            "metadata[trail_period]"=>$arPlanDetails['iTrialPeriod']

                                        ));

                                        $isValidPlan = true;

                                        write_log("subscriptions/subscribe", "Plan {$arPlanDetails['szPlanID']} updated on stripe successfully.\n");

                                    } catch (Exception $e){

                                        write_log("subscriptions/subscribe", "Create plan error: (".$e->getMessage().").\n");

                                    }

                                }

                                

                                if($isValidPlan)

                                {

                                    $isValidCustomer = false;

                                    // check if customer already exists on Stripe

                                    if($arUserDetails['szStripeID'] !='')

                                    {

                                        try {

                                            $customer = \Stripe\Customer::retrieve($arUserDetails['szStripeID']);

                                            $szStripeCustomerID = $customer->id;

                                            if($szStripeCustomerID !='')

                                            {

                                                $customer->source = $szStripeToken; // obtained with Checkout

                                                $customer->save();

                                            }

                                            $isValidCustomer = true;

                                            write_log("subscriptions/subscribe", "Customer ID {$szStripeCustomerID} exists on stripe exists on Stripe.\n");

                                        } catch (Exception $e){

                                            // error here

                                            write_log("subscriptions/subscribe", "Customer ID {$arUserDetails['szStripeID']} not found on Stripe.\n");

                                        }

                                    }



                                    // create customer on Stripe is not exists

                                    if(!$isValidCustomer)

                                    {

                                        try {

                                            $customer = \Stripe\Customer::create(array(

                                                'email' => $arUserDetails['szEmail'], 

                                                'source'  => $szStripeToken,

                                                'description' => "{$arUserDetails['szFirstName']} {$arUserDetails['szLastName']}"

                                            ));



                                            $isValidCustomer = true;

                                            $szStripeCustomerID = $customer->id;

                                            if(!empty($szStripeCustomerID))

                                            {

                                                // update stripe customer id in database

                                                $this->db->where('id', $arUserDetails['id'])->update('tbl_users', array('szStripeID'=>$szStripeCustomerID));

                                            }

                                            write_log("subscriptions/subscribe", "Customer {$szStripeCustomerID} has been created on Stripe.\n");

                                        } catch(Exception $e){

                                            // error goes here

                                            write_log("subscriptions/subscribe", "Response: Customer Error (".$e->getMessage().").\n");

                                        }

                                    }

                                    

                                    if($isValidCustomer)

                                    {

                                        // create subscription										

                                        try {

                                            if($tTrialPeriod > 0)

                                            {

                                                // create subscription for remaining term

                                                $arSetSubscription = array(

                                                    "plan" => $szPlanID,

                                                    "customer" => $szStripeCustomerID,

                                                    "trial_end" => $tTrialPeriod

                                                );

                                            }

                                            else

                                            {

                                                $arSetSubscription = array(

                                                    "plan" => $szPlanID,

                                                    "customer" => $szStripeCustomerID

                                                );

                                            }

                                            $subscription = \Stripe\Subscription::create($arSetSubscription);

                                            $arNewSubscriptionDetails['szStripeID'] = $subscription->id;

                                            $arNewSubscriptionDetails['tCreated']= (int)$subscription->created;

                                            $arNewSubscriptionDetails['tTrialStart']= (int)$subscription->trial_start;

                                            $arNewSubscriptionDetails['tTrialEnd']= (int)$subscription->trial_end;

                                            $arNewSubscriptionDetails['iCancelAtTermEnd']= (int)$subscription->cancel_at_period_end;

                                            $arNewSubscriptionDetails['tCurrentTermStart']= (int)$subscription->current_period_start;

                                            $arNewSubscriptionDetails['tCurrentTermEnd']= (int)$subscription->current_period_end;

                                            $arNewSubscriptionDetails['szStatus'] = $subscription->status;

                                            write_log("subscriptions/subscribe", "Subscription {$subscription->id} created successfully.\n");

                                        } catch (Exception $e) {

                                            write_log("subscriptions/subscribe", "Response: Subscription Error (".$e->getMessage().").\n");

                                        }

                                    }

                                    else

                                    {

                                        echo "ERROR||||Something went wrong. Please try again after sometime.";

                                    }

                                }

                                else

                                {

                                    echo "ERROR||||Something went wrong. Please try again after sometime.";

                                }

                            }

                            else

                            { 

                                try

                                {

                                    if($iSubscriptionType==2)

                                    {

                                        $fPlanAmount = $arPlanDetails['fPlanAmountYearly'];

                                    }

                                    else

                                    {

                                        $fPlanAmount = $arPlanDetails['fPlanAmountLifeTime'];

                                    } 

                                    

                                    //Transaction ID will always be a 15 digit long alpha-numeric key

                                    $szTransactionID = "UTX".generateAlphaNumericString(12);

                                    //We are passing this description just to backtrack the record by using szTransactionID or Plan Name

                                    $szStripePaymentDescription = $arPlanDetails['szPlanName']." - ".$szTransactionID;

                                    

                                    //Building Request data for creating stripe logs 

                                    $szRequestData = "Amount: ".$fPlanAmount.PHP_EOL;

                                    $szRequestData .= "Currency: USD".PHP_EOL;

                                    $szRequestData .= "Token: ".$szStripeToken."".PHP_EOL;

                                    $szRequestData .= "Description: ".$szStripePaymentDescription."".PHP_EOL;

                                            

                                    $arStripeApiLogs = array();

                                    $arStripeApiLogs['idUser'] = $arLoginUser['id'];

                                    $arStripeApiLogs['szRequestData'] = $szRequestData;

                                    $arStripeApiLogs['szReferer'] = __HTTP_REFERER__;

                                    $arStripeApiLogs['szRemoteIP'] = __REMOTE_ADDR__;

                                    $arStripeApiLogs['szUserAgent'] = __HTTP_USER_AGENT__;

                                    $arStripeApiLogs['dtCreatedOn'] = date('Y-m-d H:i:s'); 

                                    

                                    $stripeResponse = \Stripe\Charge::create(array(

                                        "amount" => $fPlanAmount*100,

                                        "currency" => "usd",

                                        "source" => $szStripeToken,

                                        "description" => $szStripePaymentDescription

                                    )); 

                                    $arStripeApiLogs['szResponseData'] = "SUCCESS||".PHP_EOL." ".print_R($stripeResponse,true);

                                      

                                    $arCustomerTransactionDetails = array();

                                    $arCustomerTransactionDetails['idUser'] = $arLoginUser['id'];

                                    $arCustomerTransactionDetails['szTransactionID'] = $szTransactionID;

                                    $arCustomerTransactionDetails['fAmount'] = $fPlanAmount;

                                    $arCustomerTransactionDetails['fAmountRefunded'] = 0;

                                    $arCustomerTransactionDetails['isPaid'] = 1;

                                    $arCustomerTransactionDetails['tCreated'] = time();

                                    $arCustomerTransactionDetails['dtAddedOn'] = date('Y-m-d H:i:s'); 

                                    $arCustomerTransactionDetails['szType'] = ($iSubscriptionType == 2 ? 'Yearly' : 'Lifetime');

                                    $arCustomerTransactionDetails['szStatus'] = 'Paid';

                                    $arCustomerTransactionDetails['szReference'] = $arPlanDetails['szPlanName'];

                                    $arCustomerTransactionDetails['szReferenceID'] = $arPlanDetails['szPlanID'];

                                     

                                    $this->Membership_Model->saveUserTransactions($arCustomerTransactionDetails);

                                    

                                    $arNewSubscriptionDetails['tCreated']= time();

                                    $arNewSubscriptionDetails['tCurrentTermStart']= time();

                                    if($iSubscriptionType == 2)

                                    {

                                        $arNewSubscriptionDetails['tCurrentTermEnd']= strtotime("+1 year");

                                    }

                                    else

                                    {

                                        $arNewSubscriptionDetails['tCurrentTermEnd']= strtotime("+20 year");

                                    } 

                                    $arNewSubscriptionDetails['szStatus'] = 'active'; 

                                    

                                } catch (Exception $ex) { 

                                    $arStripeApiLogs['szResponseData'] = "EXCEPTION||".PHP_EOL." ".$ex->getMessage();

                                    write_log("subscriptions/subscribe", "Response: Bitcoin payment Error (".$ex->getMessage().").\n");

                                }  

                                

                                //Adding stripe payment logs

                                $this->Membership_Model->saveStripeApiLogs($arStripeApiLogs);

                            }

                            

                            // check if subscription created

                            if(!empty($arNewSubscriptionDetails))

                            {

                                // save subscription details in database														

                                $arNewSubscriptionDetails['idUser'] = $arUserDetails['id'];

                                $arNewSubscriptionDetails['idPlan'] = $arPlanDetails['id'];

                                $arNewSubscriptionDetails['szPlanName'] = $arPlanDetails['szPlanName'];

                                $arNewSubscriptionDetails['szPlanID'] = $arPlanDetails['szPlanID'];

                                $arNewSubscriptionDetails['fPlanAmount'] = ($iSubscriptionType == 1 ? $arPlanDetails['fPlanAmount'] : ($iSubscriptionType == 2 ? $arPlanDetails['fPlanAmountYearly'] : $arPlanDetails['fPlanAmountLifeTime']));

                                $arNewSubscriptionDetails['szPlanInterval'] = $arPlanDetails['szPlanInterval'];

                                $arNewSubscriptionDetails['iType'] = $iSubscriptionType;

                                $idSubscription = $this->Membership_Model->saveUsereSubscription($arNewSubscriptionDetails);



                                if($idSubscription > 0)

                                {

                                    echo "SUCCESS||||Subscription created successfully";

                                }

                                else

                                {

                                    echo "ERROR||||Something went wrong. Please try again after sometime.";

                                }

                            }

                            else

                            {

                                echo "ERROR||||Something went wrong. Please try again after sometime.";

                            }

                        }

                        else

                        {

                            echo "ERROR||||Your last payment session get expired please try again.";

                        }

                    }

                }

                else

                {

                    write_log("subscriptions/subscribe", "Plan ID {$szPlanID} not found.\n");

                    echo "ERROR||||Invalid plan.";

                }

            }

            else

            {

                write_log("subscriptions/subscribe", "No valid plan was selected to subscribe.\n");

                echo "ERROR||||Invalid plan.";

            }

        }

    }
	
	
    public function history()
    {
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        else
        {
            $data['szFavIcon'] = "fa-history";
            $data['szPageName'] = "Membership";
            $data['szMetaTagTitle'] = "Payment History";
            $data['isLoadDataTable'] = true;
            $data['arLoginUser'] = $this->session->userdata('login_user');

            $this->load->model('Membership_Model'); 
            $data['arSubscription'] = $this->Membership_Model->getUserSubscription($data['arLoginUser']['id']);
            $data['arTransactions'] = $this->Membership_Model->getUserTransactions($data['arLoginUser']['id']);

            $this->load->view('layout/header', $data);
            $this->load->view('membership/paymentHistory');
            $this->load->view('layout/footer');
        }
    }
}
?>