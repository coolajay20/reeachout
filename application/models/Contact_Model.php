<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_Model extends CI_Model
{
    function saveUserDetails($data, $idLoginUser)
    {		
        $data = sanitize_array_values($data);
        $idContact = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);
        if($idContact > 0)
        {
            // get old details
            $arOldData = $this->getContactByID($idContact);

            // update user record
            $data['dtUpdatedOn'] = date('Y-m-d H:i:s');
            $this->db->where(array('id'=>$idContact));
            $this->db->update('tbl_contacts', $data);

            // add user activity
            addUserActivity($idContact, 'tbl_contacts', "Updated contact details for {$data['szFirstName']} {$data['szLastName']}.", $data, $arOldData);
        }
        else
        {
            // add user record
            if(empty($data['szUniqueKey']))
            {
                $data['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_contacts');
            }
            $data['dtAddedOn'] = date('Y-m-d H:i:s');
            $query = $this->db->insert('tbl_contacts', $data);
            $idContact = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);

            // add user activity
            addUserActivity($idContact, 'tbl_contacts', "New contact details added for {$data['szFirstName']} {$data['szLastName']}.", $data);
        }
        return $idContact;
    }

    function getUserByEmail($idUser, $szEmail)
    {
        $arContactDetails = array();
        $szEmail = trim($szEmail);
        $idUser = (int)$idUser;
        if($idUser > 0 && $szEmail != '')
        {
            $this->db->from('tbl_contacts');
            $this->db->where(array('szEmail'=>$szEmail, 'isDeleted'=>0, 'idUser'=>$idUser));
            $query = $this->db->get();
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arContactDetails = $rows[0];
            }
        }
        return $arContactDetails;
    }

    function getContactByUniqueKey($szUniqueKey)
    {
        $arContactDetails = array();
        $szUniqueKey = trim($szUniqueKey);
        if($szUniqueKey != '')
        {
            $query = $this->db->get_where('tbl_contacts',array('szUniqueKey'=>$szUniqueKey, 'isDeleted'=>0));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arContactDetails = $rows[0];
            }
        }
        return $arContactDetails;
    }

    function getContactByID($idContact)
    {
        $arContactDetails = array();
        $idContact = (int)$idContact;
        if($idContact > 0)
        {
            $query = $this->db->get_where('tbl_contacts',array('id'=>$idContact, 'isDeleted'=>0));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arContactDetails = $rows[0];
            }
        }
        return $arContactDetails;
    }
	
	
	function getDashboardInfo($idUser, $selector)  {
	
		$this->db->select('id');
		$this->db->from('tbl_contacts u');
		$this->db->where('u.isDeleted',0)->where('idUser',(int)$idUser);

		switch ($selector) {
			case 'today':
				$this->db->group_start();
				$this->db->like('dtAddedOn', date("Y-m-d"));
				$this->db->group_end();
				break;
				
			case 'week':
				$this->db->group_start();
				$this->db->where('dtAddedOn >=', date("Y-m-d", strtotime('-7 days')));
				$this->db->group_end();
				break;

			case 'month':
				$this->db->group_start();
				$this->db->where('dtAddedOn >=', date("Y-m-d", strtotime('-30 days')));
				$this->db->group_end();
				break;
				
			default: break;
		}
		return $this->db->get()->num_rows();
	}
	
    function getContacts($idUser, $arFilters=array(), $szKeyword='', $isOnlyCount=false, $iLimit=10, $iOffset=0, $szSortField='dtAddedOn', $szSortOrder='ASC')
    {
        // set fields to select
        if($isOnlyCount)
            $this->db->select('id');
        else
            $this->db->select('*');

        // from table
        $this->db->from('tbl_contacts u'); 
		
        // where
        $this->db->where('u.isDeleted',0)->where('idUser',(int)$idUser);
        // set custom filters
        setCustomRecordFilters($this, $arFilters);

        // set keyword filter
        $szKeyword = trim($szKeyword);
        if($szKeyword != '')
        {
            $this->db->group_start();
            $this->db->like('szFirstName', $szKeyword);
            $this->db->or_like('szLastName', $szKeyword);
            $this->db->or_like('szEmail', $szKeyword);
            $this->db->or_like('szPhone1', $szKeyword);
            $this->db->or_like('szPhone2', $szKeyword);
            $this->db->group_end();
        }

       
        // retrive records
        if($isOnlyCount)
        {
            return $this->db->get()->num_rows();
        }
        else
        {
            $query = $this->db->order_by($szSortField, $szSortOrder)->limit($iLimit, $iOffset)->get();
            //echo $this->db->last_query();die;
            return ($query->num_rows() > 0 ? $query->result_array() : array());
        }
    }

    function deleteContact($arContactDetails, $iDeletedBy)
    {
        // delete user
        $idContact = (int)$arContactDetails['id'];
        $this->db->where(array('id'=>$idContact));
        $this->db->update('tbl_contacts', array('isDeleted'=>1, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        if($this->db->affected_rows() > 0)
        {
            // add user activity
            addUserActivity($idContact, 'tbl_contacts', "Contact {$arContactDetails['szFirstName']} {$arContactDetails['szLastName']} deleted.");
            return true;
        }
        return false;
    }

    function getAllContactTypes()
    {
        $arContacts = array();
        $query = $this->db->get('tbl_contact_types');
        if($query->num_rows() > 0)
        {
            $arContacts = $query->result_array();
        }
        return $arContacts;
    }

    function getContactTypeById($idType)
    {
        $szType = '';
        $idType = (int)$idType;
        $query = $this->db->get_where('tbl_contact_types', array('id'=>$idType));
        if($query->num_rows() > 0)
        {
            $rows = $query->result_array();
            $szType = $rows[0]['szType'];
        }
        return $szType;
    }
    
    function getContactTags($idContact)
    {
        $this->db->select('t.*, ct.idTag');
        $this->db->from('tbl_contact_tags ct');
        $this->db->join('tbl_tags t', 't.id = ct.idTag');
        $this->db->where('idContact', $idContact);
        $this->db->where(array('t.isDeleted'=>0,'ct.isDeleted'=>0));
        $query = $this->db->get();

        return ($query->num_rows() > 0 ? $query->result_array() : array());
    }

    function addContactTag($idContact, $idTag)
    {
        $this->db->insert('tbl_contact_tags', array('idContact'=>$idContact, 'idTag'=>$idTag));
        return $idTag;
    }

    function isContactTagAlreadyExists($idContact, $idTag)
    {
        $query = $this->db->select('id')->get_where('tbl_contact_tags', array('idContact'=>$idContact, 'idTag'=>$idTag));
        return ($query->num_rows() > 0 ? $idTag : 0);
    }

    function deleteContactTag($idContact, $idTag=0)
    {
        $this->db->where('idContact', $idContact);
        if((int)$idTag > 0)
            $this->db->where('idTag', (int)$idTag);
        $this->db->update('tbl_contact_tags', array('isDeleted'=>1,'dtDeletedOn'=>date('Y-m-d H:i:s')));

        return ($this->db->affected_rows() > 0 ? true : false);
    }

   
    function saveUploadedFileLogs($data)
    {
        if(!empty($data))
        {
            $data['dtFileUploaded'] = date('Y-m-d H:i:s');
            $query = $this->db->insert('tbl_file_import_logs', $data);
            $idContact = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);

            // add user activity
            addUserActivity($idContact, 'tbl_contacts', "New file uploaded with name {$data['szFileName']}.", $data);
        }  
    }
}
?>