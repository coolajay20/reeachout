<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Email_Model extends CI_Model {



    var $arAttachmentFile = array();

    var $arEmailTo = array();

    var $szEmailSubject = "";

    var $szEmailMessage = "";

    function __construct() 

    {

        // Call the Model constructor

        parent::__construct();

    }     

    

    function sendSystemEmails($arEmailData,$szEmailTemplate)

    {

        if(!empty($szEmailTemplate))

        {

            $arEmailContent = array();

            $arEmailContent = $this->getEmailTemplateContent($szEmailTemplate);

            

            if(!empty($arEmailContent))

            {

                $szEmailBody = $arEmailContent['szEmailBody'];

                $szEmailSubject = $arEmailContent['szEmailSubject'];

                

                if (count($arEmailData) > 0)
                {

                    foreach ($arEmailData as $replace_key => $replace_value)
                    {
                        $szEmailBody = str_replace($replace_key, $replace_value, $szEmailBody);
                        $szEmailSubject= str_replace($replace_key, $replace_value, $szEmailSubject);
                    }
                } 
                try
                {
                    //echo "Email: {$arEmailData['szEmail']}<br>To: " . CUSTOMER_SUPPORT_EMAIL . "<br>Subject: $szEmailSubject<br>Message:<br>$szEmailBody";die;
                    //sendEmail($arEmailData['szEmail'], CUSTOMER_SUPPORT_EMAIL, $szEmailSubject, $szEmailBody); 

                    if(isset($arEmailData['szEmailFrom']) && !empty($arEmailData['szEmailFrom']))

                    {

                        $szEmailFrom = $arEmailData['szEmailFrom'];

                    }

                    else

                    {

                        $szEmailFrom = CUSTOMER_SUPPORT_EMAIL;

                    }

                    

                    if(isset($arEmailData['szFromUserName']) && !empty($arEmailData['szFromUserName']))

                    {

                        $szFromUserName = $arEmailData['szFromUserName'];

                    }

                    else

                    {

                        $szFromUserName = CUSTOMER_SUPPORT_TITLE;

                    }

                    

                    $arUserData = $this->session->userdata('login_user'); 

                    $idUser = $arUserData['id'];

                    /*

                    * building block for sending E-mail

                    */

                    $mailBasicDetailsAry = array();

                    $mailBasicDetailsAry['szEmailTo'] = $arEmailData['szEmail'];  

                    $mailBasicDetailsAry['szEmailSubject'] = $szEmailSubject;

                    $mailBasicDetailsAry['szEmailMessage'] = $szEmailBody; 

                    $mailBasicDetailsAry['idUser'] = $idUser; 

                    $mailBasicDetailsAry['szEmailFrom'] = $szEmailFrom;

                    $mailBasicDetailsAry['szFromUserName'] = $szFromUserName;

                    $mailBasicDetailsAry['szEmailCC'] = isset($arEmailData['szEmailCC'])?$arEmailData['szEmailCC']:'';

                    $mailBasicDetailsAry['bAttachments'] = isset($arEmailData['bAttachments'])?$arEmailData['bAttachments']:false;

                    $mailBasicDetailsAry['szAttachmentFiles'] = isset($arEmailData['szAttachmentFiles'])?$arEmailData['szAttachmentFiles']:false;

                    $mailBasicDetailsAry['szAttachmentFileName'] = isset($arEmailData['szAttachmentFileName'])?$arEmailData['szAttachmentFileName']:false;
 
                    if(__ENVIRONMENT__=='USI_DEV')
                    {
                        $this->sendEmail($mailBasicDetailsAry);
                    }
                    

                      

                } catch (Exception $ex) {

                    $status = 0;

                    $szStatusMsg = $ex->errorMessage(); //Pretty error messages from PHPMailer

                    write_log('phpmailer', $szStatusMsg . "\n");

                } 

            }

        }

    }  

    function sendEmail($mailBasicDetails)

    {

        if(!empty($mailBasicDetails))

        {    

            /*

            * Setting E-mail object variables

            */

            $this->setEmailTo($mailBasicDetails['szEmailTo']);

            $this->setAttachmentFiles($mailBasicDetails['szAttachmentFiles']); 

            $this->szEmailSubject = $mailBasicDetails['szEmailSubject'];

            $this->szEmailMessage = $mailBasicDetails['szEmailMessage'];

            

            //loading E-mail library for Send grid API

            $this->load->library('email');

            

            /*

            * Setting Request Variables for Sendgrid

            */ 

            $this->email->initialize(array(

                'protocol' => EMAIL_PROTOCOL,

                'smtp_host' => SMTP_HOST,

                'smtp_user' => SMTP_USER,

                'smtp_pass' => SMTP_PASSWORD,

                'smtp_port' => SMTP_PORT,

                'mailtype' => 'html',

                'crlf' => "\r\n",

                'newline' => "\r\n"

            ));   

            

            $emailToAry = $this->getEmailTo();

            $attachableFilesAry = $this->getAttachmentFiles();

           

            $szEmailKey = md5(serialize($emailToAry)."_".time()."_".mt_rand(0,9999));

              

            $to_ctr = 0;

            $customerEmailToAry = array();

            $additionalCCAry = array();

            

            if(count($emailToAry)>1)

            {

                foreach($emailToAry as $emailToArys)

                {

                    if($to_ctr==0)

                    {

                        $customerEmailToAry[] = $emailToArys;

                    }

                    else

                    { 

                        $additionalCCAry[] = $emailToArys;

                    }

                    $to_ctr++;

                }

                if(!empty($mailBasicDetails['szCCEmailAddress']) && !empty($additionalCCAry))

                {

                    $mailBasicDetails['szCCEmailAddress'] = array_merge($additionalCCAry, $mailBasicDetails['szCCEmailAddress']);

                }

                else if(!empty($additionalCCAry))

                {

                    $mailBasicDetails['szCCEmailAddress'] = $additionalCCAry;

                }

            }

            else

            {

                $customerEmailToAry = $emailToAry;

            } 

            $szCCEmailAddress = ""; 

            $szBCCEmailAddress = "";

            if(isset($mailBasicDetails['szCCEmailAddress']) && !empty($mailBasicDetails['szCCEmailAddress']))

            {

                $szCCEmailAddress = implode(",",$mailBasicDetails['szCCEmailAddress']);

            }

            if(isset($mailBasicDetails['szBCCEmailAddress']) && !empty($mailBasicDetails['szBCCEmailAddress']))

            {

                $szBCCEmailAddress = implode(",",$mailBasicDetails['szBCCEmailAddress']);

            } 

            $emailLogsAry = array();

            $emailLogsAry['szFromEmail'] = $mailBasicDetails['szEmailFrom'];

            $emailLogsAry['szEmailBody'] = $this->szEmailMessage ;

            $emailLogsAry['szEmailSubject'] = $this->szEmailSubject ;

            $emailLogsAry['idUser'] = $mailBasicDetails['idUser'] ; 

            $emailLogsAry['szEmailKey'] = $szEmailKey ; 

            $emailLogsAry['szCCEmailAddress'] = $szCCEmailAddress;

            $emailLogsAry['szBCCEmailAddress'] = $szBCCEmailAddress;

            $emailLogsAry['idContact'] = isset($mailBasicDetails['idContact'])?$mailBasicDetails['idContact']:'' ; 

            $emailLogsAry['idEmailCampaign'] = isset($mailBasicDetails['idEmailCampaign'])?$mailBasicDetails['idEmailCampaign']:'' ; 

            $emailLogsAry['idEmailTemplate'] = isset($mailBasicDetails['idEmailTemplate'])?$mailBasicDetails['idEmailTemplate']:'' ; 

            $emailLogsAry['iEmailType'] = isset($mailBasicDetails['iEmailType'])?$mailBasicDetails['iEmailType']:1 ; 

            $emailLogsAry['idCampaignDelivery'] = isset($mailBasicDetails['idCampaignDelivery'])?$mailBasicDetails['idCampaignDelivery']:'' ; 

            

             

            if(!empty($customerEmailToAry))

            {

                foreach($customerEmailToAry as $emailToArys)

                { 

                    $emailLogsAry['szToAddress'] = $emailToArys ;

                    if(!empty($attachableFilesAry))

                    {

                        $emailLogsAry['szAttachmentFiles'] = serialize($attachableFilesAry);

                        $emailLogsAry['iHasAttachments'] = 1; 

                    }  

                    $idEmailLog = $this->logEmails($emailLogsAry);    

                    

                    try

                    {

                        #$this->email->from($mailBasicDetails['szEmailFrom'], $mailBasicDetails['szEmailFrom']);

                        $this->email->from('support@bitcoinlabs.io', WEBSITE_NAME);

                        

                        $this->email->to($emailToArys);

                        $this->email->subject($this->szEmailSubject);

                        $this->email->message($this->szEmailMessage);

                        

                        if(!empty($szCCEmailAddress))

                        {

                            $this->email->cc($szCCEmailAddress);

                        }

                        if(!empty($szBCCEmailAddress))

                        {

                            $this->email->bcc($szBCCEmailAddress);

                        } 

                        $this->email->send(); 

                        //var_dump($this->email);

                    } 

                    catch (Exception $ex) 

                    { 

                        $szExceptionMessage = " System registered exception in sending this email. \n\n ";

                        $szExceptionMessage .= 'Exception Message: ' .$ex->getMessage();

                        $szExceptionMessage .= 'File: ' .$ex->getFile();

                        $szExceptionMessage .= 'Line: ' .$ex->getLine();

                        $szExceptionMessage .= 'Trace: ' .$ex->getTraceAsString();

                    } 

                    

                    $updateEmailLog = array();

                    $updateEmailLog['iSuccess'] = 1;

                    $updateEmailLog['id'] = $idEmailLog;

                    if(!empty($szExceptionMessage))

                    {

                        $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;

                    }

                    else

                    {

                        $updateEmailLog['szSendgridResponseLogs'] = $this->email->print_debugger(); 

                    } 

                    $this->updateEmailLogs($updateEmailLog); 

                }

            } 

        }

    }

    

    function logEmails($emailLogsAry)

    {

        if(!empty($emailLogsAry))

        {      

            $emailLogsAry['dtSent'] = date('Y-m-d H:i:s');  

            $data = sanitize_array_values($emailLogsAry);

            

            $query = $this->db->insert('tbl_emaillog', $data);

            $idEmailLogs = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);

            

            // add user activity

            addUserActivity($idEmailLogs, 'tbl_emaillog', "Sent an email with subject: {$data['szEmailSubject']} to {$data['szToAddress']}.", $data);

            return $idEmailLogs;

        }

    }

    

    function updateEmailLogs($data)

    { 

        if(!empty($data))

        {    

            $idEmailLog = (int)$data['id'];

            if($idEmailLog>0)

            { 

                $this->db->where(array('id'=>$idEmailLog));

                $this->db->update('tbl_emaillog', $data); 

            } 

        }

    }

    

    function getEmailTemplateContent($szEmailTemplate)

    {

        if(!empty($szEmailTemplate))

        {

            $arTemplate = array();

            if($szEmailTemplate=='WELCOM_EMAIL')

            { 

                $szTemplatePath = __EMAIL_TEMPLATE_PATH__."/welcome-html.php";

                $szEmailBody = "";

                if(file_exists($szTemplatePath))

                {

                    $szEmailBody = file_get_contents($szTemplatePath);

                }

                

                $arTemplate['szEmailSubject'] = "Welcome to " . WEBSITE_NAME;

                $arTemplate['szEmailBody'] = $szEmailBody;

            }

            else if($szEmailTemplate=='ACTIVATE_EMAIL')

            { 

                $szTemplatePath = __EMAIL_TEMPLATE_PATH__."/activate-html.php";

                $szEmailBody = "";

                if(file_exists($szTemplatePath))

                {

                    $szEmailBody = file_get_contents($szTemplatePath);

                }

                

                $arTemplate['szEmailSubject'] = "Welcome to " . WEBSITE_NAME;

                $arTemplate['szEmailBody'] = $szEmailBody;

            }

            else if($szEmailTemplate=='INVOICE_EMAIL')

            { 

                $szTemplatePath = __EMAIL_TEMPLATE_PATH__."/invoice-html.php";

                $szEmailBody = "";

                if(file_exists($szTemplatePath))

                {

                    $szEmailBody = file_get_contents($szTemplatePath);

                }

                

                $arTemplate['szEmailSubject'] = "New Invoice from " . WEBSITE_NAME;

                $arTemplate['szEmailBody'] = $szEmailBody;

            }

            else if($szEmailTemplate=='PAYMENT_EMAIL')

            { 

                $szTemplatePath = __EMAIL_TEMPLATE_PATH__."/payment-html.php";

                $szEmailBody = "";

                if(file_exists($szTemplatePath))

                {

                    $szEmailBody = file_get_contents($szTemplatePath);

                }

                

                $arTemplate['szEmailSubject'] = "Payment confirmation from " . WEBSITE_NAME;

                $arTemplate['szEmailBody'] = $szEmailBody;

            }

            else if($szEmailTemplate=='REFUND_EMAIL')

            { 

                $szTemplatePath = __EMAIL_TEMPLATE_PATH__."/refund-html.php";

                $szEmailBody = "";

                if(file_exists($szTemplatePath))

                {

                    $szEmailBody = file_get_contents($szTemplatePath);

                }

                

                $arTemplate['szEmailSubject'] = "Refund confirmation from " . WEBSITE_NAME;

                $arTemplate['szEmailBody'] = $szEmailBody;

            }

            else if($szEmailTemplate=='SUBSCRIPTION_EMAIL')

            { 

                $szTemplatePath = __EMAIL_TEMPLATE_PATH__."/subscription-html.php";

                $szEmailBody = "";

                if(file_exists($szTemplatePath))

                {

                    $szEmailBody = file_get_contents($szTemplatePath);

                }

                

                $arTemplate['szEmailSubject'] = "Subscription confirmation from " . WEBSITE_NAME;

                $arTemplate['szEmailBody'] = $szEmailBody;

            }
			
            else if($szEmailTemplate=='LEAD_NOTIFICATION')
            { 
                $szTemplatePath = __EMAIL_TEMPLATE_PATH__."/lead_notification-html.php";
                $szEmailBody = "";
                if(file_exists($szTemplatePath))
                {
                    $szEmailBody = file_get_contents($szTemplatePath);
                }
               $arTemplate['szEmailSubject'] = "You have a NEW LEAD! (".WEBSITE_NAME.")";
               $arTemplate['szEmailBody'] = $szEmailBody;
            }
            else if($szEmailTemplate=='NEW_SUBSCRIBER_ADDED')
            { 
                $szTemplatePath = __EMAIL_TEMPLATE_PATH__."/new_subscriber-html.php";
                $szEmailBody = "";
                if(file_exists($szTemplatePath))
                {
                    $szEmailBody = file_get_contents($szTemplatePath);
                }
                $arTemplate['szEmailSubject'] = WEBSITE_NAME." notification - New subscriber added";
                $arTemplate['szEmailBody'] = $szEmailBody;
            }
            else if($szEmailTemplate=='CUSTOMER_NEW_SUPPORT_TICKET')
            { 
                $szTemplatePath = __EMAIL_TEMPLATE_PATH__."/customer_new_support_ticket.php";
                $szEmailBody = "";
                if(file_exists($szTemplatePath))
                {
                    $szEmailBody = file_get_contents($szTemplatePath);
                }
                $arTemplate['szEmailSubject'] = WEBSITE_NAME." - New support ticket";
                $arTemplate['szEmailBody'] = $szEmailBody;
            }
            else if($szEmailTemplate=='CUSTOMER_NEW_SUPPORT_TICKET_ACK')
            { 
                $szTemplatePath = __EMAIL_TEMPLATE_PATH__."/customer_new_support_ticket_ack.php";
                $szEmailBody = "";
                if(file_exists($szTemplatePath))
                {
                    $szEmailBody = file_get_contents($szTemplatePath);
                }
                $arTemplate['szEmailSubject'] = WEBSITE_NAME." - New support ticket acknowledgement";
                $arTemplate['szEmailBody'] = $szEmailBody;
            }
            else if($szEmailTemplate=='ADMIN_NEW_SUPPORT_TICKET_ACK')
            { 
                $szTemplatePath = __EMAIL_TEMPLATE_PATH__."/admin_new_support_ticket_ack.php";
                $szEmailBody = "";
                if(file_exists($szTemplatePath))
                {
                    $szEmailBody = file_get_contents($szTemplatePath);
                }
                $arTemplate['szEmailSubject'] = WEBSITE_NAME." - New support ticket acknowledgement";
                $arTemplate['szEmailBody'] = $szEmailBody;
            }
            return $arTemplate;
        }
    }

   
    function setEmailTo($szEmailTo)

    {

        if(!empty($szEmailTo))

        {

            $this->arEmailTo = array();

            if(is_array($szEmailTo))

            {

                $this->arEmailTo = $szEmailTo;

            }

            else

            {

                $this->arEmailTo[] = $szEmailTo;

            } 

        } 

    } 

    

    function setAttachmentFiles($szAttachmentFiles)

    {

        if(!empty($szAttachmentFiles))

        {

            if(is_array($attachmentAry))

            {

                foreach($szAttachmentFiles as $szAttachmentFiles)

                {

                    $szFileBaseName = basename($szAttachmentFiles); 

                    $szAttachmentFullPath = __APP_PATH_EMAIL_ATTACHMENT__."/".$szFileBaseName; 



                    if(file_exists($szAttachmentFullPath))

                    {

                        /*

                        * If is already available in our system's attachments folder then do nothing

                        */ 

                        //@TO DO 

                    } 

                    else

                    {

                        /*

                        * If file is not exists in our attachments folder then we copy the file to our attachment folder.

                        */

                        if(file_exists($szAttachmentFiles))

                        {

                            @copy($szAttachmentFiles, $szAttachmentFullPath);

                        }

                    }

                }

                $this->arAttachmentFile = $szAttachmentFiles;

            } 

            else

            {

                $szAttachmentFiles = $szAttachmentFiles;

                $szFileBaseName = basename($szAttachmentFiles); 

                $szAttachmentFullPath = __APP_PATH_EMAIL_ATTACHMENT__."/".$szFileBaseName; 

                $this->arAttachmentFile[] = $szAttachmentFiles;



                if(file_exists($szAttachmentFullPath))

                {

                    /*

                    * If is already available in our system's attachments folder then do nothing

                    */ 

                    //@TO DO 

                } 

                else

                {

                    /*

                    * If file is not exists in our attachments folder then we copy the file to our attachment folder.

                    */

                    if(file_exists($szAttachmentFiles))

                    {

                        @copy($szAttachmentFiles, $szAttachmentFullPath);

                    }

                }

            }

        }

    }

    function getEmailTo()

    {

        return $this->arEmailTo;

    } 

    function getAttachmentFiles()

    {

        return $this->arAttachmentFile;

    }

    

    function addSendgriApiLogs($data)

    {		  

        if(!empty($data))

        {

            $query = $this->db->insert('tbl_sendgrid_logs', $data);  

        } 

    }

    

    function getEmailByKey($szEmailKey)

    {

        $arEmailData = array(); 

        if(!empty($szEmailKey))

        {

            $query = $this->db->get_where('tbl_emaillog',array('szEmailKey'=>$szEmailKey));

            if($query->num_rows() > 0)

            {

                $rows = $query->result_array();

                $arEmailData = $rows[0];

            }

        }

        return $arEmailData;

    } 

    function addEmailReportingData($data)

    {		  

        if(!empty($data))

        {

            $query = $this->db->insert('tbl_email_reporting', $data);  

        } 

    }

}

?>