    <!-- Start: Topbar -->
    <section id="content_wrapper">	
        <header id="topbar" class="alt">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
              		<a href="<?php echo base_url();?>dashboard">Dashboard</a>
                    </li>
                    <li class="crumb-link">
                        <a href="<?php echo base_url();?>users">Users</a>
                    </li>
                    <li class="crumb-trail">User Details</li>
                </ol>
            </div>        	
     	</header>
      	<!-- End: Topbar -->
      	
        <!-- Begin: Content -->
        <section id="content">
            <div class="row">
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="panel-title">
				<i class="fa fa-user"></i>
                                Personal Details
                            </span>
                            <span class="panel-controls">
                                <a href="<?php echo base_url();?>users/edit/<?php echo $arUserDetails['szUniqueKey'];?>">Edit</a>
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="text-center mb20">
                                <?php
                                if($arUserDetails['szAvatarImage'] != '')
                                {
                                    $p_image_src = BASE_USERS_UPLOAD_URL . $arUserDetails['szAvatarImage'];
                                }
                                else
                                {
                                    $p_image_src = base_url() . 'assets/images/profile.png';
                                }
                                ?>
                                <img class="avatar avatar-preview img-circle" class="mb20" src="<?php echo $p_image_src;?>" alt="Avatar Image">                                        
                            </div>
                            
                            <table class="table table-hover table-details mb20">
                                <thead>
                                    <tr>
                                        <th colspan="3" class="pn"><h3 class="mtn">Contact Details</h3></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Email</td>
                                        <td><a href="mailto:<?php echo $arUserDetails['szEmail'];?>"><?php echo $arUserDetails['szEmail'];?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td><a href="telto:<?php echo $arUserDetails['szPhone'];?>"><?php echo $arUserDetails['szPhone'];?></a></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th colspan="3" class="pn"><h3 class="mtn">Additional Information</h3></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Address</td>
                                    <td><?php echo $arUserDetails['szAddress'];?></td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td><?php echo $arUserDetails['szCity'];?></td>
                                </tr>
                                <tr>
                                    <td>Country</td>
                                    <td><?php echo $arUserDetails['szCountry'];?></td>
                                </tr>
                                <tr>
                                    <td>Last Logged In</td>
                                    <td><?php if($arUserDetails['dtLastLogin'] != '') echo convert_date($arUserDetails['dtLastLogin'],1);?></td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    </div> 
                </div>
                <div class="col-sm-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="panel-title">
				<i class="fa fa-history"></i>
                                Latest Activities
                            </span>
                        </div>
                        <div class="panel-body pn">
                            <table class="table table-hover table-striped dt-static sort_order_desc">
                                <thead>
                                    <tr>
                                        <th class="hidden">Hidden</th>
                                        <th>Action</th>
                                        <th>Date</th>
                                        <th class="hidden">Hidden</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($arUserDetails['activities'])){ foreach ($arUserDetails['activities'] as $activity){?>
                                    <tr>
                                        <th class="hidden"><?php echo strtotime($activity['dtCreatedOn']);?></th>
                                        <td><?php echo $activity['szActivity'];?></td>
                                        <td><?php echo date('m/d/Y h:ia', strtotime($activity['dtCreatedOn']));?></td>
                                        <td class="hidden">Hidden</td>
                                    </tr>
                                    <?php }}?>
                                </tbody>
                            </table>
                        </div>
                    </div>                    
                </div>
            </div>
        </section>
    </section>