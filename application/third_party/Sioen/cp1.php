<?php

// Main website configuration options

$site_config = array(
	'website_url' => 'http://'.$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI],
	'website_name' => 'MultiplyMyBTC.com',
	'default_title' => 'MultiplyMyBTC.com',
	'default_description' => 'What if you invested $100 in ethereum in 2016? Find out with this historic ethereum price calculator.',
	'default_keywords' => 'Website Keywords',
	'default_image' => 'logo_beta-highres.png'
	);


// Faucethub.io registered addresses

$ref = array(
  'BTC' => '15L7a3M82QTNtP9zfdNQV24NZQti3iREsq',
  'LTC' => 'LQWaDEHGaKbBRR6XeYZGzMqcuD3nXxK7Ce',
  'DOGE' => 'D6vDHAQs2DCpmBwBDyYmNq22VkskYM2ofJ',
  'BLK' => 'BFbGJd337MMd9EFn6dukg2XMpvENgpMAoZ',
  'DASH' => 'XoWTPAtENCUVHjr2SKrxE54XXWgxCLSJEr',
  'PPC' => 'PFY3ZjQBhDXxP52EZ8EM4Zpmw5RcSkTywP',
  'XPM' => 'AJYnAy1EPU7HT3SUDCMyVdx2ebcMBCiaE3'
  );


//////////////////////////////////////////////////////////////////////////
// Important stuff, don't change unless you know what you are doing!!! //
////////////////////////////////////////////////////////////////////////

// Allow getting the version number for support reasons

#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);

if(isset($page_var)) {
  $page_info['title']  = $site_config['website_name'] . ' - ' . $page_var['title'];
  $page_info['description'] = $page_var['description'];
  $page_info['url'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $page_info['image'] = $site_config['website_url'] . '/assets/images/' . $site_config['default_image'];
} else {
  $page_info['title']  = $site_config['default_title'];
  $page_info['description'] = $site_config['default_description'];
  $page_info['url'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $page_info['image'] = $site_config['website_url'] . '/assets/images/' . $site_config['default_image'];
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $page_info['title']; ?></title>
    <meta name="description" content="<?php echo $page_info['description']; ?>">
    <!-- Opengraph Tags -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="<?php echo $page_info['title']; ?>" />
    <meta property="og:description" content="<?php echo $page_info['description']; ?>" />
    <meta property="og:url" content="<?php echo $page_info['url']; ?>" />
    <meta property="og:image" content="<?php echo $page_info['image']; ?>" />
    <meta property="og:image:width" content="150" />
    <meta property="og:image:height" content="150" />
    <!-- Twitter Card -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="<?php echo $page_info['title']; ?>" />
    <meta name="twitter:description" content="<?php echo $page_info['description']; ?>" />
    <meta property="twitter:url" content="<?php echo $page_info['url']; ?>" />
    <meta property="twitter:image" content="<?php echo $page_info['image']; ?>" />
    <!-- Images -->
    <link rel="icon" href="<?php echo $site_config['website_url']; ?>/assets/img/favicon.png" />
    <!-- External CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->

<style>
/*
 * Globals
 */

 .h1, h1 {
     line-height: 48px;
     font-size: 33px;
 }
 h1.cover-heading.main {
     margin-bottom: 50px;
 }
/* Links */
a,
a:focus,
a:hover {
  color: #fff;
}

/* Custom default button */
.btn-default,
.btn-default:hover {
  color: #333;
  text-shadow: none; /* Prevent inheritance from `body` */
  background-color: #FFC250;
  border: 1px solid #fff;
}


/*
 * Base structure
 */

html,
body {
  height: 100%;
  background-color: #212846;
}
body {
  color: #fff;
  text-align: center;
  text-shadow: 0 1px 3px rgba(0,0,0,.5);
	
	background: url(https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/H8WuRINimqur8ud/the-growth-of-the-virtual-currency-bitcoin-bitcoin-growth-chart-candle-chart-on-the-online-forex-monitor_syyjyb1z__F0000.png) bottom center no-repeat fixed;
	background-size: cover;
}
span.value {
    color: #f6a91a;
}
.btn-green, .btn-green:focus {
    background: #f6a91a;
    border: 1px solid #f6a91a;
    color: #000;
    font-size: 14px;
    padding: 15px 40px !important;
}
/* Extra markup and styles for table-esque vertical and horizontal centering */
.site-wrapper {
  display: table;
  width: 100%;
  height: 100%; /* For at least Firefox */
  min-height: 100%;
background-color: rgba(0,0,0,0.6);
background-size: cover;
width: 100%;
}
.site-wrapper-inner {
  display: table-cell;
  vertical-align: top;
}
.cover-container {
  margin-right: auto;
  margin-left: auto;
}
input.form-control.form-amt {
    width: 70px;
}

/* Padding for spacing */
.inner {
  padding: 30px;
}


/*
 * Header
 */
.masthead-brand {
  margin-top: 10px;
  margin-bottom: 10px;
}

.masthead-nav > li {
  display: inline-block;
}
.masthead-nav > li + li {
  margin-left: 20px;
}
.masthead-nav > li > a {
  padding-right: 0;
  padding-left: 0;
  font-size: 16px;
  font-weight: bold;
  color: #fff; /* IE8 proofing */
  color: rgba(255,255,255,.75);
  border-bottom: 2px solid transparent;
}
.masthead-nav > li > a:hover,
.masthead-nav > li > a:focus {
  background-color: transparent;
  border-bottom-color: #a9a9a9;
  border-bottom-color: rgba(255,255,255,.25);
}
.masthead-nav > .active > a,
.masthead-nav > .active > a:hover,
.masthead-nav > .active > a:focus {
  color: #fff;
  border-bottom-color: #fff;
}

@media (min-width: 768px) {
  .masthead-brand {
    float: left;
  }
  .masthead-nav {
    float: right;
  }
}


/*
 * Cover
 */

.cover {
  padding: 0 20px;
}
.cover .btn-lg {
    padding: 10px 20px;
    font-weight: bold;
}
.form-control {
    box-shadow: none!important;
    border: 0px!important;
}

/*
 * Faucet Modal
 */

#faucetModal .modal-dialog {
    width: 100%;
    margin: 0px;
    overflow: hidden;
}
.modal-content.faucet-modal {
    height: 100%;
    border-radius: 0px;
    border: 0px;
}
.modal-content.faucet-modal {
    color: #111;
    text-shadow: none;
}
.modal-header {
    padding: 25px;
    border-bottom: 1px solid #212846;
    background: #212846;
    color: #fff;
}
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    max-height: calc(100vh - 100px);
    overflow-y: auto;
}
.btn-faucet {
    background: #FD5B03;
    border: 1px solid #fd5b03;
    color: #fff!important;
}
th {
    color: #212846;
}
td {
    color: #111;
}
#faucetModal td a {
    color: #337ab7;
    text-decoration: none;
}
button.close {
    color: #fff;
    font-size: 28px;
}
button.close:hover {
    color: #fff;
    font-size: 28px;
    opacity: 1;
    text-shadow: 0px!important;
}
#faucetModal .container {
    max-width: 900px;
}
/*
 * Footer
 */

.mastfoot {
  color: #999; /* IE8 proofing */
  color: rgba(255,255,255,.8);
}


/*
 * Affix and center
 */

@media (min-width: 768px) {
  /* Pull out the header and footer */
  .masthead {
    position: fixed;
    top: 0;
  }
  .mastfoot {
    position: fixed;
    bottom: 0;
  }
  /* Start the vertical centering */
  .site-wrapper-inner {
    vertical-align: middle;
  }
  /* Handle the widths */
  .masthead,
  .mastfoot,
  .cover-container {
    width: 100%; /* Must be percentage or pixels for horizontal alignment */
  }
}

@media (min-width: 992px) {
  .masthead,
  .mastfoot,
  .cover-container {
    width: 1000px;
  }
}

</style>

</head>
<body>
  <div class="site-wrapper">
    <div class="site-wrapper-inner">
      <div class="cover-container">
      
	<?php
	// Main website functions go in this class
	Class Main {
	  public static function faucetList()
		{

		}
	}
	$main = New Main();
	$faucets = $main->faucetList();
	  if(isset($_POST['month']) && isset($_POST['day']) && isset($_POST['year']) && !empty($_POST['month']) && !empty($_POST['day']) && !empty($_POST['year'])) {
		  
	  $date = htmlentities($_POST['year']) . '-' . htmlentities($_POST['month']) . '-' . htmlentities($_POST['day']);
	  $timestamp = strtotime($date);
	  $data = json_decode(@file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym=BTC&tsyms=USD&ts=' . $timestamp), 2);
	  $current = json_decode(@file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym=BTC&tsyms=USD'), 2)['BTC']['USD'];
	  }

	if($data['BTC']['USD'] > 0) {
	  $amt = htmlentities($_POST['amt']) / $data['BTC']['USD'];
	} else {
	  $amt = '0';
	}
	?>
   
   	<?php if (isset($data)) { ?>
		<div class="inner cover">
			<?php if (new DateTime() < new DateTime($date)) { ?>
				<h1 class="cover-heading">We can't predict the future price of BTC.... Try again</h1>
				<p class="lead">
					Unless you have a time machine we could use
				</p>
				<p class="lead">
					<a href="<?php echo $site_config['website_url']; ?>" class="btn btn-lg btn-default btn-green" class="btn btn-info btn-lg">Try again</a>
				</p>
			<?php  } elseif($data['BTC']['USD'] > 0) { ?>
				<h1 class="cover-heading"><?php echo 'If you invested <span class="value">$' . number_format(htmlentities($_POST['amt']), 2) . '</span> in BTC on ' . $date . '</br> Your BTC would now be worth <span class="value">$' . number_format($amt * $current); ?></span></h1>
				<p class="lead">
					Don't worry, it's never too late to get started with Cryptocurrency.</br>You can start by multiplying your Bitcoin..
				</p>
				<p class="lead">
					<button class="btn btn-lg btn-default btn-green" class="btn btn-info btn-lg" data-toggle="modal" data-target="#faucetModal">Learn More</button>
				</p>
			<?php } else { ?>
				<h1 class="cover-heading">We don't have any data recorded for that date.. Try again</h1>
				<p class="lead">
					<a href="<?php echo $site_config['website_url']; ?>" class="btn btn-lg btn-default btn-green" class="btn btn-info btn-lg">Try again</a>
				</p>
			<?php } ?>
		</div>
   
		 <!-- Faucet Modal -->
		<div id="faucetModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content faucet-modal">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  </div>
			  <div class="modal-body">
				<div class="container">
					<div align="center">
						<form method="POST" action="/api/lead">
							<h1><strong>CAPTURE PAGE TEST FORM</strong></h1>
							<input class="form-control text-center" type="input" name="fname" placeholder="Enter your First Name">
							<input class="form-control text-center" type="input" name="lname" placeholder="Enter your Last Name">
							<input class="form-control text-center" type="input" name="email" placeholder="Enter your Email Address">
							<input class="form-control text-center" type="input" name="phone" placeholder="Enter your Phone Number">
							<input type="hidden" name="tag[]" value="multiplymybtc">
							<input type="hidden" name="redirect" value="http://www.google.com">
							<input type="hidden" name="campaign" value="KzYVDQ3ayQVxr6z0VPKfQV79IUmTMt">
							<input type="hidden" name="userID" value="addixtudio">
							<button class="btn btn-default" type="submit">Submit</button>
						</form>
					</div>
				</div>
			  </div>
			</div>

		  </div>
		</div>
    <?php } ?>
     
     
<?php if (!isset($data)) { ?>
	<?php	
	$main = New Main();
	$faucets = $main->faucetList();
	$days = range(01, 31);
	$years = range(2011, date("Y"));
	$months = array(
	  '01' => 'Jan',
	  '02' => 'Feb',
	  '03' => 'March',
	  '04' => 'April',
	  '05' => 'May',
	  '06' => 'June',
	  '07' => 'July',
	  '08' => 'Aug',
	  '09' => 'Sep',
	  '10' => 'Oct',
	  '11' => 'Nov',
	  '12' => 'Dec'
	);
	?>
	
	<div class="inner cover">
	  <form class="form-inline" method="POST" action="<?php echo $site_config['website_url']; ?>">
	  <h1 class="cover-heading main">What if you invested <input type="text" class="form-control input-lg form-amt" name="amt" value="100" /> USD in BTC on
		<select name="month" class="form-control input-lg">
		<?php
		foreach($months as $k => $v) {
		  echo '<option value="' . $k . '">' . $v . '</option>';
		}
		?>
		</select>
		<select name="day" class="form-control input-lg">
		<?php
		foreach($days as $day) {
		  (strlen($day) <= 1) ? $day = '0' . $day : $day = $day;
		  echo '<option value="' . $day . '">' . $day . '</option>';
		}
		?>
		</select>
		<select name="year" class="form-control input-lg">
		<?php
		foreach($years as $year) {
		  echo '<option value="' . $year . '">' . $year . '</option>';
		}
		?>
	  </select>
	  </h1>
	  <p class="lead">
		<button type="submit" class="btn btn-lg btn-default btn-green">Find out how rich you would have been</button>
	  </p>
	</form>
	</div>
<?php } ?>
	
    
    
    
<div class="mastfoot">
  <div class="inner">
    <p>&copy Copyright <a href="<?php echo $site_config['website_url']; ?>"><?php echo $page_info['title']; ?></a> 2017 All Rights reserved.</p>
  </div>
</div>

</div>

</div>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


</body>
</html>