<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Tank_auth
 *
 * Tracking library for ISD.
 *
 * @package		Tracking
 * @author		Addixtudio Corp (http://addixtudio.com/)
 * @version		1.2.0
 * @license		Copyright (c) 2017 All Rights Reserved
 */

class Tracking
{
    private $error = array();

    function __construct()
    {
        $this->ci =& get_instance();

        $this->ci->load->library('session');
        $this->ci->load->database();
        $this->ci->load->model('User_Model');

        // start tracking
        $this->activateTracking();
    }


    function activateTracking()
    {
		//get the required data
		$ua = $this->getBrowser();
		$data['visitor_ip'] = GetHostByName($_SERVER['REMOTE_ADDR']); // this is the visitors Internet protocol
		$geoData = $this->geoCheckIP($data['visitor_ip']);
		
		$sponsorData = $this->ci->session->userdata('sponsor');
		$sponsorData = $this->ci->User_Model->getUserByLogin($sponsorData['username']);

		$data['visitor_browser']  = $ua['name']; // useragent name. chrome, firefox, safari et cetra"
		$data['visitor_platform'] = $ua['platform']; // user platform id linux, windows or macos
		$data['visitor_host']     = $_SERVER['HTTP_HOST'];
		#$data['visitor_date']     = date("d-m-Y G:i:s"); // visitors date of visit
		$data['visitor_country']  = $geoData['visitor_country'];
		$data['visitor_state']    = $geoData['visitor_state'];
		$data['visitor_town']     = $geoData['visitor_town'];
		$data['idUser']  = ($sponsorData['id'] > 0 ? $sponsorData['id'] : 0);
		
		//write the required data to database
		$query = $this->ci->db->get_where('tbl_visitors',array('visitor_ip'=>$data['visitor_ip']));
		$row = $query->result_array();
		
		if ($row[0]['visitor_ip'] == $data['visitor_ip']) {
			
			#var_dump($row[0]);
			
			$data['visitor_count'] = (int)$row[0]['visitor_count'] + 1;
			#echo "IP already in database. Date and time updated!<br>\n";

			/* if you have changed the database or 
			 * if you have another database you want to use, change
			 * 'visitors' to that name */
			$this->ci->db->where(array('visitor_ip'=>$data['visitor_ip']))->update('tbl_visitors', array('visitor_count'=>$data['visitor_count']));
			
			if ($row[0]['idUser'] == 0 ) {
				$this->ci->db->where(array('visitor_ip'=>$data['visitor_ip']))->update('tbl_visitors', array('visitor_sponsor'=>$data['visitor_sponsor']));
			}

		} else if ($data['idUser'] != 0) {
			
			$this->ci->db->insert('tbl_visitors', $data);
			#echo "IP added! <br>\n";
		}
		
		/* -- DB --
		$query = $this->ci->db->select('id, isActive, isConfirmed, szUniqueKey')
                ->from('tbl_users')
                ->where(array('isDeleted'=>0))
                ->group_start()
                ->where(array('szEmail'=>'asdf'))
                ->or_where(array('szUserName'=>'asdf'))
                ->group_end()
                ->get();
		
		var_dump($query->result_array());

    	if($query->num_rows() > 0)
        {
            $rows = $query->result_array();
            if((int)$rows[0]['isActive'] != 1)
            {
                return false;
            }
            return true;
        }
        return  false; -- DB END -- */
    }
	
	// function to get the user platform
	function getBrowser()
	{
		// get user agent information	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];

		// if browser or name are unknown
		$browser_name = 'Unknown browser';
		$platform     = 'Unknown platform';

		// Match platform	
		if (preg_match('/linux/i', $user_agent)) {

			$platform = 'Linux';

		}
		if (preg_match('/android/i', $user_agent)) {

			$platform = 'Android';

		} elseif (preg_match('/blackberry/i', $user_agent)) {

			$platform = 'BlackBerry';

		} elseif (preg_match('/webos/i', $user_agent)) {

			$platform = 'Mobile';

		} elseif (preg_match('/ipod/i', $user_agent)) {

			$platform = 'iPod';

		} elseif (preg_match('/ipad/i', $user_agent)) {

			$platform = 'iPad';

		} elseif (preg_match('/iphone/i', $user_agent)) {

			$platform = 'iPhone';

		} elseif (preg_match('/macintosh|mac os x/i', $user_agent)) {

			$platform = 'Mac';

		}
		if (preg_match('/mac_powerpc/i', $user_agent)) {

			$platform .= ' OS 9';

		} elseif (preg_match('/macintosh/i', $user_agent)) {

			$platform .= ' OS X';

		} elseif (preg_match('/windows|win32/i', $user_agent)) {

			$platform = 'Windows';

		}
		if (preg_match('/win16/i', $user_agent)) {

			$platform .= ' 3.11';

		} elseif (preg_match('/win95/i', $user_agent)) {

			$platform .= ' 95';

		} elseif (preg_match('/win98/i', $user_agent)) {

			$platform .= ' 98';

		} elseif (preg_match('/windows me/i', $user_agent)) {

			$platform .= ' ME';

		} elseif (preg_match('/windows nt 5.0/i', $user_agent)) {

			$platform .= ' 2000';

		} elseif (preg_match('/windows nt 5.1/i', $user_agent)) {

			$platform .= ' XP';

		} elseif (preg_match('/windows nt 5.2/i', $user_agent)) {

			$platform .= ' XP x64 / server 2003';

		} elseif (preg_match('/windows nt 5.0/i', $user_agent)) {

			$platform .= ' Vista';

		} elseif (preg_match('/windows nt 6.1/i', $user_agent)) {

			$platform .= ' 7';

		} elseif (preg_match('/windows nt 6.2/i', $user_agent)) {

			$platform .= ' 8';

		}

		// match browser
		if (preg_match('/MSIE/i', $user_agent) && !preg_match('/Opera/i', $user_agent)) {

			$browser_name = 'Internet Explorer';

		} elseif (preg_match('/Chrome/i', $user_agent)) {

			$browser_name = 'Chrome';

		} elseif (preg_match('/Firefox/i', $user_agent)) {

			$browser_name = 'Firefox';

		} elseif (preg_match('/Opera/i', $user_agent)) {

			$browser_name = 'Opera';

		} elseif (preg_match('/Netscape/i', $user_agent)) {

			$browser_name = 'Netscape';

		} elseif (preg_match('/Safari/i', $user_agent)) {

			$browser_name = 'Safari';

		}
		if ($platform == 'Android' || $platform == 'BlackBerry' || $platform == 'Mobile' || $platform == 'iPod' || $platform == 'iPad' || $platform == 'iPhone') {

			if (preg_match('/mobile/i', $user_agent)) {

				$browser_name = 'Handheld browser';

			}

		}

		return array(
			'userAgent' => $user_agent,
			'name' => $browser_name,
			'platform' => $platform
		);
	}
	
	//Get an array with geoip-infodata
	function geoCheckIP($visitor_ip)
	{
		//check, if the provided ip is valid
		if (!filter_var($visitor_ip, FILTER_VALIDATE_IP)) {
			throw new InvalidArgumentException("IP is not valid");
		}

		//contact ip-server
		$response = @file_get_contents('http://www.netip.de/search?query=' . $visitor_ip);
		if (empty($response)) {
			throw new InvalidArgumentException("Error contacting Geo-IP-Server");
		}

		//Array containing all regex-patterns necessary to extract ip-geoinfo from page
		$patterns            = array();
		//$patterns["isp"]     = '#Domain: (.*?)&nbsp;#i';
		$patterns["visitor_country"] = '#Country: (.*?)&nbsp;#i';
		$patterns["visitor_state"]   = '#State/Region: (.*?)<br#i';
		$patterns["visitor_town"]    = '#City: (.*?)<br#i';

		//Array where results will be stored
		$ipInfo = array();

		foreach ($patterns as $key => $pattern) {

			$ipInfo[$key] = preg_match($pattern, $response, $value) && !empty($value[1]) ? $value[1] : 'not found';
		}

		return $ipInfo;
	}
}