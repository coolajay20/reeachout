
    <!-- Custom styles for this template -->
    <style>

		.navbar {
		  margin-bottom: 0;
		}

		.jumbotron {
		  padding-top: 6rem;
		  padding-bottom: 6rem;
		  margin-bottom: 0;
		  background-color: #fff;
		}

		.jumbotron p:last-child {
		  margin-bottom: 0;
		}

		.jumbotron-heading {
		  font-weight: 300;
		}

		.jumbotron .container {
		  max-width: 40rem;
		}

		.album {
		  min-height: 50rem; /* Can be removed; just added for demo purposes */
		  padding-top: 3rem;
		  padding-bottom: 3rem;
		  background-color: #f7f7f7;
		}

		.card {
		  float: left;
		  width: 33.333%;
		  padding: .75rem;
		  margin-bottom: 2rem;
		  border: 0;
		}

		.card > img {
		  margin-bottom: .75rem;
		}

		.card-text {
		  font-size: 85%;
		}

		footer {
		  padding-top: 3rem;
		  padding-bottom: 3rem;
		}

		footer p {
		  margin-bottom: .25rem;
		}  
		
		.embed-container { position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; border: 3px #333 solid; }
	</style>

	<section class="text-center">
      <div class="container">
		<div class="alert alert-warning">
			<strong>Notice:</strong> The training section is still under development. To get started right away with using this system, simply share the funnels we have built for you. The links are coded to you so that when a prospect clicks to signup or request more information, you will be credited. <a href="https://bitcoinlabs.io/marketing/funnels">https://bitcoinlabs.io/marketing/funnels</a>
		</div>
      </div>
    </section>

    <div class="album text-muted text-center" style="opacity: .1">
      <div class="container">
			<h1 class="box-heading">Which One of The Following BEST Describes YOU?</h1>
			<p class="lead text-muted">Choose from one of the three options that most resembles where you are currently at in your business right now.</p>
        <div class="row">
        
          <div class="col-sm-12 col-lg-4 py-4">
          	<img src="<?php echo base_url();?>assets/images/icons/Business-30.png" alt="Just Getting Started">
           	<h3><strong>1. "I'm Just Getting Started!"</strong></h3>
            <p class="card-text">Click below for your free training to learn where to start and the next action steps for you to take in your business to see results!</p>
            <a href="#" class="btn btn-primary btn-lg" style="display: inline-block;">Begin Training</a>
          </div>
          <div class="col-sm-12 col-lg-4 py-4">
          	<img src="<?php echo base_url();?>assets/images/icons/Business-60.png" alt="I'm Ready to Share!">
           	<h3><strong>2. "I'm Ready to Share!"</strong></h3>
            <p class="card-text">If you'd like to learn more about sharing this opportunity and reaching more people with USI Tech, click below!</p>
            <a href="#" class="btn btn-primary btn-lg" style="display: inline-block;">Get Social</a>
          </div>
          <div class="col-sm-12 col-lg-4 py-4">
          	<img src="<?php echo base_url();?>assets/images/icons/Business-15.png" alt="I'm Ready to Grow!">
           	<h3><strong>3. "I'm Ready to Grow!"</strong></h3>
            <p class="card-text">For free training to scale your business, reach a larger audience, and impact more people, click below for trainings!</p>
            <a href="#" class="btn btn-primary btn-lg" style="display: inline-block;">Start Growing</a>
          </div>

        </div>

      </div>
    </div>