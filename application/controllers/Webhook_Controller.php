<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
class Webhook_Controller extends CI_Controller  
{ 
    function __construct() 
    { 
        parent::__construct();    
         
        /* Load form helper */  
        $this->load->helper('form');
 
        /* Load form validation library */  
        $this->load->library('form_validation'); 

        /* Changing error delimiters Globally */ 
        $this->form_validation->set_error_delimiters('', '');      
    }    

    public function campaign() 
    {  
        echo "Hello welcome this is webhook end point for E-mail Campaign.";
        die;
        $postdata = file_get_contents("php://input");
        if(!empty($postdata))
        { 
            $responseAry = json_decode($postdata);
            $responseArys = $responseAry[0];

            $newResponseAry = array();
            if(!empty($responseArys))
            {
                foreach($responseArys as $key=>$responseAryss)
                {
                    $newResponseAry[$key] = $responseAryss ;
                }
            }  
            $szEmailKey = $newResponseAry['email_key']; 
            $szEmailStatus = $newResponseAry['event']; 
            
            /* Load Email Model */ 
            $this->load->model('Email_Model');
            
            /* Load User Model */ 
            $this->load->model('User_Model');
            
            $addSendgridApiLogs = array();
            $addSendgridApiLogs['szEmailKey'] = $szEmailKey;
            $addSendgridApiLogs['szWebhookData'] = serialize($newResponseAry);
            $addSendgridApiLogs['szReferrer'] = __HTTP_REFERER__;
            $addSendgridApiLogs['dtCreated'] = date('Y-m-d H:i:s');
            
            //Logging sendgrid webhook post data
            $this->Email_Model->addSendgriApiLogs($addSendgridApiLogs); 
            
            if(!empty($szEmailKey))
            {  
                //fetching email details by sendgrid api unique email key
                $arEmailData = $this->Email_Model->getEmailByKey($szEmailKey); 
                if(!empty($arEmailData) && isset($arEmailData['id']) && $arEmailData['id']>0)
                {
                    if(isset($arEmailData['iCampaignEmail']) && $arEmailData['iEmailType']==2)
                    {
                        //Adding email reporting data for E-mail campaign
                        $arAddReportingData = array(); 
                        $arAddReportingData['idUser'] = $arEmailData['idUser'];
                        $arAddReportingData['idContact'] = $arEmailData['idContact'];
                        $arAddReportingData['idEmailCampaign'] = $arEmailData['idEmailCampaign'];
                        $arAddReportingData['idEmailTemplate'] = $arEmailData['idEmailTemplate'];
                        $arAddReportingData['szEmailKey'] = $szEmailKey;
                        $arAddReportingData['szEmailStatus'] = $szEmailStatus;
                        $arAddReportingData['dtEmailStatusUpdated'] = date('Y-m-d H:i:s');  
                        
                        $this->Email_Model->addEmailReportingData($arUpdateEmailData); 
                    } 
                    
                    //Updating latest email status on email
                    $arUpdateEmailData = array();
                    $arUpdateEmailData['szEmailStatus'] = $szEmailStatus;
                    $arUpdateEmailData['dtEmailStatusUpdated'] = date('Y-m-d H:i:s');
                    $arUpdateEmailData['id'] = $arEmailData['id'];
                    $this->Email_Model->updateEmailLogs($arUpdateEmailData);
                }
            }
        }
    }
    
    function stripe()
    {
        // Retrieve the request's body and parse it as JSON
        $input = @file_get_contents("php://input");
        $event_json = json_decode($input);
        $log_file = "subscriptions/payments_" . date('Ymd');
        if(!empty($event_json))
        {
            write_log($log_file, "\n*************New notification from Stripe on " . date('m/d/Y h:i A') . "*************\n"); //Data: " . print_r($event_json,true) . "\n");
            
            if($event_json->type == 'customer.subscription.updated' || $event_json->type == 'customer.subscription.created' || $event_json->type == 'customer.subscription.deleted')
            {
                $data = array();
                $arSubscription = (array)$event_json->data->object;
                
                // get user details
                $idUser = 0;
                $arUserDetails = array();
                if(trim($arSubscription['customer']) != '')
                {
                    $query = $this->db->select('id, szFirstName, szEmail')->from('tbl_users')->where('szStripeID', trim($arSubscription['customer']))->get();
                    if($query->num_rows() > 0)
                    {
                        $rows = $query->result_array();
                        $idUser = $rows[0]['id'];
                        $arUserDetails = $rows[0];
                    }
                
                    if($idUser > 0)
                    {
                        if(!empty($arSubscription['id']))
                        {
                            // get user subscription
                            $this->load->model('Membership_Model');
                            $arUserSubscription = $this->Membership_Model->getUserSubscriptionByStripeID($arSubscription['id']);
                            if(!empty($arUserSubscription))
                            {                        
                                // Save Subscription
                                $data['id'] = $arUserSubscription['id'];
                                $data['tTrialStart']= (int)$arSubscription['trial_start'];
                                $data['tTrialEnd']= (int)$arSubscription['trial_end'];
                                $data['iCancelAtTermEnd']= (int)$arSubscription['cancel_at_period_end'];
                                $data['tCurrentTermStart']= (int)$arSubscription['current_period_start'];
                                $data['tCanceledAt'] = (int)$arSubscription['canceled_at'];
                                $data['tCurrentTermEnd'] = (int)$arSubscription['current_period_end'];
                                $data['szStatus'] = $arSubscription['status'];
                                $this->Membership_Model->updateUsereSubscription($data);


                                //log the event
                                write_log($log_file, "Success: Subscription {$arSubscription['id']} saved, Data: ".print_r($data, true).", EVENT: {$event_json->type} TIME: " . date("m/d/Y h:i:s A") . "!\n\n");					
                            }
                            else
                            {
                                 write_log($log_file, "No subscription found in our database for ID #{$arSubscription['id']}, Query: " . $this->Membership_Model->db->last_query() . ", on " . date('m/d/Y h:i A') . ".\n");
                            }
                        }
                        else
                        {	
                            write_log($log_file, "Subscription Error: Subscription id is Blank, EVENT: {$event_json->type} TIME: " . date("m/d/Y h:i:s A") . "!\n\n");					
                        }
                    }
                    else
                    {
                        write_log($log_file, "No user found in our database with Strip customer ID {$arSubscription['customer']} for payment ID {$arSubscription['id']} on " . date('m/d/Y h:i A') . ".\n");
                    }
                }
                else
                {
                    write_log($log_file, "No customer was set on stripe for the payment ID {$arSubscription['id']} on " . date('m/d/Y h:i A') . ".\n");
                }                                
            }
            else if($event_json->type == 'charge.succeeded' || $event_json->type == 'charge.captured' || $event_json->type == 'charge.pending' || $event_json->type == 'charge.updated' || $event_json->type == 'charge.refunded')
            {
                $arCharge = (array)$event_json->data->object;
                
                // get user details
                $idUser = 0;
                $arUserDetails = array();
                if(trim($arCharge['customer']) != '')
                {
                    $query = $this->db->select('id, szFirstName, szEmail')->from('tbl_users')->where('szStripeID', trim($arCharge['customer']))->get();
                    if($query->num_rows() > 0)
                    {
                        $rows = $query->result_array();
                        $idUser = $rows[0]['id'];
                        $arUserDetails = $rows[0];
                    }
                
                    if($idUser > 0)
                    {
                        // get refunds details
                        $fTotalRefunds = 0;
                        $arRefunds = $arCharge['refunds']->data;
                        if(!empty($arRefunds))
                        {
                            foreach($arRefunds as $refund)
                            {
                                $refund = (array)$refund;						
                                $fTotalRefunds += (float)($refund['amount']/100);
                            }
                        }
                        
                        // get user subscription
                        $this->load->model('Membership_Model');
                        $arSubscription = $this->Membership_Model->getUserSubscription($idUser);
                        
                        $arTransaction=array();
                        $arTransaction['idUser'] = $idUser;
                        $arTransaction['szTransactionID'] = $arCharge['id'];
                        $arTransaction['fAmount'] = (float)($arCharge['amount']/100);
                        $arTransaction['fAmountRefunded'] = $fTotalRefunds;
                        $arTransaction['isRefunded'] = (int)$arCharge['refunded'];
                        $arTransaction['isPaid'] = (int)$arCharge['paid'];
                        $arTransaction['tCreated'] = $arCharge['created'];
                        $arTransaction['szStatus']= ((int)$arCharge['refunded'] == 1 ? 'Refunded' : ($fTotalRefunds > 0 ? 'Partially Refunded' : ($arTransaction['isPaid'] == 1 ? 'Paid' : 'Pending')));
                        $arTransaction['szReference'] = (!empty($arSubscription) ? $arSubscription['szPlanName'] : '');
                        $arTransaction['szReferenceID'] = (!empty($arSubscription) ? $arSubscription['szPlanID'] : '');
                        $arTransaction['szType'] = 'Monthly';

                        // save in database
                        if($idTransaction = $this->Membership_Model->saveUserTransactions($arTransaction))
                        {
                            write_log($log_file, "Transaction has been saved successfully with ID #{$idTransaction} and Data: " . print_r($arTransaction,true) . " on " . date('m/d/Y h:i A') . ".\n");
                        }
                        else
                        {
                            write_log($log_file, "Transaction not get saved on " . date('m/d/Y h:i A') . ".\n");						
                        }
                        
                        // send email
                        $arEmailData = array();
                        $arEmailData['szFirstName'] = $arUserDetails['szFirstName'];
                        $arEmailData['szEmail'] = $arUserDetails['szEmail'];
                        $arEmailData['szSiteName'] = WEBSITE_NAME;
                        $arEmailData['szInvoiceNumber'] = $arTransaction['szTransactionID'];
                        if($event_json->type == 'charge.succeeded')
                        {
                            $this->load->model('Email_Model');
                            $arEmailData['fAmount'] = format_number($arTransaction['fAmount']);
                            $this->Email_Model->sendSystemEmails($arEmailData,'PAYMENT_EMAIL');
                        }
                        else if($event_json->type == 'charge.refunded')
                        {
                            $this->load->model('Email_Model');
                            $arEmailData['fAmount'] = format_number($arTransaction['fAmountRefunded']);
                            $this->Email_Model->sendSystemEmails($arEmailData,'REFUND_EMAIL');
                        }
                    }
                    else
                    {
                        write_log($log_file, "No user found in our database with Strip customer ID {$arCharge['customer']} for payment ID {$arCharge['id']} on " . date('m/d/Y h:i A') . ".\n");
                    }
                }
                else
                {
                    write_log($log_file, "No customer was set on stripe for the payment ID {$arCharge['id']} on " . date('m/d/Y h:i A') . ".\n");
                }
            }
            else
            {
                write_log($log_file, "Unknown notification type {$event_json->type} on " . date('m/d/Y h:i A') . ".\n");
            }
        }
        else
        {
            write_log($log_file, "Invalid notification with no data on " . date('m/d/Y h:i A') . ".\n\n");
        }
    } 
}

?>