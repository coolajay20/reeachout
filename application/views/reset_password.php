<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>Reset Password</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/skin/default_skin/css/theme.css">
  
  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body class="admin-validation-page external-page external-alt sb-l-c sb-r-c">
  <!-- Start: Main -->
  <div id="main" class="animated fadeIn">

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- begin canvas animation bg -->
      <div id="canvas-wrapper">
        <canvas id="demo-canvas"></canvas>
      </div>

      <!-- Begin: Content -->
      <section id="content" class="animated fadeIn">

        <div class="admin-form theme-info mw500" style="margin-top: 10%;" id="login">
          <div class="row mb15 table-layout">

            <div class="col-xs-6 pln">
              	<img src="<?php echo base_url();?>assets/images/logo_beta.png" title="Reset Password | Bitcoin Labs" class="img-responsive" style="max-width: 275px;">            	
            </div>

            <div class="col-xs-6 va-b">
              <h3 class="text-right">
                Reset Password
              </h3>
            </div>
          </div>
		  
		  <?php if($passwordChanged){?>
		  <div class="alert alert-success" style="margin-top: 3%;">
          	<i class="fa fa-check pr10"></i>
           	<strong> Congratulations!</strong>
           	Your acount password has been updated successfully, <a href="<?php echo base_url();?>login">click here to login</a>.
     	  </div>
		  <?php } else {?>
          <div class="panel">

            <form method="post" action="<?php echo base_url();?>reset-password/<?php echo $szUserKey;?>" id="reset-password-form" class="validate-form">
              <div class="panel-body p15">

                <div class="alert alert-micro alert-border-left alert-info pastel alert-dismissable mn">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <i class="fa fa-info pr10"></i> Enter your
                  <b>New Password</b>!
                </div>

              </div>
              <!-- end .form-body section -->
              <div class="panel-footer p25 pv15">

                <?php $szNewPasswordError = form_error('arChange[szNewPassword]');?>
	          	<div class="section">
               		<label for="szNewPassword" class="field prepend-icon<?php if(!empty($szNewPasswordError)){?> state-error<?php }?>">
                   		<input name="arChange[szNewPassword]" id="szNewPassword" class="gui-input required" placeholder="New password" type="password" value="<?php echo set_input_value('arChange[szNewPassword]');?>" autocomplete="off">
                 		<label for="szNewPassword" class="field-icon">
                   			<i class="fa fa-user"></i>
                      	</label>
                  	</label>
              		<?php if(!empty($szNewPasswordError)){?><em class="state-error" for="szNewPassword"><?php echo $szNewPasswordError;?></em><?php }?>
                        </div>
                    		
              	<?php $szRepeatPasswordError = form_error('arChange[szRepeatPassword]');?>
	           	<div class="section">
               		<label for="szRepeatNewPassword" class="field prepend-icon<?php if(!empty($szRepeatPasswordError)){?> state-error<?php }?>">
                 		<input name="arChange[szRepeatNewPassword]" id="szRepeatNewPassword" class="gui-input required matches" placeholder="Confirm new password" type="password" value="<?php echo set_input_value('arChange[szRepeatNewPassword]');?>" autocomplete="off">
                    	<label for="szRepeatNewPassword" class="field-icon">
                       		<i class="fa fa-user"></i>
                      	</label>
                	</label>
              		<?php if(!empty($szRepeatPasswordError)){?><em class="state-error" for="szRepeatNewPassword"><?php echo $szRepeatPasswordError;?></em><?php }?>
              		</div>
        		
        		<div class="section clearfix">
        			<button type="submit" class="button btn-primary pull-right"> Reset Password </button>	
        		</div>
              </div>
              <!-- end .form-footer section -->
			  <input type="hidden" name="p_func" value="Forgot Password">
            </form>

          </div>
          <?php }?>

        </div>

      </section>
      <!-- End: Content -->

    </section>
    <!-- End: Content-Wrapper -->

  </div>
  <!-- End: Main -->


  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  
  <!-- jQuery Validate Plugin-->
  <script src="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Validate Addon -->
  <script src="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="<?php echo base_url();?>theme/assets/js/utility/utility.js"></script>
  <script src="<?php echo base_url();?>theme/assets/js/demo/demo.js"></script>
  <script src="<?php echo base_url();?>theme/assets/js/main.js"></script>

  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core      
    Core.init();

    // Init Demo JS
    Demo.init();
  });
  
  // Init Base URL
  var base_url = '<?php echo base_url();?>';
  </script>
  
  <!-- Custom Validation Script -->
  <script src="<?php echo base_url();?>assets/js/custom.validate.js?<?php echo filemtime('./assets/js/custom.validate.js');?>"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {
 	validate_form();
  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>

</html>