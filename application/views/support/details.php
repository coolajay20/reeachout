<div class="row">
    <div class="col-sm-15"> 
        <div class="panel panel-default"> 
            <div class="panel-heading">
                <span class="panel-title">Support Ticket Details</span>
                <div class="pull-right">
                    <div class="btn-group mr10"> 
                        <?php if(isset($szType) && $szType!='new'){ ?>
                            <button class="btn btn-sm btn-white tooltips" type="button" data-toggle="tooltip" title="Mark as New" onclick="updateStatus('new');"><i class="glyphicon glyphicon-hdd"></i></button>
                        <?php } if(isset($szType) && $szType!='closed'){ ?>
                            <button class="btn btn-sm btn-white tooltips" type="button" data-toggle="tooltip" title="Mark as Closed" onclick="updateStatus('closed');"><i class="glyphicon glyphicon-hdd"></i></button>
                        <?php } if(isset($szType) && $szType!='pending'){ ?>
                            <button class="btn btn-sm btn-white tooltips" type="button" data-toggle="tooltip" title="Mark as Pending" onclick="updateStatus('pending');"><i class="glyphicon glyphicon-exclamation-sign"></i></button>
                        <?php } if(isset($szType) && $szType!='trash'){ ?>
                            <button class="btn btn-sm btn-white tooltips" type="button" data-toggle="tooltip" title="Delete" onclick="updateStatus('trash');"><i class="glyphicon glyphicon-trash"></i></button>
                        <?php } ?>
                    </div>
                </div><!-- pull-right -->
            </div> 
            <div class="panel-body">
                <form method="post" id="support_ticket_form" name="support_ticket_form">
                    <input type="hidden" name="arTicket[szUniqueKey][]" value="<?php echo $arTicketDetails['szUniqueKey'] ?>" id="<?php echo $arTicketDetails['szUniqueKey'] ?>">
                </form>
                <div class="detailBox">
                    <div class="titleBox">
                      <label>Comment Box</label> 
                    </div>
                    <div class="commentBox"> 
                        <table class="table table-email" style="width:100%;">
                            <tr>
                                <td style="width:33%">Name: <br><h4 class="text-primary"><?php echo get_userdetail($arTicketDetails['idUser'],'szFirstName') .' '. get_userdetail($arTicketDetails['idUser'],'szLastName') ?></h4></td>
                                <td style="width:33%">Category: <br><h4 class="text-primary"><?php echo (isset($szCategory)? $szCategory : 'N/A'); ?></h4></td>
                                <td style="width:33%">Status: <br><h4 class="text-primary"><?php echo (isset($szType) ? ucfirst($szType) : 'N/A'); ?></h4></td>
                            </tr>
                        </table>
                    </div>
                    <div class="actionBox">
                        <ul class="commentList"> 
                            <?php 
                            if(!empty($arTicketThreadDetails))
                            {
                                foreach($arTicketThreadDetails as $arTicketThreadDetailss)
                                {
                                    if(get_userdetail($arTicketThreadDetailss['idUser'],'szAvatarImage') != '') {
                                        $szImageFileName = get_userdetail($arTicketThreadDetailss['idUser'],'szAvatarImage');
                                        $szImagePath = USERS_UPLOAD_PATH."".$szImageFileName; 
                                        if(file_exists($szImagePath))
                                        {
                                            $avatar_src = BASE_USERS_UPLOAD_URL . $szImageFileName;
                                        }
                                        else
                                        {
                                            $avatar_src = base_url() . 'assets/images/profile.png';
                                        } 
                                        //$avatar_src = BASE_USERS_UPLOAD_URL . get_userdetail($arTicketThreadDetailss['idUser'],'szAvatarImage');
                                    } else { 
                                        $avatar_src = base_url() . 'assets/images/profile.png';
                                    } 
                                    ?> 
                                    <li>
                                        <div class="commenterImage">
                                          <img alt="" src="<?php echo $avatar_src; ?>" class="media-object">
                                        </div>
                                        <div class="commentText">
                                            <p class=""><?php echo substr($arTicketThreadDetailss['szMessage'],0) ?></p> <span class="date sub-text">on <?php echo convert_date($arTicketThreadDetailss['dtCreatedOn'],1); ?></span> 
                                        </div>
                                    </li> 
                            <?php } } ?> 
                        </ul>
                        <?php if(isset($szType) && ($szType=='new' || $szType=='pending')){ ?>
                            <form class="form-inline" method="post" name="add_comment_form" id="add_comment_form" action="javascript:void(0);">
                                <div class="form-group no-margin">
                                    <input class="form-control" type="text" name="arComments[szMessage]" id="szMessage" placeholder="Your comments" />
                                    <input type="hidden" name="arComments[szUniqueKey]" value="<?php echo $arTicketDetails['szUniqueKey'] ?>">
                                    <input type="hidden" name="p_func" value="Add comments">
                                </div>
                                <div class="form-group no-margin">
                                    <button class="btn btn-primary" onclick="addComments();">Add</button>
                                </div>
                            </form>
                        <?php } else if(isset($szType) && ($szType=='closed')){ ?>
                            <p> This communication has been marked as <strong>Closed</strong>. Please <a href="javascript:void(0)" onclick="updateStatus('new');">click here</a> to re-open this support ticket.
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>