$('.dt-static').each(function(){
    sort_order = ($(this).hasClass('sort_order_desc') ? 'decs' : 'asc');
    $(this).dataTable({
            "order": [[ 0, sort_order ]],
            "aoColumnDefs": [{
           'bSortable': false,
           'aTargets': [-1]
         }],
         "oLanguage": {
           "oPaginate": {
             "sPrevious": "",
             "sNext": ""
           }
         },
         "iDisplayLength": 10,
         "bLengthChange": true,
         "aLengthMenu": [
           [5, 10, 20, 30, 40, 50, -1],
           [5, 10, 20, 30, 40, 50, "All"]
         ],
         "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"<"pn pt8 col-sm-5 pull-left"l><"col-sm-2 text-right"i>p>',
         "oTableTools": {
           "sSwfPath": base_url + "theme/vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
         }
    });
});

$('.dt-dynamic').each(function(){
    var obj = this;
    var url = $.trim($(obj).attr('data-url'));
    var func = $(obj).attr('data-func');
    var form = $(obj).attr('data-form');
    if(func != undefined) url += "?p_func=" + $.trim(func);
    if(form != undefined) url += (func != 'undefined' ? '&' : '?') + $('#' + $.trim(form)).serialize();
    $(obj).find('thead th').each(function(){           
        if($(this).attr('data-sort') != undefined){
            url += '&sort_options[]=' + $.trim($(this).attr('data-sort'));
        }
    });
    
    $(obj).DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": url,
        "order": [[ 0, 'asc' ]],
        "aoColumnDefs": [{
            'bSortable': false,
            'aTargets': [-1]
        }],
        "oLanguage": {
            "oPaginate": {
                    "sPrevious": "",
                    "sNext": ""
            }
        },
        "iDisplayLength": 10,
        "aLengthMenu": [
            [5, 10, 20, 30, 40, 50, 100],
            [5, 10, 20, 30, 40, 50, 100]
        ],
        "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"<"pn pt8 col-sm-5 pull-left"l><"col-sm-2 text-right"i>p>',
        "oTableTools": {
            "sSwfPath": base_url + "theme/vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
        },
        "createdRow": function( row, data, dataIndex ) {
            var i = 1;
            $(obj).find('thead th').each(function(){
                if($(this).attr('data-class') != undefined){
                    $(row).children(':nth-child('+i+')').addClass($.trim($(this).attr('data-class')));                    
                }
                i++;
            });
        },
        searchDelay: 700,
        "drawCallback": function( settings ) {
            popoverHandler();
            $('[data-toggle=tooltip]').tooltip();
        }
    });
});
