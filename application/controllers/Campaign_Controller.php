<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /* Load Campaign Model */
        $this->load->model('Campaign_Model');

        /* Load form helper */ 
        $this->load->helper('form');

        /* Load form validation library */ 
        $this->load->library('form_validation');

        /* Changing error delimiters Globally */
        $this->form_validation->set_error_delimiters('', '');
    }
    
    public function all()
    {
        $this->index();
    }
    
    public function index($arParams = array())
    {
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        else if(!hasPermission('manage.campaigns'))
        {
            redirect(base_url() . 'dashboard', 'refresh');
        }
        else if(trim($this->input->get('p_func')) == 'Get_Campaigns')
        {
            $arLoginUser = $this->session->userdata('login_user');
            list($iLimit, $iOffset, $iDrawPage, $szKeyword, $arFilters, $szSortField, $szSortOrder) = parseDTRequest($this);
            $iTotalRecords = $this->Campaign_Model->getCampaigns($arFilters, $szKeyword, true);
            $arCampaigns = $this->Campaign_Model->getCampaigns($arFilters, $szKeyword, false, $iLimit, $iOffset, $szSortField, $szSortOrder); 
            $response = array("draw"=>$iDrawPage, "recordsTotal"=>$iTotalRecords, "recordsFiltered"=>$iTotalRecords, "data"=>array()); 
            
            if(!empty($arCampaigns))
            {
                $i = 0;
                foreach($arCampaigns as $template)
                {					
                    $szActionLinks = '<ul>';
                    $szActionLinks .= "<li><a href='".base_url()."campaigns/edit/".$template['szUniqueKey']."' class='text-info'><i class='fa fa-edit'></i> Edit</a></li>";                    
                    $szActionLinks .= "<li><a href='".base_url()."campaigns/delete/".$template['szUniqueKey']."' class='text-info' data-type='Campaign'><i class='fa fa-trash'></i> Delete</a></li>";
                    $szActionLinks .= '</ul>'; 
                    
                    $response['data'][$i][0] = "<p class='fs16'><span class='text-info'>{$template['szName']}</p>"; 
                    $response['data'][$i][1] = count($this->Campaign_Model->getAllContactsForCampaign($template['id']));
                    $response['data'][$i][2] = ($template['isActive'] ? '<span class="text-success"><i class="fa fa-check-circle"></i> Yes</span>' : '<span class="text-danger"><i class="fa fa-times-circle"></i> No</span>');
                    $response['data'][$i][3] = convert_date($template['dtAddedOn'],2);
                    $response['data'][$i][4] = '<button class="btn btn-sm btn-rounded" data-toggle="popover" data-placement="bottom" data-html="true" data-content="'.$szActionLinks.'"><i class="fa fa-ellipsis-h"></i></button>';
                    $i++;				
                }
            }
            echo json_encode($response,true);
        }
        else
        {
            $data['szFavIcon'] = "fa-envelope";
            $data['szPageName'] = "Autoresponder";
            $data['szMetaTagTitle'] = "All Campaigns";		
            $data['isLoadDataTable'] = true;
            $data['isLoadBootBox'] = true;
            $data['arLoginUser'] = $this->session->userdata('login_user');
            
            $data = array_merge($data, $arParams);

            $this->load->view('layout/header', $data);
            $this->load->view('campaigns/list');
            $this->load->view('layout/footer');
        }
    }
    
    function edit($szUserKey='')
    {
        $this->add($szUserKey);
    }

    function add($szUserKey='')
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }
        else if(!hasPermission('manage.campaigns'))
        {
            redirect(base_url().'dashboard', 'refresh');
        }
        else
        {
            // set/initialize varibales
            $data['idCampaign'] = 0;
            $data['isFormError'] = false;
            $data['arCampaignDetails'] = array();
            $data['isDetailsSaved'] = false;
            $szUserKey = trim($szUserKey);		
            $data['arLoginUser'] = $this->session->userdata('login_user');

            if($szUserKey != '')
            {
                $arCampaignDetails = $this->Campaign_Model->getCampaignByUniqueKey($szUserKey);
                if(!empty($arCampaignDetails))
                {
                    $data['idCampaign'] = $arCampaignDetails['id'];
                    $data['arCampaignDetails'] = $arCampaignDetails;
                }
            }
            
            if($this->input->post('p_func') == 'Save Campaign Details')
            {
                $this->form_validation->set_rules('arCampaign[szName]', 'Campaign name', "trim|required|is_unique[tbl_campaigns:szName:id:{$data['idCampaign']}]");
                if ($this->form_validation->run() == true)
                { 
                    $arCampaignDetails = $this->input->post('arCampaign', true);
                    if($data['idCampaign'] > 0) {
                        $arCampaignDetails['id'] = $data['idCampaign'];
                    }

                    if($data['idCampaign'] = $this->Campaign_Model->saveCampaignDetails($arCampaignDetails, $data['arLoginUser']['id']))
                    {
                        $data['isDetailsSaved'] = true;  
                        $data['arCampaignDetails'] = $this->Campaign_Model->getCampaignByID($data['idCampaign']);
                    }
                }
                else
                {
                    $data['isFormError'] = true;
                }
            }
            // load the emails
            if($data['idCampaign'] > 0)
            {
                $data['arTemplates'] = $this->Campaign_Model->getCampaignTemplates($data['idCampaign']);
            }

            $data['szPageName'] = "Autoresponder";
            $data['szMetaTagTitle'] = ($data['idCampaign'] > 0 ? "Edit Campaign" : "Add New Campaign");	
            $data['isLoadAdminForm'] = true;
            $data['isLoadValidationScript'] = true;
            $data['isLoadDTPickerScript'] = true;

            $this->load->view('layout/header', $data);
            $this->load->view('campaigns/add_edit');
            $this->load->view('layout/footer');
        }
    }
    
    function delete($szUserKey='')
    {  
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }
        else if(!hasPermission('manage.campaigns'))
        {
            redirect(base_url().'dashboard', 'refresh');
        }
        else
        {
            $data = array();
            $szUserKey = trim($szUserKey);
            if($szUserKey != '')
            {
                $arTemplateDetails = $this->Campaign_Model->getCampaignByUniqueKey($szUserKey);
                if(!empty($arTemplateDetails))
                {
                    if(1)
                    {
                        if((int)$arTemplateDetails['isDeleted'] == 0)
                        {
                            // get login user details
                            $arLoginUser = $this->session->userdata('login_user');
                            $idUser = (int)$arTemplateDetails['id'];
                            
                            // delete the record
                            if($this->Campaign_Model->deleteCampaign($arTemplateDetails, $arLoginUser['id']))
                            {
                                $data['szSuccessMessage'] = "Campaign {$arTemplateDetails['szName']} has been deleted successfully.";                                
                            }
                            else
                            {
                                $data['szErrorMessage'] = "Problem while deleting campaign {$arTemplateDetails['szName']}.";
                            }
                        }
                        else
                        {
                            $data['szErrorMessage'] = "Email template {$arTemplateDetails['szName']} is already deleted.";
                        }
                    }
                    else
                    {
                        $data['szErrorMessage'] = "Campaign {$arTemplateDetails['szName']} is not removable.";
                    }
                }
                else
                {
                    $data['szErrorMessage'] = 'Campaign record not exists.';
                }
            }
            else
            {
                $data['szErrorMessage'] = 'Invalid campaign delete request.';
            }
            $this->index($data);
        }
    }
    
    public function removeEmail($szCampaignKey, $szEmailKey)
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }
        else if(!hasPermission('manage.campaigns'))
        {
            redirect(base_url().'dashboard', 'refresh');
        }
        else
        {
            $data = array();
            $szCampaignKey = trim($szCampaignKey);
            if($szCampaignKey != '')
            {
                $arCampaignDetails = $this->Campaign_Model->getCampaignByUniqueKey($szCampaignKey);
                if(!empty($arCampaignDetails))
                {
                    $szEmailKey = trim($szEmailKey);
                    if($szEmailKey != '')
                    {
                        $arTemplateDetails = $this->Campaign_Model->getTemplateByUniqueKey($szEmailKey);
                        //print_r($arTemplateDetails);die;
                        if(!empty($arTemplateDetails))
                        {
                            // remove the email
                            $this->db->where(array('id'=>$arTemplateDetails['id'], 'idCampaign'=>$arCampaignDetails['id']))->update('tbl_email_templates', array('idCampaign'=>0));
                        }
                    }
                }
            }
        }
        $this->add($szCampaignKey);
    }
    
    public function templates($arg1='', $arg2='')
    {
        if($arg1 == 'all')
        {
            $this->allTemplates();
        }
        else if($arg1 == 'delete')
        {
            $this->deleteTemplate($arg2);
        }
        else if($arg1 == 'add' || $arg1 == 'edit')
        {
            $this->addEditTemplate($arg2);
        }
    }

    public function allTemplates($arParams = array())
    { 
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        else if(!hasPermission('manage.campaigns'))
        {
            redirect(base_url() . 'dashboard', 'refresh');
        }
        else if(trim($this->input->get('p_func')) == 'Get_Templates')
        {
            $arLoginUser = $this->session->userdata('login_user');
            list($iLimit, $iOffset, $iDrawPage, $szKeyword, $arFilters, $szSortField, $szSortOrder) = parseDTRequest($this);
            $iTotalRecords = $this->Campaign_Model->getTemplates($arFilters, $szKeyword, true);
            $arTemplates = $this->Campaign_Model->getTemplates($arFilters, $szKeyword, false, $iLimit, $iOffset, $szSortField, $szSortOrder); 
            $response = array("draw"=>$iDrawPage, "recordsTotal"=>$iTotalRecords, "recordsFiltered"=>$iTotalRecords, "data"=>array()); 
            
            if(!empty($arTemplates))
            {
                $i = 0;
                foreach($arTemplates as $template)
                {

					$szActionLinks = '<li><a href="'.base_url()."campaigns/templates/edit/".$template['szUniqueKey'].'">Edit</a></li>';
					$szActionLinks .= '<li><a href="'.base_url()."campaigns/templates/delete/".$template['szUniqueKey'].'">Delete</a></li>';
                    
                    $response['data'][$i][0] = "<p class='fs16'><span class='text-info'>{$template['szName']}</p>";
                    $response['data'][$i][1] = "<p class='fs16'><span class='text-info'>{$template['szSubject']}</p>";
                    $response['data'][$i][2] = "<p class='fs16'><span class='text-info'>" . ($template['idCampaign'] > 0 ? $template['idCampaign'] : 'None') . "</p>";
                    $response['data'][$i][3] = convert_date($template['dtAddedOn'],2);
                    $response['data'][$i][4] = '<div class="btn-group"><a data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-cog"></i></a><ul role="menu" class="dropdown-menu pull-right">'.$szActionLinks.'</ul></div>';
                    $i++;				
                }
            }
            echo json_encode($response,true);
        }
        else
        {
            $data['szFavIcon'] = "fa-envelope";
            $data['szPageName'] = "Autoresponder";
            $data['szMetaTagTitle'] = "All Emails";		
            $data['isLoadDataTable'] = true;
            $data['isLoadBootBox'] = true;
            $data['arLoginUser'] = $this->session->userdata('login_user');
            
            $data = array_merge($data, $arParams);

            $this->load->view('layout/header', $data);
            $this->load->view('campaigns/emails/list');
            $this->load->view('layout/footer');
        }
    }

    function addEditTemplate($szUserKey='')
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }
        else if(!hasPermission('manage.campaigns'))
        {
            redirect(base_url().'dashboard', 'refresh');
        }
        else
        {
            // set/initialize varibales
            $data['idTemplate'] = 0;
            $data['isFormError'] = false;
            $data['arTemplateDetails'] = array();
            $data['isDetailsSaved'] = false;
            $szUserKey = trim($szUserKey);		
            $data['arLoginUser'] = $this->session->userdata('login_user');

            if($szUserKey != '')
            {
                $arTemplateDetails = $this->Campaign_Model->getTemplateByUniqueKey($szUserKey);
                if(!empty($arTemplateDetails))
                {
                    $data['idTemplate'] = $arTemplateDetails['id'];
                    $data['arTemplateDetails'] = $arTemplateDetails;
                }
            }
               
            if($this->input->post('p_func') == 'Save Template Details')
            {
                $this->form_validation->set_rules('arTemplate[szName]', 'Email name', "trim|required|is_unique[tbl_email_templates:szName:id:{$data['idTemplate']}]");
                $this->form_validation->set_rules('arTemplate[szSubject]', 'Subject line', "trim|required");
                $this->form_validation->set_rules('arTemplate[szMessageContent]', 'Email message', 'trim|required');
                $this->form_validation->set_rules('arTemplate[idCampaign]', 'Campaign', 'trim|required|integer|greater_than[0]');
                $this->form_validation->set_rules('arTemplate[iDays]', 'Number of days', 'trim|required|integer|greater_than_equal_to[0]');                                

                if ($this->form_validation->run() == true)
                { 
                    $arTemplateDetails = $this->input->post('arTemplate', true);
                    if($data['idTemplate'] > 0) $arTemplateDetails['id'] = $data['idTemplate'];

                    if($data['idTemplate'] = $this->Campaign_Model->saveTemplateDetails($arTemplateDetails, $data['arLoginUser']['id']))
                    {
                        $data['isDetailsSaved'] = true;
                        $data['arTemplateDetails'] = $this->Campaign_Model->getTemplateByID($data['idTemplate']);
                    }
                }
                else
                {
                    $data['isFormError'] = true;
                    //print_r($this->form_validation->error_array());die;
                }
            }
            $data['arContactMergeTags'] = array('szFirstName','szLastName','szFullName','szEmail','szHomePhone','szMobile','szWorkPhone','szUnsubscribe');
            $data['arUserMergeTags'] = array('szUserFirstName','szUserLastName','szUserFullName','szUserEmail','szUserPhone','szSiteUserName','szUSIUserName','szUserAddress','szUserCity','szUserState','szUserCountry'); 
            $data['arSocialMergeTags'] = array('szFacebookLink','szGoogleLink','szTwitterID','szLinkedInID','szDribbbleID','szSkypeID');
              
            // load all active campaign
            $data['arActiveCampaigns'] = $this->Campaign_Model->getAllActiveCampaigns();

            $data['szPageName'] = "Autoresponder";
            $data['szMetaTagTitle'] = ($data['idTemplate'] > 0 ? "Edit Email Template" : "Add New Email Template");	
            $data['isLoadBootBox'] = true;
            $data['isLoadAdminForm'] = true;
            $data['isLoadValidationScript'] = true;
            $data['isLoadEditorScript'] = true;

            $this->load->view('layout/header', $data);
            $this->load->view('campaigns/emails/add_edit');
            $this->load->view('layout/footer');
        }
    }
    
    function deleteTemplate($szUserKey='')
    {  
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }
        else if(!hasPermission('manage.campaigns'))
        {
            redirect(base_url().'dashboard', 'refresh');
        }
        else
        {
            $data = array();
            $szUserKey = trim($szUserKey);
            if($szUserKey != '')
            {
                $arTemplateDetails = $this->Campaign_Model->getTemplateByUniqueKey($szUserKey);
                if(!empty($arTemplateDetails))
                {
                    if(1)
                    {
                        if((int)$arTemplateDetails['isDeleted'] == 0)
                        {
                            // get login user details
                            $arLoginUser = $this->session->userdata('login_user');
                            $idUser = (int)$arTemplateDetails['id'];
                            
                            // delete the record
                            if($this->Campaign_Model->deleteTemplate($arTemplateDetails, $arLoginUser['id']))
                            {
                                $data['szSuccessMessage'] = "Email template {$arTemplateDetails['szName']} has been deleted successfully.";                                
                            }
                            else
                            {
                                $data['szErrorMessage'] = "Problem while deleting email {$arTemplateDetails['szName']}.";
                            }
                        }
                        else
                        {
                            $data['szErrorMessage'] = "Email template {$arTemplateDetails['szName']} is already deleted.";
                        }
                    }
                    else
                    {
                        $data['szErrorMessage'] = "Email template {$arTemplateDetails['szName']} is not removable.";
                    }
                }
                else
                {
                    $data['szErrorMessage'] = 'Email template record not exists.';
                }
            }
            else
            {
                $data['szErrorMessage'] = 'Invalid email template delete request.';
            }
            $this->allTemplates($data);
        }
    }
}
?>