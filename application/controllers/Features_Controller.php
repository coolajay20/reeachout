<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Features_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        else if(!hasPermission('manage.settings'))
        {
            redirect(base_url() . 'dashboard', 'refresh');
        }
        else
        {
            /* Load Features Model */
            $this->load->model('Features_Model');

            /* Load form helper */ 
            $this->load->helper('form');

            /* Load form validation library */ 
            $this->load->library('form_validation');

            /* Changing error delimiters Globally */
            $this->form_validation->set_error_delimiters('', '');
        }
    }

    public function index($arParams=array())
    {
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        else
        {
			$data['szFavIcon'] = "fa-star";
            $data['szPageName'] = "Membership (Admin)";
            $data['szMetaTagTitle'] = "Features";
            $data['arLoginUser'] = $this->session->userdata('login_user');
            $data['isLoadDataTable'] = true;
            $data['arFeatures'] = $this->Features_Model->getAllFeatures();

            $data = array_merge($data, $arParams);
            
            // set breadcrumb
            $data['arBreadCrumb'][] = array('name'=>'Dashboard', 'link'=>'dashboard');
            $data['arBreadCrumb'][] = array('name'=>'Features', 'link'=>'features');

            $this->load->view('layout/header', $data);
            $this->load->view('features/list');
            $this->load->view('layout/footer');
        }
    }

    public function edit($szFeaturesKey='')
    {
        $this->add($szFeaturesKey);		
    }

    public function add($szFeaturesKey='')
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }				
        else
        {
            $data['idFeatures'] = 0;
            $data['isFormError'] = false;
            $data['isDetailsSaved'] = false;
            $data['arFeaturesDetails'] = array();
            $data['arLoginUser'] = $this->session->userdata('login_user');	

            if($this->input->post('p_func') == 'Save Features Details')
            {			
                $idFeatures = (int)$this->input->post('arFeatures[id]');

                //Set validation rules 
                $this->form_validation->set_rules('arFeatures[szFeatureName]', 'Features name', "trim|required|is_unique[tbl_features:szFeatureName:id:{$idFeatures}]"); 

                if($this->form_validation->run() == TRUE) 
                {				
                    $arFeaturesDetails = $this->input->post('arFeatures');
                    if($data['idFeatures'] = $this->Features_Model->saveFeaturesDetails($arFeaturesDetails, $data['arLoginUser']['id']))
                    {
                        $data['isDetailsSaved'] = true;  
                    }
                }
                else
                {
                    $data['isFormError'] = true;
                }
            }

            if($szFeaturesKey)
            {
                $data['arFeaturesDetails'] = $this->Features_Model->getFeaturesByUniqueKey($szFeaturesKey);
                $data['idFeatures'] = (isset($data['arFeaturesDetails']['id']) ? (int)$data['arFeaturesDetails']['id'] : 0);
            }
            else if($data['idFeatures'] > 0)
            {
                $data['arFeaturesDetails'] = $this->Features_Model->getFeaturesByID($data['idFeatures']);
            }

			$data['szFavIcon'] = ($data['idFeatures'] > 0 ? 'fa-pencil' : 'fa-plus');
            $data['szPageName'] = "Membership (Admin)";
            $data['szMetaTagTitle'] = ($data['idFeatures'] > 0 ? 'Edit Features' : 'Add New Feature');	
            $data['isLoadAdminForm'] = true;
            $data['isLoadCMSScripts'] = true;
            $data['isLoadValidationScript'] = true;

            $this->load->view('layout/header', $data);
            $this->load->view('features/add_edit');
            $this->load->view('layout/footer');
        }
    }

    function delete($szFeaturesKey='')
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }	

        $message = array();
        $szFeaturesKey = trim($szFeaturesKey);
        if($szFeaturesKey != '')
        {
            $arFeaturesDetails = $this->Features_Model->getFeaturesByUniqueKey($szFeaturesKey);
            if(!empty($arFeaturesDetails))
            {
                if($arFeaturesDetails['isDeleted'] == 0)
                {
                    $arLoginUser = $this->session->userdata('login_user');
                    $this->Features_Model->deleteFeatures($arFeaturesDetails, $arLoginUser['id']);
                    $message['szSuccessMessage'] = 'Features has been deleted successfully';
                }
                else
                {
                    $message['szErrorMessage'] = "Features is already deleted.";
                }
            }
            else
            {
                $message['szErrorMessage'] = "Features not found.";
            }
        }
        else
        {
            $message['szErrorMessage'] = "Invalid Features delete request.";
        } 
        $this->index($message);
    }
}
?>