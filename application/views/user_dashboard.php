	<section id="content_wrapper">
            
		<div class="alert alert-warning">
		  <strong>Notice:</strong> System is currently under development and not all features are fully functional. We'll provide more information regarding our official launch soon.
		</div>

            <!-- Start: Topbar -->

            <header id="topbar" class="alt">

        	<div class="topbar-left">

                    <ol class="breadcrumb">

                        <li class="crumb-active">

                            Dashboard

            		</li>

                    </ol>

        	</div>        	

     	</header>

      	<!-- End: Topbar -->



      	<!-- Begin: Content -->

      	<section id="content" class="animated fadeIn">

            <div class="row"> 

                <div class="panel panel-primary">

                    <div class="panel-heading">

                        <span class="panel-title">

                            <i class="fa fa-user"></i>

                            Personal Details

                        </span>

                        <span class="panel-controls">

                            <a href="<?php echo base_url();?>users/edit/<?php echo $arUserDetails['szUniqueKey'];?>">Edit</a>

                        </span>

                    </div>

                    <div class="panel-body">

                        <table class="table table-hover table-details mb20"> 

                            <tr>

                                <td>

                                    <div class="text-center mb20">

                                        <?php

                                        if($arUserDetails['szAvatarImage'] != '')

                                        {

                                            $p_image_src = BASE_USERS_UPLOAD_URL . $arUserDetails['szAvatarImage'];

                                        }

                                        else

                                        {

                                            $p_image_src = base_url() . 'assets/images/profile.png';

                                        }

                                        ?>

                                        <img class="avatar avatar-preview img-circle" class="mb20" src="<?php echo $p_image_src;?>" alt="Avatar Image">      
                                        
                                        <p align="center"><a href="<?php echo base_url();?>users/edit/<?php echo $arUserDetails['szUniqueKey'];?>">Edit Profile</a></p>                           
                                    </div> 

                                </td>

                                <td>

                                    <table class="table table-hover table-details mb20">

                                        <thead>

                                            <tr>

                                                <th colspan="3" class="pn"><h3 class="mtn">Contact Details</h3></th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                            <tr>

                                                <td>Email</td>

                                                <td>

                                                    <a href="mailto:<?php echo $arUserDetails['szEmail'];?>"><?php echo $arUserDetails['szEmail'];?></a>

                                                    <span class="text-<?php echo ($arLoginUser['confirmed'] == 1 ? 'success' : 'danger');?>"><i class="fa fa-<?php echo ($arLoginUser['confirmed'] == 1 ? 'check' : 'times');?>-circle"></i> <?php echo ($arLoginUser['confirmed'] == 1 ? '' : 'Not ');?>Verified</span>

                                                </td>

                                            </tr>

                                            <tr>

                                                <td>Phone</td>

                                                <td><a href="telto:<?php echo $arUserDetails['szPhone'];?>"><?php echo $arUserDetails['szPhone'];?></a></td>

                                            </tr>

                                        </tbody>

                                    </table>

                                    <table class="table table-hover">

                                        <thead>

                                            <tr>

                                                <th colspan="3" class="pn"><h3 class="mtn">Additional Information</h3></th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                            <tr>

                                                <td>Replicated Site URL (<a href="/marketing/funnels">View Marketing Funnels</a>)</td>

                                                <td><a href="http://usitech.io/?id=<?php echo $arUserDetails['szUserName'];?>" target="_blank">http://usitech.io/?id=<?php echo $arUserDetails['szUserName'];?></a></td>

                                            </tr>

                                            <tr>

                                                <td>Last Logged In</td>

                                                <td><?php if($arUserDetails['dtLastLogin'] != '') echo date('m/d/Y h:ia', strtotime($arUserDetails['dtLastLogin']));?></td>

                                            </tr>

                                        </tbody> 

                                    </table>

                                </td>

                            </tr> 

                        </table> 

                    </div>

                </div>  

            </div>

            <?php if($arLoginUser['confirmed'] == 1){?>

            <div class="row">

                <div class="panel panel-primary">

                    <div class="panel-heading">

                        <span class="panel-title">Subscription Details</span>

                        <span class="panel-controls">

                            <a href="#membership" data-toggle="collapse" data-target="#membership"><i class="fa fa-minus"></i></a>

                        </span>

                    </div>

                    <div class="panel-body collapse in" id="membership">

                        <?php 

                            if(!empty($arSubscription))

                            { 

                                $szInterval = ($arSubscription['iType'] == 1 ? 'Monthly' : ($arSubscription['iType'] == 2 ? 'Yearly' : 'Lifetime'));



                                $szTitle = "";

                                if($arSubscription['tCurrentTermEnd'] > time()) 

                                {

                                    if($arSubscription['tTrialEnd'] > time())

                                    { 

                                        $szTitle = 'On trial till ' . date('d. M, Y', $arSubscription['tTrialEnd']);

                                        $szStatus = '<span class="label label-warning">Trial</span>';

                                    }

                                    else

                                    {

                                        $szStatus = '<span class="label label-success">Active</span>';

                                    }

                                }

                                else

                                {

                                    $szTitle = 'Cancelled since ' . date('d. M, Y', $arSubscription['tCurrentTermEnd']);

                                    $szStatus = '<span class="label label-danger">Cancelled</span>'; 

                                }

                        ?>

                            <table class="table table-hover table-details mb20">

                                <tr>

                                    <th>Plan</th>

                                    <th>Amount</th>

                                    <th>Billing Cycle</th>

                                    <th>Status</th>

                                    <th>Next Billing</th>

                                    <?php if($arSubscription['tCurrentTermEnd'] > time()){?>

                                    <th>Cancel</th>

                                    <?php }?>

                                </tr>

                                <tr>

                                    <td><?php echo $arSubscription['szPlanName'];?></td>

                                    <td><?php echo "$".number_format((float)$arSubscription['fPlanAmount'],2);?></td>

                                    <td><?php echo $szInterval;?></td>

                                    <td>

                                        <a href="#" data-toggle="tooltip" data-title="<?php echo (!empty($szTitle)?$szTitle:''); ?>" style="text-decoration: none;"><?php echo $szStatus; ?></a>

                                    </td> 

                                    <td>

                                        <?php 

                                            if($arSubscription['tCurrentTermEnd'] > time() && $arSubscription['iType'] != 3)

                                            {

                                                echo date("d. M, Y", ($arSubscription['tCurrentTermEnd']+1)); 

                                            } else { 

                                                echo "N/A";

                                           }

                                        ?> 

                                    </td>

                                    <?php if($arSubscription['tCurrentTermEnd'] > time()){?>

                                    <td><a href="javascript:void(0);" data-user="1" data-end="<?php echo date("d M, Y", ($arSubscription['tCurrentTermEnd']+1));?>" data-id="<?php echo $arSubscription['id'];?>" class="btn btn-sm btn-default" onclick="deleteSubscription(this);"><i class="fa fa-times-circle-o colordanger"></i> Cancel</a></td>

                                    <?php }?>

                                </tr>   

                            </table>

                        <?php } else { ?>

                        <h3><span class="text-danger">You are currently not subscribed to any plan. </span> <a href="<?php echo base_url();?>membership/subscription">Subscribe Now</a></h3>

                        <?php } ?> 

                    </div>

                </div>

            </div> 

            <div class="row">

                <div class="panel panel-primary">

                    <div class="panel-heading">

                        <span class="panel-title">Invoices</span>

                        <span class="panel-controls">

                            <a href="#invoices" data-toggle="collapse" data-target="#invoices"><i class="fa fa-minus"></i></a>

                        </span>

                    </div>

                    <div class="panel-body collapse in" id="invoices">

                        <table class="subscriptions-table table table-striped table-hover mt10">

                            <thead>

                                <tr>

                                    <th>Customer</th>

                                    <th>Invoice</th>

                                    <th>Amount</th> 

                                    <th>Status</th>

                                    <th>Date Created</th> 

                                </tr>

                            </thead>



                            <?php if(!empty($arTransactions)){ ?>

                            <tbody>

                            <?php  

                                foreach( $arTransactions as $Transactions )

                                {    

                                    ?>

                                    <tr>

                                        <td><?php echo $Transactions['szFirstName']." ".$Transactions['szLastName']; ?><br><small class="colorgray"><?php echo (isset($Transactions['szEmail'])? "(".$Transactions['szEmail'].")" :''); ?></small></td>

                                        <td><?php echo $Transactions['szTransactionID']; ?></td> 

                                        <td><?php echo "$".number_format((float)$Transactions['fAmount']); ?></td> 

                                        <td><span class="label label-success"><?php echo $Transactions['szStatus']; ?></span></td>

                                        <td><?php echo date('d M, Y', $Transactions['tCreated']); ?></td> 

                                    </tr> 

                                <?php  } ?> 

                            </tbody> 

                            <?php } else {?>

                                <tfoot>

                                    <tr>

                                        <td colspan="8" class="aligncenter">

                                            <em>no transaction found</em>

                                        </td>

                                    </tr>

                                </tfoot>

                            <?php }?> 

                        </table> 

                    </div>

                </div>

            </div>

            <?php } else {?>

                <div id="email_confirmation_popup">

                    <div class="bootbox modal fade bootbox-confirm in" tabindex="-1" role="dialog" style="display: block; padding-right: 17px;" aria-hidden="false">

                        <div class="modal-backdrop fade in" style="height: 100%;"></div>

                        <div class="modal-dialog">

                            <div class="modal-content">

                                <div class="modal-header">

                                    <button type="button" class="bootbox-close-button close" data-dismiss="modal" onclick="showHide('email_confirmation_popup')">×</button>

                                    <h4 class="modal-title"><?php echo $szNotificationPopupTitle; ?></h4>

                                </div>

                                <div class="modal-body">

                                    <div class="bootbox-body">

                                        <h3 class="text-danger mb20"><?php echo $szNotificationPopupMessage; ?></h3>

                                    </div>

                                </div>

                                <?php if($isConfirmationLinkResend){?>

                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-default" onclick="showHide('email_confirmation_popup')"><i class="fa fa-times"></i> Close</button> 

                                    </div>

                                <?php } else {?> 

                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-default" onclick="showHide('email_confirmation_popup')"><i class="fa fa-times"></i> Close</button>

                                        <button type="button" class="btn btn-primary" onclick="resendActivationLink('<?php echo $arUserDetails['szUniqueKey']; ?>')"><i class="fa fa-check"></i> Resend</button>

                                    </div> 

                                <?php }?>

                            </div>

                        </div>

                    </div>

                </div> 

            <?php }?>

     	</section>