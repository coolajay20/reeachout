<?php
$globalStats = get_global_stats();

if(!empty($arSubscription)) { 
	$szInterval = ($arSubscription['iType'] == 1 ? 'Monthly' : ($arSubscription['iType'] == 2 ? 'Yearly' : 'Lifetime'));
	$szTitle = "";
}

if($arSubscription['szStatus'] == 'trialing') {
	if($arSubscription['tTrialEnd'] > time()) { 
		$szTitle = 'On trial till ' . date('d. M, Y', $arSubscription['tTrialEnd']);
		$szStatus = '<span class="label label-warning">Trial</span>';
	} else {
		$szStatus = '<span class="label label-success">Active</span>';
	}
} if ($arSubscription['szStatus'] == 'canceled') {
	$szTitle = 'Cancelled since ' . date('d. M, Y', $arSubscription['tCurrentTermEnd']);
	$szStatus = '<span class="label label-danger">Cancelled</span>'; 
}

#var_dump($arSubscription);
?>
<?php if($isSetupComplete == false && !hasPermission('manage.users')){?>
	<div id="email_confirmation_popup">
	   <div class="modal-backdrop fade in"></div>
		<div class="bootbox modal fade bootbox-confirm in" tabindex="-1" role="dialog" style="display: block;" aria-hidden="false">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="bootbox-close-button close" data-dismiss="modal" onclick="showHide('email_confirmation_popup')">×</button>
						<h4 class="modal-title"><?php echo $arUserDetails['szFirstName']; ?>, you ready to get the most from this system?</h4>
					</div>
					<div class="modal-body">
						<p>You're almost there! Let's get everything finished on this list and we'll move forward.</p>
							<ul id="todolist" style="list-style: none; padding: 0;">
								<li id="step1"><i class="fa fa-check"></i>Create Account</li>
								<li id="step2">
									<i class="fa <?php echo ($arLoginUser['confirmed'] != 0 ? 'fa-check' : 'fa-times');  ?>"></i>Verify Email
									
									<?php if ($arLoginUser['confirmed'] == 0) { ?>
									<button class="btn btn-primary btn-xs" onclick="resendActivationLink('<?php echo $arUserDetails['szUniqueKey']; ?>')">Resend</button>
									<p>We sent a verification email to <strong><?php echo $arUserDetails['szEmail']; ?></strong> (<a href="/users/edit/<?php echo $arUserDetails['szUniqueKey']; ?>"><strong>Change Email</strong></a>)<br><em>Has it been longer than 5 minutes and you couldn't even find it in spam? Click 'Resend' and we'll email you another verification link.</em></p>
									<?php } ?>
									
								</li>
								<li id="step3">
									<i class="fa <?php echo ($arLoginUser['avatar'] != '' ? 'fa-check' : 'fa-times');  ?>"></i>Finish Profile
									
									<?php if ($arLoginUser['avatar'] == '') { ?>
									<p>Make sure all your contact details are correct, add any social media links you want displayed on funnel pages, and upload a nice photo of you. (<a href="/users/edit/<?php echo $arUserDetails['szUniqueKey']; ?>"><strong>Update Profile</strong></a>)</p>
									<?php } ?>
									
								</li>
								<li id="step4">
									<i class="fa <?php echo (!empty($arSubscriptions) ? 'fa-check' : 'fa-times'); ?>"></i><a href="/membership/subscription">Activate Subscription</a>
									<p>Select the membership that's right for you. We'll give you 7 days to try it out at no charge, simply hit cancel if you don't produce any results within your first week.</p>
								</li>
							</ul>
							<hr/>
							<span><strong><?php echo $isSetupProgress ?>% Complete</strong></span>
							<div class="progress progress-striped active">
								<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $isSetupProgress ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $isSetupProgress ?>%">
								</div>
							</div>
							
						<style>#todolist .fa-check {color: darkgreen;} #todolist .fa-times{color: darkred; padding-right: 8px !important;} #todolist .fa {font-size: 20px; padding-right: 5px;} #todolist li {font-size: 15px; line-height: 1.5em; font-weight: bold;} #todolist p {font-weight:  100; font-size: 13px; padding-left: 25px;}</style>
						
					</div>
					<?php if($isConfirmationLinkResend){?>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" onclick="showHide('email_confirmation_popup')"><i class="fa fa-times"></i> Close</button>
						</div>
					<?php } else {?> 
						<div class="modal-footer">
						
						</div> 
					<?php }?>
				</div>
			</div>
		</div>
	</div> 
	<?php } ?>
       
	<div class="row">
       
         <div class="col-sm-6 col-md-3">
          <div class="panel panel-default panel-stat">

			<div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<?php echo base_url();?>assets/images/256x256-USI-icon.png" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label"><strong>USI-Tech Customer Support</strong></small>
                    <h5><strong><a href="tel:17025221640">+1 (702) 522-1640</a></strong></h5>
                  </div>
                </div><!-- row -->
                
                <div class="mb10"></div>
                
                <div class="row">
                  <div class="col-xs-4">
                    <a class="" href="https://usitech-int.com/login.html" target="_blank"><i class="fa fa-sign-in"></i>Login</a><br>
                    <a href="https://usitechsupport.freshdesk.com/en/support/home" target="_blank"><i class="fa fa-support"></i>Support</a>
                  </div>
                   <div class="col-xs-8">
                    <h6><strong>USI Replicated Link:</strong> <small>(<a href="/marketing/funnels/">More Tools</a>)</small><br><a target="_blank" href="https://<?php echo $arUserDetails['szUSIUserName']; ?>.usitech-int.com">https://<?php echo $arUserDetails['szUSIUserName']; ?>.usitech-int.com</a></h6>
                  </div>
                </div><!-- row -->
                  
              </div><!-- stat -->
              
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
        
        <div class="col-sm-6 col-md-3">
          <div class="panel panel-default panel-stat">
            <div class="panel-heading">
              
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<?php echo base_url();?>assets/images/icons/Business-43.png" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">Total Unique Visitors</small>
                    <h1><?php echo $globalStats['visitors']['total'] ?></h1>
                  </div>
                </div><!-- row -->
                
                <div class="mb15"></div>
                
                <div class="row">
                  <div class="col-xs-4">
                    <a class="btn btn-primary btn-sm disabled" href="/visitors"><div><i class="fa fa-list"></i>View All</div></a>
                  </div>
                  
                  <div class="col-xs-8">
                    <small class="stat-label">Conversion Rate</small>
                    <h4><?php echo @round(($globalStats['subscribers']['total'] / $globalStats['visitors']['total']) * 100); ?>%</h4>
                  </div>
                </div><!-- row -->
              </div><!-- stat -->
              
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
        
        <div class="col-sm-6 col-md-3">
          <div class="panel panel-default panel-stat">
            <div class="panel-heading">
              
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<?php echo base_url();?>assets/images/icons/Business-62.png" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">Total Contacts Subscribed</small>
                    <h1><? echo $globalStats['subscribers']['total']; ?></h1>
                  </div>
                </div><!-- row -->
                
                <div class="mb15"></div>
                
                <div class="row">
                  <div class="col-xs-4">
                    <a class="btn btn-primary btn-sm" href="/contacts/all"><div><i class="fa fa-list"></i>Manage</div></a>
                  </div>
                  
                  <div class="col-xs-4">
                    <small class="stat-label">Today</small>
                    <h4><? echo $globalStats['subscribers']['today']; ?></h4>
                  </div>
                  
                  <div class="col-xs-4">
                    <small class="stat-label">This Month</small>
                    <h4><? echo $globalStats['subscribers']['month']; ?></h4>
                  </div>
                </div><!-- row -->
                  
              </div><!-- stat -->
              
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
        
        <?php if(hasPermission('manage.users')) { ?>
		<div class="col-sm-6 col-md-3">
          <div class="panel panel-default panel-stat">
            <div class="panel-heading">
              
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<?php echo base_url();?>assets/images/icons/Business-04.png" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">Total Members</small>
                    <h1><?php echo $globalStats['users']['total']; ?></h1>
                  </div>
                </div><!-- row -->
                
                <div class="mb15"></div>
                
                <div class="row">
                  <div class="col-xs-4">
                    <a class="btn btn-primary btn-sm" href="/users"><div><i class="fa fa-users"></i>Details</div></a>
                  </div>
                  
                  <div class="col-xs-4">
                    <small class="stat-label">Active</small>
                    <h4><?php echo $globalStats['users']['active']; ?> / <?php echo @round(($globalStats['users']['active'] / $globalStats['users']['total']) * 100); ?>%</h4>
                  </div>
                  
                  <div class="col-xs-4">
                    <small class="stat-label">Revenue</small>
                    <h4>$<?php echo $globalStats['users']['active'] * 33; ?><small> /mo</small></h4>
                  </div>
                </div><!-- row -->
                  
              </div><!-- stat -->
              
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
        
        <?php } else { ?>
        
		<div class="col-sm-6 col-md-3">
          <div class="panel panel-default panel-stat">
            <div class="panel-heading">
				  <div class="stat">
					<div class="row">
					  <div class="col-xs-4">
						<img src="<?php echo base_url();?>assets/images/icons/Business-25.png" alt="" />
					  </div>
					  <div class="col-xs-8">
						<small class="stat-label">Membership Details</small>
						<?php
						if ($arSubscription['tCurrentTermEnd'] > time()) {
							echo '<h4>'.$arSubscription['szPlanName'].' ($'.number_format((float)$arSubscription['fPlanAmount'],2).' / '.$szInterval.')</h4>';
						
							} else { 
		
								echo '<h5><span class="text-danger">You are currently not subscribed to any plan. </span></h5>';
							}
						?> 
						
						<?php echo (!empty($szTitle)?$szTitle:''); ?>
					  	<?php echo $szStatus; ?></a>
					  </div>
					</div><!-- row -->
					
					<div class="mb10"></div>
					
					<div class="row">
					  <div class="col-xs-4">
						<?php
						if ($arSubscription['szStatus'] == 'active' || $arSubscription['szStatus'] == 'trialing') {
							echo '<a href="javascript:void(0);" data-user="1" data-end="'.date("d M, Y", ($arSubscription['tCurrentTermEnd']+1)).'" data-id="'.$arSubscription['id'].'" class="btn btn-default btn-sm" onclick="deleteSubscription(this);"><div><i class="fa fa-times"></i>Cancel</div></a>';
						} else if ($arSubscription['szStatus'] == 'canceled') {
							echo '<a href="/membership/subscription" class="btn btn-success btn-sm"><div>Renew</div></a>';
						} else {
							echo '<a href="/membership/subscription" class="btn btn-success btn-sm"><div>Activate</div></a>';
						}
						?>
					  </div>

					  <div class="col-xs-8">
						  <small class="stat-label">Refer 3 Your Subscription is Free <i class="fa fa-question tooltips" data-toggle="tooltip" title="" data-original-title="Maintain at least three active members who you personally referred and your subscription to this system is FREE. Share this link: https://bitcoinlabs.io/ref/<?php echo $arUserDetails['szUserName']; ?>"></i> <?php echo $globalStats['referrals']['total'] ?> / <?php echo $globalStats['referrals']['active'] ?></small>
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
							  <span class="sr-only"></span>
							</div>
						  </div>
					  </div>
					</div><!-- row -->

				  </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
        <? } ?>
        
      </div><!-- row -->
      
	<div class="row">
	

	
		<div class="col-sm-12 col-md-12">
		  <div class="panel panel-default">
			<div class="panel-body">
			  <div class="row">
				<div class="col-sm-12">
			  
				  <h5 class="subtitle mb5">System Performance</h5>
				  <p class="mb15">This shows you how many visitors viewed your site compared to how many subscribed.</p>
				  <div id="basicflot" style="width: 100%; height: 300px; margin-bottom: 20px"></div>
				</div><!-- col-sm-8 -->
			  </div><!-- row -->
			</div><!-- panel-body -->
		  </div><!-- panel -->
		</div><!-- col-sm-9 -->

	  </div><!-- row -->
	 
    <div class="collapse bg-inverse" id="navbarHeader">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 py-4">
            <h4 class="text-white">...</h4>
            <p class="text-muted">...</p>
          </div>
          <div class="col-sm-4 py-4">
            <h4 class="text-white">...</h4>
            <ul class="list-unstyled">
              <li><a href="#" class="text-white">...</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
      
   	<button class="navbar-toggler pull-right" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
	  <span class="navbar-toggler-icon"><i class="fa fa-list"></i></span>
	</button>
	
<style>
.panel-stat .stat {
    color: #000;
}
.panel-stat .stat img {
    max-width: 50%;
}
.panel-stat i {
    font-size: 15px;
    padding: 0 5px 0 5px;
	border: 0;
}
.panel-stat .btn i {
    font-size: 13px;
    padding: 0 5px 0 0 !important;
	border: 0;
	opacity: 1 !important;
}
.progress {
	margin-bottom: 10px;
}
</style>


You were referred by: <?php echo $arUserDetails['arSponsorDetails']['fname']." ".$arUserDetails['arSponsorDetails']['lname'] ?>