<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership_Model extends CI_Model 
{

    function __construct() 
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function getAllSubscriptions($isOnlyCount=false)
    {
        if($isOnlyCount)
        {
            $this->db->select('s.id');
        }
        else
        {
            $this->db->select('*, s.id as idSubscription, s.fPlanAmount as fSubscriptionAmount');
        }
        
        $this->db->from('tbl_user_subscriptions s'); 
        $this->db->join('tbl_plans p', 's.idPlan=p.id', 'inner');
        $this->db->join('tbl_users u', 's.idUser=u.id', 'inner'); 
        $this->db->where('s.isDeleted', 0);
        $this->db->order_by('u.szFirstName','asc');         
        
        if($isOnlyCount)
        {
            return $this->db->get()->num_rows();
        }
        else
        {
            $query = $this->db->get(); 
            if($query->num_rows() != 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        } 
    }
    
    public function getUserSubscription($idUser)
    {
        $idUser = (int)$idUser;
        $arSubscription = array();
        $query = $this->db->from('tbl_user_subscriptions')->where('isDeleted', 0)->where('idUser', $idUser)->order_by('tCurrentTermEnd', 'DESC')->get();
        if($query->num_rows() != 0)
        {
            $row = $query->result_array();
            $arSubscription = $row[0];
        }
        return $arSubscription;
    }
    
    public function getUserTransactions($idUser)
    {
        $idUser = (int)$idUser;
        $arSubscription = array();
        //$query = $this->db->from('tbl_user_transactions')->where('idUser', $idUser)->order_by('tCurrentTermEnd', 'DESC')->get();
        $this->db->select('*');
        $this->db->from('tbl_user_transactions ut');  
        $this->db->join('tbl_users u', 'ut.idUser=u.id', 'inner');  
        $query = $this->db->where(array('ut.idUser'=>$idUser,'ut.isDeleted'=>0))->order_by('ut.tCreated', 'DESC')->get();
        if($query->num_rows() != 0)
        {
            $arSubscription = $query->result_array(); 
        }
        return $arSubscription;
    }
    
    public function getAllTransactions()
    { 
        $arSubscription = array();
        //$query = $this->db->from('tbl_user_transactions')->where('idUser', $idUser)->order_by('tCurrentTermEnd', 'DESC')->get();
        $this->db->select('*');
        $this->db->from('tbl_user_transactions ut');  
        $this->db->join('tbl_users u', 'ut.idUser=u.id', 'inner');  
        $query = $this->db->where(array('ut.isDeleted'=>0,'u.isDeleted'=>0));
        $query = $this->db->order_by('ut.tCreated', 'DESC')->get();
        if($query->num_rows() != 0)
        {
            $arSubscription = $query->result_array(); 
        }
        return $arSubscription;
    }
    
    public function saveUsereSubscription($data)
    {
        $idSubscription = 0;
        $this->db->insert('tbl_user_subscriptions', $data);
        if($this->db->affected_rows() > 0)
        {
            $idSubscription = $this->db->insert_id();
        }
        return $idSubscription;
    }
    
    public function saveStripeApiLogs($data)
    {
        $idSubscription = 0;
        $this->db->insert('tbl_stripeapilogs', $data);
        if($this->db->affected_rows() > 0)
        {
            $idSubscription = $this->db->insert_id();
        }
        return $idSubscription;
    }
    public function saveUserTransactions($data)
    {
        $idTransaction = 0;
        if(!empty($data['szTransactionID']))
        {
            $query = $this->db->select('id')->from('tbl_user_transactions')->where('szTransactionID', trim($data['szTransactionID']))->get();
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $idTransaction = $rows[0]['id'];
            }
        }
        if($idTransaction > 0)
        {
            $this->db->where('id', $idTransaction)->update('tbl_user_transactions', $data);
        }
        else
        {
            $this->db->insert('tbl_user_transactions', $data);
            if($this->db->affected_rows() > 0)
            {
                $idTransaction = $this->db->insert_id();
            }
        }
        return $idTransaction;
    }
    
    public function getSubscriptionByID($idSubscription)
    {
        $idSubscription = (int)$idSubscription;
        $arSubscription = array();
        $query = $this->db->from('tbl_user_subscriptions')->where('id', $idSubscription)->order_by('tCurrentTermEnd', 'DESC')->get();
        if($query->num_rows() != 0)
        {
            $row = $query->result_array();
            $arSubscription = $row[0];
        }
        return $arSubscription;
    }
    
    public function deleteUsereTransaction($data)
    {
        $idUser = (int)$data['idUser'];
        $this->db->where('idUser', $idUser)->update('tbl_user_transactions', $data);
        return $idUser;
    }
    
    public function updateUsereSubscription($data)
    {
        $idSubscription = (int)$data['id'];
        $this->db->where('id', $idSubscription)->update('tbl_user_subscriptions', $data);
        return $idSubscription;
    }
    
    function deleteCustomerSubscription($arSubscriptionDetails)
    { 
        $idSubscription = (int)$arSubscriptionDetails['id'];
        // get login user details
        $arLoginUser = $this->session->userdata('login_user');
        
        // cancel subscription on stripe as well
        if((int)$arSubscriptionDetails['iType'] == 1 && $arSubscriptionDetails['tCurrentTermEnd'] > time() && trim($arSubscriptionDetails['szStripeID']) != '')
        {
            // load strip library
            $this->load->library('CI_Stripe');

            $arCancelSubscription = array();
            try {
                // first retrieve the subscription from Stripe											
                \Stripe\Stripe::setApiKey(STRIPE_ACCESS_KEY);
                $obj = \Stripe\Subscription::retrieve(trim($arSubscriptionDetails['szStripeID']));

                // now cancel it
                $arCancelSubscription = $obj->cancel();
                write_log("subscriptions/subscribe", "Subscription {$arSubscriptionDetails['szStripeID']} for user {$arLoginUser['name']} ({$arLoginUser['email']})get cancelled successfully.\n\n");
            } catch (Exception $e) {							
                write_log("subscriptions/subscribe", "Un-subscribe error: " . $e->getMessage() . "\n\n");
            }
        }        
        
        // update the subscription details in DB
        $arUpdate['id'] = $idSubscription;
        $arUpdate['tCurrentTermEnd'] = $arUpdate['tCanceledAt'] = time();
        $arUpdate['szStatus'] = 'canceled';
        if(hasPermission('manage.settings'))
        {
            $arUpdate['isDeleted'] = 1;
            $arUpdate['iDeletedBy'] = $arLoginUser['id'];
            $arUpdate['dtDeletedOn'] = date('Y-m-d H:i:s');
        }

        $idSubscription = $this->updateUsereSubscription($arUpdate);
        return $idSubscription;
    }
    
    public function getUserSubscriptionByStripeID($idStripe)
    {
        $idStripe = trim($idStripe);
        $arSubscription = array();
        $query = $this->db->from('tbl_user_subscriptions')->where('szStripeID', $idStripe)->get();
        if($query->num_rows() != 0)
        {
            $row = $query->result_array();
            $arSubscription = $row[0];
        }
        return $arSubscription;
    }
}
?>