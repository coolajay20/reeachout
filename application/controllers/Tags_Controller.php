<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tags_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        if(!is_user_login()) {
			
            redirect(base_url() . 'login', 'refresh');
			
        } else if(!hasPermission('manage.contacts')) {
			
            redirect(base_url() . 'dashboard', 'refresh');

        } else {
            /* Load Plan Model */
            $this->load->model('Tags_Model');

            /* Load form helper */ 
            $this->load->helper('form');

            /* Load form validation library */ 
            $this->load->library('form_validation');

            /* Changing error delimiters Globally */
            $this->form_validation->set_error_delimiters('', '');
        }
    }

    public function index($arParams=array())
    {
        if(!is_user_login()) {
			
            redirect(base_url() . 'login', 'refresh');
			
        } else {
			
			$data['szFavIcon'] = "fa-tags";
            $data['szPageName'] = "Contacts";
            $data['szMetaTagTitle'] = "Manage Tags";
            $data['arLoginUser'] = $this->session->userdata('login_user');
            $data['isLoadDataTable'] = true;
            $data['arTags'] = $this->Tags_Model->getAllTags($data['arLoginUser']['id']);

            $data = array_merge($data, $arParams);

            // set breadcrumb
            #$data['arBreadCrumb'][] = array('name'=>'Dashboard', 'link'=>'dashboard');
            #$data['arBreadCrumb'][] = array('name'=>'Manage Tags', 'link'=>'tags');

            $this->load->view('layout/header', $data);
            $this->load->view('tags/list');
            $this->load->view('layout/footer');
        }
    } 

    public function edit($szTagKey='')
    {
        if(!is_user_login()) {

            redirect(base_url().'login', 'refresh');
			
        } else if(!hasPermission('manage.contacts')) {

            redirect(base_url() . 'dashboard', 'refresh');

        } else { 

            $this->add($szTagKey);	
        } 	
    }

    public function add($szTagKey='')
    {
        if(!is_user_login())

        {
            redirect(base_url().'login', 'refresh');
        }	

        else if(!hasPermission('manage.contacts'))
        {
            redirect(base_url() . 'dashboard', 'refresh');
			
        } else { 

            $data['idTags'] = 0;
            $data['isFormError'] = false;
            $data['isDetailsSaved'] = false;
            $data['arTagDetails'] = array();
            $data['arLoginUser'] = $this->session->userdata('login_user');	

            if($this->input->post('p_func') == 'Save Tag Details')
            {			
                $idTags = (int)$this->input->post('arTag[id]');

                //Set validation rules 
                $this->form_validation->set_rules('arTag[szTagName]', 'Tag name', "trim|required|is_unique[tbl_tags:szTagName:idUser:{$data['arLoginUser']['id']}:id:{$idTags}]");
				
                if($this->form_validation->run() == TRUE) 
                {				
                    $arTagDetails = $this->input->post('arTag');
                    $arTagDetails['idUser'] = $data['arLoginUser']['id'];
                    if($data['idTags'] = $this->Tags_Model->saveTagDetails($arTagDetails, $data['arLoginUser']['id']))
                    {
                        $data['isDetailsSaved'] = true;  
                    }
                } else {

                    $data['isFormError'] = true;
                }
            }

            if($szTagKey)

            {
                $data['arTagDetails'] = $this->Tags_Model->getTagsByUniqueKey($szTagKey);
                $data['idTags'] = (isset($data['arTagDetails']['id']) ? (int)$data['arTagDetails']['id'] : 0);
            }

            else if($data['idTags'] > 0)
            {
                $data['arTagDetails'] = $this->Tags_Model->getTagsByID($data['idTags']);
            }

            $data['szPageName'] = "Tags";
            $data['szMetaTagTitle'] = ($data['idTags'] > 0 ? 'Edit Tag' : 'Add New Tag');	
            $data['isLoadAdminForm'] = true;
            $data['isLoadCMSScripts'] = true;
            $data['isLoadValidationScript'] = true; 

            // set breadcrumb 
            $data['arBreadCrumb'][] = array('name'=>'Dashboard', 'link'=>'dashboard');
            $data['arBreadCrumb'][] = array('name'=>'Manage Tags', 'link'=>'tags');
            if(!empty($data['arTagDetails']))

            {
                $data['arBreadCrumb'][] = array('name'=>'Edit Tag'); 
				
            } else {
				
                $data['arBreadCrumb'][] = array('name'=>'Add New Tag');
            }

            $this->load->view('layout/header', $data);
            $this->load->view('tags/add_edit');
            $this->load->view('layout/footer');
        }
    } 

    function delete($szTagKey='')
    {
        if(!is_user_login()) {
			
            redirect(base_url().'login', 'refresh');
        } else if(!hasPermission('manage.contacts')) {

            redirect(base_url() . 'dashboard', 'refresh');
        }

        $message = array();
        $szTagKey = trim($szTagKey);
        if($szTagKey != '')

        {
            $arTagDetails = $this->Tags_Model->getTagsByUniqueKey($szTagKey);
            if(!empty($arTagDetails))

            {
                if($arTagDetails['isDeleted'] == 0)
                {
                    $arLoginUser = $this->session->userdata('login_user');
                    $this->Tags_Model->deleteTags($arTagDetails, $arLoginUser['id']);
                    $message['szSuccessMessage'] = 'Tag has been deleted successfully';
					
                } else {

                    $message['szErrorMessage'] = "Tag is already deleted.";
                }
				
            } else {
				
                $message['szErrorMessage'] = "Tag not found.";
            }
			
        } else {
            $message['szErrorMessage'] = "Invalid Tag delete request.";
        } 

        $this->index($message);
    }
}
?>