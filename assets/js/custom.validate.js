function validate_form() {
	
    /* check if there is any form to validate with class 'validate-form' */
    if($('.validate-form').length > 0){
        $('.validate-form').each(function(){
            $(this).validate({
                ignore: [],

                /* @validation states + elements 
                ------------------------------------------- */
                onkeyup: false,
                errorClass: "state-error",
                validClass: "state-success",
                errorElement: 'em',

                /* @validation highlighting + error placement  
                ---------------------------------------------------- */

                highlight: function(element, errorClass, validClass) {
                    $(element).closest('.field').addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).closest('.field').removeClass(errorClass).addClass(validClass);
                },
                errorPlacement: function(error, element) {
                    if (element.is(":radio") || element.is(":checkbox")) {
                        element.closest('.option-group').after(error);
                    } else {
                        error.insertAfter(element.parent());
                    }
                },
                submitHandler: function(form){
                    form.submit();
                }
            });	    		    
        });

        /* set rules to validate */
        $('.required').each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: $(this).attr('placeholder') + ' is required.'
                }
            });
        });

        $('.email').each(function(){
            $(this).rules("add", {email:true});
        });

        $('.only_digits').each(function(){
            $(this).rules("add", {
                digits:true
            });
        });

        $('.matches').each(function(){
            $(this).rules("add", {
                equalTo: '#'+$(this).attr('id').replace('Repeat', ''),
                messages: {
                    equalTo: $(this).attr('placeholder') + ' doesn\'t match.'
                }
            });
        });

        $('.minlength').each(function(){
            $(this).rules("add", {minlength:$.trim($(this).attr('data-min-len'))});
        });

        $('.maxlength').each(function(){
            $(this).rules("add", {maxlength:$.trim($(this).attr('data-max-len'))});
        });
    }
}

$(document).ready(function() {
    validate_form();
});