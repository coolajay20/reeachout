<?php



defined('BASEPATH') OR exit('No direct script access allowed');







function sanitize_all_html_input($value)



{



    if(!empty($value))



    {



        $value=strip_tags($value);







        if(strpos($value,"'>") !== false)



            $value=str_replace("'>","",$value);



        if(strpos($value,'">') !== false)



            $value=str_replace('">',"",$value);



    }







    return $value;



}







function sanitize_post_field_value($value)



{



    return htmlentities(trim($value));



}







function encrypt($string, $encrypt=true)



{



    if($encrypt)



        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(ENCRYPT_KEY), $string, MCRYPT_MODE_CBC, md5(md5(ENCRYPT_KEY))));



    else



        return $string;



}







function decrypt($encrypted, $decrypt=true)



{



    if($decrypt)



        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(ENCRYPT_KEY), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5(ENCRYPT_KEY))), "\0");



    else



        return $encrypted;



}







function generateRandomString($characters = "0123456789", $length = 6) 



{



    $charactersLength = strlen($characters);



    $randomString = '';



    for ($i = 0; $i < $length; $i++) {



        $randomString .= $characters[rand(0, $charactersLength - 1)];



    }



    return $randomString;



}







function generateAlphaNumericString($length = 6) 



{



    $characters = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";



    $charactersLength = strlen($characters);



    $randomString = '';



    for ($i = 0; $i < $length; $i++) {



        $randomString .= $characters[rand(0, $charactersLength - 1)];



    }



    return $randomString;



}



function get_user_avatar($idUser = '')
{
	$CI = get_instance();
	
	$userDetails = $CI->User_Model->getUserByID($idUser);
	
    /*     $avatar = array();
        $idUser = (int)$idUser;
        if($idUser > 0)
        {
            $query = $this->db->get_where('tbl_users',array('id'=>$idUser, 'isDeleted'=>0));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $avatar = $rows[0];
            }
        }
        #return $avatar; */
	
	#return BASE_USERS_UPLOAD_URL . "Thomas-Evangelista-1504121403.png";
	return base_url() . 'assets/images/profile.png';
}



function set_user_login($data, $remember)

{



    $CI = get_instance();



    $userdata['id'] = $data['id'];

    $userdata['email'] = $data['szEmail'];

    $userdata['user_name'] = $data['szUserName'];

    $userdata['name'] = "{$data['szFirstName']} {$data['szLastName']}";

	$userdata['avatar'] = $data['szAvatarImage'];

    $userdata['confirmed'] = $data['isConfirmed'];

	$userdata['language'] = $data['szLanguage'];



    // set data to session

    $CI->session->set_userdata('login_user', $userdata);

	

    if($remember)



    {	



        //set data to cookie and keep for 1 month



        $encKey1 = "#12EQ#83#";$encKey2="#AR3UIL#452#";$encKey3="#AD3WEB#42#";$encKey4="#AD3WEB#44#";$encKey4="#AD3WEB#55#";



        $cookieData = $userdata['id'] . "~$encKey1" . $userdata['name'] . "~$encKey2" . $userdata['email'] . "~$encKey3" . $userdata['user_name'] . "~$encKey4" . $userdata['confirmed'] . "~$encKey5";







        $encryptedC = base64_encode($cookieData);



        $cookie_time = 60*60*24*30;



        set_cookie(USER_COOKIE, $encryptedC, $cookie_time);



    }



}







function is_user_login()
{
    $email = '';
    $remember = 0;
    $business = 0;
	
    $CI = get_instance();
    $user_cookie = get_cookie(USER_COOKIE);

    // first check from session
    $userdata = $CI->session->userdata('login_user');

    if(!empty($userdata) && $userdata['id'] > 0)
    {
        $email = $userdata['email'];
        $username = $userdata['user_name'];
    }
    else if($user_cookie) // now check cookie
    {

        $remember = 1;
        $encKey1 = "#12EQ#83#";$encKey2="#AR3UIL#452#";$encKey3="#AD3WEB#42#";$encKey4="#AD3WEB#44#";$encKey5="#AD3WEB#55#";
        $decryptedC1 = base64_decode($user_cookie);
        $decryptedC2 = preg_replace("/$encKey1/", "", $decryptedC1);
        $decryptedC3 = preg_replace("/$encKey2/", "", $decryptedC2);
        $decryptedC4 = preg_replace("/$encKey3/", "", $decryptedC3);
        $decryptedC5 = preg_replace("/$encKey4/", "", $decryptedC4);
        $decryptedC6 = preg_replace("/$encKey5/", "", $decryptedC5);

        list($userdata['id'], $userdata['name'], $userdata['email'], $userdata['user_name'], $userdata['confirmed']) = explode("~", $decryptedC6);

        $email = $userdata['email'];
        $username = $userdata['user_name'];
    }

    if($email != '')
    {
        $CI->load->model('User_Model');
        $arDetails = $CI->User_Model->getUserByEmail($email);

        if(!empty($arDetails) && $arDetails['isActive'] == 1)
        {
            set_user_login($arDetails, $remember);
            if($remember)
            {
                // add login history
				addLoginHistory($arDetails);
            }

            $szClass = $CI->router->fetch_class();
            $szMethod = $CI->router->fetch_method();
			
            // check for confirmed email
            if($arDetails['isConfirmed'] != 1)
            {                
                if($szClass != 'Home_Controller' && $szClass != 'User_Controller')
                {
                    redirect(base_url() . "dashboard");
                }
            }

            // check for active subscription            
            $CI->load->model('Membership_Model');
            $arSubscription = $CI->Membership_Model->getUserSubscription($arDetails['id']);
            if(empty($arSubscription) || (!empty($arSubscription) && $arSubscription['tCurrentTermEnd'] <= time()))
            {
                if($szClass != 'Home_Controller' && $szClass != 'User_Controller' && $szClass != 'Membership_Controller' && $arDetails['idRole'] != 1)
                {
                    redirect(base_url() . "dashboard");
                }
                else if($szClass == 'Membership_Controller' && $szMethod == 'history')
                {
                    redirect(base_url() . "dashboard");
                }
            }

            return true;
        }
        else
        {	

            if(isset($_SERVER['REDIRECT_URL']) && $_SERVER['REDIRECT_URL'] != '/login' && $_SERVER['REDIRECT_URL'] != '/index.php/login')

                $CI->session->set_userdata('redirect_url', str_replace('/index.php', '', $_SERVER['REDIRECT_URL']));

            // remove session and cookie
            $CI->session->unset_userdata('login_user');
            delete_cookie(USER_COOKIE);

            return false;
        }
    }
    else
    {

        if(isset($_SERVER['REDIRECT_URL']) && $_SERVER['REDIRECT_URL'] != '/login' && $_SERVER['REDIRECT_URL'] != '/index.php/login')

            $CI->session->set_userdata('redirect_url', str_replace('/index.php', '', $_SERVER['REDIRECT_URL']));

        return false;
    }
}





function addLoginHistory($data)



{



    $values['idUser'] = (int)$data['id'];



    if($values['idUser'] > 0)



    {



        // get CI instance



        $CI = get_instance();



        



        $values['szName'] = "{$data['szFirstName']} {$data['szLastName']}";



        $values['szEmail'] = $data['szEmail'];



        $values['szActivity'] = 'Logged in.';



        $values['idRecord'] = $values['idUser'];



        $values['szRecordTable'] = 'tbl_users';



        $values['szIPAddress'] = $_SERVER['REMOTE_ADDR'];



        $values['szUserAgent'] = $_SERVER['HTTP_USER_AGENT'];



        $CI->db->insert('tbl_user_activity', $values);



        return $CI->db->affected_rows() > 0;



    }



    return false;



}







function user_logout()



{



    // get CI instance



    $CI = get_instance();







    // remove session and cookie



    $CI->session->unset_userdata('login_user');



    delete_cookie(USER_COOKIE);



}







function sendEmail($to,$from,$subject,$message,$attach_file='')



{



    // Get a reference to the controller object



    $CI = get_instance();



    $CI->load->library('CI_PHPMailer');



    



    $mail = new PHPMailer();



	



    //$mail->isSMTP();



    try



    {



        // Set SMTP Server



        /*$mail->SMTPDebug  = 0;                     	// enables SMTP debug information (for testing)



        if(__SMTP_AUTH__ === true)



        {



            $mail->SMTPAuth   = true;             	// enable SMTP authentication



            $mail->SMTPSecure = "ssl";         		// sets the prefix to the servier



        }



        $mail->Host = __SMTP_HOST__;      			// SMTP server host



        $mail->Port = __SMTP_PORT__;       			// SMTP server port



        if(__SMTP_USERNAME__ != '')



        {



            $mail->Username   = __SMTP_USERNAME__;	// SMTP Server Username



        }



        if(__SMTP_PASSWORD__ != '')



        {



            $mail->Password   = __SMTP_PASSWORD__;	// SMTP Server Password



        }*/







        // Manage Sender Address



        $from_name = "";



        $from_email = trim($from);



        if($from_email != "")



        {



            $arFrom = explode("<",$from_email);



            if(count($arFrom) > 1)



            {



                if(strlen(trim($arFrom[0])) > 0)



                {



                    $from_name = trim($arFrom[0]);



                }



                $from_email = str_replace(">","",$arFrom[1]);



                //$from_email = "tyagi6931@gmail.com";



            }



        }







        // Manage receiver addresses



        $to_addresses = array();



        if($to != '')



        {



            $ar_addresses = explode(",", $to);



            if(!empty($ar_addresses))



            {



                $ctr = 0;



                foreach($ar_addresses as $address)



                {



                    $address_name = "";



                    $address_email = trim($address);



                    $ar_address = explode("<",$address_email);



                    if(count($ar_address) > 1)



                    {



                        if(strlen(trim($ar_address[0])) > 0)



                        {



                            $address_name = trim($ar_address[0]);



                        }



                        $address_email = str_replace(">","",$ar_address[1]);



                    }



                    //$address_email = "lalit@whiz-solutions.com";



                    $to_addresses[$ctr]['NAME'] = $address_name;



                    $to_addresses[$ctr]['EMAIL'] = $address_email;



                    $ctr++;



                }



            }



        }







        // Manage Carbon-Copy Addresses



        /*$cc_addresses = array();



        if($cc != '')



        {



            $ar_addresses = explode(",", $cc);



            if(!empty($ar_addresses))



            {



                $ctr = 0;



                foreach($ar_addresses as $address)



                {



                    $address_name = "";



                    $address_email = trim($address);



                    $ar_address = explode("<",$address_email);



                    if(count($ar_address) > 1)



                    {



                        if(strlen(trim($ar_address[0])) > 0)



                        {



                            $address_name = trim($ar_address[0]);



                        }



                        $address_email = str_replace(">","",$ar_address[1]);



                    }



                    //$address_email = "ltyagi33@yahoo.com";



                    $cc_addresses[$ctr]['NAME'] = $address_name;



                    $cc_addresses[$ctr]['EMAIL'] = $address_email;



                    $ctr++;



                }



            }



        }







        // Manage Blind-Carbon-Copy Addresses



        $bcc_addresses = array();



        if($bcc != '')



        {



            $ar_addresses = explode(",", $bcc);



            if(!empty($ar_addresses))



            {



                $ctr = 0;



                foreach($ar_addresses as $address)



                {



                    $address_name = "";



                    $address_email = trim($address);



                    $ar_address = explode("<",$address_email);



                    if(count($ar_address) > 1)



                    {



                        if(strlen(trim($ar_address[0])) > 0)



                        {



                            $address_name = trim($ar_address[0]);



                        }



                        $address_email = str_replace(">","",$ar_address[1]);



                    }



                    //$address_email = "tyagi6931@outlook.com";



                    $bcc_addresses[$ctr]['NAME'] = $address_name;



                    $bcc_addresses[$ctr]['EMAIL'] = $address_email;



                    $ctr++;



                }



            }



        }*/







        //echo "Hello $from_name : $from_email" . " | " . print_r($to_addresses, true) . " | " . print_r($cc_addresses, true) . " | " . print_r($bcc_addresses, true);die; 







        $mail->Sender = CUSTOMER_SUPPORT_EMAIL;			



        $mail->addReplyTo($from_email, $from_name);



        foreach($to_addresses as $address)$mail->addAddress($address['EMAIL'], $address['NAME']);



        if(!empty($cc_addresses)){foreach($cc_addresses as $address){$mail->addCC($address['EMAIL'], $address['NAME']);}}



        if(!empty($bcc_addresses)){foreach($bcc_addresses as $address){$mail->addBCC($address['EMAIL'], $address['NAME']);}}



        $mail->setFrom(CUSTOMER_SUPPORT_EMAIL, "Customer Support");



        $mail->Subject = $subject;



        $mail->CharSet = 'utf-8';



        $body = str_replace('\\r', '', trim($message));







        $mail->MsgHTML($body);



        if($attach_file != '' && file_exists($attach_file))



        {



            $mail->AddAttachment($attach_file); // attachment



        }







        $mail_sent = $mail->Send();



        write_log('phpmailer', "Mail Data: [To: {$to}, From: {$from}, Subject: {$subject}, Message: {$message}], Status: {$mail_sent}\n");



    }



    catch (phpmailerException $e) {



        $status = 0;



        $szStatusMsg = $e->errorMessage(); //Pretty error messages from PHPMailer



        write_log('phpmailer', $szStatusMsg . "\n");



    } catch (Exception $e) {



        $status = 0;



        $szStatusMsg = $e->getMessage(); //Boring error messages from anything else!



        write_log('phpmailer', $szStatusMsg . "\n");



    }







    unset($mail);







    return $mail_sent;



}







function write_log($log, $message, $write_log=__WRITE_LOG__)



{



    if($write_log)



    {



        $file = fopen(APPPATH . "logs/{$log}.log", "a+");



        fwrite($file, $message);



        fclose($file);



    }



}







function sanitize_array_values($data, $exclude=array(), $exclude2=array())



{



    if(!empty($data))



    {



        foreach($data as $key=>$value)



        {



            if(!in_array($key, $exclude) && !in_array($key, $exclude2))



                $data[$key] = sanitize_all_html_input(trim(str_replace('[removed]', '', $value)));



            else if(!in_array($key, $exclude2))



                $data[$key] = trim(str_replace('[removed]', '', $value));



            else



                $data[$key] = $value;



        }



    }



    return $data;



}



 



function set_input_value($input, $default=false)



{



    if($default !== false)



    {



        return trim(str_replace('[removed]', '',set_value($input, $default)));



    }



    else



    {



        return trim(str_replace('[removed]', '',set_value($input)));



    }



}







function sanitizeCustomRecordFilters($arFilters, $szAlias='')



{



    if(isset($arFilters['where']) && !empty($arFilters['where']))



    {



        foreach($arFilters['where'] as $key=>$value)



        {



            if($szAlias != '')



            {



                if(is_array($value) || (!is_array($value) && trim($value) !== ''))



                {



                    $arFilters['where']["{$szAlias}.{$key}"] = $value;      				



                }



                else



                {



                    unset($arFilters['where'][$key]);



                }



            }



            else



            {



                if(!(is_array($value) || (!is_array($value) && trim($value) !== '')))



                {



                    unset($arFilters['where'][$key]);



                }



            }



        }



        if(empty($arFilters['where'])) unset($arFilters['where']);



    }







    if(isset($arFilters['like']) && !empty($arFilters['like']))



    {



        foreach($arFilters['like'] as $key=>$value)



        {



            if($szAlias != '')



            {



                if(is_array($value) || (!is_array($value) && trim($value) !== ''))



                {



                    $arFilters['like']["{$szAlias}.{$key}"] = $value;      				



                }



                else



                {



                    unset($arFilters['like'][$key]);



                }



            }



            else



            {



                if(!(is_array($value) || (!is_array($value) && trim($value) !== '')))



                {



                    unset($arFilters['like'][$key]);



                }



            }



        }







        if(empty($arFilters['like'])) unset($arFilters['like']);



    }



    return $arFilters;



}







function setCustomRecordFilters($obj, $arFilters)



{



    // set where filters



    if(!empty($arFilters) && isset($arFilters['where'])) 



    {



        $i = 0;



        foreach($arFilters['where'] as $column=>$value)



        {



            if(is_array($value))



            {



                $obj->db->group_start();



                foreach($value as $i=>$v)



                {



                    if($i > 0)



                        $obj->db->or_where($column, $v);



                    else



                        $obj->db->where($column, $v);



                }



                $obj->db->group_end();



            }



            else



            {



                $obj->db->where($column, $value);



            }



            $i++;



        }



    }



	



    // set like filter



    if(!empty($arFilters) && isset($arFilters['like'])) 



    {



        foreach($arFilters['like'] as $column=>$value)



        {



            if(is_array($value))



            {



                $obj->db->group_start();



                foreach($value as $i=>$v)



                {



                    if($i > 0)



                        $obj->db->or_like($column, $v);



                    else



                        $obj->db->like($column, $v);



                }



                $obj->db->group_end();



            }



            else



            {



                $obj->db->like($column, $value);



            }



        }



    }



}







function format_number($number)



{



    return number_format((float)$number, 2, ".", "");



}







function lang($szVariableName, $szFileName='common')
{
    // load CI instance
    $CI = get_instance();

	//set language based on user profile default selection
	if ($CI->session->userdata('site_lang') != null) {

		$szLanguage = $CI->session->userdata('site_lang');

	} else {

		$login_user = $CI->session->userdata('login_user');

		switch ($login_user['language']) {
		case 0:
			$szLanguage = "english";
			break;

		case 1:
			$szLanguage = "spanish";
			break;
		}
	}

    // load language file
    $CI->lang->load($szFileName, $szLanguage);
    return $CI->lang->line($szVariableName);
}







function getUniqueKeyForDataRecord($szTableName)



{



    $characters = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";



    $charactersLength = strlen($characters);



    $randomString = '';



    for ($i = 0; $i < 30; $i++) {



        $randomString .= $characters[rand(0, $charactersLength - 1)];



    }



    if(isDataRecordUniqueKeyExists($szTableName, $randomString))



    {



    	$randomString = getUniqueKeyForDataRecord($szTableName);



    }



    return $randomString;



}







function isDataRecordUniqueKeyExists($szTableName, $szUniqueKey)



{



    // get CI instance



    $CI = get_instance();



	



    return $CI->db->select('id')->from($szTableName)->where('szUniqueKey', $szUniqueKey)->get()->num_rows();



}







function addUserActivity($idRecord, $szRecordTable, $szActivity, $arNewData=array(), $arOldData=array())



{



    $idActivity = 0;



    $CI = get_instance();



    $szActivity = trim($szActivity);



    $arUser = $CI->session->userdata('login_user');







    if(!empty($arOldData))



    {



        foreach($arOldData as $key=>$value)



        {



            if(!isset($arNewData[$key]) || (isset($arNewData[$key]) && $arNewData[$key] == $value))



            {					



                if(isset($arNewData[$key]) && $arNewData[$key] == $value)



                    unset($arNewData[$key]);



                unset($arOldData[$key]);



            }



        }



    }







    if(!empty($arUser) && $szActivity != '')



    {



        $data['idUser'] = $arUser['id'];



        $data['szName'] = $arUser['name'];



        $data['szEmail'] = $arUser['email'];


        $data['dtCreatedOn'] = date('Y-m-d H:i:s');  
        $data['idRecord'] = $idRecord;  
        $data['szRecordTable'] = $szRecordTable; 
        $data['szOldData'] = (!empty($arOldData) ? serialize($arOldData) : ''); 
        $data['szNewData'] = (!empty($arNewData) ? serialize($arNewData) : '');  
        $data['szActivity'] = $szActivity;         
        $data['szIPAddress'] = $_SERVER['REMOTE_ADDR']; 
        $data['szUserAgent'] = $_SERVER['HTTP_USER_AGENT']; 

        $CI->db->insert('tbl_user_activity', $data);



        $idActivity = ($CI->db->affected_rows() > 0 ? $CI->db->insert_id() : 0);



    }



    return $idActivity;



}







function parseDTRequest($obj)



{



    // set search variables



    $return[0] = (int)$obj->input->get('length',true);



    if($return[0] <= 0) $return[0] = 10;



    $return[1] = (int)$obj->input->get('start',true);



    if($return[1] < 0) $return[1] = 0;



    $return[2] = $obj->input->get('draw',true); 



    $return[3] = trim($obj->input->get('search[value]', true));      		







    // custom filters



    $return[4] = array();



    if(trim($obj->input->get('p_action', true)) == 'Filter Records')



    {



        $return[4] = sanitizeCustomRecordFilters($obj->input->get('sort_options',true));



    }







    // handle sorting



    $arSortOptions = $obj->input->get('sort_options',true);



    $arSorting = $obj->input->get('order[0]',true);



    $return[5] = (isset($arSorting['column']) ? $arSortOptions[(int)$arSorting['column']] : 'id');



    $return[6] = (isset($arSorting['dir']) && $arSorting['dir'] == 'desc' ? 'DESC' : 'ASC');



    



    return $return;



}







function hasActiveSubscription()



{



    $isHasPermission = false;



    $CI = get_instance();



    $szPermissionName = trim($szPermissionName);



    $arUser = $CI->session->userdata('login_user');



    



    //@TO DO



    /*



    * Will implement this section once we have tblsubscription in place



    */



}



function hasPermission($szPermissionName)



{



    $isHasPermission = false;



    $CI = get_instance();



    $szPermissionName = trim($szPermissionName);



    $arUser = $CI->session->userdata('login_user');



    



    if(isset($arUser['id']) && (int)$arUser['id'] > 0)



    {



        $query = $CI->db->select('u.id')



            ->from('tbl_users u')



            ->join('tbl_roles r', 'r.id = u.idRole')



            ->join('tbl_role_permissions rp', 'rp.idRole = r.id')



            ->join('tbl_permissions p', 'p.id = rp.idPermission')



            ->where(array('u.id'=>$arUser['id'], 'u.isDeleted'=>0, 'r.isDeleted'=>0, 'rp.isDeleted'=>0, 'p.isDeleted'=>0, 'p.szPermissionName'=>$szPermissionName))



            ->get();



        //echo $CI->db->last_query();die;



        return $query->num_rows() > 0;



    }



    return $isHasPermission;



}



function __isValidDate($dtDateTime)



{ 



    $dtNewDate = date('Y-m-d',strtotime($dtDateTime));  



    if(!empty($dtDateTime) && $dtNewDate!='1970-01-01')



    {



        return true;



    }



    else



    {



        return false;



    } 



}







function getLoginUserLatestActivity()



{



    $CI = get_instance();



    $arActivity = array();



    $arUser = $CI->session->userdata('login_user');



    


    if(!empty($arUser))



    {



        $CI->load->model('User_Model');



        $arActivity['user'] = $CI->User_Model->getUserByID($arUser['id']);



        $arActivity['logs'] = $CI->User_Model->getUserActivity($arUser['id'], '', false, 10, 0, 'a.dtCreatedOn', 'DESC', true, true);



    }



    return $arActivity;



}


function get_userdetail($idUser = NULL, $detail = NULL) {
	if ($idUser > 0) {
		$CI = get_instance();
		$CI->load->model('User_Model');
		
		$userDetail = $CI->User_Model->getUserByID($idUser);
		return $userDetail[$detail];
	}
}
	
function get_global_stats() {
	
	$CI = get_instance();
	
	$CI->load->model('User_Model');
	$CI->load->model('Contact_Model');
	$CI->load->model('Membership_Model');
	$CI->load->model('Funnel_Model');
	
	$arUser = $CI->session->userdata('login_user');
	
	// Get Subscriber Details
	$globalStats['subscribers'] = array(
		'total' => $CI->Contact_Model->getDashboardInfo($arUser['id'], 'total'),
		'today' => $CI->Contact_Model->getDashboardInfo($arUser['id'], 'today'),
		'week' => $CI->Contact_Model->getDashboardInfo($arUser['id'], 'week'),
		'month' => $CI->Contact_Model->getDashboardInfo($arUser['id'], 'month'),
		);
	
	// Get Visitor Details
	$globalStats['visitors'] = array(
		'total' => $CI->Funnel_Model->getVisitors($arUser['id'], 'total'),
		'today' => $CI->Funnel_Model->getVisitors($arUser['id'], 'today'),
		'week' => $CI->Funnel_Model->getVisitors($arUser['id'], 'week'),
		'month' => $CI->Funnel_Model->getVisitors($arUser['id'], 'month'),
		);
	
	// Get Referral Details
	$globalStats['referrals'] = array(
		'total' => $CI->User_Model->getUsers($arUser['id'], 'total', $isOnlyCount=true),
		'active' => $CI->User_Model->getUsers($arUser['id'], 'active', $isOnlyCount=true),
		);
	
	// Get Users Details
	if(hasPermission('manage.users')) {
		$iTotalUsers = $CI->User_Model->getUsers(false,false,true);
		$iTotalActiveSubscriptions = $CI->Membership_Model->getAllSubscriptions(true);

		$globalStats['users']['total'] = (int)$iTotalUsers;
		$globalStats['users']['active'] = $iTotalActiveSubscriptions;
		$globalStats['users']['cancelled'] = $iTotalActiveSubscriptions;
	} 
	
	// Get Membership Details
	$arSubscription = $CI->Membership_Model->getUserSubscription($arUser['id']);
	if ( !empty($arSubscription)) {
		$globalStats['subscription'] = $CI->Membership_Model->getUserSubscription($arUser['id']);
	} else if(hasPermission('manage.users')){
		$globalStats['subscription']['szStatus'] = "Super Admin";
	} else {
		$globalStats['subscription']['szStatus'] = "Inactive";
	}
	
	// Get Membership Details
	return $globalStats;
}


function lang_backup($szLanguageKey)
{
    $CI = get_instance();
    return $CI->lang->line($szLanguageKey);
}



function lang_selection() {

	

	echo '

	

          <li>



            <div class="btn-group">



              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">



                <img style="border-radius: 0;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/US_flag_51_stars.svg/1200px-US_flag_51_stars.svg.png" alt="English" />



                    English



                <span class="caret"></span>



              </button>



              <ul class="dropdown-menu dropdown-menu-usermenu pull-right">



                <li><a href="?lang=english"><img style="border-radius: 0;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/US_flag_51_stars.svg/1200px-US_flag_51_stars.svg.png" width="35px" alt="English" /> English</a></li>



              </ul>



            </div>



          </li>

	';



}

function convert_date($dtDateTime,$format=false)
{
    if(!empty($dtDateTime) && $dtDateTime!='0000-00-00 00:00:00' && $dtDateTime!='0000-00-00')
    {
        /*
        * This function will convert the date into customer's local time zone
        */ 
        $dtReturnDateTime = $dtDateTime;
        if(date_default_timezone_get()) 
        {
            $szServerTimeZone = 'America/Los_Angeles'; //default server time zone
            $szCustomerTimeZone = getCustomerTimeZone(); 
            write_log('debug', "Server Time Zone: ".$szServerTimeZone." CustomerTimeZone: ".$szCustomerTimeZone." \n");
            
            if(!empty($szCustomerTimeZone))
            {
                $date = new DateTime($dtDateTime,new DateTimeZone($szServerTimeZone));   
                $dtServerDateTime = $date->format('Y-m-d H:i:s'); 
                
                $date->setTimezone(new DateTimeZone($szCustomerTimeZone));  
                $dtCustomerDateTime = $date->format('Y-m-d H:i:s'); 
                $dtReturnDateTime = $dtCustomerDateTime;
            } 
        }
        if(!empty($dtReturnDateTime))
        {
            /*
            * format=1 means m/d/Y h:ia
            * format=2 means M d, Y
            * format=2 means m/d/Y 
            */
            if($format==1)
            {
                return date('m/d/Y h:ia',  strtotime($dtReturnDateTime));
            }
            else if($format==2)
            {
                return date('M d, Y',  strtotime($dtReturnDateTime));
            }
            else if($format==3)
            {
                return date('m/d/Y',  strtotime($dtReturnDateTime));
            }
            else if($format==4)
            {
                return date('Y-m-d H:i:s',  strtotime($dtReturnDateTime));
            }
            else
            {
                return date('Y-m-d');
            }
        }
    } 
}

function getCustomerTimeZone()
{ 
    //$_SESSION['szCustomerDateTimeZoneGlobal'] = array();
    if(!empty($_SESSION['szCustomerDateTimeZoneGlobal']))
    {
        return $_SESSION['szCustomerDateTimeZoneGlobal'];
    }
    else
    {
        $ret_ary = array();
        $ret_ary = getCountryCodeByIPAddress();
        
        write_log('debug', "getCountryCodeByIPAddress: ".print_R($ret_ary,true)." \n");
        
        if(empty($ret_ary))
        {
            $ret_ary['szCountryCode'] = 'US';
            $ret_ary['szRegion'] = 'CA';
        }  
        $szCustomerTimeZone = get_time_zone($ret_ary['szCountryCode'],$ret_ary['szRegion']); 
        $_SESSION['szCustomerDateTimeZoneGlobal'] = $szCustomerTimeZone ; 
        return $szCustomerTimeZone;
    }
}

function getCountryCodeByIPAddress()
{      
    $szUserIpAddress = getRealIP();  
    $ipReturnAry = array(); 
    $ipReturnAry = ip_info($szUserIpAddress); 
                
    $ret_ary = array();
    if(!empty($ipReturnAry))
    {
        $ret_ary['szZipCode'] = $ipReturnAry['szPostcode'] ;
        $ret_ary['szCityName'] = $ipReturnAry['szCity'] ;
        $ret_ary['szRegionName'] = $ipReturnAry['szRegion'] ;
        $ret_ary['szCountryCode'] = $ipReturnAry['szCountryCode'] ;
        $ret_ary['szCountryName'] = $ipReturnAry['szCountryName'] ;  
        $ret_ary['szIPAddress'] = $szUserIpAddress;  
    }  
    return $ret_ary ;  
}

function getRealIP() 
{ 
    $ipaddress = '';
    if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) 
    {
        $ipaddress =  $_SERVER['HTTP_CF_CONNECTING_IP'];
    } 
    else if (isset($_SERVER['HTTP_X_REAL_IP'])) 
    {
        $ipaddress = $_SERVER['HTTP_X_REAL_IP'];
    }
    else if (isset($_SERVER['HTTP_CLIENT_IP']))
    {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    }
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
    {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    }
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_FORWARDED']))
    {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    }
    else if(isset($_SERVER['REMOTE_ADDR']))
    {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    }        
    else
    {
        $ipaddress = 'UNKNOWN';
    }   
    return $ipaddress; 
}

function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) 
{ 
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    ); 
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $posturl = "http://www.geoplugin.net/json.gp?ip=" . $ip;

        $response = curl_get_file_contents($posturl,true);  

        $ipdat = @json_decode($response);
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "szCity"     => @$ipdat->geoplugin_city,
                        "szRegion"   => @$ipdat->geoplugin_regionName,
                        "szCountryName"  => @$ipdat->geoplugin_countryName,
                        "szCountryCode"  => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}

function curl_get_file_contents($URL,$bGeoplugin=false) 
{
    $c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $URL);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
    if($bGeoplugin)
    { 
        curl_setopt($c, CURLOPT_TIMEOUT_MS, '5000');
    } 
    $contents = curl_exec($c); 
    $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
    curl_close($c);
    if ($contents) return $contents;
    else return FALSE;
 }
         
function get_time_zone($country,$region) 
{ 
  switch ($country) 
  { 
    case "US":
        switch ($region) 
        {  
            case "AL":
                $timezone = "America/Chicago";
                break;
            case "AK":
                $timezone = "America/Anchorage";
                break;
            case "AZ":
                $timezone = "America/Phoenix";
                break;
            case "AR":
                $timezone = "America/Chicago";
                break;
            case "CA":
                $timezone = "America/Los_Angeles";
                break;
            case "CO":
                $timezone = "America/Denver";
                break;
            case "CT":
                $timezone = "America/New_York";
                break;
            case "DE":
                $timezone = "America/New_York";
                break;
            case "DC":
                $timezone = "America/New_York";
                break;
            case "FL":
                $timezone = "America/New_York";
                break;
            case "GA":
                $timezone = "America/New_York";
                break;
            case "HI":
                $timezone = "Pacific/Honolulu";
                break;
            case "ID":
                $timezone = "America/Denver";
                break;
            case "IL":
                $timezone = "America/Chicago";
                break;
            case "IN":
                $timezone = "America/Indianapolis";
                break;
            case "IA":
                $timezone = "America/Chicago";
                break;
            case "KS":
                $timezone = "America/Chicago";
                break;
            case "KY":
                $timezone = "America/New_York";
                break;
            case "LA":
                $timezone = "America/Chicago";
                break;
            case "ME":
                $timezone = "America/New_York";
                break;
            case "MD":
                $timezone = "America/New_York";
                break;
            case "MA":
                $timezone = "America/New_York";
                break;
            case "MI":
                $timezone = "America/New_York";
                break;
            case "MN":
                $timezone = "America/Chicago";
                break;
            case "MS":
                $timezone = "America/Chicago";
                break;
            case "MO":
                $timezone = "America/Chicago";
                break;
            case "MT":
                $timezone = "America/Denver";
                break;
            case "NE":
                $timezone = "America/Chicago";
                break;
            case "NV":
                $timezone = "America/Los_Angeles";
                break;
            case "NH":
                $timezone = "America/New_York";
                break;
            case "NJ":
                $timezone = "America/New_York";
                break;
            case "NM":
                $timezone = "America/Denver";
                break;
            case "NY":
                $timezone = "America/New_York";
                break;
            case "NC":
                $timezone = "America/New_York";
                break;
            case "ND":
                $timezone = "America/Chicago";
                break;
            case "OH":
                $timezone = "America/New_York";
                break;
            case "OK":
                $timezone = "America/Chicago";
                break;
            case "OR":
                $timezone = "America/Los_Angeles";
                break;
            case "PA":
                $timezone = "America/New_York";
                break;
            case "RI":
                $timezone = "America/New_York";
                break;
            case "SC":
                $timezone = "America/New_York";
                break;
            case "SD":
                $timezone = "America/Chicago";
                break;
            case "TN":
                $timezone = "America/Chicago";
                break;
            case "TX":
                $timezone = "America/Chicago";
                break;
            case "UT":
                $timezone = "America/Denver";
                break;
            case "VT":
                $timezone = "America/New_York";
                break;
            case "VA":
                $timezone = "America/New_York";
                break;
            case "WA":
                $timezone = "America/Los_Angeles";
                break;
            case "WV":
                $timezone = "America/New_York";
                break;
            case "WI":
                $timezone = "America/Chicago";
                break;
            case "WY":
                $timezone = "America/Denver";
            default:
                $timezone = "America/Chicago";
                break;
        } 
    break;
    case "CA":
        switch ($region) 
        { 
            case "AB":
                $timezone = "America/Edmonton";
                break;
            case "BC":
                $timezone = "America/Vancouver";
                break;
            case "MB":
                $timezone = "America/Winnipeg";
                break;
            case "NB":
                $timezone = "America/Halifax";
                break;
            case "NL":
                $timezone = "America/St_Johns";
                break;
            case "NT":
                $timezone = "America/Yellowknife";
                break;
            case "NS":
                $timezone = "America/Halifax";
                break;
            case "NU":
                $timezone = "America/Rankin_Inlet";
                break;
            case "ON":
                $timezone = "America/Rainy_River";
                break;
            case "PE":
                $timezone = "America/Halifax";
                break;
            case "QC":
                $timezone = "America/Montreal";
                break;
            case "SK":
                $timezone = "America/Regina";
                break;
            case "YT":
                $timezone = "America/Whitehorse";
            default:
                $timezone = "America/Vancouver";
                break;
        } 
        break;
        case "AU":
        switch ($region) 
        { 
            case "01":
                $timezone = "Australia/Canberra";
                break;
            case "02":
                $timezone = "Australia/NSW";
                break;
            case "03":
                $timezone = "Australia/North";
                break;
            case "04":
                $timezone = "Australia/Queensland";
                break;
            case "05":
                $timezone = "Australia/South";
                break;
            case "06":
                $timezone = "Australia/Tasmania";
                break;
            case "07":
                $timezone = "Australia/Victoria";
                break;
            case "08":
                $timezone = "Australia/West";
            default:
                $timezone = "Australia/Canberra";
                break;
        } 
        break;
        case "AS":
            $timezone = "US/Samoa";
            break;
        case "CI":
            $timezone = "Africa/Abidjan";
            break;
        case "GH":
            $timezone = "Africa/Accra";
            break;
        case "DZ":
            $timezone = "Africa/Algiers";
            break;
        case "ER":
            $timezone = "Africa/Asmera";
            break;
        case "ML":
            $timezone = "Africa/Bamako";
            break;
        case "CF":
            $timezone = "Africa/Bangui";
            break;
        case "GM":
            $timezone = "Africa/Banjul";
            break;
        case "GW":
            $timezone = "Africa/Bissau";
            break;
        case "CG":
            $timezone = "Africa/Brazzaville";
            break;
        case "BI":
            $timezone = "Africa/Bujumbura";
            break;
        case "EG":
            $timezone = "Africa/Cairo";
            break;
        case "MA":
            $timezone = "Africa/Casablanca";
            break;
        case "GN":
            $timezone = "Africa/Conakry";
            break;
        case "SN":
            $timezone = "Africa/Dakar";
            break;
        case "DJ":
            $timezone = "Africa/Djibouti";
            break;
        case "SL":
            $timezone = "Africa/Freetown";
            break;
        case "BW":
            $timezone = "Africa/Gaborone";
            break;
        case "ZW":
            $timezone = "Africa/Harare";
            break;
        case "ZA":
            $timezone = "Africa/Johannesburg";
            break;
        case "UG":
            $timezone = "Africa/Kampala";
            break;
        case "SD":
            $timezone = "Africa/Khartoum";
            break;
        case "RW":
            $timezone = "Africa/Kigali";
            break;
        case "NG":
            $timezone = "Africa/Lagos";
            break;
        case "GA":
            $timezone = "Africa/Libreville";
            break;
        case "TG":
            $timezone = "Africa/Lome";
            break;
        case "AO":
            $timezone = "Africa/Luanda";
            break;
        case "ZM":
            $timezone = "Africa/Lusaka";
            break;
        case "GQ":
            $timezone = "Africa/Malabo";
            break;
        case "MZ":
            $timezone = "Africa/Maputo";
            break;
        case "LS":
            $timezone = "Africa/Maseru";
            break;
        case "SZ":
            $timezone = "Africa/Mbabane";
            break;
        case "SO":
            $timezone = "Africa/Mogadishu";
            break;
        case "LR":
            $timezone = "Africa/Monrovia";
            break;
        case "KE":
            $timezone = "Africa/Nairobi";
            break;
        case "TD":
            $timezone = "Africa/Ndjamena";
            break;
        case "NE":
            $timezone = "Africa/Niamey";
            break;
        case "MR":
            $timezone = "Africa/Nouakchott";
            break;
        case "BF":
            $timezone = "Africa/Ouagadougou";
            break;
        case "ST":
            $timezone = "Africa/Sao_Tome";
            break;
        case "LY":
            $timezone = "Africa/Tripoli";
            break;
        case "TN":
            $timezone = "Africa/Tunis";
            break;
        case "AI":
            $timezone = "America/Anguilla";
            break;
        case "AG":
            $timezone = "America/Antigua";
            break;
        case "AW":
            $timezone = "America/Aruba";
            break;
        case "BB":
            $timezone = "America/Barbados";
            break;
        case "BZ":
            $timezone = "America/Belize";
            break;
        case "CO":
            $timezone = "America/Bogota";
            break;
        case "VE":
            $timezone = "America/Caracas";
            break;
        case "KY":
            $timezone = "America/Cayman";
            break;
        case "CR":
            $timezone = "America/Costa_Rica";
            break;
        case "DM":
            $timezone = "America/Dominica";
            break;
        case "SV":
            $timezone = "America/El_Salvador";
            break;
        case "GD":
            $timezone = "America/Grenada";
            break;
        case "FR":
            $timezone = "Europe/Paris";
            break;
        case "GP":
            $timezone = "America/Guadeloupe";
            break;
        case "GT":
            $timezone = "America/Guatemala";
            break;
        case "GY":
            $timezone = "America/Guyana";
            break;
        case "CU":
            $timezone = "America/Havana";
            break;
        case "JM":
            $timezone = "America/Jamaica";
            break;
        case "BO":
            $timezone = "America/La_Paz";
            break;
        case "PE":
            $timezone = "America/Lima";
            break;
        case "NI":
            $timezone = "America/Managua";
            break;
        case "MQ":
            $timezone = "America/Martinique";
            break;
        case "UY":
            $timezone = "America/Montevideo";
            break;
        case "MS":
            $timezone = "America/Montserrat";
            break;
        case "BS":
            $timezone = "America/Nassau";
            break;
        case "PA":
            $timezone = "America/Panama";
            break;
        case "SR":
            $timezone = "America/Paramaribo";
            break;
        case "PR":
            $timezone = "America/Puerto_Rico";
            break;
        case "KN":
            $timezone = "America/St_Kitts";
            break;
        case "LC":
            $timezone = "America/St_Lucia";
            break;
        case "VC":
            $timezone = "America/St_Vincent";
            break;
        case "HN":
            $timezone = "America/Tegucigalpa";
            break;
        case "YE":
            $timezone = "Asia/Aden";
            break;
        case "JO":
            $timezone = "Asia/Amman";
            break;
        case "TM":
            $timezone = "Asia/Ashgabat";
            break;
        case "IQ":
            $timezone = "Asia/Baghdad";
            break;
        case "BH":
            $timezone = "Asia/Bahrain";
            break;
        case "AZ":
            $timezone = "Asia/Baku";
            break;
        case "TH":
            $timezone = "Asia/Bangkok";
            break;
        case "LB":
            $timezone = "Asia/Beirut";
            break;
        case "KG":
            $timezone = "Asia/Bishkek";
            break;
        case "BN":
            $timezone = "Asia/Brunei";
            break;
        case "IN":
            $timezone = "Asia/Calcutta";
            break;
        case "MN":
            $timezone = "Asia/Choibalsan";
            break;
        case "LK":
            $timezone = "Asia/Colombo";
            break;
        case "BD":
            $timezone = "Asia/Dhaka";
            break;
        case "AE":
            $timezone = "Asia/Dubai";
            break;
        case "TJ":
            $timezone = "Asia/Dushanbe";
            break;
        case "HK":
            $timezone = "Asia/Hong_Kong";
            break;
        case "TR":
            $timezone = "Asia/Istanbul";
            break;
        case "IL":
            $timezone = "Asia/Jerusalem";
            break;
        case "AF":
            $timezone = "Asia/Kabul";
            break;
        case "PK":
            $timezone = "Asia/Karachi";
            break;
        case "NP":
            $timezone = "Asia/Katmandu";
            break;
        case "KW":
            $timezone = "Asia/Kuwait";
            break;
        case "MO":
            $timezone = "Asia/Macao";
            break;
        case "PH":
            $timezone = "Asia/Manila";
            break;
        case "OM":
            $timezone = "Asia/Muscat";
            break;
        case "CY":
            $timezone = "Asia/Nicosia";
            break;
        case "KP":
            $timezone = "Asia/Pyongyang";
            break;
        case "QA":
            $timezone = "Asia/Qatar";
            break;
        case "MM":
            $timezone = "Asia/Rangoon";
            break;
        case "SA":
            $timezone = "Asia/Riyadh";
            break;
        case "KR":
            $timezone = "Asia/Seoul";
            break;
        case "SG":
            $timezone = "Asia/Singapore";
            break;
        case "TW":
            $timezone = "Asia/Taipei";
            break;
        case "GE":
            $timezone = "Asia/Tbilisi";
            break;
        case "BT":
            $timezone = "Asia/Thimphu";
            break;
        case "JP":
            $timezone = "Asia/Tokyo";
            break;
        case "LA":
            $timezone = "Asia/Vientiane";
            break;
        case "AM":
            $timezone = "Asia/Yerevan";
            break;
        case "BM":
            $timezone = "Atlantic/Bermuda";
            break;
        case "CV":
            $timezone = "Atlantic/Cape_Verde";
            break;
        case "FO":
            $timezone = "Atlantic/Faeroe";
            break;
        case "IS":
            $timezone = "Atlantic/Reykjavik";
            break;
        case "GS":
            $timezone = "Atlantic/South_Georgia";
            break;
        case "SH":
            $timezone = "Atlantic/St_Helena";
            break;
        case "CL":
            $timezone = "Chile/Continental";
            break;
        case "NL":
            $timezone = "Europe/Amsterdam";
            break;
        case "AD":
            $timezone = "Europe/Andorra";
            break;
        case "GR":
            $timezone = "Europe/Athens";
            break;
        case "YU":
            $timezone = "Europe/Belgrade";
            break;
        case "DE":
            $timezone = "Europe/Berlin";
            break;
        case "SK":
            $timezone = "Europe/Bratislava";
            break;
        case "BE":
            $timezone = "Europe/Brussels";
            break;
        case "RO":
            $timezone = "Europe/Bucharest";
            break;
        case "HU":
            $timezone = "Europe/Budapest";
            break;
        case "DK":
            $timezone = "Europe/Copenhagen";
            break;
        case "IE":
            $timezone = "Europe/Dublin";
            break;
        case "GI":
            $timezone = "Europe/Gibraltar";
            break;
        case "FI":
            $timezone = "Europe/Helsinki";
            break;
        case "SI":
            $timezone = "Europe/Ljubljana";
            break;
        case "GB":
            $timezone = "Europe/London";
            break;
        case "LU":
            $timezone = "Europe/Luxembourg";
            break;
        case "MT":
            $timezone = "Europe/Malta";
            break;
        case "BY":
            $timezone = "Europe/Minsk";
            break;
        case "MC":
            $timezone = "Europe/Monaco";
            break;
        case "NO":
            $timezone = "Europe/Oslo";
            break;
        case "CZ":
            $timezone = "Europe/Prague";
            break;
        case "LV":
            $timezone = "Europe/Riga";
            break;
        case "IT":
            $timezone = "Europe/Rome";
            break;
        case "SM":
            $timezone = "Europe/San_Marino";
            break;
        case "BA":
            $timezone = "Europe/Sarajevo";
            break;
        case "MK":
            $timezone = "Europe/Skopje";
            break;
        case "BG":
            $timezone = "Europe/Sofia";
            break;
        case "SE":
            $timezone = "Europe/Stockholm";
            break;
        case "EE":
            $timezone = "Europe/Tallinn";
            break;
        case "AL":
            $timezone = "Europe/Tirane";
            break;
        case "LI":
            $timezone = "Europe/Vaduz";
            break;
        case "VA":
            $timezone = "Europe/Vatican";
            break;
        case "AT":
            $timezone = "Europe/Vienna";
            break;
        case "LT":
            $timezone = "Europe/Vilnius";
            break;
        case "PL":
            $timezone = "Europe/Warsaw";
            break;
        case "HR":
            $timezone = "Europe/Zagreb";
            break;
        case "IR":
            $timezone = "Asia/Tehran";
            break;
        case "MG":
            $timezone = "Indian/Antananarivo";
            break;
        case "CX":
            $timezone = "Indian/Christmas";
            break;
        case "CC":
            $timezone = "Indian/Cocos";
            break;
        case "KM":
            $timezone = "Indian/Comoro";
            break;
        case "MV":
            $timezone = "Indian/Maldives";
            break;
        case "MU":
            $timezone = "Indian/Mauritius";
            break;
        case "YT":
            $timezone = "Indian/Mayotte";
            break;
        case "RE":
            $timezone = "Indian/Reunion";
            break;
        case "FJ":
            $timezone = "Pacific/Fiji";
            break;
        case "TV":
            $timezone = "Pacific/Funafuti";
            break;
        case "GU":
            $timezone = "Pacific/Guam";
            break;
        case "NR":
            $timezone = "Pacific/Nauru";
            break;
        case "NU":
            $timezone = "Pacific/Niue";
            break;
        case "NF":
            $timezone = "Pacific/Norfolk";
            break;
        case "PW":
            $timezone = "Pacific/Palau";
            break;
        case "PN":
            $timezone = "Pacific/Pitcairn";
            break;
        case "CK":
            $timezone = "Pacific/Rarotonga";
            break;
        case "WS":
            $timezone = "Pacific/Samoa";
            break;
        case "KI":
            $timezone = "Pacific/Tarawa";
            break;
        case "TO":
            $timezone = "Pacific/Tongatapu";
            break;
        case "WF":
            $timezone = "Pacific/Wallis";
            break;
        case "TZ":
            $timezone = "Africa/Dar_es_Salaam";
            break;
        case "VN":
            $timezone = "Asia/Phnom_Penh";
            break;
        case "KH":
            $timezone = "Asia/Phnom_Penh";
            break;
        case "CM":
            $timezone = "Africa/Lagos";
            break;
        case "DO":
            $timezone = "America/Santo_Domingo";
            break;
        case "ET":
            $timezone = "Africa/Addis_Ababa";
            break;
        case "FX":
            $timezone = "Europe/Paris";
            break;
        case "HT":
            $timezone = "America/Port-au-Prince";
            break;
        case "CH":
            $timezone = "Europe/Zurich";
            break;
        case "AN":
            $timezone = "America/Curacao";
            break;
        case "BJ":
            $timezone = "Africa/Porto-Novo";
            break;
        case "EH":
            $timezone = "Africa/El_Aaiun";
            break;
        case "FK":
            $timezone = "Atlantic/Stanley";
            break;
        case "GF":
            $timezone = "America/Cayenne";
            break;
        case "IO":
            $timezone = "Indian/Chagos";
            break;
        case "MD":
            $timezone = "Europe/Chisinau";
            break;
        case "MP":
            $timezone = "Pacific/Saipan";
            break;
        case "MW":
            $timezone = "Africa/Blantyre";
            break;
        case "NA":
            $timezone = "Africa/Windhoek";
            break;
        case "NC":
            $timezone = "Pacific/Noumea";
            break;
        case "PG":
            $timezone = "Pacific/Port_Moresby";
            break;
        case "PM":
            $timezone = "America/Miquelon";
            break;
        case "PS":
            $timezone = "Asia/Gaza";
            break;
        case "PY":
            $timezone = "America/Asuncion";
            break;
        case "SB":
            $timezone = "Pacific/Guadalcanal";
            break;
        case "SC":
            $timezone = "Indian/Mahe";
            break;
        case "SJ":
            $timezone = "Arctic/Longyearbyen";
            break;
        case "SY":
            $timezone = "Asia/Damascus";
            break;
        case "TC":
            $timezone = "America/Grand_Turk";
            break;
        case "TF":
            $timezone = "Indian/Kerguelen";
            break;
        case "TK":
            $timezone = "Pacific/Fakaofo";
            break;
        case "TT":
            $timezone = "America/Port_of_Spain";
            break;
        case "VG":
            $timezone = "America/Tortola";
            break;
        case "VI":
            $timezone = "America/St_Thomas";
            break;
        case "VU":
            $timezone = "Pacific/Efate";
            break;
        case "RS":
            $timezone = "Europe/Belgrade";
            break;
        case "ME":
            $timezone = "Europe/Podgorica";
            break;
        case "AX":
            $timezone = "Europe/Mariehamn";
            break;
        case "GG":
            $timezone = "Europe/Guernsey";
            break;
        case "IM":
            $timezone = "Europe/Isle_of_Man";
            break;
        case "JE":
            $timezone = "Europe/Jersey";
            break;
        case "BL":
            $timezone = "America/St_Barthelemy";
            break;
        case "MF":
            $timezone = "America/Marigot";
            break;
        case "AR":
            switch ($region) 
            { 
                case "01":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "02":
                    $timezone = "America/Argentina/Catamarca";
                    break;
                case "03":
                    $timezone = "America/Argentina/Tucuman";
                    break;
                case "04":
                    $timezone = "America/Argentina/Rio_Gallegos";
                    break;
                case "05":
                    $timezone = "America/Argentina/Cordoba";
                    break;
                case "06":
                    $timezone = "America/Argentina/Tucuman";
                    break;
                case "07":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "08":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "09":
                    $timezone = "America/Argentina/Tucuman";
                    break;
                case "10":
                    $timezone = "America/Argentina/Jujuy";
                    break;
                case "11":
                    $timezone = "America/Argentina/San_Luis";
                    break;
                case "12":
                    $timezone = "America/Argentina/La_Rioja";
                    break;
                case "13":
                    $timezone = "America/Argentina/Mendoza";
                    break;
                case "14":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "15":
                    $timezone = "America/Argentina/San_Luis";
                    break;
                case "16":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "17":
                    $timezone = "America/Argentina/Salta";
                    break;
                case "18":
                    $timezone = "America/Argentina/San_Juan";
                    break;
                case "19":
                    $timezone = "America/Argentina/San_Luis";
                    break;
                case "20":
                    $timezone = "America/Argentina/Rio_Gallegos";
                    break;
                case "21":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "22":
                    $timezone = "America/Argentina/Catamarca";
                    break;
                case "23":
                    $timezone = "America/Argentina/Ushuaia";
                    break;
                case "24":
                    $timezone = "America/Argentina/Tucuman";
                    break;
            } 
        break;
        case "BR":
            switch ($region) 
            { 
                case "01":
                    $timezone = "America/Rio_Branco";
                    break;
                case "02":
                    $timezone = "America/Maceio";
                    break;
                case "03":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "04":
                    $timezone = "America/Manaus";
                    break;
                case "05":
                    $timezone = "America/Bahia";
                    break;
                case "06":
                    $timezone = "America/Fortaleza";
                    break;
                case "07":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "08":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "11":
                    $timezone = "America/Campo_Grande";
                    break;
                case "13":
                    $timezone = "America/Belem";
                    break;
                case "14":
                    $timezone = "America/Cuiaba";
                    break;
                case "15":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "16":
                    $timezone = "America/Belem";
                    break;
                case "17":
                    $timezone = "America/Recife";
                    break;
                case "18":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "20":
                    $timezone = "America/Fortaleza";
                    break;
                case "21":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "22":
                    $timezone = "America/Recife";
                    break;
                case "23":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "24":
                    $timezone = "America/Porto_Velho";
                    break;
                case "25":
                    $timezone = "America/Boa_Vista";
                    break;
                case "26":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "27":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "28":
                    $timezone = "America/Maceio";
                    break;
                case "29":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "30":
                    $timezone = "America/Recife";
                    break;
                case "31":
                    $timezone = "America/Araguaina";
                    break;
            } 
        break;
    case "CD":
        switch ($region) 
        { 
            case "02":
                $timezone = "Africa/Kinshasa";
                break;
            case "05":
                $timezone = "Africa/Lubumbashi";
                break;
            case "06":
                $timezone = "Africa/Kinshasa";
                break;
            case "08":
                $timezone = "Africa/Kinshasa";
                break;
            case "10":
                $timezone = "Africa/Lubumbashi";
                break;
            case "11":
                $timezone = "Africa/Lubumbashi";
                break;
            case "12":
                $timezone = "Africa/Lubumbashi";
                break;
        } 
    break;
    case "CN":
        switch ($region) 
        { 
            case "01":
                $timezone = "Asia/Shanghai";
                break;
            case "02":
                $timezone = "Asia/Shanghai";
                break;
            case "03":
                $timezone = "Asia/Shanghai";
                break;
            case "04":
                $timezone = "Asia/Shanghai";
                break;
            case "05":
                $timezone = "Asia/Harbin";
                break;
            case "06":
                $timezone = "Asia/Chongqing";
                break;
            case "07":
                $timezone = "Asia/Shanghai";
                break;
            case "08":
                $timezone = "Asia/Harbin";
                break;
            case "09":
                $timezone = "Asia/Shanghai";
                break;
            case "10":
                $timezone = "Asia/Shanghai";
                break;
            case "11":
                $timezone = "Asia/Chongqing";
                break;
            case "12":
                $timezone = "Asia/Shanghai";
                break;
            case "13":
                $timezone = "Asia/Urumqi";
                break;
            case "14":
                $timezone = "Asia/Chongqing";
                break;
            case "15":
                $timezone = "Asia/Chongqing";
                break;
            case "16":
                $timezone = "Asia/Chongqing";
                break;
            case "18":
                $timezone = "Asia/Chongqing";
                break;
            case "19":
                $timezone = "Asia/Harbin";
                break;
            case "20":
                $timezone = "Asia/Harbin";
                break;
            case "21":
                $timezone = "Asia/Chongqing";
                break;
            case "22":
                $timezone = "Asia/Harbin";
                break;
            case "23":
                $timezone = "Asia/Shanghai";
                break;
            case "24":
                $timezone = "Asia/Chongqing";
                break;
            case "25":
                $timezone = "Asia/Shanghai";
                break;
            case "26":
                $timezone = "Asia/Chongqing";
                break;
            case "28":
                $timezone = "Asia/Shanghai";
                break;
            case "29":
                $timezone = "Asia/Chongqing";
                break;
            case "30":
                $timezone = "Asia/Chongqing";
                break;
            case "31":
                $timezone = "Asia/Chongqing";
                break;
            case "32":
                $timezone = "Asia/Chongqing";
                break;
            case "33":
                $timezone = "Asia/Chongqing";
                break;
        } 
    break;
    case "EC":
    switch ($region) 
    { 
        case "01":
            $timezone = "Pacific/Galapagos";
            break;
        case "02":
            $timezone = "America/Guayaquil";
            break;
        case "03":
            $timezone = "America/Guayaquil";
            break;
        case "04":
            $timezone = "America/Guayaquil";
            break;
        case "05":
            $timezone = "America/Guayaquil";
            break;
        case "06":
            $timezone = "America/Guayaquil";
            break;
        case "07":
            $timezone = "America/Guayaquil";
            break;
        case "08":
            $timezone = "America/Guayaquil";
            break;
        case "09":
            $timezone = "America/Guayaquil";
            break;
        case "10":
            $timezone = "America/Guayaquil";
            break;
        case "11":
            $timezone = "America/Guayaquil";
            break;
        case "12":
            $timezone = "America/Guayaquil";
            break;
        case "13":
            $timezone = "America/Guayaquil";
            break;
        case "14":
            $timezone = "America/Guayaquil";
            break;
        case "15":
            $timezone = "America/Guayaquil";
            break;
        case "17":
            $timezone = "America/Guayaquil";
            break;
        case "18":
            $timezone = "America/Guayaquil";
            break;
        case "19":
            $timezone = "America/Guayaquil";
            break;
        case "20":
            $timezone = "America/Guayaquil";
            break;
        case "22":
            $timezone = "America/Guayaquil";
            break;
    } 
    break;
case "ES":
    switch ($region) { 
  case "07":
      $timezone = "Europe/Madrid";
      break;
  case "27":
      $timezone = "Europe/Madrid";
      break;
  case "29":
      $timezone = "Europe/Madrid";
      break;
  case "31":
      $timezone = "Europe/Madrid";
      break;
  case "32":
      $timezone = "Europe/Madrid";
      break;
  case "34":
      $timezone = "Europe/Madrid";
      break;
  case "39":
      $timezone = "Europe/Madrid";
      break;
  case "51":
      $timezone = "Africa/Ceuta";
      break;
  case "52":
      $timezone = "Europe/Madrid";
      break;
  case "53":
      $timezone = "Atlantic/Canary";
      break;
  case "54":
      $timezone = "Europe/Madrid";
      break;
  case "55":
      $timezone = "Europe/Madrid";
      break;
  case "56":
      $timezone = "Europe/Madrid";
      break;
  case "57":
      $timezone = "Europe/Madrid";
      break;
  case "58":
      $timezone = "Europe/Madrid";
      break;
  case "59":
      $timezone = "Europe/Madrid";
      break;
  case "60":
      $timezone = "Europe/Madrid";
      break;
  } 
  break;
case "GL":
    switch ($region) { 
  case "01":
      $timezone = "America/Thule";
      break;
  case "02":
      $timezone = "America/Godthab";
      break;
  case "03":
      $timezone = "America/Godthab";
      break;
  } 
  break;
case "ID":
    switch ($region) { 
  case "01":
      $timezone = "Asia/Pontianak";
      break;
  case "02":
      $timezone = "Asia/Makassar";
      break;
  case "03":
      $timezone = "Asia/Jakarta";
      break;
  case "04":
      $timezone = "Asia/Jakarta";
      break;
  case "05":
      $timezone = "Asia/Jakarta";
      break;
  case "06":
      $timezone = "Asia/Jakarta";
      break;
  case "07":
      $timezone = "Asia/Jakarta";
      break;
  case "08":
      $timezone = "Asia/Jakarta";
      break;
  case "09":
      $timezone = "Asia/Jayapura";
      break;
  case "10":
      $timezone = "Asia/Jakarta";
      break;
  case "11":
      $timezone = "Asia/Pontianak";
      break;
  case "12":
      $timezone = "Asia/Makassar";
      break;
  case "13":
      $timezone = "Asia/Makassar";
      break;
  case "14":
      $timezone = "Asia/Makassar";
      break;
  case "15":
      $timezone = "Asia/Jakarta";
      break;
  case "16":
      $timezone = "Asia/Makassar";
      break;
  case "17":
      $timezone = "Asia/Makassar";
      break;
  case "18":
      $timezone = "Asia/Makassar";
      break;
  case "19":
      $timezone = "Asia/Pontianak";
      break;
  case "20":
      $timezone = "Asia/Makassar";
      break;
  case "21":
      $timezone = "Asia/Makassar";
      break;
  case "22":
      $timezone = "Asia/Makassar";
      break;
  case "23":
      $timezone = "Asia/Makassar";
      break;
  case "24":
      $timezone = "Asia/Jakarta";
      break;
  case "25":
      $timezone = "Asia/Pontianak";
      break;
  case "26":
      $timezone = "Asia/Pontianak";
      break;
  case "30":
      $timezone = "Asia/Jakarta";
      break;
  case "31":
      $timezone = "Asia/Makassar";
      break;
  case "33":
      $timezone = "Asia/Jakarta";
      break;
  } 
  break;
case "KZ":
    switch ($region) { 
  case "01":
      $timezone = "Asia/Almaty";
      break;
  case "02":
      $timezone = "Asia/Almaty";
      break;
  case "03":
      $timezone = "Asia/Qyzylorda";
      break;
  case "04":
      $timezone = "Asia/Aqtobe";
      break;
  case "05":
      $timezone = "Asia/Qyzylorda";
      break;
  case "06":
      $timezone = "Asia/Aqtau";
      break;
  case "07":
      $timezone = "Asia/Oral";
      break;
  case "08":
      $timezone = "Asia/Qyzylorda";
      break;
  case "09":
      $timezone = "Asia/Aqtau";
      break;
  case "10":
      $timezone = "Asia/Qyzylorda";
      break;
  case "11":
      $timezone = "Asia/Almaty";
      break;
  case "12":
      $timezone = "Asia/Qyzylorda";
      break;
  case "13":
      $timezone = "Asia/Aqtobe";
      break;
  case "14":
      $timezone = "Asia/Qyzylorda";
      break;
  case "15":
      $timezone = "Asia/Almaty";
      break;
  case "16":
      $timezone = "Asia/Aqtobe";
      break;
  case "17":
      $timezone = "Asia/Almaty";
      break;
  } 
  break;
case "MX":
    switch ($region) { 
  case "01":
      $timezone = "America/Mexico_City";
      break;
  case "02":
      $timezone = "America/Tijuana";
      break;
  case "03":
      $timezone = "America/Hermosillo";
      break;
  case "04":
      $timezone = "America/Merida";
      break;
  case "05":
      $timezone = "America/Mexico_City";
      break;
  case "06":
      $timezone = "America/Chihuahua";
      break;
  case "07":
      $timezone = "America/Monterrey";
      break;
  case "08":
      $timezone = "America/Mexico_City";
      break;
  case "09":
      $timezone = "America/Mexico_City";
      break;
  case "10":
      $timezone = "America/Mazatlan";
      break;
  case "11":
      $timezone = "America/Mexico_City";
      break;
  case "12":
      $timezone = "America/Mexico_City";
      break;
  case "13":
      $timezone = "America/Mexico_City";
      break;
  case "14":
      $timezone = "America/Mazatlan";
      break;
  case "15":
      $timezone = "America/Chihuahua";
      break;
  case "16":
      $timezone = "America/Mexico_City";
      break;
  case "17":
      $timezone = "America/Mexico_City";
      break;
  case "18":
      $timezone = "America/Mazatlan";
      break;
  case "19":
      $timezone = "America/Monterrey";
      break;
  case "20":
      $timezone = "America/Mexico_City";
      break;
  case "21":
      $timezone = "America/Mexico_City";
      break;
  case "22":
      $timezone = "America/Mexico_City";
      break;
  case "23":
      $timezone = "America/Cancun";
      break;
  case "24":
      $timezone = "America/Mexico_City";
      break;
  case "25":
      $timezone = "America/Mazatlan";
      break;
  case "26":
      $timezone = "America/Hermosillo";
      break;
  case "27":
      $timezone = "America/Merida";
      break;
  case "28":
      $timezone = "America/Monterrey";
      break;
  case "29":
      $timezone = "America/Mexico_City";
      break;
  case "30":
      $timezone = "America/Mexico_City";
      break;
  case "31":
      $timezone = "America/Merida";
      break;
  case "32":
      $timezone = "America/Monterrey";
      break;
  } 
  break;
case "MY":
    switch ($region) { 
  case "01":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "02":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "03":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "04":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "05":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "06":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "07":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "08":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "09":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "11":
      $timezone = "Asia/Kuching";
      break;
  case "12":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "13":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "14":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "15":
      $timezone = "Asia/Kuching";
      break;
  case "16":
      $timezone = "Asia/Kuching";
      break;
  } 
  break;
case "NZ":
    switch ($region) { 
  case "85":
      $timezone = "Pacific/Auckland";
      break;
  case "E7":
      $timezone = "Pacific/Auckland";
      break;
  case "E8":
      $timezone = "Pacific/Auckland";
      break;
  case "E9":
      $timezone = "Pacific/Auckland";
      break;
  case "F1":
      $timezone = "Pacific/Auckland";
      break;
  case "F2":
      $timezone = "Pacific/Auckland";
      break;
  case "F3":
      $timezone = "Pacific/Auckland";
      break;
  case "F4":
      $timezone = "Pacific/Auckland";
      break;
  case "F5":
      $timezone = "Pacific/Auckland";
      break;
  case "F7":
      $timezone = "Pacific/Chatham";
      break;
  case "F8":
      $timezone = "Pacific/Auckland";
      break;
  case "F9":
      $timezone = "Pacific/Auckland";
      break;
  case "G1":
      $timezone = "Pacific/Auckland";
      break;
  case "G2":
      $timezone = "Pacific/Auckland";
      break;
  case "G3":
      $timezone = "Pacific/Auckland";
      break;
  } 
  break;
case "PT":
    switch ($region) { 
  case "02":
      $timezone = "Europe/Lisbon";
      break;
  case "03":
      $timezone = "Europe/Lisbon";
      break;
  case "04":
      $timezone = "Europe/Lisbon";
      break;
  case "05":
      $timezone = "Europe/Lisbon";
      break;
  case "06":
      $timezone = "Europe/Lisbon";
      break;
  case "07":
      $timezone = "Europe/Lisbon";
      break;
  case "08":
      $timezone = "Europe/Lisbon";
      break;
  case "09":
      $timezone = "Europe/Lisbon";
      break;
  case "10":
      $timezone = "Atlantic/Madeira";
      break;
  case "11":
      $timezone = "Europe/Lisbon";
      break;
  case "13":
      $timezone = "Europe/Lisbon";
      break;
  case "14":
      $timezone = "Europe/Lisbon";
      break;
  case "16":
      $timezone = "Europe/Lisbon";
      break;
  case "17":
      $timezone = "Europe/Lisbon";
      break;
  case "18":
      $timezone = "Europe/Lisbon";
      break;
  case "19":
      $timezone = "Europe/Lisbon";
      break;
  case "20":
      $timezone = "Europe/Lisbon";
      break;
  case "21":
      $timezone = "Europe/Lisbon";
      break;
  case "22":
      $timezone = "Europe/Lisbon";
      break;
  } 
  break;
case "RU":
    switch ($region) { 
  case "01":
      $timezone = "Europe/Volgograd";
      break;
  case "02":
      $timezone = "Asia/Irkutsk";
      break;
  case "03":
      $timezone = "Asia/Novokuznetsk";
      break;
  case "04":
      $timezone = "Asia/Novosibirsk";
      break;
  case "05":
      $timezone = "Asia/Vladivostok";
      break;
  case "06":
      $timezone = "Europe/Moscow";
      break;
  case "07":
      $timezone = "Europe/Volgograd";
      break;
  case "08":
      $timezone = "Europe/Samara";
      break;
  case "09":
      $timezone = "Europe/Moscow";
      break;
  case "10":
      $timezone = "Europe/Moscow";
      break;
  case "11":
      $timezone = "Asia/Irkutsk";
      break;
  case "13":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "14":
      $timezone = "Asia/Irkutsk";
      break;
  case "15":
      $timezone = "Asia/Anadyr";
      break;
  case "16":
      $timezone = "Europe/Samara";
      break;
  case "17":
      $timezone = "Europe/Volgograd";
      break;
  case "18":
      $timezone = "Asia/Krasnoyarsk";
      break;
  case "20":
      $timezone = "Asia/Irkutsk";
      break;
  case "21":
      $timezone = "Europe/Moscow";
      break;
  case "22":
      $timezone = "Europe/Volgograd";
      break;
  case "23":
      $timezone = "Europe/Kaliningrad";
      break;
  case "24":
      $timezone = "Europe/Volgograd";
      break;
  case "25":
      $timezone = "Europe/Moscow";
      break;
  case "26":
      $timezone = "Asia/Kamchatka";
      break;
  case "27":
      $timezone = "Europe/Volgograd";
      break;
  case "28":
      $timezone = "Europe/Moscow";
      break;
  case "29":
      $timezone = "Asia/Novokuznetsk";
      break;
  case "30":
      $timezone = "Asia/Vladivostok";
      break;
  case "31":
      $timezone = "Asia/Krasnoyarsk";
      break;
  case "32":
      $timezone = "Asia/Omsk";
      break;
  case "33":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "34":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "35":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "36":
      $timezone = "Asia/Anadyr";
      break;
  case "37":
      $timezone = "Europe/Moscow";
      break;
  case "38":
      $timezone = "Europe/Volgograd";
      break;
  case "39":
      $timezone = "Asia/Krasnoyarsk";
      break;
  case "40":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "41":
      $timezone = "Europe/Moscow";
      break;
  case "42":
      $timezone = "Europe/Moscow";
      break;
  case "43":
      $timezone = "Europe/Moscow";
      break;
  case "44":
      $timezone = "Asia/Magadan";
      break;
  case "45":
      $timezone = "Europe/Samara";
      break;
  case "46":
      $timezone = "Europe/Samara";
      break;
  case "47":
      $timezone = "Europe/Moscow";
      break;
  case "48":
      $timezone = "Europe/Moscow";
      break;
  case "49":
      $timezone = "Europe/Moscow";
      break;
  case "50":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "51":
      $timezone = "Europe/Moscow";
      break;
  case "52":
      $timezone = "Europe/Moscow";
      break;
  case "53":
      $timezone = "Asia/Novosibirsk";
      break;
  case "54":
      $timezone = "Asia/Omsk";
      break;
  case "55":
      $timezone = "Europe/Samara";
      break;
  case "56":
      $timezone = "Europe/Moscow";
      break;
  case "57":
      $timezone = "Europe/Samara";
      break;
  case "58":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "59":
      $timezone = "Asia/Vladivostok";
      break;
  case "60":
      $timezone = "Europe/Kaliningrad";
      break;
  case "61":
      $timezone = "Europe/Volgograd";
      break;
  case "62":
      $timezone = "Europe/Moscow";
      break;
  case "63":
      $timezone = "Asia/Yakutsk";
      break;
  case "64":
      $timezone = "Asia/Sakhalin";
      break;
  case "65":
      $timezone = "Europe/Samara";
      break;
  case "66":
      $timezone = "Europe/Moscow";
      break;
  case "67":
      $timezone = "Europe/Samara";
      break;
  case "68":
      $timezone = "Europe/Volgograd";
      break;
  case "69":
      $timezone = "Europe/Moscow";
      break;
  case "70":
      $timezone = "Europe/Volgograd";
      break;
  case "71":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "72":
      $timezone = "Europe/Moscow";
      break;
  case "73":
      $timezone = "Europe/Samara";
      break;
  case "74":
      $timezone = "Asia/Krasnoyarsk";
      break;
  case "75":
      $timezone = "Asia/Novosibirsk";
      break;
  case "76":
      $timezone = "Europe/Moscow";
      break;
  case "77":
      $timezone = "Europe/Moscow";
      break;
  case "78":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "79":
      $timezone = "Asia/Irkutsk";
      break;
  case "80":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "81":
      $timezone = "Europe/Samara";
      break;
  case "82":
      $timezone = "Asia/Irkutsk";
      break;
  case "83":
      $timezone = "Europe/Moscow";
      break;
  case "84":
      $timezone = "Europe/Volgograd";
      break;
  case "85":
      $timezone = "Europe/Moscow";
      break;
  case "86":
      $timezone = "Europe/Moscow";
      break;
  case "87":
      $timezone = "Asia/Novosibirsk";
      break;
  case "88":
      $timezone = "Europe/Moscow";
      break;
  case "89":
      $timezone = "Asia/Vladivostok";
      break;
  } 
  break;
case "UA":
    switch ($region) { 
  case "01":
      $timezone = "Europe/Kiev";
      break;
  case "02":
      $timezone = "Europe/Kiev";
      break;
  case "03":
      $timezone = "Europe/Uzhgorod";
      break;
  case "04":
      $timezone = "Europe/Zaporozhye";
      break;
  case "05":
      $timezone = "Europe/Zaporozhye";
      break;
  case "06":
      $timezone = "Europe/Uzhgorod";
      break;
  case "07":
      $timezone = "Europe/Zaporozhye";
      break;
  case "08":
      $timezone = "Europe/Simferopol";
      break;
  case "09":
      $timezone = "Europe/Kiev";
      break;
  case "10":
      $timezone = "Europe/Zaporozhye";
      break;
  case "11":
      $timezone = "Europe/Simferopol";
      break;
  case "13":
      $timezone = "Europe/Kiev";
      break;
  case "14":
      $timezone = "Europe/Zaporozhye";
      break;
  case "15":
      $timezone = "Europe/Uzhgorod";
      break;
  case "16":
      $timezone = "Europe/Zaporozhye";
      break;
  case "17":
      $timezone = "Europe/Simferopol";
      break;
  case "18":
      $timezone = "Europe/Zaporozhye";
      break;
  case "19":
      $timezone = "Europe/Kiev";
      break;
  case "20":
      $timezone = "Europe/Simferopol";
      break;
  case "21":
      $timezone = "Europe/Kiev";
      break;
  case "22":
      $timezone = "Europe/Uzhgorod";
      break;
  case "23":
      $timezone = "Europe/Kiev";
      break;
  case "24":
      $timezone = "Europe/Uzhgorod";
      break;
  case "25":
      $timezone = "Europe/Uzhgorod";
      break;
  case "26":
      $timezone = "Europe/Zaporozhye";
      break;
  case "27":
      $timezone = "Europe/Kiev";
      break;
  } 
  break;
case "UZ":
    switch ($region) { 
  case "01":
      $timezone = "Asia/Tashkent";
      break;
  case "02":
      $timezone = "Asia/Samarkand";
      break;
  case "03":
      $timezone = "Asia/Tashkent";
      break;
  case "06":
      $timezone = "Asia/Tashkent";
      break;
  case "07":
      $timezone = "Asia/Samarkand";
      break;
  case "08":
      $timezone = "Asia/Samarkand";
      break;
  case "09":
      $timezone = "Asia/Samarkand";
      break;
  case "10":
      $timezone = "Asia/Samarkand";
      break;
  case "12":
      $timezone = "Asia/Samarkand";
      break;
  case "13":
      $timezone = "Asia/Tashkent";
      break;
  case "14":
      $timezone = "Asia/Tashkent";
      break;
  } 
  break;
case "TL":
    $timezone = "Asia/Dili";
    break;
case "PF":
    $timezone = "Pacific/Marquesas";
    break;
case "SX":
    $timezone = "America/Curacao";
    break;
case "BQ":
    $timezone = "America/Curacao";
    break;
case "CW":
    $timezone = "America/Curacao";
    break;
  } 
  return $timezone; 
}

function getLoginUserLatestContacts()
{
    $CI = get_instance();
    $arContacts = array();
    $arUser = $CI->session->userdata('login_user');

    if(!empty($arUser))
    {
        $query = $CI->db->select('*')
                ->from('tbl_contacts')
                ->where('isDeleted',0)
                ->where('isNew',1)
                ->where('idUser',(int)$arUser['id'])
                ->like('dtAddedOn', date("Y-m-d"))->order_by('dtAddedOn', 'DESC')->limit(10, 0)->get();
        //echo $CI->db->last_query();die;
        if($query->num_rows() > 0)
        {
            $arContacts = $query->result_array();
        }
    }
    return $arContacts;
}

function getLoginUserLatestMessages()
{
    $CI = get_instance();
    $arContacts = array();
    $arUser = $CI->session->userdata('login_user');

    if(!empty($arUser))
    {
        $query = $CI->db->select('*')
                ->from('tbl_notification_user_mapping m')
                ->join('tbl_notifications n', 'n.id = m.idNotification')
                ->where('n.isDeleted',0)
                ->where('m.isViewed',0)
                ->where('m.idUser',(int)$arUser['id'])
                ->order_by('n.dtAddedOn', 'DESC')
                ->get();
        //echo $CI->db->last_query();die;
        if($query->num_rows() > 0)
        {
            $arContacts = $query->result_array();
        }
    }
    return $arContacts;
}
?>