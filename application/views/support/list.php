<div class="row"> 
    <div class="col-sm-3 col-lg-2">
        <a href="<?php echo base_url();?>support" class="btn btn-danger btn-block btn-compose-email">Create New Ticket</a> 
        <ul class="nav nav-pills nav-stacked nav-email">
            <li <?php if(isset($szType) && $szType=='new'){ ?> class="active" <?php } ?>>
                <a href="<?php echo base_url();?>support/all/new">
                    <span class="badge pull-right"><?php echo isset($iTotalNewRecords)? $iTotalNewRecords : '0'; ?></span>
                    <i class="glyphicon glyphicon-inbox"></i> New
                </a>
            </li>
            <li <?php if(isset($szType) && $szType=='pending'){ ?> class="active" <?php } ?>>
                <a href="<?php echo base_url();?>support/all/pending">
                    <span class="badge pull-right"><?php echo isset($iTotalPendingRecords)? $iTotalPendingRecords : '0'; ?></span>
                    <i class="glyphicon glyphicon-star"></i> Pending
                </a>
            </li>
            <li <?php if(isset($szType) && $szType=='closed'){ ?> class="active" <?php } ?>>
                <a href="<?php echo base_url();?>support/all/closed">
                    <span class="badge pull-right"><?php echo isset($iTotalClosedRecords)? $iTotalClosedRecords : '0'; ?></span>
                    <i class="glyphicon glyphicon-send"></i> Closed
                </a>
            </li>
            <li <?php if(isset($szType) && $szType=='trash'){ ?> class="active" <?php } ?>>
                <a href="<?php echo base_url();?>support/all/trash">
                    <span class="badge pull-right"><?php echo isset($iTotalTrashedRecords)? $iTotalTrashedRecords : '0'; ?></span>
                    <i class="glyphicon glyphicon-trash"></i> Trash
                </a>
            </li>
        </ul> 
        <div class="mb30"></div> 
    </div><!-- col-sm-3 --> 
    <div class="col-sm-9 col-lg-10"> 
        <div class="panel panel-default">
            <div class="panel-body"> 
                <div class="pull-right">
                    <div class="btn-group mr10">
                        <?php if(isset($szType) && $szType!='new'){ ?>
                            <button class="btn btn-sm btn-white tooltips" type="button" data-toggle="tooltip" title="Mark as New" onclick="updateStatus('new');"><i class="glyphicon glyphicon-hdd"></i></button>
                        <?php } if(isset($szType) && $szType!='closed'){ ?>
                            <button class="btn btn-sm btn-white tooltips" type="button" data-toggle="tooltip" title="Mark as Closed" onclick="updateStatus('closed');"><i class="glyphicon glyphicon-hdd"></i></button>
                        <?php } if(isset($szType) && $szType!='pending'){ ?>
                            <button class="btn btn-sm btn-white tooltips" type="button" data-toggle="tooltip" title="Mark as Pending" onclick="updateStatus('pending');"><i class="glyphicon glyphicon-exclamation-sign"></i></button>
                        <?php } if(isset($szType) && $szType!='trash'){ ?>
                            <button class="btn btn-sm btn-white tooltips" type="button" data-toggle="tooltip" title="Delete" onclick="updateStatus('trash');"><i class="glyphicon glyphicon-trash"></i></button>
                        <?php } ?>
                    </div>
                </div><!-- pull-right -->  
                <div class="table-responsive">
                    <h5 class="subtitle mb5">INBOX - <?php echo strtoupper($szType); ?></h5>
                    <form method="post" id="support_ticket_form" name="support_ticket_form">
                        <table class="table table-email table-tickets dt-dynamic" id="customer_data_listing" data-url="<?php echo base_url();?>support/all/<?php echo $szType; ?>" data-func="Get_Tickets">
                            <thead style="display:none;">
                                <tr>
                                    <th></th>             	
                                    <th>Type</th>
                                    <th>Message</th>
                                    <th>Priority</th> 
                                    <th data-class="table-action"></th>
                                </tr>
                            </thead>
                        </table>
                    </form>
                </div><!-- table-responsive --> 
            </div><!-- panel-body -->
        </div><!-- panel --> 
    </div><!-- col-sm-9 --> 
</div><!-- row -->