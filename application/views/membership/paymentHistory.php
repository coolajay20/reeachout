	<div class="panel-body collapse in" id="invoices">

		<table class="subscriptions-table table table-striped table-hover mt10">

			<thead>

				<tr>

					<th>Customer</th>

					<th>Invoice</th>

					<th>Amount</th> 

					<th>Status</th>

					<th>Date Created</th> 

				</tr>

			</thead>



			<?php if(!empty($arTransactions)){ ?>

			<tbody>

			<?php  

				foreach( $arTransactions as $Transactions )

				{    

					?>

					<tr>

						<td><?php echo $Transactions['szFirstName']." ".$Transactions['szLastName']; ?><br><small class="colorgray"><?php echo (isset($Transactions['szEmail'])? "(".$Transactions['szEmail'].")" :''); ?></small></td>

						<td><?php echo $Transactions['szTransactionID']; ?></td> 

						<td><?php echo "$".number_format((float)$Transactions['fAmount']); ?></td> 

						<td><span class="label label-success"><?php echo $Transactions['szStatus']; ?></span></td>

						<td><?php echo convert_date(gmdate('Y-m-d H:i:s', $Transactions['tCreated']),2); ?></td> 

					</tr> 

				<?php  } ?> 

			</tbody> 

			<?php } else {?>

				<tfoot>

					<tr>

						<td colspan="8" class="aligncenter">

							<em>no transaction found</em>

						</td>

					</tr>

				</tfoot>

			<?php }?> 

		</table> 

	</div>