<?php
class LanguageLoader
{
    function initialize() {
		
        $CI =& get_instance();
		$CI->load->helper('language');
		
		if (isset($_GET['lang'])) {
			switch($_GET['lang']) {
				case "en":
					$setlang = "english";
					break;
				case "sp":
					$setlang = "spanish";
					break;
				case "pt":
					$setlang = "portuguese";
					break;
				case "default":
					$setlang = null;
					break;
				default:
					$setlang = "english";
			}
			$CI->session->set_userdata('site_lang', $setlang);
			$CI->lang->load('common',$setlang);
			redirect($_SERVER['HTTP_REFERER']);
		}
		
		#echo $CI->session->userdata('login_user')['language'];
		#echo $CI->session->userdata('site_lang');

		/*/set language based on user profile default selection
		switch ($ci->session->userdata('login_user')['language']) {
		case 0:
			$setlang = "english";
			break;
		case 1:
			$setlang = "spanish";
			break;
		}
		
		//set lang session based on profile default
		#$ci->session->set_userdata('site_lang', $setlang);
		
		#$siteLang = $ci->session->userdata('site_lang');
        $ci->lang->load('common',$setlang); */
    }
}
?>