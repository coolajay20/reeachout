<section id="content_wrapper">
    <?php require_once APPPATH . 'views/breadcrumb.php';?> 
    <!-- Begin: Content -->
    <section id="content" class="animated fadeIn">  
        <div class="panel panel-visible" id="spy3">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> Subscriptions
                </div>
             </div>
            <div class="panel-body pn">       
                <table class="dt-static subscriptions-table table table-striped table-hover mt10">
                    <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Amount</th>
                            <th>Details</th>
                            <th>Plan Name</th> 
                            <th>Status</th>
                            <th>Date Created</th>
                            <th></th>
                        </tr>
                    </thead>
                    
                    <?php if(!empty($arSubscriptions)){ ?>
                    <tbody>
                    <?php 
                    
                        foreach( $arSubscriptions as $subscription )
                        { 
                            if($subscription['iType']==3)
                            {
                                $details = "Billing Cycle: Lifetime";
                            }
                            else
                            {
                                if($subscription['iType']==1)
                                {
                                    $szInterval = "Monthly";
                                }
                                else if($subscription['iType']==2)
                                {
                                    $szInterval = "Yearly"; 
                                } 
                                $details = "Billing Cycle: ".$szInterval.PHP_EOL; 
                                if($subscription['tCurrentTermEnd'] > time())
                                {
                                    $details .= "Next Billing: ".date("d. M, Y", ($subscription['tCurrentTermEnd']+1)).PHP_EOL; 
                                }
                            } 

                            $szTitle = "";
                            if($subscription['tCurrentTermEnd'] > time()) 
                            {
                                if($subscription['szStatus'] == 'past_due')
                                {
                                    $szStatus = '<span class="label label-info">Past Due</span>';
                                }
                                else if($subscription['szStatus'] == 'unpaid')
                                {
                                    $szStatus = '<span class="label label-info">Unpaid</span>';
                                }
                                else if($subscription['tTrialEnd'] > time())
                                {
                                    $szTitle = 'On trial till ' . date('d. M, Y', $subscription['tTrialEnd']);
                                    $szStatus = '<span class="label label-warning">Trial</span>';
                                }
                                else
                                {
                                    $szStatus = '<span class="label label-success">Active</span>';
                                }
                            }
                            else
                            {
                                $szStatus = '<span class="label label-danger">Cancelled</span>';
                                $szTitle = 'Cancelled since ' . date('d. M, Y', $subscription['tCurrentTermEnd']);
                            }
                            ?>
                            <tr>
                                <td><?php echo $subscription['szFirstName']." ".$subscription['szLastName']; ?><br><small class="colorgray"><?php echo (isset($subscription['szEmail'])? "(".$subscription['szEmail'].")" :''); ?></small></td>
                                <td><?php echo "$".number_format((float)$subscription['fSubscriptionAmount']); ?></td>
                                <td> 
                                    <span class="displaynone"><?php echo $details; ?></span>
                                    <a href="#" data-toggle="tooltip" data-title="<?php echo $details; ?>"><i class="fa fa-list-ul colorprimary"></i></a>
                                </td>
                                <td>
                                    <span class="displaynone"><?php echo (isset($subscription['szPlanName'])?$subscription['szPlanName']:''); ?></span>
                                    <a href="#" data-toggle="tooltip" data-title="<?php echo (isset($subscription['szPlanName'])?$subscription['szPlanName']:''); ?>"><i class="fa fa-file-text-o colorprimary"></i></a>
                                </td> 
                                <td>
                                    <a href="#" data-toggle="tooltip" data-title="<?php echo (!empty($szTitle)?$szTitle:''); ?>" style="text-decoration: none;"><?php echo $szStatus; ?></a>
                                </td>
                                <td><?php echo convert_date(gmdate('Y-m-d H:i:s', $subscription['tCreated']),2); ?></td>
                                <td class="alignright"> 
                                    <a href="javascript:void(0);" data-user="0" data-name="<?php echo "{$subscription['szFirstName']} {$subscription['szLastName']}";?>" data-ended="<?php echo ($subscription['tCurrentTermEnd'] <= time() ? 1 : 0);?>" data-end="<?php echo date("d M, Y", ($subscription['tCurrentTermEnd']+1));?>" data-id="<?php echo $subscription['idSubscription'];?>" class="btn btn-sm btn-default" onclick="deleteSubscription(this);"><i class="fa fa-trash-o colordanger"></i> Delete</a>
                                </td>
                            </tr> 
                        <?php  } ?> 
                    </tbody> 
                    <?php } else {?>
                        <tfoot>
                            <tr>
                                <td colspan="8" class="aligncenter">
                                    <em>no subscriptions found</em>
                                </td>
                            </tr>
                        </tfoot>
                    <?php }?> 
                </table> 
            </div>
        </div>
    </section>
</section>