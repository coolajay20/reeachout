<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags_Model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getAllTags($idUser,$onlyActive=false)
    {
        $this->db->select('t.*, COUNT(ct.id) AS iNumContacts');
        $this->db->from('tbl_tags t');
        $this->db->join('tbl_contact_tags ct', 'ct.idTag = t.id AND ct.isDeleted = 0', 'left');
        $this->db->order_by('szTagName ', 'ASC');
        $this->db->where(array('t.isDeleted'=>0,'t.idUser'=>$idUser)); 
        if($onlyActive)
            $this->db->where(array('t.isActive'=>1));
        $this->db->group_by('t.id');
        $query = $this->db->get();
        return ($query->num_rows() > 0 ? $query->result_array() : array());
    }

    function getTagsByUniqueKey($szUniqueKey)
    {
        $arDetails = array();
        $szUniqueKey = trim($szUniqueKey);
        if($szUniqueKey != '')
        {
            $query = $this->db->get_where('tbl_tags', array('szUniqueKey'=>$szUniqueKey));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    }

    function getTagsByID($idTags)
    {
        $arDetails = array();
        $idTags = (int)$idTags;
        if($idTags > 0)
        {
            $query = $this->db->get_where('tbl_tags', array('id'=>$idTags));
            if($query->num_rows() > 0)
            {
                $rows = $query->result_array();
                $arDetails = $rows[0];
            }
        }
        return $arDetails;
    } 
    function deleteTags($arTags, $iDeletedBy)
    {
        $idTags = (int)$arTags['id'];
        $this->db->where(array('id'=>$idTags));
        $this->db->update('tbl_tags', array('isDeleted'=>1, 'iDeletedBy'=>$iDeletedBy, 'dtDeletedOn'=>date('Y-m-d H:i:s')));
        if($this->db->affected_rows())
        {
            // delete the related contact mapping
            $this->db->where('idTag', $idTags)->update('tbl_contact_tags', array('isDeleted'=>1,'dtDeletedOn'=>date('Y-m-d H:i:s')));
            
            // add user activity
            addUserActivity($idTags, 'tbl_tags', "Tag: {$arTags['szTagName']} deleted."); 
            return true;
        }
        return false;
    }

    function saveTagDetails($data, $idLoginUser)
    {		
        $data = sanitize_array_values($data);
        $idTags = (isset($data['id']) && (int)$data['id'] > 0 ? (int)$data['id'] : 0);

        if($idTags > 0)
        {
            // get old details
            $arOldData = $this->getTagsByID($idTags);
            
            // update Tags
            $data['iUpdatedBy'] = $idLoginUser;
            $data['dtUpdatedOn'] = date('Y-m-d H:i:s');
            $this->db->where(array('id'=>$idTags));
            $this->db->update('tbl_tags', $data);
            
            // add user activity
            addUserActivity($idTags, 'tbl_tags', "Tag {$data['szTagName']} updated.", $data, $arOldData);
        }
        else
        {
            // add new user
            $data['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_tags');
            $data['iAddedBy'] = $idLoginUser;
            $data['dtAddedOn'] = date('Y-m-d H:i:s');
            $query = $this->db->insert('tbl_tags', $data);
            $idTags = ($this->db->affected_rows() > 0 ? $this->db->insert_id() : 0);
            
            // add user activity
            addUserActivity($idTags, 'tbl_tags', "New tag {$data['szTagName']} added.", $data);
        }
        return $idTags;
    }         
	
    function isTagExists($idUser, $szTagName)
    {
        $query = $this->db->get_where('tbl_tags', array('szTagName'=>$szTagName, 'idUser'=>$idUser));
        return ($query->num_rows() > 0 ? $query->result_array() : array());
    }
}
?>