<!-- Start: Topbar -->
    <section id="content_wrapper">	
        <?php require_once APPPATH . 'views/breadcrumb.php';?>
      	
        <!-- Begin: Content -->
        <form method="post" action="<?php echo base_url();?>plans/<?php echo ($idPlan > 0 ? "edit/{$arPlanDetails['szUniqueKey']}" : "add");?>" id="profile-form" class="validate-form">	
            <section id="content" class="table-layout">
                <div class="tray tray-center">		     	
                    <div class="mw1000 center-block">    
                        <?php if($isDetailsSaved){?>
                        <div class="alert alert-success p5">
                            <i class="fa fa-check pr10"></i>
                            <strong> Congratulation!</strong>
                            User details has been saved successfully.
                        </div>
                        <?php } if($isFormError){?>
                        <div class="alert alert-danger p5">
                            <i class="fa fa-times pr10"></i>
                            <strong> Invalid Data!</strong>
                            Please fix the errors below and try again.
                        </div>
                        <?php }?>
	
                        <div class="panel panel-primary panel-border top mb35">
                                <div class="panel-heading">
                                    <span class="panel-title">
                                        <i class="fa fa-bookmark-o"></i>
                                        Plan Details
                                    </span>
                                </div>
                                <div class="panel-body bg-light" style="padding-bottom:0;">
                                    <div class="admin-form">
                                        <div class="section row">
                                            <label for="szPlanName" class="field-label col-md-2 text-right-not-md">Name:<span class="text-danger">*</span></label>
                                            <?php $szPlanNameError = form_error('arPlan[szPlanName]');?>
                                            <div class="col-md-10">
                                                <label for="szPlanName" class="field prepend-icon<?php if(!empty($szPlanNameError)){?> state-error<?php }?>">
                                                    <input type="text" name="arPlan[szPlanName]" data-toggle="slug" data-slug="szPlanID" id="szPlanName" class="gui-input required" placeholder="Name" value="<?php echo set_input_value('arPlan[szPlanName]',(isset($arPlanDetails['szPlanName']) ? $arPlanDetails['szPlanName'] : ''));?>">
                                                    <label for="szPlanName" class="field-icon">
                                                        <i class="fa fa-star"></i>
                                                    </label>
                                                </label>
                                                <?php if(!empty($szPlanNameError)){?><em class="state-error" for="szPlanName"><?php echo $szPlanNameError;?></em><?php }?>
                                            </div>
                                        </div>

                                        <div class="section row">
                                            <label for="szPlanID" class="field-label col-md-2 text-right-not-md">Plan Id:<span class="text-danger">*</span></label>
                                            <?php $szPlanIdError = form_error('arPlan[szPlanID]');?>
                                            <div class="col-md-10">
                                                <label for="szPlanID" class="field prepend-icon<?php if(!empty($szPlanIdError)){?> state-error<?php }?>">
                                                    <input type="text" name="arPlan[szPlanID]" id="szPlanID" class="gui-input required" placeholder="Plan Id" value="<?php echo set_input_value('arPlan[szPlanID]', (isset($arPlanDetails['szPlanID']) ? $arPlanDetails['szPlanID'] : ''));?>">
                                                    <label for="szPlanName" class="field-icon">
                                                        <i class="fa fa-star"></i>
                                                    </label>
                                                </label>
                                                <?php if(!empty($szPlanIdError)){?><em class="state-error" for="szPlanID"><?php echo $szPlanIdError;?></em><?php }?>
                                            </div>
                                        </div> 
                                        <div class="section row">
                                            <label for="fPlanAmount" class="field-label col-md-2 text-right-not-md">Monthly:<span class="text-danger">*</span></label>
                                            <?php $szAmountError = form_error('arPlan[fPlanAmount]');?>
                                            <div class="col-md-10">
                                                <label for="fPlanAmount" class="field prepend-icon<?php if(!empty($szAmountError)){?> state-error<?php }?>">
                                                    <input type="text" name="arPlan[fPlanAmount]" id="fServicePlanAmount" class="gui-input required number" placeholder="Amount (Monthly)" value="<?php echo set_input_value('arPlan[fPlanAmount]',(isset($arPlanDetails['fPlanAmount']) ? $arPlanDetails['fPlanAmount'] : ''));?>">
                                                    <label for="fPlanAmount" class="field-icon">
                                                        <i class="fa fa-dollar"></i>
                                                    </label>
                                                </label>
                                                <?php if(!empty($szAmountError)){?><em class="state-error" for="fPlanAmount"><?php echo $szAmountError;?></em><?php }?>
                                            </div>
                                        </div> 
                                        <div class="section row">
                                            <label for="fPlanAmountYearly" class="field-label col-md-2 text-right-not-md">Yearly:<span class="text-danger">*</span></label>
                                            <?php $fPlanAmountYearlyError = form_error('arPlan[fPlanAmountYearly]');?>
                                            <div class="col-md-10">
                                                <label for="fPlanAmountYearly" class="field prepend-icon<?php if(!empty($fPlanAmountYearlyError)){?> state-error<?php }?>">
                                                    <input type="text" name="arPlan[fPlanAmountYearly]" id="fServicePlanAmount" class="gui-input required number" placeholder="Amount (Yearly)" value="<?php echo set_input_value('arPlan[fPlanAmountYearly]',(isset($arPlanDetails['fPlanAmountYearly']) ? $arPlanDetails['fPlanAmountYearly'] : ''));?>">
                                                    <label for="fPlanAmountYearly" class="field-icon">
                                                        <i class="fa fa-dollar"></i>
                                                    </label>
                                                </label>
                                                <?php if(!empty($fPlanAmountYearlyError)){?><em class="state-error" for="fPlanAmount"><?php echo $fPlanAmountYearlyError;?></em><?php }?>
                                            </div>
                                        </div>  
                                        <div class="section row">
                                            <label for="fPlanAmountLifeTime" class="field-label col-md-2 text-right-not-md">Lifetime:<span class="text-danger">*</span></label>
                                            <?php $fPlanAmountLifeTimeError = form_error('arPlan[fPlanAmountLifeTime]');?>
                                            <div class="col-md-10">
                                                <label for="fPlanAmountLifeTime" class="field prepend-icon<?php if(!empty($fPlanAmountLifeTimeError)){?> state-error<?php }?>">
                                                    <input type="text" name="arPlan[fPlanAmountLifeTime]" id="fServicePlanAmount" class="gui-input required number" placeholder="Amount (Lifetime)" value="<?php echo set_input_value('arPlan[fPlanAmountLifeTime]',(isset($arPlanDetails['fPlanAmountLifeTime']) ? $arPlanDetails['fPlanAmountLifeTime'] : ''));?>">
                                                    <label for="fPlanAmountLifeTime" class="field-icon">
                                                        <i class="fa fa-dollar"></i>
                                                    </label>
                                                </label>
                                                <?php if(!empty($fPlanAmountLifeTimeError)){?><em class="state-error" for="fPlanAmount"><?php echo $fPlanAmountLifeTimeError;?></em><?php }?>
                                            </div>
                                        </div>
                                        <div class="section row">
                                            <label for="iTrialPeriod" class="field-label col-md-2 text-right-not-md">Trail (Days):<span class="text-danger">*</span></label>
                                            <?php $iTrialPeriodError = form_error('arPlan[iTrialPeriod]');?>
                                            <div class="col-md-10">
                                                <label for="iTrialPeriod" class="field prepend-icon<?php if(!empty($iTrialPeriodError)){?> state-error<?php }?>">
                                                    <input type="text" name="arPlan[iTrialPeriod]" id="iTrialPeriod" class="gui-input required number" placeholder="Days" value="<?php echo set_input_value('arPlan[iTrialPeriod]',(isset($arPlanDetails['iTrialPeriod']) ? $arPlanDetails['iTrialPeriod'] : ''));?>">
                                                    <label for="iTrialPeriod" class="field-icon">
                                                        <i class="fa fa-times"></i>
                                                    </label>
                                                </label>
                                                <?php if(!empty($iTrialPeriodError)){?><em class="state-error" for="iTrialPeriod"><?php echo $iTrialPeriodError;?></em><?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <aside class="tray tray-right tray350" data-tray-height="match" style="height: 535px;">
                        <div class="panel panel-system panel-border top mb35">
                            <div class="panel-heading">
                                <span class="panel-title">
                                    <i class="fa fa-tags"></i> <?php echo ($idPlan > 0 ? 'Save Plan Details' : 'Add New Plan');?>
                                </span>										
                            </div>
                            <div class="panel-body bg-light" style="padding-bottom:0;">
                                <div class="admin-form">
                                    <?php if(!empty($arFeatures)){?>		                    
                                    <div class="section mb10">
                                        <label class="field-label text-muted fs16 mb10">Permissions</label>
                                        <div class="option-group field" id="iSellOnline">
                                            <?php foreach ($arFeatures as $i=>$feature){?>
                                            <label class="option option-primary mb10">
                                                <input name="arPlanFeatures[<?php echo $i;?>]" value="<?php echo $feature['id'];?>" type="checkbox" <?php echo ((int)set_input_value("arPlanFeatures[$i]", 0) == $feature['id'] ? 'checked' : (in_array($feature['id'], $arPlanFeatures) ? 'checked' : ''));?>>
                                                <span class="checkbox"></span> <?php echo $feature['szFeatureName'];?>
                                            </label><br>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <?php }?>
                                    
                                    <div class="section">
                                        <label for="isActive" class="field-label text-muted fs16 mb10">Is Active?</label>
                                        <label for="isActive" class="field select">
                                            <select name="arPlan[isActive]" id="isActive" class="gui-input required" placeholder="Suspended">
                                                <option value="0" <?php echo (set_input_value('arPlan[isActive]', (isset($arPlanDetails['isActive']) ? $arPlanDetails['isActive'] : 1)) == 0 ? 'selected' : '');?>>No</option>
                                                <option value="1" <?php echo (set_input_value('arPlan[isActive]', (isset($arPlanDetails['isActive']) ? $arPlanDetails['isActive'] : 1)) == 1 ? 'selected' : '');?>>Yes</option>
                                            </select>							                    
                                            <label for="isActive" class="field-icon">
                                                <i class="arrow"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer text-right">
                                <div class="admin-form">
                                    <a href="<?php echo base_url();?>plans" class="button btn-default"> Cancel </a>
                                    <button type="submit" class="button btn-primary"> <?php echo ($idPlan > 0 ? 'Save' : 'Add');?> </button>
                                    <input type="hidden" name="p_func" value="Save Plan Details">	
                                    <input type="hidden" name="arPlan[id]" value="<?php echo $idPlan;?>">
                                    <input type="hidden" name="arPlan[szPlanInterval]" value="Monthly"> 
                                </div>			                    	
                            </div>
                        </div>
                    </aside>
                </section>
            </form>
	</section>