	<section id="content_wrapper">
            <?php require_once APPPATH . 'views/breadcrumb.php';?>

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">
                <?php if(isset($szSuccessMessage) && $szSuccessMessage != ''){?>
                <div class="alert alert-success p5"><?php echo $szSuccessMessage;?></div>
                <?php } if(isset($szErrorMessage) && $szErrorMessage != ''){?>
                <div class="alert alert-danger p5"><?php echo $szErrorMessage;?></div>
                <?php }?>

                <p class="text-right">
                    <a href="<?php echo base_url();?>permissions/add" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New Permission</a>
                </p>

                <div class="panel panel-visible" id="spy3">
                    <div class="panel-heading">
                        <div class="panel-title hidden-xs">
                            <span class="glyphicon glyphicon-tasks"></span> Permissions
                        </div>
                     </div>
                    <div class="panel-body pn">                        
                        <table class="table table-responsive table-striped table-hover" id="example" cellspacing="0" width="100%">                    
                            <thead>
                                <tr>                 	
                                    <th>Name</th>
                                    <th>Added On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>                 	
                                    <th>Name</th>
                                    <th>Added On</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <?php if(!empty($arPermissions)){?>
                            <tbody>
                                <?php foreach($arPermissions as $permission){?>
                                <tr>
                                    <td id="permission-name"><?php echo $permission['szDisplayName'];?></td>                  
                                    <td><?php echo convert_date($permission['dtAddedOn'],3);?></td>
                                    <td class="action-links"> 
                                        <?php 
                                        $szActionLinks = '<ul>';
                                        $szActionLinks .= "<li><a href='".base_url()."permissions/edit/".$permission['szUniqueKey']."' class='text-info'><i class='fa fa-edit'></i> Edit</a></li>";
                                        if($permission['isRemovable'] == 1)
                                        {
                                            $szActionLinks .= "<li class='last'><a href='".base_url()."permissions/delete/".$permission['szUniqueKey']."' class='text-info btn-delete-record' data-type='Permission'><i class='fa fa-trash'></i> Delete</a></li>";
                                        }
                                        $szActionLinks .= '</ul>';
                                        ?>
                                        <button class="btn btn-sm btn-rounded" data-toggle="popover" data-placement="bottom" data-html="true" data-content="<?php echo $szActionLinks;?>"><i class="fa fa-ellipsis-h"></i></button>
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <?php }?>
                        </table>
                    </div>
                </div>
            </section>
  	</section>