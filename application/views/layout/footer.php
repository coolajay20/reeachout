<?php $globalStats = get_global_stats(); ?>
	
		</div><!-- contentpanel -->
  </div><!-- mainpanel -->
  
</section>

<!-- START New Theme Scripts -->
<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery-ui-1.10.3.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/modernizr.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/toggles.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/retina.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.cookies.js"></script>

<?php if(isset($isLoadDashboardScripts) && $isLoadDashboardScripts == true){?>
<script src="<?php echo base_url();?>assets/backend/js/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/flot/jquery.flot.spline.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/morris.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/raphael-2.1.0.min.js"></script>
<? } ?>

<script src="<?php echo base_url();?>assets/backend/js/custom.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.functions.js?<?php echo filemtime('./assets/js/custom.functions.js');?>"></script>

<?php if(isset($isLoadDashboardScripts) && $isLoadDashboardScripts == true){?>
<script>
jQuery(document).ready(function(){
	
	"use strict";
	
	function showTooltip(x, y, contents) {
		jQuery('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css( {
		  position: 'absolute',
		  display: 'none',
		  top: y + 5,
		  left: x + 5
		}).appendTo("body").fadeIn(200);
	}
    
   var uploads = [[0, <?php echo $globalStats['visitors']['total'] ?>], [1, 1], [2,1], [3, 1], [4, 1], [5, 1], [6, 1], [7, 1]];
	var downloads = [[0, <? echo $globalStats['subscribers']['total']; ?>], [1, 1], [2,1], [3, 1], [4, 1], [5, 1], [6, 1], [7, 1]];
	
	var plot = jQuery.plot(jQuery("#basicflot"),
		[{ data: uploads,
         label: "Visitors",
         color: "#1CAF9A"
        },
        { data: downloads,
          label: "Subscribers",
          color: "#428BCA"
        }
      ],
      {
			series: {
				lines: {
					show: false
				},
				splines: {
					show: true,
					tension: 0.1,
					lineWidth: 1,
					fill: 0.45
				},
				shadowSize: 0
			},
			points: {
				show: true
			},
		  legend: {
          position: 'nw'
        },
		  grid: {
          hoverable: true,
          clickable: true,
          borderColor: '#ddd',
          borderWidth: 1,
          labelMargin: 10,
          backgroundColor: '#fff'
        },
		  yaxis: {
          min: 0,
          max: 100,
          color: '#eee'
        },
        xaxis: {
          min: 0,
          max: 7,
          color: '#eee'
        }
		});
		
	 var previousPoint = null;
	 jQuery("#basicflot").bind("plothover", function (event, pos, item) {
      jQuery("#x").text(pos.x.toFixed(2));
      jQuery("#y").text(pos.y.toFixed(2));
			
		if(item) {
		  if (previousPoint != item.dataIndex) {
			 previousPoint = item.dataIndex;
						
			 jQuery("#tooltip").remove();
			 var x = item.datapoint[0].toFixed(2),
			 y = item.datapoint[1].toFixed(2);
	 			
			 showTooltip(item.pageX, item.pageY,
				  item.series.label + " of " + x + " = " + y);
		  }
			
		} else {
		  jQuery("#tooltip").remove();
		  previousPoint = null;            
		}
		
	 });
		
	 jQuery("#basicflot").bind("plotclick", function (event, pos, item) {
		if (item) {
		  plot.highlight(item.series, item.datapoint);
		}
	 });
    
    // Donut Chart
   var m1 = new Morris.Donut({
        element: 'donut-chart2',
        data: [
          {label: "Chrome", value: 30},
          {label: "Firefox", value: 20},
          {label: "Opera", value: 20},
          {label: "Safari", value: 20},
          {label: "Internet Explorer", value: 10}
        ],
        colors: ['#D9534F','#1CAF9A','#428BCA','#5BC0DE','#428BCA']
    });
    
    
   var m2 = new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'line-chart',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: [
            { y: '2006', a: 50, b: 0 },
            { y: '2007', a: 60,  b: 25 },
            { y: '2008', a: 45,  b: 30 },
            { y: '2009', a: 40,  b: 20 },
            { y: '2010', a: 50,  b: 35 },
            { y: '2011', a: 60,  b: 50 },
            { y: '2012', a: 65, b: 55 }
        ],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B'],
        gridTextColor: 'rgba(255,255,255,0.5)',
        lineColors: ['#fff', '#fdd2a4'],
        lineWidth: '2px',
        hideHover: 'always',
        smooth: false,
        grid: false
   });
	
	// Trigger Resize in Morris Chart
   var delay = (function() {
		var timer = 0;
		return function(callback, ms) {
			clearTimeout(timer);
			timer = setTimeout(callback, ms);
		};
    })();

   jQuery(window).resize(function() {
		delay(function() {
			m1.redraw();
			m2.redraw();
	}, 200);
   }).trigger('resize');
    
   jQuery('#sparkline').sparkline([4,3,3,1,4,3,2,2,3,10,9,6], {
		  type: 'bar', 
		  height:'30px',
        barColor: '#428BCA'
   });
	
    
    jQuery('#sparkline2').sparkline([9,8,8,6,9,10,6,5,6,3,4,2], {
		  type: 'bar', 
		  height:'30px',
        barColor: '#999'
    });
    
    // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
	 
	
	// Do not use the code below. It's for demo purposes only
	var c = jQuery.cookie('change-skin');
   if (jQuery('.panel-stat').length > 0 && c == 'dodgerblue') {
      jQuery('.panel-stat').each(function(){
         if ($(this).hasClass('panel-danger')) {
            $(this).removeClass('panel-danger').addClass('panel-warning');
         }
      });
   }
	
	if (jQuery('#basicflot').length > 0 && c == 'greyjoy') {
      plot.setData([{
			data: uploads,
			color: '#dd5702',
			label: 'Uploads',
			lines: {
				show: true,
				fill: true,
				lineWidth: 1
			},
			splines: {
				show: false
			}
		},
		{
			data: downloads,
			color: '#cc0000',
			label: 'Downloads',
			lines: {
				show: true,
				fill: true,
				lineWidth: 1
			},
			splines: {
				show: false
			}
		}]);
		plot.draw();
   }
    
});
</script>
<? } ?>

<?php if(isset($isLoadValidationScript) && $isLoadValidationScript == true){?>
<!-- jQuery Validate Plugin-->
<script src="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

<!-- jQuery Validate Addon -->
<script src="<?php echo base_url();?>theme/assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>
<?php }?>

<?php if(!isset($isLoadBootBox)){?>
<!-- Bootbox Plugin -->
<script src="<?php echo base_url();?>theme/assets/js/utility/bootbox/bootbox.js"></script>  	
  	
<?php } if(isset($isLoadBootBox) && $isLoadBootBox == true){?>
<!-- Bootbox Plugin -->
<script src="<?php echo base_url();?>theme/assets/js/utility/bootbox/bootbox.js"></script>  

<?php } if(isset($isLoadCroppieScript) && $isLoadCroppieScript == true){?>
<!-- Croppie Plugin -->
<script src="<?php echo base_url();?>theme/vendor/plugins/croppie/croppie.js"></script>

<?php } if((isset($isLoadDTPickerScript) && $isLoadDTPickerScript == true)){?>
<!-- Time/Date Plugin Dependencies -->
<script src="<?php echo base_url();?>theme/vendor/plugins/globalize/globalize.min.js"></script>
<script src="<?php echo base_url();?>theme/vendor/plugins/moment/moment.min.js"></script>  	

<!-- DateTime Plugin -->
<script src="<?php echo base_url();?>theme/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>

<?php }if(isset($isLoadTagsTypeAhead) && $isLoadTagsTypeAhead == true){?>
<script src="<?php echo base_url();?>theme/vendor/plugins/typeahead/bootstrap-typeahead.min.js"></script>
<?php }?>


<!-- START (Load HTML Editor Scripts) -->
<?php if(isset($isLoadEditorScript) && $isLoadEditorScript == true){?>
<script src="<?php echo base_url();?>assets/backend/js/wysihtml5-0.3.0.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
    
    "use strict";
    
  // HTML5 WYSIWYG Editor
  jQuery('#wysiwyg').wysihtml5({color: true,html:true});
	
});
</script>
<?php } ?>
<!-- END (Load HTML Editor Scripts) -->

<!-- Initialization -->
<script type="text/javascript">
jQuery(document).ready(function() {

"use strict";

// Init Theme Core    
//Core.init();

// Init Demo JS    
//Demo.init();
});

<?php if(isset($isLoadDTPickerScript) && $isLoadDTPickerScript == true){?>
$('.datepicker').datepicker({
	dateFormat: 'mm/dd/yy',
	changeMonth: true,
	changeYear: true,
	yearRange: '1970:<?php date('Y');?>'
});
$('.timepicker').datetimepicker({
	pickDate: false
});
<?php }?>

// Init Base URL
var base_url = '<?php echo base_url();?>';
</script>

<?php if(isset($isLoadValidationScript) && $isLoadValidationScript == true){?>
<script src="<?php echo base_url();?>assets/js/custom.validate.js?<?php echo filemtime('./assets/js/custom.validate.js');?>"></script>

<!-- Custom Datatables -->
<?php } if(isset($isLoadDataTable) && $isLoadDataTable == true){?>
<script src="<?php echo base_url();?>assets/backend/js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.datatable.js"></script>

<!-- Croppie Scripts -->
<?php } if(isset($isLoadCroppieScript) && $isLoadCroppieScript == true){?>
<script src="<?php echo base_url();?>assets/js/cropper.function.js?<?php echo filemtime('./assets/js/cropper.function.js');?>"></script>

<?php } if(isset($isSubscriptionScriptRequired) && $isSubscriptionScriptRequired == true){?>
<script src="https://checkout.stripe.com/checkout.js"></script>    
<script src="<?php echo base_url();?>assets/js/custom.stripe.js?<?php echo filemtime('./assets/js/custom.stripe.js');?>"></script>

<?php } if(isset($isLoadUploadScript) && $isLoadUploadScript===true){ ?>
<script src="<?php echo base_url();?>assets/js/custom.fileupload.js?<?php echo filemtime('./assets/js/custom.fileupload.js');?>"></script>
<?php }?> 

</body>
</html>