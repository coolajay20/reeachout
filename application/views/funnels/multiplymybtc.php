<?php

// Main website configuration options

$site_config = array(
	'website_url' => 'http://'.$_SERVER[HTTP_HOST],
	'website_name' => 'Multiply My ɃTC',
	'default_title' => 'What Would Your Bitcoin be Worth Today?',
	'default_description' => 'Did you know? If you had purchased $100 of Bitcoin in 2011, it would be worth over 1 Million today! It\'s never too late to get involved, we predict the value will continue to rise and this is just the beginning.',
	'default_keywords' => '',
	'default_image' => 'fbshare-multiplybtc.jpg'
	);


// Faucethub.io registered addresses

$ref = array(
  'BTC' => '15L7a3M82QTNtP9zfdNQV24NZQti3iREsq',
  'LTC' => 'LQWaDEHGaKbBRR6XeYZGzMqcuD3nXxK7Ce',
  'DOGE' => 'D6vDHAQs2DCpmBwBDyYmNq22VkskYM2ofJ',
  'BLK' => 'BFbGJd337MMd9EFn6dukg2XMpvENgpMAoZ',
  'DASH' => 'XoWTPAtENCUVHjr2SKrxE54XXWgxCLSJEr',
  'PPC' => 'PFY3ZjQBhDXxP52EZ8EM4Zpmw5RcSkTywP',
  'XPM' => 'AJYnAy1EPU7HT3SUDCMyVdx2ebcMBCiaE3'
  );


//////////////////////////////////////////////////////////////////////////
// Important stuff, don't change unless you know what you are doing!!! //
////////////////////////////////////////////////////////////////////////

// Allow getting the version number for support reasons

#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);

if(isset($page_var)) {
  $page_info['title']  = $site_config['website_name'] . ' - ' . $page_var['title'];
  $page_info['description'] = $page_var['description'];
  $page_info['url'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $page_info['image'] = $site_config['website_url'] . '/assets/images/fbshare/' . $site_config['default_image'];
} else {
  $page_info['title']  = $site_config['default_title'];
  $page_info['description'] = $site_config['default_description'];
  $page_info['url'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $page_info['image'] = $site_config['website_url'] . '/assets/images/fbshare/' . $site_config['default_image'];
}

// Main website functions go in this class
Class Main {
  public static function faucetList()
	{

	}
}
$main = New Main();
$faucets = $main->faucetList();
  if(isset($_POST['month']) && isset($_POST['day']) && isset($_POST['year']) && !empty($_POST['month']) && !empty($_POST['day']) && !empty($_POST['year'])) {

  $date = htmlentities($_POST['year']) . '-' . htmlentities($_POST['month']) . '-' . htmlentities($_POST['day']);
  $timestamp = strtotime($date);
  $data = json_decode(@file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym=BTC&tsyms=USD&ts=' . $timestamp), 2);
  $current = json_decode(@file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym=BTC&tsyms=USD'), 2)['BTC']['USD'];
  }

if($data['BTC']['USD'] > 0) {
  $amt = htmlentities($_POST['amt']) / $data['BTC']['USD'];
} else {
  $amt = '0';
}
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $page_info['title']; ?></title>
    <meta name="description" content="<?php echo $page_info['description']; ?>">
    <!-- Opengraph Tags -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="<?php echo $page_info['title']; ?>" />
    <meta property="og:description" content="<?php echo $page_info['description']; ?>" />
    <meta property="og:url" content="<?php echo $page_info['url']; ?>" />
    <meta property="og:image" content="<?php echo $page_info['image']; ?>" />
    
    <!-- Images -->
    <link rel="icon" href="<?php echo $site_config['website_url']; ?>/assets/img/favicon.png" />
    <!-- External CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->

	<style>
	/*
	 * Globals
	 */

	 .h1, h1 {
		 line-height: 58px;
		 font-size: 33px;
	 }
	 h1.cover-heading {
		 margin-bottom: 50px;
		 text-align: center;
	 }

	/* Links */
	a,
	a:focus,
	a:hover {
	  color: #fff;
	}

	/* Custom default button */
	.btn-default,
	.btn-default:hover {
	  color: #333;
	  text-shadow: none; /* Prevent inheritance from `body` */
	  background-color: #FFC250;
	  border: 1px solid #fff;
	}


	/*
	 * Base structure
	 */

	html,
	body {
	  height: 100%;
	}
	body  {
		font-size: 1.6em;
		line-height: auto;
		color: #fff;
		text-shadow: 0 1px 3px rgba(0,0,0,.5);

		background: url(https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/H8WuRINimqur8ud/the-growth-of-the-virtual-currency-bitcoin-bitcoin-growth-chart-candle-chart-on-the-online-forex-monitor_syyjyb1z__F0000.png) #000 bottom center no-repeat fixed;
	}
	.lead {
		font-size: 35px;
		padding: 10px;
	}
	span.value {
		color: #f6a91a;
		font-weight: bold;
		font-size: 52px;
	}
	.btn-green, .btn-green:focus {
		background: #f6a91a;
		border: 1px solid #f6a91a;
		color: #000;
		font-size: 14px;
		padding: 15px 40px !important;
		font-weight: bold;
	}
	/* Extra markup and styles for table-esque vertical and horizontal centering */
	.site-wrapper {
	  display: table;
	  width: 100%;
	  height: 100%; /* For at least Firefox */
	  min-height: 100%;
	background-color: rgba(0,0,0,0.8);
	background-size: cover;
	width: 100%;
	}
	.site-wrapper-inner {
	  display: table-cell;
	  vertical-align: top;
	}
	.cover-container {
	  margin-right: auto;
	  margin-left: auto;
	}
	input.form-control.form-amt {
		width: 70px;
	}

	/* Padding for spacing */
	.inner {
	  padding: 30px;
	}


	/*
	 * Header
	 */
	.masthead-brand {
	  margin-top: 10px;
	  margin-bottom: 10px;
	}

	.masthead-nav > li {
	  display: inline-block;
	}
	.masthead-nav > li + li {
	  margin-left: 20px;
	}
	.masthead-nav > li > a {
	  padding-right: 0;
	  padding-left: 0;
	  font-size: 16px;
	  font-weight: bold;
	  color: #fff; /* IE8 proofing */
	  color: rgba(255,255,255,.75);
	  border-bottom: 2px solid transparent;
	}
	.masthead-nav > li > a:hover,
	.masthead-nav > li > a:focus {
	  background-color: transparent;
	  border-bottom-color: #a9a9a9;
	  border-bottom-color: rgba(255,255,255,.25);
	}
	.masthead-nav > .active > a,
	.masthead-nav > .active > a:hover,
	.masthead-nav > .active > a:focus {
	  color: #fff;
	  border-bottom-color: #fff;
	}

	@media (min-width: 768px) {
	  .masthead-brand {
		float: left;
	  }
	  .masthead-nav {
		float: right;
	  }
	}


	/*
	 * Cover
	 */

	.cover {
	  padding: 0 20px;
	}
	.cover .btn-lg {
		padding: 10px 20px;
		font-weight: bold;
	}
	.form-control {
		box-shadow: none!important;
		border: 0px!important;
	}

	/*
	 * Faucet Modal
	 */

	#faucetModal .modal-dialog {
		width: 100%;
		margin: 0px;
		overflow: hidden;
	}
	.modal-content.faucet-modal {
		height: 100%;
		border-radius: 0px;
		border: 0px;
	}
	.modal-content.faucet-modal {
		color: #111;
		text-shadow: none;
	}
	.modal-header {
		padding: 25px;
		border-bottom: 1px solid #212846;
		background: #212846;
		color: #fff;
	}
	.modal-dialog{
		overflow-y: initial !important
	}
	.modal-body{
		max-height: calc(100vh - 100px);
		overflow-y: auto;
	}
	.btn-faucet {
		background: #FD5B03;
		border: 1px solid #fd5b03;
		color: #fff!important;
	}
	th {
		color: #212846;
	}
	td {
		color: #111;
	}
	#faucetModal td a {
		color: #337ab7;
		text-decoration: none;
	}
	button.close {
		color: #fff;
		font-size: 28px;
	}
	button.close:hover {
		color: #fff;
		font-size: 28px;
		opacity: 1;
		text-shadow: 0px!important;
	}
	#faucetModal .container {
		max-width: 900px;
	}

	/* Responsive Video Frame Styles */
	.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; border: 3px #333 solid; }

	/*
	 * Footer
	 */

	.mastfoot {
	  color: #999; /* IE8 proofing */
	  color: rgba(255,255,255,.8);
	}


	/*
	 * Affix and center
	 */

	@media (min-width: 768px) {
	  /* Pull out the header and footer */
	  .masthead {
		position: fixed;
		top: 0;
	  }
	  .mastfoot {
		position: fixed;
		bottom: 0;
	  }
	  /* Start the vertical centering */
	  .site-wrapper-inner {
		vertical-align: middle;
	  }
	  /* Handle the widths */
	  .masthead,
	  .mastfoot,
	  .cover-container {
		width: 100%; /* Must be percentage or pixels for horizontal alignment */
	  }
	}

	@media (min-width: 992px) {
	  .masthead,
	  .mastfoot,
	  .cover-container {
		width: 1200px;
	  }
	}

	.gateway {
		border: #f6a91a 7px dashed;
	} .mask {background-color: #000;}

	div#playlist {
		height: 100%;
		background-color: rgba(255,255,255,.1);
		padding: 10px 10px 25px 10px;
		margin-left: -15px;
	}

	#playlist ul {list-style-type: none; padding: 0;}
		#playlist a {opacity: .4;}
	#playlist .active a {opacity: 1;}

	#playlist ul li.active:before {
	  content: '✓ ';
	}

	span.countdown {
		font-size: 11px;
	}

	img#logo {
		width: 100%;
		max-width: 700px;
		margin: 0 auto;
		padding: 15px;
	}



	/****** 10.6 PEOPLE DIRECTORY ******/

	#sponsorbox .media-left {text-align: left;}
	#sponsorbox .thumbnail {background: none; border-radius: 50%;}
		
	.media-left, .media>.pull-left {
    padding-right: 4px;
	}

	.letter-list {
		list-style: none;
		padding: 0;
		margin: 0;
		-moz-box-shadow: 0 3px 0 rgba(12,12,12,0.03);
		-webkit-box-shadow: 0 3px 0 rgba(12,12,12,0.03);
		box-shadow: 0 3px 0 rgba(12,12,12,0.03);
	}

	.letter-list li {
		border-left: 1px solid #eee;
		display: table-cell;
		width: 1%;
	}

	.letter-list li:first-child {
		border-left: 0;
	}

	.letter-list li a {
		display: block;
		padding: 8px 0;
		text-align: center;
		text-transform: uppercase;
		background: #f7f7f7;
		-moz-transition: all 0.2s ease-out 0s;
		-webkit-transition: all 0.2s ease-out 0s;
		transition: all 0.2s ease-out 0s;
	}

	.letter-list li:first-child a {
		-moz-border-radius: 3px 0 0 3px;
		-webkit-border-radius: 3px 0 0 3px;
		border-radius: 3px 0 0 3px;
	}

	.letter-list li:last-child a {
		-moz-border-radius: 0 3px 3px 0;
		-webkit-border-radius: 0 3px 3px 0;
		border-radius: 0 3px 3px 0;
	}

	.letter-list li a:hover {
		color: #fff;
		background: #428BCA;
		text-decoration: none;
		-moz-border-radius: 3px;
		-webkit-border-radius: 3px;
		border-radius: 3px;
	}

	.people-item {
		background: none;
		padding: 20px;
		font-size: 13px;
		-moz-border-radius: 3px;
		-webkit-border-radius: 3px;
		border-radius: 3px;
		-moz-box-shadow: 0 3px 0 rgba(12,12,12,0.03);
		-webkit-box-shadow: 0 3px 0 rgba(12,12,12,0.03);
		box-shadow: 0 3px 0 rgba(12,12,12,0.03);
		margin-bottom: 20px;
	}

	.people-item .media {
		padding: 0;
	}

	.people-item .media-object {
		-moz-border-radius: 3px;
		-webkit-border-radius: 3px;
		border-radius: 3px;
		margin-right: 10px;
		width: 110px;
		height: 110px;
	}

	.people-item .fa {
		margin-right: 5px;
	}

	.person-name {
		margin: 0 0 5px 0;
		color: #428BCA;
	}

	.people-item .social-list {
		margin-top: 10px;
		margin-bottom: 0;
		list-style: none;
		padding: 0;
	}

	.people-item .social-list::after {
		clear: both;
		display: block;
		content: '';
	}

	.people-item .social-list li {
		float: left;
		margin-right: 5px;
		margin-bottom: 5px;
	}

	.people-item .social-list li a {
		font-size: 16px;
		border: 1px solid #ddd;
		padding: 3px 5px;
		width: 30px;
		display: block;
		text-align: center;
		-moz-border-radius: 2px;
		-webkit-border-radius: 2px;
		border-radius: 2px;
		color: #666;
	}

	.people-item .social-list li a:hover {
		background-color: #eee;
		color: #333;
	}

	.people-item .social-list li a .fa {
		margin-right: 0;
	}

	/* Start Responsive Styling */
	@media (max-width: 500px) {
	  h1 {
		font-size: 1.1em;
		  line-height: 1.6em;
	  }

		h1.cover-heading {font-size: 1.1em; line-height: 1.6em;}

	select {width: 100%; margin-top: 10px; text-align: center;}
	input {margin: 10px 0;}

	.lead {
		font-size: 16px;
		text-align: center;
	}
	span.value {font-size: 24px;}

	span.value:nth-child(2) {
	font-size: 39px;
	line-height: 45px;
	}

	.masked {position: relative}
	input.form-control.form-amt {width: 70%; margin: 0 auto; font-size: 30px; text-align: center;}
	}
		#footer {padding: 25px 10px; font-size: 14px
			
	}
		
	section#cta {
		background-color: rgba(255,255,255,.12);
		padding: 20px 40px;
		border-radius: 25px;
		margin-bottom: 15px;
	}
		
		blockquote {font-size: 14px;}

	/*********************************************\
	************** END STYLING ***************\
	*************************************/
	</style>

</head>
<body>

<div class="site-wrapper">
	
	<img id="logo" class="img-responsive" src="<?php echo base_url();?>assets/images/marketing/cp-assets/new_btc_logo.png" />
	
    	<!-- CONTROL WHAT DISPLAYS IF NO DATA IS PASSED-->
		<?php if (!isset($data)) { ?>
		<?php
		$main = New Main();
		$faucets = $main->faucetList();
		$days = range(01, 31);
		$years = range(2011, date("Y"));
		$months = array(
		  '01' => 'Jan',
		  '02' => 'Feb',
		  '03' => 'March',
		  '04' => 'April',
		  '05' => 'May',
		  '06' => 'June',
		  '07' => 'July',
		  '08' => 'Aug',
		  '09' => 'Sep',
		  '10' => 'Oct',
		  '11' => 'Nov',
		  '12' => 'Dec'
		);
		?>
		<section id="header" class="container">
	  		<div class="row">
			  <form class="form-inline text-center" method="POST" action="<?php echo $site_config['website_url']; ?>">
				  <h1 class="cover-heading main">What if you purchased <input type="text" class="form-control input-lg form-amt" name="amt" value="100" /> USD in ɃTC on
					<select name="month" class="form-control input-lg">
					<?php
					foreach($months as $k => $v) {
					  echo '<option value="' . $k . '">' . $v . '</option>';
					}
					?>
					</select>
					<select name="day" class="form-control input-lg">
					<?php
					foreach($days as $day) {
					  (strlen($day) <= 1) ? $day = '0' . $day : $day = $day;
					  echo '<option value="' . $day . '">' . $day . '</option>';
					}
					?>
					</select>
					<select name="year" class="form-control input-lg">
					<?php
					foreach($years as $year) {
					  echo '<option value="' . $year . '">' . $year . '</option>';
					}
					?>
					</select>
					</h1>
					<button id="part1" type="submit" class="btn btn-xlg btn-default btn-green">Find Out How Wealthy You Would Have Been <i class="fa fa-arrow-right"></i></button>
				</form>
			</div>
		</section>
		<?php } ?>


		<!-- START IF DATA IS PASSED AND DATE SET IN FUTURE -->
		<?php if (isset($data) && new DateTime() < new DateTime($date)) { ?>
		<section id="header" class="container">
			<div class="row text-center">
				<h3 class="cover-heading">We can't predict the future price of ɃTC, unless you have a time machine we could use..</h3>
				<p class="text-center">
					<a href="<?php echo $site_config['website_url']; ?>" class="btn btn-lg btn-default btn-green" class="btn btn-info btn-lg">Try again</a>
				</p>
			</div>
		</section>
		<!-- ELSE, IF ɃTC HAS VALUE -->		
		<?php  } elseif($data['BTC']['USD'] > 0) { ?>
		<section id="header" class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 style="background-color: padding: 20px 25px; border-radius: 25px; margin: 0 10% 10px 10%;" class="cover-heading"><?php echo 'If you purchased <span class="value">$' . number_format(htmlentities($_POST['amt']), 2) . '</span> in ɃTC on ' . $date . ', Your ɃTC would now be worth <span class="value">$' . number_format($amt * $current); ?></span></h1>
				</div>
			</div>
		</section>
		
		<section id="video-group" class="container hidden">
			<div class="row">
				<div class="col-lg-9">
					<div class="embed-container"><iframe id="player1" src='https://player.vimeo.com/video/243528217?autoplay=0&api=1&player_id=player1' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
					</div>
					<div class="progress">
					  <div class="progress-bar" role="progressbar" aria-valuenow="100"
					  aria-valuemin="0" aria-valuemax="100" style="width:100%">
						<span class="progress-data">Press Play</span>
					  </div>
					</div>
				</div>
				<div id="playlist" class="col-lg-3 text-center">
					<img id="logo" class="img-responsive" src="/assets/images/new_usi_logo.png" />
					<div class="title"><h3 style="margin-top: 0;"><strong>Table of Contents:</strong></h3></div>
					<div class="links">
						<ul>
							<li id="0sec"><a class="playlink" onclick="seekTo(000);" href="javascript:void(0);">00:00 - Introduction</a></li>
							<li id="60sec"><a class="playlink" onclick="seekTo(074);" href="javascript:void(0);">01:01 - About USI Tech</a></li>
							<li id="277sec"><a class="playlink" onclick="seekTo(277);" href="javascript:void(0);">04:37 - What if?</a></li>
							<li id="467sec"><a class="playlink" onclick="seekTo(467);" href="javascript:void(0);">07:47 - The ɃTC Package</a></li>
							<li id="535sec"><a class="playlink" onclick="seekTo(535);" href="javascript:void(0);">08:55 - Making Your Purchase</a></li>
							<li id="594sec"><a class="playlink" onclick="seekTo(594);" href="javascript:void(0);">09:54 - The Compound Effect</a></li>
							<li id="931sec"><a class="playlink" onclick="seekTo(931);" href="javascript:void(0);">15:31 - Getting Started</a></li>
						</ul>
					</div>
					<div>
					<button id="control" class="btn btn-success">Play</button></a>
						<span class="countdown">Ready</span>
					</div>
					<div id="bonus" class="hidden" style="margin-top: 5px;">
						<a style="opacity: 1;" href="https://bitcoinlabs.io/usi/gst" class="btn btn-success btn-lg">Continue <i class="fa fa-chevron-right"></i></a>
					</div>
				</div>
			</div>
		</section>
		
		
		<?php  if(!empty($sponsor)) { ?>
		<section id="cta" class="container">
			<div id="gateway" class="row">
				<div class="col-lg-7">
					<h2><strong>Learn How to Acquire &amp; Multiply Ƀitcoin</strong></h2>
					
					<p>Recently a friend of mine shared with me a presentation that showed me how to multiply my Bitcoin purchases by 400% or more in less than a year with very little risk. I decided to watch it and as they explained how it works, it all began to make perfect sense.</p>
					
					<p>I know other people doing something very similar. However... until now, this was only for people that had some serious money to play with.</p>
					
					<ul style="list-style: none; font-size: 1.3em">
						<li><i class="fa fa-check" style="color: green; font-size: 1.6em;"></i> <strong>No...</strong> this is not the Forex market.</li>
						<li><i class="fa fa-check" style="color: green; font-size: 1.6em;"></i> <strong>No...</strong> this is not an Investment.</li>
						<li><i class="fa fa-check" style="color: green; font-size: 1.6em;"></i> <strong>No...</strong> this is not a Ponzi scheme.</li>
						<li><i class="fa fa-check" style="color: green; font-size: 1.6em;"></i> <strong>No... </strong>you are not required to recruit.</li>
						<li><i class="fa fa-check" style="color: green; font-size: 1.6em;"></i> <strong>No...</strong> you have not seen this before.</li>
					</ul>
				</div>
				
				<div class="col-lg-5 text-center">
					<h2 class="headline text-center"><strong><i class="fa fa-lock" style="color: #FFC250;"></i> Unlock Bonus #1</strong></h2>
					<h4>You can register using the form below to access an exclusive opportunity video presentation:</h4>
					<form name="apiform" id="apiform" method="POST" data-toggle="validator" role="form" class="form-horizontal">
						<div class="form-group">
							<div class="col-sm-5 col-xs-12">
								<input class="form-control input-lg" type="input" name="fname" placeholder="First Name" required>
							</div>
							<div class="col-sm-7 col-xs-12">
								<input class="form-control input-lg" type="input" name="lname" placeholder="Last Name" required>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-lg-12">
								<input class="form-control input-lg" type="email" name="email" placeholder="Email Address" style="" data-error="Bruh, that email address is invalid" required>
							</div>
						</div>
							
						<div class="form-group">
							<div class="col-lg-12">
								<input class="form-control input-lg" type="phone" name="phone" placeholder="Phone Number" style="" required>
							</div>
						</div>

						<input type="hidden" name="tag[]" value="multiplymybtc">
						<input type="hidden" name="campaign" value="OrQemsr9zLKP5KaGYOuIOST3hZ8zMD">
						<input type="hidden" name="userID" value="<?php echo $sponsor['username']; ?>">
						<button id="submit" type="submit" class="btn btn-default btn-lg text-center" style="margin-top: 15px; padding: 15px 40px"><strong>Accept Invitation</strong> <i class="fa fa-arrow-right"></i></button>
					</form>
				</div>
	  		</div>
		</section>
 		
  		<section class="container">
	  		<div class="row">
				<div class="col-lg-8">
					<blockquote>"After spending quite a bit of time going over everything and discussing the future plans for the company... I have to say that I am very impressed by everything I have seen so far. The owners are all highly successful, have a great deal of experience, and a well-thought-out plan."</blockquote>
				</div>
				<!-- START DISPLAY SPONSOR BOX -->
				<div id="sponsorbox" class="col-md-4 col-xs-offset-0">
					<div class="people-item">
					  <div class="media">
						<img alt="" src="http://bitcoinlabs.io/<?php echo $sponsor['avatar'] == '' ? "assets/images/profile.png" : "uploads/users/".$sponsor['avatar']; ?>" class="thumbnail media-object pull-left">
						<div class="media-body media-left">
						  <h4 class="person-name"><?php echo $sponsor['fname'].' '.$sponsor['lname']; ?></h4>
						  <div class="text-muted"><i class="fa fa-map-marker"></i> <?php echo $sponsor['city'].', '.$sponsor['country']; ?></div>
						  <div class="text-muted"><i class="fa fa-briefcase"></i> Business Partner</div>
						  <ul class="social-list">
							<li><a href="mailto:<?php echo $sponsor['email'] ?>" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email"><i class="fa fa-envelope-o"></i></a></li>

							<?php if (!empty($sponsor['fb'])) { ?>
								<li><a target="_blank" href="http://fb.com/<?php echo $sponsor['fb'] ?>" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
							<?php } if (!empty($sponsor['twitter'])) { ?>
								<li><a target="_blank" href="http://twitter.com/<?php echo $sponsor['twitter'] ?>" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
							<?php } if (!empty($sponsor['linkedin'])) { ?>
								<li><a target="_blank" href="http://linkedin.com/user/<?php echo $sponsor['linkedin'] ?>" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
							<?php } if (!empty($sponsor['skype'])) { ?>
								<li><a target="_blank" href="http://skype.com/<?php echo $sponsor['skype'] ?>" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Skype"><i class="fa fa-skype"></i></a></li>
							<?php } ?>
						  </ul>
						</div>
					  </div>
					</div>
				</div>
				<!-- END DISPLAY SPONSOR BOX -->
			</div>
		</section>
   	
   		<?php } ?>
   		<?php } ?>
    	
		<section id="footer" class="container">
			<div class="row">  
				<div class="col-lg-12 text-center">
					<p>&copy; 2017 <a href="<?php echo $site_config['website_url']; ?>"><?php echo $site_config['website_name']; ?></a>. All Rights reserved. <a href="/privacy/" target="_blank">Privacy</a> | <a href="/terms/" target="_blank">Terms</a></p>
				</div>
			</div>
		</section>
		
	</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://f.vimeocdn.com/js/froogaloop2.min.js"></script>

<script>
$(function() {
	var iframe = $('#player1')[0];
	var player = $f(iframe);
	var status = $('.status');
	var control = $('#control');
	var progress = $('.progress-data');
	$totalSec = 0;

	// When the player is ready, add listeners for pause, finish, and playProgress
	player.addEvent('ready', function() {
		status.text('ready');
		player.addEvent('pause', onPause);
        player.addEvent('finish', onFinish);
		player.addEvent('playProgress', onPlayProgress);		
	});

	// Call the API when a button is pressed
	control.bind('click', function() {
		player.api($(this).text().toLowerCase());
		$(this).blur();
	});
	
	function onPause() {
        status.text('paused');
		progress.text('Press Play');
		control.removeClass('btn-default').addClass('btn-success').text('Play');
    }

    function onFinish() {
        status.text('finished');
    }
		
	function onPlayProgress(data) {
		
		$dataSeconds = parseInt(data.seconds,0);
		
		if ($dataSeconds > $totalSec) {
			$totalSec = parseInt(data.seconds,0);
		}
		 
		var countdown = 931;
		var linkSelector = String('#' + parseInt(data.seconds,0) + 'sec');
		//alert(linkSelector);
		$(linkSelector).addClass('active');
		
		control.removeClass('btn-success').addClass('btn-default').text('Pause');
		progress.text(parseInt((data.percent * 100),0) + '%');
		$('.countdown').text('-' + secondsTimeSpanToHMS(parseInt(countdown - data.seconds),0));
		$('.progress-bar').css('width', (data.percent * 100) + '%');
		
		if (data.seconds > countdown) {
			$('#control').addClass('hidden')
			$('.countdown').addClass('hidden')
			$('#bonus').removeClass('hidden')
		}
	}
});
	
// Set lesson times (in seconds)
var seconds = 000;

// Variables for status messages
var main_status = jQuery('.main_status');

// Message displayed when link is playing
var playNotice = ' - <strong><i>Now playing</i></strong>';

// Function to hide all status messages
hidePlayNoticeAll = function() {
	//jQuery(".link_1_status").html("");
}

seekTo = function(value) {
	if ($totalSec > value) {
		player_1.api('seekTo', value);
	} else {
		
	}
}

// Load Vimeo API for the embedded video
var iframe_player = jQuery('#player1')[0];
var player_1 = $f(iframe_player);

// Option listeners for pause, finish, and playProgress
// Note: If you want to use this, you must define the functions or links wont work 
player_1.addEvent('ready', function() {
    player_1.addEvent('pause', onPause);
    player_1.addEvent('finish', onFinish);
    player_1.addEvent('playProgress', onPlayProgress);
}); 

// Functions for each listener event
function onPause(id) { // When the video is paused, do this.
    jQuery(".main_status").html('Paused');
}
function onFinish(id) { // When the video is finished, do this.
    jQuery(".main_status").html('Finished');
}

function onPlayProgress(data, id) { // While the video is playing, do this.
    jQuery(".main_status").html('Playing - ' + data.seconds + 's played'); // data.percent can also be used.
}

// Function to control what happens when each lesson link is clicked
function setupLinks() {
        
}
setupLinks();
	
function secondsTimeSpanToHMS(s) {
    var h = Math.floor(s/3600); //Get whole hours
    s -= h*3600;
    var m = Math.floor(s/60); //Get remaining minutes
    s -= m*60;
    return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
}
	
//form control
$(document).ready(function(){
	$(function(){
		$('#apiform').on('submit',function(event){
		   event.preventDefault();
		   event.stopPropagation();

			var values = {};
			$.each($('#apiform').serializeArray(), function(i, field) {
				
				values[field.name] = field.value;
				alert(field.name);
				alert(field.value);
			});
			
			var dataString = "fname=" + values['fname'] + "&lname=" + values['lname'] + "&email=" + values['email'] + "&phone=" + values['phone'] + "&campaign=" + values['campaign'] + "&tag[]=" + values['tag[]'] + "&userID=" + values['userID'];
			
			//var dataString = JSON.stringify(values);
			//alert(dataString);
			
			$.ajax({
				//contentType: "application/json",
				//dataType: "json",
				type: "POST",
				url: "/api/lead",
				data: dataString,
				beforeSend: function(data) {
					$("#submit").attr("disabled", "disabled");
					$("#submit").text("Processing your request..");
				},
			  	success: function(data) {
					console.log(data);
					data = JSON.parse(data);
					
					if (data["result"] == "success") {
						$('#cta').fadeOut(0);
						$('#video-group').removeClass('hidden');
					} else {
						alert(data['message']);
						$("#submit").text('Try Again');
					}
					
					//$('.cover-heading').removeAttr('style', 'background-color');
					//alert('success' + data);
					//player_1.api('play');
					//$("span.fname").html(values['fname']);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					//alert('error' + textStatus).delay(1000);
					$("#submit").text("Oops, please try again..");
				},
				complete: function(){
					$("#submit").removeAttr("disabled", "disabled");
				}
			});
		});
	});
});
	
$( "#part1" ).bind( "click", function() {
	//$("#part1").attr("disabled", "disabled");
	$("#part1").text("Please wait, we're calculating..");
});
	
</script>


</body>
</html>