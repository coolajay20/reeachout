<!-- Begin: Content -->

<form method="post" action="<?php echo base_url();?>campaigns/templates/<?php echo ($idTemplate > 0 ? "edit/{$arTemplateDetails['szUniqueKey']}" : "add");?>" id="formSirTrevor" class="validate-form">	

	<section id="content" class="table-layout">

		<div class="tray tray-center">

			<div class="mw1000 center-block">   

                            <?php if(!empty($arActiveCampaigns)){?>

				<?php if($isDetailsSaved){?>

				<div class="alert alert-success p5">

					<i class="fa fa-check pr10"></i>

					<strong> Congratulations!</strong>

					Template details has been saved successfully.

				</div>

				<?php } if($isFormError=== TRUE){?>

				<div class="alert alert-danger p5">

					<i class="fa fa-times pr10"></i>

					<strong> Invalid Data!</strong>

					Please fix the errors below and try again.

				</div>

				<?php } else if($isFormError != ''){?>

				<div class="alert alert-danger p5">

					<i class="fa fa-times pr10"></i>

					<strong> Sorry!</strong>

					<?php echo $isFormError;?>

				</div>

				<?php }?>



				<div class="panel panel-primary panel-border top mb15">

                                    <div class="panel-heading">

                                        <span class="panel-title">

                                            <i class="fa fa-user"></i>

                                            Basic Information

                                        </span>

                                    </div> 

                                    <div class="panel-body bg-light col-lg-7" style="padding-bottom:0;">

                                        <div class="admin-form"> 

                                            <div class="row">

                                                <label for="szName" class="field-label text-muted fs16 mb10 col-md-12">Name<span class="text-danger">*</span></label>

                                                <?php $szNameError = form_error('arTemplate[szName]');?>

                                                <label for="szName" class="col-md-6 field prepend-icon<?php if(!empty($szNameError)){?> state-error<?php }?>">

                                                    <div class="input-group mb15">

                                                      <span class="input-group-addon"><i class="fa fa-user"></i></span>

                                                      <input type="text" maxlength="255" name="arTemplate[szName]" id="szName" class="form-control" placeholder="Email Name" value="<?php echo set_input_value('arTemplate[szName]', (isset($arTemplateDetails['szName']) ? $arTemplateDetails['szName'] : ''));?>">

                                                    </div>

                                                </label>

                                                <?php if(!empty($szNameError)){?><em class="state-error" for="szName"><?php echo $szNameError;?></em><?php }?>								

                                            </div>



                                            <div class="row">

                                                <label for="szSubject" class="field-label text-muted fs16 mb10 col-md-12">Subject line<span class="text-danger">*</span></label>

                                                <?php $szSubjectError = form_error('arTemplate[szSubject]');?>

                                                <label for="szSubject" class="col-md-6 field prepend-icon<?php if(!empty($szSubjectError)){?> state-error<?php }?>">

                                                    <div class="input-group mb15">

                                                      <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>

                                                      <input type="text" maxlength="255" name="arTemplate[szSubject]" id="szSubject" class="form-control" placeholder="Subject line" value="<?php echo set_input_value('arTemplate[szSubject]', (isset($arTemplateDetails['szSubject']) ? $arTemplateDetails['szSubject'] : ''));?>">

                                                    </div>

                                                </label>

                                                <?php if(!empty($szSubjectError)){?><em class="state-error" for="szSubject"><?php echo $szSubjectError;?></em><?php }?>								

                                            </div>



                                            <div class="row">

                                                <label for="idCampaign" class="field-label text-muted fs16 mb10 col-md-12">Campaign<span class="text-danger">*</span></label>

                                                <?php $idCampaignError = form_error('arTemplate[idCampaign]');?>

                                                <label for="idCampaign" class="col-md-6 field prepend-icon<?php if(!empty($idCampaignError)){?> state-error<?php }?>">

                                                    <?php $idCampaign = set_input_value('arTemplate[idCampaign]', (isset($arTemplateDetails['idCampaign']) ? $arTemplateDetails['idCampaign'] : ''));?>

                                                    <select name="arTemplate[idCampaign]" id="idCampaign" class="form-control mb15" placeholder="Campaign">

                                                        <option value="">Select...</option>

                                                        <?php foreach($arActiveCampaigns as $campaign){?>

                                                        <option value="<?php echo $campaign['id'];?>" <?php echo ($campaign['id'] == $idCampaign ? 'selected' : '');?>><?php echo $campaign['szName'];?></option>

                                                        <?php }?>

                                                    </select>

                                                </label>

                                                <?php if(!empty($idCampaignError)){?><em class="state-error" for="idCampaign"><?php echo $idCampaignError;?></em><?php }?>								

                                            </div>



                                            <div class="row">

                                                <label for="iDays" class="field-label text-muted fs16 mb10 col-md-12">Send Email<span class="text-danger">*</span></label>

                                                <?php if(!isset($iDaysError)) $iDaysError = form_error('arTemplate[iDays]');?>

                                                <label for="iDays" class="field prepend-icon col-md-2<?php if(!empty($iDaysError)){?> state-error<?php }?>">

                                                    <div class="input-group mb15">

                                                        <a href="javascript:void(0);" class="qty-handler input-group-addon minus" data-target="#iDays"><i class="fa fa-minus"></i></a>

                                                        <input type="text" name="arTemplate[iDays]" id="iDays" class="form-control datepicker" placeholder="Number of days" value="<?php echo set_input_value('arTemplate[iDays]', (isset($arTemplateDetails['iDays']) ? $arTemplateDetails['iDays'] : 0));?>">

                                                        <a href="javascript:void(0);" class="qty-handler input-group-addon plus" data-target="#iDays"><i class="fa fa-plus"></i></a>

                                                    </div>

                                                </label>

                                                <?php if(!empty($iDaysError)){?><em class="state-error" for="iDays"><?php echo $iDaysError;?></em><?php }?>

                                                <div class="col-sm-2 pt10">Day(s)</div>

                                            </div> 

                                            <div class="row">

                                                <label for="szMessageContent" class="field-label text-muted fs16 col-md-12">Message Content<span class="text-danger">*</span></label>

                                                <?php $szMessageContentError = form_error('arTemplate[szMessageContent]');?>

                                                <div class="col-md-12">

                                                    <textarea id="wysiwyg" placeholder="Enter text here..." class="form-control" rows="10" name="arTemplate[szMessageContent]"><?php echo set_input_value('arTemplate[szMessageContent]', (isset($arTemplateDetails['szMessageContent']) ? $arTemplateDetails['szMessageContent'] : ''));?></textarea>									

                                                </div>

                                                <?php if(!empty($szMessageContentError)){?><span class="text-danger" for="szMessageContent"><?php echo $szMessageContentError;?></em><?php }?>

                                            </div> 

                                        </div>

										<div class="panel-footer text-center">

											<div class="admin-form">

												<a href="<?php echo base_url().'campaigns/templates/all' ?>" class="btn btn-default"> Cancel </a>

												<button type="submit" class="btn btn-primary"><?php echo ($idTemplate > 0 ? 'Save' : 'Add'); ?></button>

												<input type="hidden" name="p_func" value="Save Template Details">

											</div>			                    	

										</div>
                                    </div> 

                                    

                                    <div class="panel panel-primary panel-border top mb15 col-lg-5">

                                        <div class="panel-body bg-light" style="padding-bottom:0;">

                                            <div class="admin-form"> 

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        
                                                        <i class="fa fa-tags"></i><h2>Merge Tags</h2>

                                                        The following merge tags are available for you to customize your message based on Contacts, Users and Social Link attributes

                                                    </div> 

                                                </div>

                                                <br>

                                                <div class="row">

                                                    <div class="col-md-6">

                                                        <strong>CONTACT MERGE TAGS</strong><br><br>

                                                        <?php

                                                            if(!empty($arContactMergeTags))

                                                            {

                                                                foreach ($arContactMergeTags as $contactMergeTags)

                                                                {

                                                                    echo "<span>".$contactMergeTags."</span><br>";

                                                                }

                                                            }

                                                        ?>

                                                        <br><strong>SOCIAL MERGE TAGS</strong><br><br>

                                                        <?php

                                                            if(!empty($arSocialMergeTags))

                                                            {

                                                                foreach ($arSocialMergeTags as $socialMergeTags)

                                                                {

                                                                    echo "<span>".$socialMergeTags."</span><br>";

                                                                }

                                                            }

                                                        ?>

                                                        <br>

                                                    </div>

                                                    <div class="col-md-6">

                                                        <strong>USER MERGE TAGS</strong><br><br>

                                                        <?php

                                                            if(!empty($arUserMergeTags))

                                                            {

                                                                foreach ($arUserMergeTags as $userMergeTags)

                                                                {

                                                                    echo "<span>".$userMergeTags."</span><br>";

                                                                }

                                                            }

                                                        ?>

                                                        <br>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

				</div>

                            <?php } else {?>

                            <div class="alert alert-danger clearfix">Please first add at least one campaign to add/edit email. <a href="<?php echo base_url();?>campaigns/add" class="btn btn-primary pull-right">Add New Campaign</a></div>

                            <?php }?>

			</div>

		</div> 

	</section>

</form>