<?php if(isset($szSuccessMessage) && $szSuccessMessage != ''){?>
<div class="alert alert-success p5"><?php echo $szSuccessMessage;?></div>
<?php } if(isset($szErrorMessage) && $szErrorMessage != ''){?>
<div class="alert alert-danger p5"><?php echo $szErrorMessage;?></div>
<?php }?>

<p class="text-left">
	<a href="<?php echo base_url();?>features/add" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New Feature</a>
</p>

<div class="panel panel-visible" id="spy3">
	<div class="panel-body pn">                        
		<table class="table table-responsive table-striped table-hover" id="example" cellspacing="0" width="100%">                    
			<thead>
				<tr>                 	
					<th class="wd55">Name</th>
					<th class="wd15">Is Active?</th>
					<th class="wd15">Added On</th>
					<th class="wd15">Action</th>
				</tr>
			</thead>
			<?php if(!empty($arFeatures)){?>
			<tbody>
				<?php foreach($arFeatures as $Feature){?>
				<tr>
					<td id="feature-name"><?php echo $Feature['szFeatureName'];?></td>       
					<td><?php echo ($Feature['isActive'] == 1 ? '<span class="text-success"><i class="fa fa-check-circle"></i> Yes</span>' : '<span class="text-danger"><i class="fa fa-times-circle"></i> No</span>');?></td>
					<td><?php echo date("m/d/Y", strtotime($Feature['dtAddedOn']));?></td>
					<td class="action-links"> 
						<?php 
						$szActionLinks = '<ul>';
						$szActionLinks .= "<li><a href='".base_url()."features/edit/".$Feature['szUniqueKey']."' class='text-info'><i class='fa fa-edit'></i> Edit</a></li>";
						$szActionLinks .= "<li class='last'><a href='".base_url()."features/delete/".$Feature['szUniqueKey']."' class='text-info btn-delete-record' data-type='Feature'><i class='fa fa-trash'></i> Delete</a></li>";
						$szActionLinks .= '</ul>';
						?>
						<button class="btn btn-sm btn-rounded" data-toggle="popover" data-placement="bottom" data-html="true" data-content="<?php echo $szActionLinks;?>"><i class="fa fa-ellipsis-h"></i></button>
					</td>
				</tr>
				<?php }?>
			</tbody>
			<?php }?>
		</table>
	</div>
</div>