<?php if(isset($szSuccessMessage) && $szSuccessMessage != ''){?>
<div class="alert alert-success p5"><?php echo $szSuccessMessage;?></div>
<?php } if(isset($szErrorMessage) && $szErrorMessage != ''){?>
<div class="alert alert-danger p5"><?php echo $szErrorMessage;?></div>
<?php }?>

<div class="panel panel-visible" id="spy3">
	<div class="panel-heading">
		<div class="panel-title">
			<p class="text-left">
				<a href="<?php echo base_url();?>tags/add" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New Tag</a>
			</p>
		</div>
	 </div>
	 <table class="mb30 table table-striped">
		<thead>
		  <tr>
			<th>Name</th>
			<th>Contacts</th> 
		  </tr>
		</thead>
		<?php if(!empty($arTags)){?>
		<tbody class="table-striped">
		  <?php foreach($arTags as $tags){?>
		  <tr>
			<td><?php echo $tags['szTagName'];?></td>
			<td><?php echo number_format((float)$tags['iNumContacts']);?></td>
			<td class="table-action">
				<?php $szActionLinks = '<a href="'.base_url()."tags/edit/".$tags['szUniqueKey'].'"><i class="fa fa-pencil"></i></a>'; $szActionLinks .= '<a href="'.base_url()."tags/delete/".$tags['szUniqueKey'].'" class="delete-row"><i class="fa fa-trash-o"></i></a>'; echo $szActionLinks; ?>
			</td>
		  </tr>
		  <?php }?>
		</tbody>
		<?php }?>
	</table>
</div>