<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Api_Controller extends CI_Controller  

{ 
    function __construct() 
    { 
        parent::__construct();  
		
        /* Load form helper */  
        $this->load->helper('form');

        /* Load form validation library */  
        $this->load->library('form_validation'); 

        /* Changing error delimiters Globally */ 
        $this->form_validation->set_error_delimiters('', '');      
		
    }    


    public function lead()

    { 
        $authorizedHosts = array("www.usitech.io", "multiplymybtc.com", "bitcoinlabs.io");

        if (in_array(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST), $authorizedHosts)) {

            /* Load User Model */ 
            $this->load->model('User_Model');

            /* Load User Model */ 
            $this->load->model('Contact_Model');

            /* Load Campaign Model */ 
            $this->load->model('Campaign_Model');

            /* Load User Model */ 
            $this->load->model('Tags_Model');

			$formData = json_decode(json_encode($_POST),true);

            $szUserName = filter_var($formData['userID'], FILTER_SANITIZE_STRING);
            $szCampaignKey = filter_var($formData['campaign'], FILTER_SANITIZE_STRING);
            $arUserDetails = $this->User_Model->getUserByLogin($szUserName);

            if(!empty($arUserDetails) && !empty($formData['email']))

            {
                $arContactData = array();
                $arContactData['szFirstName'] = filter_var($formData['fname'], FILTER_SANITIZE_STRING);
                $arContactData['szLastName'] = filter_var($formData['lname'], FILTER_SANITIZE_STRING);
                $arContactData['szEmail'] = filter_var($formData['email'], FILTER_SANITIZE_STRING);
                $arContactData['szPersonalPhone'] = filter_var($formData['phone'], FILTER_SANITIZE_STRING);
                $arContactData['szRedirect'] = filter_var($formData['redirect'], FILTER_VALIDATE_URL);
                $arContactData['iAddedByApi'] = 1;
                $arContactData['idType'] = 1; //default value: None
                $arContactData['idUser'] = $arUserDetails['id'];
                $arContactData['szIPAddress'] = __REMOTE_ADDR__;
                $arContactData['szBrowser'] = __HTTP_USER_AGENT__;
                $arContactData['szReferrer'] = __HTTP_REFERER__;

                $idContact = $this->Contact_Model->saveUserDetails($arContactData,$arUserDetails['id']);  

                if($idContact>0)

                {

                    //tags should be a comma seprated string.
                    $arTags = $formData['tag'];
                    $arCampaignKey = $formData['campaign'];  

                    if(!empty($arCampaignKey) && is_array($arCampaignKey))

                    {
                        foreach ($arCampaignKey as $arCampaignKeys)

                        {
                            $arCampaigns = $this->Campaign_Model->getCampaignByUniqueKey($arCampaignKeys);  
                            if(!empty($arCampaigns) && is_array($arCampaigns))
                            {
                                $idCampaign = $arCampaigns['id'];
                                $this->Campaign_Model->addContactToCampaign($idContact,$idCampaign);
                            } 
                        }
                    }
                    else
                    { 
                        $arCampaigns = $this->Campaign_Model->getCampaignByUniqueKey($arCampaignKey);

                        if(!empty($arCampaigns) && is_array($arCampaigns))
                        {
                            $idCampaign = $arCampaigns['id']; 
                            $this->Campaign_Model->addContactToCampaign($idContact,$idCampaign);
                        } 
                    }

                    if(!empty($arTags))
                    {
                        foreach($arTags as $tag)
                        { 
                            $arTagDetails = $this->Tags_Model->isTagExists($arUserDetails['id'], $tag);
                            if(!empty($arTagDetails))
                            { 
                                $idTag = $this->Contact_Model->isContactTagAlreadyExists($idContact, $arTagDetails[0]['id']);
                                if(empty($idTag))
                                {
                                    $arOldTags[] = $this->Contact_Model->addContactTag($idContact, $arTagDetails[0]['id']);
                                }
                                else
                                {
                                    $arOldTags[] = $idTag;
                                } 
                            }
                            else
                            {
                                // save tag to miscellaneous category
                                $arTag['idUser'] = $arUserDetails['id'];
                                $arTag['szTagName'] = $tag;
                                $arTag['isActive'] = 1;
                                $idTag = $this->Tags_Model->saveTagDetails($arTag, $arUserDetails['id']);
                                $this->Contact_Model->addContactTag($idContact, $idTag);
                                $arOldTags[] = $idTag;
                            } 
                        }
                    }
					
                    /*/Send notification to upline
                    $arEmailData = array();
                    $arEmailData['szFirstName'] = $arUserDetails['szFirstName'];
                    $arEmailData['szEmail'] = $arUserDetails['szEmail'];
                    $arEmailData['szUserName'] = $arUserDetails['szUserName'];
					$arEmailData['szTest'] = 'testing';

                    //Sending welcome E-mail to customer
                    $this->Email_Model->sendSystemEmails($arEmailData,'LEAD_NOTIFICATION');

					//Add contact to mailchimp list if user has it enabled.
					/* if ($szUserName == 'usiglobal') {
						
						$this->load->library('MailChimp');

						$list_id="b126e82957";
						$result = $this->mailchimp->post(
							"lists/$list_id/members",[
							'email_address' => $arContactData['szEmail'],
							'merge_fields' => [
								'FNAME'=>$arContactData['szFirstName'],
								'LNAME'=>$arContactData['szLastName'],
								'PHONE'=>$arContactData['szPersonalPhone']],
							'status' => 'subscribed',]);
					} */

					//redirect user to url if set
                    if ($arContactData['szRedirect']) {
                       redirect( $arContactData['szRedirect'] , 'refresh');
                    }

                    $json = '{"result": "success","message": "Contact was successfully added to the system"}';

                } else {

                    $json = '{"result": "error","message": "Contact could not be added into the system"}';
                }

            } else {

				$json = '{"result": "error","message": "No customer found with matching userID"}';
            }   

        } else {
			
            //$this->load->view('403');
			//http_response_code(500);
			$json = '{"result": "error","message": "No authorization for this site!"}';
        }

		echo $json;
    } 

	/** Make an API request **/
    public function request($params = array(), $method = 'GET', $json_body = true, $token='') {

        # = array("www.usitech.io, usitech.io");
        #if (in_array($_SERVER['HTTP_HOST'], $authorizedHosts)) {
			//if (in_array(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST), $authorizedHosts)) {

			if ($_GET['token'] != '0a4fb6826dfcdf0279cec6870cb75be1') { exit(); }

			if (isset($_GET['uid'])) {

				/* Set Header and Process JSON request */
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');

				/* Load User Model */ 
				$this->load->model('User_Model');

				/* Get UserID from from parameters */
				$params['uid'] = $_GET['uid'];

				$userdata = $this->User_Model->validateUser($params['uid']);

				$data = array(
					'isactive' => $userdata["isActive"],
					'fname' => $userdata["szFirstName"],
					'lname' => $userdata["szLastName"],
					'phone' => $userdata["szPhone"],
					'avatar' => $userdata["szAvatarImage"],
					'usilink' => $userdata["szUSIUserName"],
					'fb' => $userdata["szFacebookLink"],
					'google' => $userdata["szGoogleLink"],
					'twitter' => $userdata["szTwitterID"],
					'linkedin' => $userdata["szLinkedInID"],
					'dribble' => $userdata["szDribbbleID"],
					'skype' => $userdata["szSkypeID"]
				);

				$json = json_encode($data, JSON_PRETTY_PRINT);

				if ($json === false) {
					// Avoid echo of empty string (which is invalid JSON), and
					// JSONify the error message instead:
					$json = json_encode(array("jsonError", json_last_error_msg()));
					if ($json === false) {
						// This should not happen, but we go all the way now:
						$json = '{"jsonError": "unknown"}';
					}
					// Set HTTP response status code to: 500 - Internal Server Error
					http_response_code(500);
				}
				echo $json;
			}
		#}
	}
}
?>