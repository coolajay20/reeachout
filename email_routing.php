#!/usr/bin/php -q
<?php
// Remove the query string
$_SERVER['QUERY_STRING'] = '';
// Include the codeigniter framework
define("REQUEST", "external");
ob_start();
require('index.php');
ob_end_clean();

$ci =& get_instance();
#var_dump($ci->session->userdata('login_user'));

/* Load Email Model */
$ci->load->model('Email_Model');

/* Load User Model */
$ci->load->model('User_Model');

/* Load Database */
$ci->load->database();

// fetch data from stdin
$data = file_get_contents("php://stdin");

// extract the body
// NOTE: a properly formatted email's first empty line defines the separation between the headers and the message body
list($data, $body) = explode("\n\n", $data, 2);

// explode on new line
$data = explode("\n", $data);

// define a variable map of known headers
$patterns = array(
  'Return-Path',
  'X-Original-To',
  'Delivered-To',
  'Received',
  'To',
  'Message-Id',
  'Date',
  'From',
  'Subject',
);

// define a variable to hold parsed headers
$headers = array();

// loop through data
foreach ($data as $data_line) {

  // for each line, assume a match does not exist yet
  $pattern_match_exists = false;

  // check for lines that start with white space
  // NOTE: if a line starts with a white space, it signifies a continuation of the previous header
  if ((substr($data_line,0,1)==' ' || substr($data_line,0,1)=="\t") && $last_match) {

    // append to last header
    $headers[$last_match][] = $data_line;
    continue;

  }

  // loop through patterns
  foreach ($patterns as $key => $pattern) {

    // create preg regex
    $preg_pattern = '/^' . $pattern .': (.*)$/';

    // execute preg
    preg_match($preg_pattern, $data_line, $matches);

    // check if preg matches exist
    if (count($matches)) {

      $headers[$pattern][] = $matches[1];
      $pattern_match_exists = true;
      $last_match = $pattern;

    }

  }

  // check if a pattern did not match for this line
  if (!$pattern_match_exists) {
    $headers['UNMATCHED'][] = $data_line;
  }

}

#$query = $ci->db->get_where('tbl_visitors',array('visitor_ip'=>$data['visitor_ip']));

$explodeTo = explode('@', $headers['To']['0']);

$explodeFrom = explode('<', $headers['From']['0']);

$arUserDetails = $ci->User_Model->getUserByLogin($explodeTo[0]);

$db_headers = array(
	'userID' => $arUserDetails['id'],
	'subject' => $headers['Subject']['0'],
	'from_email' => str_replace('>', '', $explodeFrom['1']),
	'from_name' => $explodeFrom['0'],
	'msg_body' => json_encode($body),
	'msg_data' => json_encode($headers)
);

$ci->db->insert('tbl_messages', $db_headers);
#$ci->db->where(array('visitor_ip'=>$data['visitor_ip']))->update('tbl_visitors', array('visitor_count'=>$data['visitor_count']));

//Sending welcome email to customer
$mailBasicDetails = array();
$mailBasicDetails['szEmailTo'] = $$db_headers['from_email'];
$mailBasicDetails['szEmailSubject'] = 'Relay Reply';
$mailBasicDetails['szEmailMessage'] = json_encode($body);
/* SEND EMAIL -> */ $ci->Email_Model->sendEmail($mailBasicDetails);
?>