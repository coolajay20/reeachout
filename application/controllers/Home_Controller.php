<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
class Home_Controller extends CI_Controller  
{ 
    function __construct() 
    { 
        parent::__construct(); 
        /* Load User Model */ 
        $this->load->model('User_Model');

        /* Load Email Model */ 
        $this->load->model('Email_Model');

        /* Load Membership Model */ 
        $this->load->model('Membership_Model');  
		
		/* Load Funnel Model */
        #$this->load->model('Funnel_Model');
		
        /* Load form helper */  
        $this->load->helper('form');
 
        /* Load form validation library */  
        $this->load->library('form_validation'); 

        /* Changing error delimiters Globally */ 
        $this->form_validation->set_error_delimiters('', '');
		
    } 

    public function index($data = NULL) 
    { 
			  
		  if (is_user_login()) { 
			  
            redirect(base_url() . 'dashboard', 'refresh');
			
       	 } else { 
			  
            $data = array();
            $data['szPageName'] = "Home"; 
            $data['szMetaTagTitle'] = WEBSITE_NAME_LONG; 
            $data['arLoginUser'] = $this->session->userdata('login_user');
            
            $this->load->view('layout/frontend/header', $data);
            $this->load->view('home');
            $this->load->view('layout/frontend/footer');  
        } 
    }
	
    /* OLD DASHBOARD (NEW DASHBOARD CODE BELOW)
	public function dashboard() 
    { 
        if(!is_user_login())  
        { 
            redirect(base_url().'login', 'refresh'); 
        }		 
        $data['szPageName'] = "Dashboard"; 
        $data['szMetaTagTitle'] = "User Dashboard"; 
        $data['arLoginUser'] = $this->session->userdata('login_user');
 
        if(hasPermission('manage.users')) 
        {  
            $iTotalUsers = $this->User_Model->getUsers(false,false,true);
            $iTotalActiveSubscriptions = $this->Membership_Model->getAllSubscriptions(true);

            $data['iTotalUsers'] = (int)$iTotalUsers; 
            $data['iTotalActiveSubscriptions'] = $iTotalActiveSubscriptions;
			
            $this->load->view('layout/header', $data);
            $this->load->view('dashboard');
            $this->load->view('layout/footer'); 
        } 
        else 
        { 
            $this->load->model('Membership_Model'); 
            $data['arSubscription'] = $this->Membership_Model->getUserSubscription($data['arLoginUser']['id']);
            $data['arTransactions'] = $this->Membership_Model->getUserTransactions($data['arLoginUser']['id']);

            $arUserDetails = $this->User_Model->getUserByID($data['arLoginUser']['id']);
            
            $data['isConfirmationLinkResend'] = false;
            $data['szNotificationPopupTitle'] = "Notification";
            $data['szNotificationPopupMessage'] = "Please confirm your email. If you don't receive the email yet then please use 'Resend' to receive email confirmation link again.";

            if(!empty($arUserDetails)) 
            { 
                $data['arUserDetails'] = $arUserDetails;
                $data['arUserDetails']['dtLastLogin'] = $this->User_Model->getUserLastLogin($arUserDetails['id']);

                $arCountry = array(); 
                if((int)$arUserDetails['idCountry'] > 0) 
                { 
                    $arCountry = $this->User_Model->getCountries($arUserDetails['idCountry']);      
                } 
                $data['arUserDetails']['szCountry'] = (!empty($arCountry) ? $arCountry[0]['szName'] : ''); 
            } 
            $this->load->view('layout/header', $data);
            $this->load->view('user_dashboard');
            $this->load->view('layout/footer');
        } 
    } */
	
	/* NEW DASHBOARD */
	public function dashboard()
    {
		
        if(!is_user_login()) { 
            redirect(base_url().'login', 'refresh'); 
        }
	
        $data['arLoginUser'] = $this->session->userdata('login_user');
		
        /* Load Contact Model */ 
         //$this->load->model('Contact_Model');

		$data['arSubscription'] = $this->Membership_Model->getUserSubscription($data['arLoginUser']['id']);
		$data['arTransactions'] = $this->Membership_Model->getUserTransactions($data['arLoginUser']['id']);

		$arUserDetails = $this->User_Model->getUserByID($data['arLoginUser']['id']);
		
		if ($arUserDetails['isConfirmed'] == 0 || $arUserDetails['szAvatarImage'] == '' || empty($data['arSubscription'])) {
			$data['isSetupComplete'] = false;
			
			$data['isSetupProgress'] = 25;
			if ($arUserDetails['isConfirmed'] != 0) { $data['isSetupProgress'] = $data['isSetupProgress'] + 25; }
			if ($arUserDetails['szAvatarImage'] != '') { $data['isSetupProgress'] = $data['isSetupProgress'] + 25; }
			if (!empty($data['arSubscription'])) { $data['isSetupProgress'] = $data['isSetupProgress'] + 25; }
			
		} else {
			$data['isSetupComplete'] = true;
		}
		
		$data['isConfirmationLinkResend'] = false;

		if(!empty($arUserDetails)) { 
			$data['arUserDetails'] = $arUserDetails;
			$data['arUserDetails']['dtLastLogin'] = $this->User_Model->getUserLastLogin($arUserDetails['id']);
			$arCountry = array(); 

			if((int)$arUserDetails['idCountry'] > 0) { 
				$arCountry = $this->User_Model->getCountries($arUserDetails['idCountry']);      
			}
			
			$data['arUserDetails']['szCountry'] = (!empty($arCountry) ? $arCountry[0]['szName'] : '');
			
			$data['arUserDetails']['arSponsorDetails']['fname'] = $this->User_Model->getUserByID($arUserDetails['szSponsor'])['szFirstName'];
			$data['arUserDetails']['arSponsorDetails']['lname'] = $this->User_Model->getUserByID($arUserDetails['szSponsor'])['szLastName'];
		}

		$data['isLoadDashboardScripts'] = true;
	
		$data['szFavIcon'] = "fa-dashboard";
        $data['szPageName'] = "Home";
        $data['szMetaTagTitle'] = "Dashboard";
		
		$this->load->view('layout/header', $data);
		$this->load->view('dashboard');
		$this->load->view('layout/footer');
	}
	/* END NEW DASHBOARD */
    
    public function signup()
    { 
        if(is_user_login())
        {
            redirect(base_url().'dashboard', 'refresh');
        } 
        $data = array();
        $data['idUser'] = 0;
        $showLogin = true;		
        if(sanitize_all_html_input(trim($this->input->post('p_func'))) == "Register")
        {   
            $this->form_validation->set_rules('arUser[szFirstName]', 'First name', 'trim|required');
            $this->form_validation->set_rules('arUser[szLastName]', 'Last name', 'trim|required');	
            $this->form_validation->set_rules('arUser[szUSIUserName]', 'USI Username', 'trim|required|alpha_numeric');	
            $this->form_validation->set_rules('arUser[szEmail]', 'Email address', "trim|required|valid_email|is_unique[tbl_users:szEmail:id:{$data['idUser']}]"); 
            $this->form_validation->set_rules('arUser[szUserName]', 'Username', "trim|required|alpha_numeric|is_unique[tbl_users:szUserName:id:{$data['idUser']}]"); 
            $this->form_validation->set_rules('arUser[szPassword]', 'Password', "trim|required|min_length[6]|max_length[25]");
          
            if ($this->form_validation->run() == true)
            { 
                $arUserDetails = $this->input->post('arUser', true); 

                $szOriginalPassword = $arUserDetails['szPassword'];
                if(trim($arUserDetails['szPassword']) != '') 
                    $arUserDetails['szPassword'] = encrypt(trim($arUserDetails['szPassword']));
                else
                    unset($arUserDetails['szPassword']);
                unset($arUserDetails['szPasswordRepeat']);
                
                // set other details
                if(isset($arUserDetails['id'])) unset($arUserDetails['id']);
                $arUserDetails['idRole'] = 2;
                $arUserDetails['isActive'] = 1;
                $arUserDetails['isConfirmed'] = 0;
                $arUserDetails['dtLastLinkOn'] = date('Y-m-d H:i:s');
                $arUserDetails['szUniqueKey'] = getUniqueKeyForDataRecord('tbl_users');
                $arUserDetails['szLastLinkKey'] = generateAlphaNumericString(20);
				
				$arUserDetails['szSponsor'] = trim($this->input->post('szSponsor'));
				#$arUserDetails['szSponsor'] = $this->User_Model->validateUser($arUserDetails['idSponsor'])['id'];
 
                if($data['idUser'] = $this->User_Model->saveUserDetails($arUserDetails,1))
                {
                    $arUserDetails = $this->User_Model->getUserByID($data['idUser']);  
                    $data['isDetailsSaved'] = true;
                    $data['arUserDetails'] = $arUserDetails;
                     
                    //Sending welcome email to customer
                    $arEmailData = array();
                    $arEmailData['szFirstName'] = $arUserDetails['szFirstName'];
                    $arEmailData['szEmail'] = $arUserDetails['szEmail'];
                    $arEmailData['szUserName'] = $arUserDetails['szUserName'];
                    $arEmailData['szPassword'] = $szOriginalPassword;
                    $arEmailData['szSiteName'] = WEBSITE_NAME; 
                    $arEmailData['szActivationLink'] = base_url()."activate/{$arUserDetails['szUniqueKey']}/{$arUserDetails['szLastLinkKey']}";

                    //Sending welcome E-mail to customer
                    $this->Email_Model->sendSystemEmails($arEmailData,'ACTIVATE_EMAIL');
                        
                    //Customer successfully registered into our system. Autologin user into our system
                    $this->User_Model->loginUserBySocialMedia($arUserDetails); 
                    
                    if(is_user_login())
                    {
                       redirect(base_url().'dashboard', 'refresh');
                    }
                }
            }
            else
            { 
                $data['isFormError'] = true;
            }
        } 
        
        if($data['idUser'] > 0)
        {
            $data['szMetaTagTitle'] = "Registration";
            $data['isRegistrationDone'] = true;
            $this->form_validation->clear_field_data();
            //$this->load->view('notification', $data);
            $data['arCountries'] = $this->User_Model->getCountries();
            $this->load->view('signup', $data);
        }
        else 
        { 
            $data['arCountries'] = $this->User_Model->getCountries();
            $this->load->view('signup', $data);
        }
    }

    function resendActivationLink($szUserKey='')
    {
        if(!is_user_login())
        {
            redirect(base_url().'login', 'refresh');
        }

        $data = array();
        $szUserKey = trim($szUserKey);		
        $arUserDetails = $this->User_Model->getUserByUniqueKey($szUserKey);
        if(!empty($arUserDetails))
        {
            if((int)$arUserDetails['isConfirmed'] == 0)
            {
                $dtLinkTime = strtotime("+24 hour" . $arUserDetails['dtLastLinkOn']); 
                $data['isConfirmationLinkResend'] = true;
                $szLinkKey = generateAlphaNumericString(20);

                // change confirmation status
                $this->db->where('szUniqueKey', $szUserKey)->update('tbl_users', array('dtLastLinkOn'=>date('Y-m-d H:i:s'),'szLastLinkKey'=>$szLinkKey));

                //Sending account activation link to customer
                $arEmailData = array();
                $arEmailData['szFirstName'] = $arUserDetails['szFirstName'];
                $arEmailData['szEmail'] = $arUserDetails['szEmail'];
                $arEmailData['szUserName'] = $arUserDetails['szUserName'];
                $arEmailData['szPassword'] = decrypt($arUserDetails['szPassword']);
                $arEmailData['szSiteName'] = WEBSITE_NAME; 
                $arEmailData['szActivationLink'] = base_url()."activate/{$arUserDetails['szUniqueKey']}/{$szLinkKey}";

                //Sending welcome E-mail to customer
                $this->Email_Model->sendSystemEmails($arEmailData,'ACTIVATE_EMAIL'); 
            }
            else
            {
                $data['isAlreadyConfirmed'] = true;
            }
        }
        else
        {
            $data['isNotValidUser'] = true;
        }

        if($data['isConfirmationLinkResend'])
        { 
            echo "SUCCESS||||";
            echo "<p class='alert-success'>We have sent you a new verification link. Please follow the instructions to verify your account. <em>This link will be valid only for next 24 hours.</em></p>"; 
        }
        else
        {
            echo "ERROR||||";
            echo "<p class='alert-warning'>There was a problem while processing your request.</p>";
        } 
    }
    
    function activate($szUserKey='', $szLinkKey='')
    {
        if(is_user_login())
        {
            //redirect(base_url().'dashboard', 'refresh');
        }

        $data = array();
        $szUserKey = trim($szUserKey);		
        $arUserDetails = $this->User_Model->getUserByUniqueKey($szUserKey);
        if(!empty($arUserDetails) && $arUserDetails['szLastLinkKey'] != '' && $arUserDetails['szLastLinkKey'] == $szLinkKey)
        {
            if((int)$arUserDetails['isConfirmed'] == 0)
            {
                $dtLinkTime = strtotime("+24 hour" . $arUserDetails['dtLastLinkOn']);
                if($dtLinkTime > time())
                {
                    $data['isEmailConfirmed'] = true;

                    // change confirmation status
                    $this->db->where('szUniqueKey', $szUserKey)->update('tbl_users', array('isConfirmed'=>1,'dtLastLinkOn'=>'0000-00-00 00:00:00', 'szLastLinkKey'=>''));
                    
                    //Customer successfully activated his account. Autologin user into our system
                    $this->User_Model->loginUserBySocialMedia($arUserDetails);                    
                    if(is_user_login())
                    {
						redirect(base_url().'dashboard', 'refresh');
                        //redirect(base_url().'membership/subscription', 'refresh');
                    } 
                    else
                    {
                        redirect(base_url().'dashboard', 'refresh');
                    }
                }
                else
                {
                    $data['szUserKey'] = $szUserKey;
                    $data['isConfirmLinkExpired'] = true;
                }
            }
            else
            {
                $data['isAlreadyConfirmed'] = true;
            }
        }
        else
        {
            $data['isNotValidUser'] = true;
        }

        $data['szMetaTagTitle'] = "Verify Email Address";
        $this->load->view('notification', $data);
    }
    
    public function signup_backup() 
    {  
        if(is_user_login()) 
        { 
            redirect(base_url().'dashboard', 'refresh'); 
        }  
        $data = array();

        $data['idUser'] = 0;

        $showLogin = true;		

        if(sanitize_all_html_input(trim($this->input->post('p_func'))) == "Register")

        {   

            $this->form_validation->set_rules('arUser[szFirstName]', 'First name', 'trim|required');

            $this->form_validation->set_rules('arUser[szLastName]', 'Last name', 'trim|required');	

            $this->form_validation->set_rules('arUser[szUSIUserName]', 'USI Username', 'trim|required|alpha_numeric');	

            $this->form_validation->set_rules('arUser[szEmail]', 'Email address', "trim|required|valid_email|is_unique[tbl_users:szEmail:id:{$data['idUser']}]"); 

            $this->form_validation->set_rules('arUser[szUserName]', 'Username', "trim|required|alpha_numeric|is_unique[tbl_users:szUserName:id:{$data['idUser']}]"); 

            $this->form_validation->set_rules('arUser[szPassword]', 'Password', "trim|required|min_length[6]|max_length[25]");

          

            if ($this->form_validation->run() == true)

            { 

                $arUserDetails = $this->input->post('arUser', true); 



                $arUserDetails['idRole'] = 2;

                $arUserDetails['isActive'] = 1;

                $szOriginalPassword = $arUserDetails['szPassword'];

                if(trim($arUserDetails['szPassword']) != '') 

                    $arUserDetails['szPassword'] = encrypt(trim($arUserDetails['szPassword']));

                else

                    unset($arUserDetails['szPassword']);

                unset($arUserDetails['szPasswordRepeat']);

 

                if($data['idUser'] = $this->User_Model->saveUserDetails($arUserDetails,1))

                {

                    $arUserDetails = $this->User_Model->getUserByID($data['idUser']);  

                    $data['isDetailsSaved'] = true;

                    $data['arUserDetails'] = $arUserDetails;

                     

                    //Sending welcome email to customer

                    $arEmailData = array();

                    $arEmailData['szFirstName'] = $arUserDetails['szFirstName'];

                    $arEmailData['szEmail'] = $arUserDetails['szEmail'];

                    $arEmailData['szPassword'] = $szOriginalPassword;

                    $arEmailData['szSiteName'] = WEBSITE_NAME; 

                    $arEmailData['szLoginPageUrl'] = base_url().'login';



                    //Sending welcome E-mail to customer

                    $this->Email_Model->sendSystemEmails($arEmailData,'WELCOM_EMAIL');

                        

                    //Customer successfully registered into our system. Autologin user into our system

                    $this->User_Model->loginUserBySocialMedia($arUserDetails); 

                    

                    if(is_user_login())

                    {

                        redirect(base_url().'membership/subscription', 'refresh');

                    } 

                    else

                    {

                        redirect(base_url().'dashboard', 'refresh');

                    }

                }

            }

            else

            { 

                $data['isFormError'] = true;

            }

        }  

        $data['arCountries'] = $this->User_Model->getCountries();

        if($showLogin)

        {

            $this->load->view('signup', $data);

        }

    }

    

    public function login() 
    {	 
        if(is_user_login()) 
        { 
            redirect(base_url().'dashboard', 'refresh'); 
        }  
        $data = array();
        $showLogin = true;
        if(sanitize_all_html_input(trim($this->input->post('p_func'))) == "Log In") 
        { 
            /* Set validation rule for fields in the form */ 
            $this->form_validation->set_rules('szEmail', 'Username or Email Address', 'required|valid_login');
            $this->form_validation->set_rules('szPassword', 'Password', 'required'); 

            if ($this->form_validation->run() == true) 
            {        	  
                $szEmail = sanitize_all_html_input(trim($this->input->post('szEmail'))); 
                $this->form_validation->set_rules("szPassword", "Password", "valid_password[{$szEmail}]"); 
                if ($this->form_validation->run() == TRUE) 
                { 
                    // set user login session 
                    $arUserDetails = $this->User_Model->getUserByLogin($szEmail); 
                    $remember = (int)$this->input->post('remember'); 
                    set_user_login($arUserDetails, $remember); 

                    // add login history 
                    addLoginHistory($arUserDetails); 
                    // go to dashboard 
                    $redirect_url = $this->session->userdata('redirect_url');

                    if(!empty($redirect_url)) 
                    { 
                        //$this->session->unset_userdata('redirect_url'); 
                        //redirect(base_url().substr($redirect_url, 1), 'refresh'); 
                        redirect(base_url().'dashboard', 'refresh'); 
                    } 
                    else 
                    { 
                        redirect(base_url().'dashboard', 'refresh'); 
                    } 
                } 
            } 
        } 
        if($showLogin) 
        { 
            $this->load->view('login', $data); 
        } 
    } 
	
    function logout() 
    { 
        user_logout(); 
        redirect(base_url() . 'login', 'refresh'); 
    }  
	
    function changePassword() 
    { 
        if(!is_user_login()) 
        { 
            redirect(base_url().'login', 'refresh'); 
        }  
        $data['passwordChanged'] = false; 
        $arLoginUser = $data['arLoginUser'] = $this->session->userdata('login_user'); 
        if(sanitize_all_html_input(trim($this->input->post('p_func'))) == "Change Password") 
        { 
            $this->form_validation->set_message('matches', '{field} doesn\'t match.'); 
            $this->form_validation->set_message('differs', '{field} is same as the old password.');  
            /* Set validation rule for fields in the form */  
            $this->form_validation->set_rules('arChange[szOldPassword]', 'Old password', "required|min_length[6]|max_length[25]|is_match[tbl_users:szPassword:szEmail:{$arLoginUser['email']}]");
            $this->form_validation->set_rules('arChange[szNewPassword]', 'New password', 'required|min_length[6]|max_length[25]|differs[arChange[szOldPassword]]');
            $this->form_validation->set_rules('arChange[szRepeatNewPassword]', 'Confirm password', 'required|min_length[6]|max_length[25]|matches[arChange[szRepeatNewPassword]]');
 
            if ($this->form_validation->run() == TRUE) 
            { 
                $data['passwordChanged'] = true; 
                // update in DB 
                $this->User_Model->updateUserPassword($arLoginUser, $this->input->post('arChange[szNewPassword]'));
            } 
        } 
        $data['szMetaTagTitle'] = $data['szPageName'] = "Change Your Password";
        $data['isLoadAdminForm'] = true; 
        $data['isLoadValidationScript'] = true;  
        
        $this->load->view('layout/header', $data); 
        $this->load->view('change_password.php'); 
        $this->load->view('layout/footer');
    }



    public function forgotPassword() 
    { 
        $data['isLinkSent'] = false; 
        $data['isPasswordLinkAlreadySent'] = false; 
        if(sanitize_all_html_input(trim($this->input->post('p_func'))) == "Forgot Password") 
        { 
            $this->form_validation->set_rules('szEmail', 'Your email address', 'required|valid_email|registered_email');
            if ($this->form_validation->run() == true) 
            { 
                $szEmail = sanitize_all_html_input(trim($this->input->post('szEmail', true))); 
                $arRegister = $this->User_Model->getUserByEmail($szEmail);  
                $dtLinkTime = strtotime("+24 hour" . $arRegister['dtLastLinkOn']); 
                if($dtLinkTime > time()) 
                { 
                    $data['isPasswordLinkAlreadySent'] = true;	 
                } 
                else 
                { 
                    $data['isLinkSent'] = true; 
                    $data['szEmail'] = $arRegister['szEmail']; 
                    // send confirmation link

                    $message = " 
                    Hello {$arRegister['szFirstName']},<br><br> 
                    <a href=\"" . base_url() . "reset-password/{$arRegister['szUniqueKey']}\"><b>Click Here to reset your password.</b></a><br>
                    Please remember that this link is valid only for 24 hours from the time it was sent to you.<br>

                    If you need assistance, please contact site admin.

                    ";

                    $subject = "USI Tech Marketing System reset your password"; 
                    $to = "{$arRegister['szFirstName']} {$arRegister['szLastName']} <{$arRegister['szEmail']}>";
                    $from = CUSTOMER_SUPPORT_EMAIL; 
                    sendEmail($to, $from, $subject, $message); 
                    // reset the link date time

                    $this->User_Model->resetConfirmationLinkTime($arRegister['szUniqueKey'], date("Y-m-d H:i:s"));
                } 
            } 
        }  
        if($data['isPasswordLinkAlreadySent']) 
        { 
            $data['szMetaTagTitle'] = "Admin Forgot Password"; 
            $this->load->view('notification', $data); 
        } 
        else 
        { 
            $this->load->view('forgot_password', $data); 
        } 
    } 

    function resetPassword($szUserKey='') 
    { 
        $data = array(); 
        $szUserKey = trim($szUserKey);	 
        $arUserDetails = $this->User_Model->getUserByUniqueKey($szUserKey); 

        if(!empty($arUserDetails)) 
        { 
            if((int)$arUserDetails['isActive'] == 1) 
            { 
                $dtLinkTime = strtotime("+24 hour" . $arUserDetails['dtLastLinkOn']); 
                if($dtLinkTime > time()) 
                { 
                    $data['passwordChanged'] = false; 
                    $this->form_validation->set_message('matches', '{field} doesn\'t match.'); 
                    $this->form_validation->set_message('differs', '{field} is same as the old password.');
                    /* Set validation rule for fields in the form */  
                    $this->form_validation->set_rules('arChange[szNewPassword]', 'New password', 'required|min_length[6]|max_length[25]');
                    $this->form_validation->set_rules('arChange[szRepeatNewPassword]', 'Confirm new password', 'required|matches[arChange[szRepeatNewPassword]]');
 
                    if ($this->form_validation->run() == TRUE) 
                    { 
                        $data['passwordChanged'] = true; 
                        // update in DB 
                        $arUser['id'] = $arUserDetails['id']; 
                        $arUser['name'] = "{$arUserDetails['szFirstName']} {$arUserDetails['szLastName']}";
                        $this->User_Model->updateUserPassword($arUser, $this->input->post('arChange[szNewPassword]', true));
 
                        // reset the link date time 
                        $this->User_Model->resetConfirmationLinkTime($szUserKey, "0000-00-00 00:00:00"); 
                    } 
                    $data['szEmail'] = $arUserDetails['szEmail']; 
                    $data['szUserKey'] = $szUserKey; 
                    $this->load->view('reset_password', $data); 
                } 
                else 
                { 
                    $data['szUserKey'] = $szUserKey; 
                    $data['isPasswordLinkExpired'] = true; 
                    $data['szMetaTagTitle'] = "Admin Reset Password"; 
                    $this->load->view('notification', $data); 
                } 
            } 
            else 
            { 
                $data['szUserKey'] = $szUserKey; 
                $data['isNotValidUser'] = true; 
                $data['szMetaTagTitle'] = "Reset Password"; 
                $this->load->view('notification', $data); 
            } 
        } 
        else 
        { 
            $data['isNotValidUser'] = true; 
            $data['szMetaTagTitle'] = "Reset Password"; 
            $this->load->view('notification', $data); 
        }			 
    } 

    public function upload() 
    { 
        $attachment = $this->input->post('attachment'); 
        $uploadedFile = $_FILES['attachment']['tmp_name']['file']; 

        $path = $_SERVER["DOCUMENT_ROOT"].'/uploads/cms';  
        $url = base_url().'uploads/cms'; 

        // create an image name 
        $fileName = $attachment['name'];  

        // upload the image 
        move_uploaded_file($uploadedFile, $path.'/'.$fileName);  
        $this->output->set_output( 
            json_encode( 
                array('file' =>  
                    array( 
                        'url' => $url . '/' . $fileName,  
                        'filename' => $fileName 
                    ) 
                ) 
            ), 
            200, 
            array('Content-Type' => 'application/json') 
        ); 
    } 
    function activity($szUser='', $szUserKey='') 
    { 
        if(!is_user_login()) 
        { 
            redirect(base_url().'login', 'refresh'); 
        } 
        else if(!hasPermission('users.activity')) 
        { 
            redirect(base_url().'dashboard', 'refresh'); 
        } 
        // get user details 
        $data['idUser'] = 0; 
        $data['arUserDetails'] = array(); 
        $szUserKey = trim($szUserKey); 
        if($szUserKey != '') 
        { 
            $arUserDetails = $this->User_Model->getUserByUniqueKey($szUserKey); 
            if(!empty($arUserDetails)) 
            { 
                $data['idUser'] = $arUserDetails['id']; 
                $data['arUserDetails'] = $arUserDetails; 
            } 
        } 

        if(trim($this->input->get('p_func')) == 'Get Activity') 
        {                  
            list($iLimit, $iOffset, $iDrawPage, $szKeyword, $arFilters, $szSortField, $szSortOrder) = parseDTRequest($this); 
            $iTotalRecords = $this->User_Model->getUserActivity($data['idUser'], $szKeyword, true); 
            $arUsers = $this->User_Model->getUserActivity($data['idUser'], $szKeyword, false, $iLimit, $iOffset, $szSortField, $szSortOrder); 
            $response = array("draw"=>$iDrawPage, "recordsTotal"=>$iTotalRecords, "recordsFiltered"=>$iTotalRecords, "data"=>'');
 
            if(!empty($arUsers)) 
            { 
                $i = 0; 
                foreach($arUsers as $user) 
                {			 
                    $response['data'][$i][0] = strtotime($user['dtCreatedOn']); 
                    $response['data'][$i][1] = "<a href=\"" . base_url() . "activity/user/{$user['szUniqueKey']}\">{$user['szFirstName']} {$user['szLastName']}</a>";

                    $response['data'][$i][2] = $user['szIPAddress']; 
                    $response['data'][$i][3] = $user['szActivity']; 
                    $response['data'][$i][4] = date("m/d/Y h:ia", strtotime($user['dtCreatedOn']));    
                    $response['data'][$i][5] = '<a href="#" data-toggle="popover" title="User Agent" data-content="'.$user['szUserAgent'].'" data-placement="left" class="btn btn-sm btn-rounded btn-primary"><i class="fa fa-info-circle"></i></button>'; 
                    $i++; 
                } 
            } 
            echo json_encode($response); 
        } 
        else 
        {      
            $data['szPageName'] = "Activity Log"; 
            $data['szMetaTagTitle'] = "Activity Log";	 
            $data['isLoadDataTable'] = true; 
            $data['arLoginUser'] = $this->session->userdata('login_user'); 
            $this->load->view('layout/header', $data); 
            $this->load->view('activity/list'); 
            $this->load->view('layout/footer'); 
        } 
    } 

    public function subscriptions()
    { 
        if(!is_user_login()) 
        { 
            redirect(base_url().'login', 'refresh'); 
        } 
        else if(!hasPermission('manage.settings'))
        { 
            redirect(base_url().'dashboard', 'refresh'); 
        } 
        else 
        {       
            $data['szPageName'] = "Membership (Admin)"; 
            $data['szMetaTagTitle'] = "Subscriptions"; 
            $data['arLoginUser'] = $this->session->userdata('login_user'); 
            $data['arSubscriptions'] = $this->Membership_Model->getAllSubscriptions();
            $data['isLoadDataTable'] = true;

            $this->load->view('layout/header', $data); 
            $this->load->view('setting/subscriptions'); 
            $this->load->view('layout/footer'); 
        } 
    }  
    function deleteSubscription() 
    { 
        // load subscription details 
        $idSubscription = (int)$this->input->post('p_id',true); 
        $arSubscriptionDetails = $this->Membership_Model->getSubscriptionByID($idSubscription); 

        if(!empty($arSubscriptionDetails) && (int)$arSubscriptionDetails['isDeleted'] == 0) 
        { 
            // get login user details 
            $arLoginUser = $this->session->userdata('login_user');  
            if(!is_user_login()) 
            { 
                echo "ERROR||||Your login session has been expired, click here to <a href=\"".base_url()."login\">login again</a>."; 
            } 
            else 
            { 
                if(hasPermission('manage.settings') || $arSubscriptionDetails['idUser'] == $arLoginUser['id']) 
                { 
                    $this->Membership_Model->deleteCustomerSubscription($arSubscriptionDetails); 
                    echo "SUCCESS||||Subscription deleted successfully"; 
                } 
                else 
                { 
                    echo "ERROR||||You don't have permisson to perform this action"; 
                } 
            } 
        } 
        else 
        { 
            echo "ERROR||||Subscription not exists."; 
        } 
    }

    

    public function invoices() 
    { 
        if(!is_user_login()) 
        { 
            redirect(base_url().'login', 'refresh'); 
        } 
        else if(!hasPermission('manage.settings')) 
        { 
            redirect(base_url().'dashboard', 'refresh'); 
        } 
        else 
        {		 
            $data['szFavIcon'] = "fa-usd"; 
            $data['szPageName'] = "Membership (Admin)"; 
            $data['szMetaTagTitle'] = "Invoices";
            $data['isLoadDataTable'] = true;
            $data['arLoginUser'] = $this->session->userdata('login_user');

            $data['arTransactions'] = $this->Membership_Model->getAllTransactions();  

            $this->load->view('layout/header', $data); 
            $this->load->view('setting/invoices'); 
            $this->load->view('layout/footer'); 
        } 
    }
    function testEmail()
    {
        $szUniqueKey= "Key";
        $szLastLinkKey = "link";
        //Sending welcome email to customer
        $arEmailData = array();
        $arEmailData['szFirstName'] = "Om";
        $arEmailData['szEmail'] = 'thomas@shopaddix.com';
        $arEmailData['szUserName'] = "admin";
        $arEmailData['szPassword'] = "blahblah";
        $arEmailData['szSiteName'] = WEBSITE_NAME; 
        $arEmailData['szActivationLink'] = base_url()."activate/{$szUniqueKey}/{$szLastLinkKey}";

        //Sending welcome E-mail to customer
        $this->Email_Model->sendSystemEmails($arEmailData,'ACTIVATE_EMAIL');
    }
    
    // Lead API Functionality
    public function leadAPI()
    {
        $authorizedHosts = array("app.usitech.io", "www.usitech.io");
        if (in_array(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST), $authorizedHosts)) { 
            $leadData = array(
                'fname' => filter_var($_POST['fname'], FILTER_SANITIZE_STRING),
                'lname' => filter_var($_POST['lname'], FILTER_SANITIZE_STRING),
                'email' => filter_var($_POST['email'], FILTER_VALIDATE_EMAIL),
                'phone' => filter_var($_POST['phone'], FILTER_SANITIZE_NUMBER_INT),
                'tag' => filter_var($_POST['tag'], FILTER_SANITIZE_STRING),
                'redirect' => filter_var($_POST['redirect'], FILTER_VALIDATE_URL),
                'userID' => filter_var($_POST['userID'], FILTER_SANITIZE_STRING)
            );

            //Display data submitted via webform (for testing only)
            echo json_encode($leadData,true);

            //Redirect user after successful form submission
            #header('Location: '.$leadData['redirect']); 
        } else {
            echo "No authorization for this site!";
        }
    }
    
    function switchLang($language)
    { 
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
        
        redirect($_SERVER['HTTP_REFERER']);
    } 
    
    function unsubscribe($szCampaignKey,$szContactKey) 
    {   
        /* Load Contact Model */ 
        $this->load->model('Contact_Model');
        $this->load->model('Campaign_Model');
        
        $data = array();  
        $data['szMetaTagTitle'] = CUSTOMER_SUPPORT_TITLE. " - Campaign Unsubscribe";  
        $data['isUnsubscribed'] = false;
            
        /* Fetching contact details based on unique key */
        $contactDataAry = $this->Contact_Model->getContactByUniqueKey($szContactKey); 
        /* Fetching Campaign details based on unique key */
        $campaignDataAry = $this->Campaign_Model->getCampaignByUniqueKey($szCampaignKey);
        
        $idContact = $contactDataAry['id'];
        $idCampaign = $campaignDataAry['id'];
        $isValidUnsubscribeLink = false;
        
        if($idContact>0 && $idCampaign>0)
        { 
            $isValidUnsubscribeLink = true;
            $arContactCampaignMapping = $this->Campaign_Model->getAllCampaignsForContact($idContact,$idCampaign); 
            if(count($arContactCampaignMapping)>0)
            {
                //no email
            } 
        }
        
        if($isValidUnsubscribeLink)
        { 
            if(sanitize_all_html_input(trim($this->input->post('p_func'))) == "Campaign_Unsubscribe") 
            {
                $arUnsubscribe = $this->input->post('arUnsubscribe', true);  
                $arUnsubscriberLogs = array();
                $arUnsubscriberLogs['idContact'] = $idContact;
                $arUnsubscriberLogs['idCampaign'] = $idCampaign;
                $arUnsubscriberLogs['szRemarks'] = $arUnsubscribe['szRemarks']; 
                $arUnsubscriberLogs['szRemoteIP'] = __REMOTE_ADDR__;
                $arUnsubscriberLogs['szReferer'] = __HTTP_REFERER__;
                $arUnsubscriberLogs['szUserAgent'] = __HTTP_USER_AGENT__;
                
                //Adding unsubscribe logs
                $this->Campaign_Model->addUnsubscribeLogger($arUnsubscriberLogs); 
                
                $arUnsubscribeCampaign = array();
                $arUnsubscribeCampaign['idContact'] = $idContact;
                $arUnsubscribeCampaign['idCampaign'] = $idCampaign;
                $arUnsubscribeCampaign['szStatus'] = __EMAIL_STATUS_UNSUBSCRIBE__;
                 
                //Updating campaign emails status from Pending > Unsubscribe
                $this->Campaign_Model->unsubscribeCampaigns($arUnsubscribeCampaign); 
                
                $data['isUnsubscribed'] = true;
            }
            $this->load->view('unsubscribe', $data); 
        }
        else
        {
            $this->load->view('404', $data); 
        }
    } 
    
    public function inbox($szMessageKey='') 
    { 
        $this->load->model('Notification_Model');
        if(!is_user_login())
        {
            redirect(base_url() . 'login', 'refresh');
        }
        /*else if(!hasPermission('manage.campaigns'))
        {
            redirect(base_url() . 'dashboard', 'refresh');
        }*/
        else if(trim($this->input->get('p_func')) == 'Get_Notifications')
        {
            $arLoginUser = $this->session->userdata('login_user');
            list($iLimit, $iOffset, $iDrawPage, $szKeyword, $arFilters, $szSortField, $szSortOrder) = parseDTRequest($this);
            $arFilters['where']['m.idUser'] = $arLoginUser['id'];
            $iTotalRecords = $this->Notification_Model->getAllNotifications($arFilters, $szKeyword, true);
            $arNotifications = $this->Notification_Model->getAllNotifications($arFilters, $szKeyword, false, $iLimit, $iOffset, $szSortField, $szSortOrder); 
            $response = array("draw"=>$iDrawPage, "recordsTotal"=>$iTotalRecords, "recordsFiltered"=>$iTotalRecords, "data"=>array()); 
            
            if(!empty($arNotifications))
            {
                $i = 0;
                foreach($arNotifications as $template)
                {					
                    $szActionLinks = '<ul>';
                    $szActionLinks .= "<li><a href='".base_url()."inbox/".$template['szUniqueKey']."' class='text-info'><i class='fa fa-eye'></i> View</a></li>";                    
                    $szActionLinks .= '</ul>'; 
                    
                    $response['data'][$i][0] = "<p class='fs16'><span class='text-info'>{$template['szTitle']}</p>"; 
                    $response['data'][$i][1] = convert_date($template['dtAddedOn'],2);
                    $response['data'][$i][2] = '<button class="btn btn-sm btn-rounded" data-toggle="popover" data-placement="bottom" data-html="true" data-content="'.$szActionLinks.'"><i class="fa fa-ellipsis-h"></i></button>';
                    $i++;				
                }
            }
            echo json_encode($response,true);
        }
        else
        {
            $data['szFavIcon'] = "fa-envelope";
            $data['szPageName'] = "All Messages";
            $data['szMetaTagTitle'] = "Inbox";		
            $data['isLoadDataTable'] = true;
            $data['isLoadBootBox'] = true;
            $data['arLoginUser'] = $this->session->userdata('login_user');
            
            //$data = array_merge($data, $arParams);
            
            $data['arMessageDetails'] = array();
            if($szMessageKey != '')
            {
                $data['arMessageDetails'] = $this->Notification_Model->getNotificationByUniqueKey($szMessageKey);
                if(!empty($data['arMessageDetails'])) 
                {
                    $data['szPageName'] = $data['arMessageDetails']['szTitle'];
                    $this->db->where('idNotification', $data['arMessageDetails']['id'])->where('idUser', $data['arLoginUser']['id'])->update('tbl_notification_user_mapping', array('isViewed'=>1));                
                }
            }
            
            // check contact view
            if(empty($data['arMessageDetails'])) 
            {
                $this->db->where('idUser', $data['arLoginUser']['id'])->update('tbl_notification_user_mapping', array('isViewed'=>1));
            }

            $this->load->view('layout/header', $data);
            $this->load->view('inbox/list');
            $this->load->view('layout/footer');
        }
    }
}
?>