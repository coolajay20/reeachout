<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Marketing_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /* Load Marketing Model */
        $this->load->model('Marketing_Model');

        /* Load User Model */ 
        $this->load->model('User_Model');

        /* Load form helper */ 
        #$this->load->helper('form');

        /* Load form validation library */ 
        #$this->load->library('form_validation');

        /* Changing error delimiters Globally */
        #$this->form_validation->set_error_delimiters('', '');     	     

    }

    public function index()
    {
		if (is_user_login()) {
			
			$data['szFavIcon'] = "fa-desktop";
			$data['szPageName'] = "Marketing";
			$data['szMetaTagTitle'] = "Marketing Tools";

			$this->load->view('layout/header', $data);
			$this->load->view('marketing/index', $data);
			$this->load->view('layout/footer');
			
		} else {
			
			redirect(base_url() . 'dashboard', 'refresh');
		}
    }

    public function funnels($arParams = array())
    { 
        if(!is_user_login()) {
			
            redirect(base_url() . 'login', 'refresh');
			
        } else {
			
			$data['arLoginUser'] = $this->session->userdata('login_user');
			$data['szFavIcon'] = "fa-desktop";
            $data['szPageName'] = "Marketing";
            $data['szMetaTagTitle'] = "Marketing Funnels";

            $data = array_merge($data, $arParams);

            $this->load->view('layout/header', $data);
            $this->load->view('marketing/funnels');
            $this->load->view('layout/footer');
        }
    }
	
    public function leads($arParams = array())
    { 
        if(!is_user_login()) {
			
            redirect(base_url() . 'login', 'refresh');
			
        } else {
			
			$data['szFavIcon'] = "fa-bullseye";
            $data['szPageName'] = "Marketing";
            $data['szMetaTagTitle'] = "Lead Packs";
			
            $data['arLoginUser'] = $this->session->userdata('login_user');

            $data = array_merge($data, $arParams);

            $this->load->view('layout/header', $data);
            $this->load->view('marketing/leads');
            $this->load->view('layout/footer');
        }
    }
	
    public function campaigns($arParams = array())
    { 
        if(!is_user_login()) {
			
            redirect(base_url() . 'login', 'refresh');
			
        } else {
			
			$data['szFavIcon'] = "fa-envelope";
            $data['szPageName'] = "Marketing";
            $data['szMetaTagTitle'] = "Campaigns";
			
            $data['arLoginUser'] = $this->session->userdata('login_user');

            $data = array_merge($data, $arParams);

            $this->load->view('layout/header', $data);
            $this->load->view('marketing/campaigns');
            $this->load->view('layout/footer');
        }
    }
}
?>