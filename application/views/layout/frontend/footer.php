    <!-- Footer -->
    <footer id="footer" class="py-5 bg-dark">
      <div class="container">
        <div class="logo text-center mb-2"><a href="/"><img src="<?php echo base_url();?>assets/images/logo_beta.png" alt=""></a></div>
        <p class="m-0 text-center text-white">&copy; 2017 Bitcoin Labs Digital Marketing System. All Rights Reserved</p>
        <div class="text-center"><small><a href="/terms/">Terms</a> - <a href="/privacy/">Privacy</a> - <a href="/login/">Member Login</a></small></div>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
	<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/popper/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    
	<script>
	/* Sticky CTA */
	$(window).scroll(function() {
		$("section#cta").removeClass("sticky");
		$("section#cta .container").addClass("py-3");
		
		if($(window).scrollTop() + $(window).height() < ($(document).height() - 300) ) {
			//you are at bottom
			$("section#cta").addClass("sticky");
			$("section#cta .container").removeClass("py-3");
		}
	});
		
	/* Disable Text Selection */
	(function($){

	  $.fn.ctrlCmd = function(key) {

		var allowDefault = true;

		if (!$.isArray(key)) {
		   key = [key];
		}

		return this.keydown(function(e) {
			for (var i = 0, l = key.length; i < l; i++) {
				if(e.keyCode === key[i].toUpperCase().charCodeAt(0) && e.metaKey) {
					allowDefault = false;
				}
			};
			return allowDefault;
		});
	};


	$.fn.disableSelection = function() {

		this.ctrlCmd(['a', 'c']);

		return this.attr('unselectable', 'on')
				   .css({'-moz-user-select':'-moz-none',
						 '-moz-user-select':'none',
						 '-o-user-select':'none',
						 '-khtml-user-select':'none',
						 '-webkit-user-select':'none',
						 '-ms-user-select':'none',
						 'user-select':'none'})
				   .bind('selectstart', false);
	};

	})(jQuery); $(':not(input,select,textarea)').disableSelection();
	</script>

  </body>
</html>