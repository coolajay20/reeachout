<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>szSiteName help desk!</title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
Dear szFirstName,
<br /><br />
Thank you for contacting us. We have successfully received your request. You can also track your support ticket by szSupportPageLink<br />
<br /><br />
Please copy and paste following url into your browser if the above link does not work.
<br />
<br />
szSupportPageUrl
<br />
<br />
Best regards, <br />
The szSiteName Team
</td>
</tr>
</table>
</div>
</body>
</html>