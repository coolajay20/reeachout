<?php
defined('BASEPATH') OR exit('No direct script access allowed');

#if (!isset($_GET['bypass'])) { exit('The system is currently in maintance mode.'); }
 
/* Fetch error message */  
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Done-For-You Marketing System to put you on the fast track to generating your own leads and nurturing them into potential clients and business partners.">
  <meta name="keywords" content="usi tech marketing system signup">
  <meta name="author" content=""> 
    <meta property="og:title" content="USI Tech Digital Marketing System" />
    <meta property="og:description" content="Done-For-You Marketing System to put you on the fast track to generating your own leads and nurturing them into potential clients and business partners." />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://app.usitech.io/signup" />
    <meta property="og:image" content="<?php echo base_url();?>assets/images/fbshare_signup.jpg" /> 
    <link rel="shortcut icon" href="images/favicon.png" type="image/png"> 
    <title>Unsubscribe | USI Tech Digital Marketing System</title> 
    <link href="<?php echo base_url();?>assets/backend/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body>

<!-- Custom Styles -->
<style>
.signuppanel form {
	color: #fff;
}
	
.signuppanel label.checkboxclass {
    display: inline;
	padding-left: 0;
	position: static;
}
	
.signuppanel checkbox {
	margin-left: 5px;
}
	
.signuppanel .help-block {
	font-style: italic;
	font-size: 12px;
	color: #ccc;
	line-height: 1.3em;
}
	
.signuppanel a {
	color: #bbb;
}

.signup-footer {
    border-top: none;
    margin-top: 0;
}
	
@media screen and (max-width: 640px) {
	.pull-right, .logopanel {
		display: block !important;
		float: none !important;
		text-align: center;
	}
	
	@media screen and (max-width: 640px)
	.signuppanel .form-control {
		margin-bottom: 0px;
	}
}
	
em.state-error {
    color: white;
    background-color: darkred;
    padding: 5px 8px 5px 4px;
	font-size: 10px;
}
</style>
<!-- END Custom Styles -->

<section> 
    <div class="signuppanel"> 
        <div class="row"> 
            <div class="col-md-12"> 
                <div class="signup-info">
                    <div class="logopanel">
                        <img class="mb10" src="<?php echo base_url();?>assets/images/logo_beta.png" alt=""> 
                    </div><!-- logopanel -->
                </div><!-- signup-info --> 
                
                    <?php if(isset($isUnsubscribed) && $isUnsubscribed){ ?>
                        <div class="alert alert-success" style="margin-top: 3%;"> 
                            <i class="fa fa-check pr10"></i> 
                            <strong> Congratulations!</strong> 
                            You have successfully unsubscribed from <?php echo __WEBSITE_SITE_TITLE__."'s"; ?> email campaign. 
                      </div>
                    <?php } else { ?>
                        <form id="unsubscribe" name="unsubscribe" class="form-horizontal" role="form" method="POST" action=""> 
                            <h4><?php echo lang('campaign_unsubscribe_heading');?></h4> 
                            <!-- First and Last Name Inputs -->
                            <div class="form-group"> 
                                <div class="col-md-10">
                                    <textarea class="form-control" name="arUnsubscribe[szRemarks]" id="szRemarks" placeholder="<?php echo lang('campaign_unsubscribe_placeholder');?>"><?php echo set_input_value('arUnsubscribe[szRemarks]');?></textarea>
                                    <?php if(!empty($szRemarksError)){?><em class="state-error" for="szRemarks"><?php echo $szRemarksError;?></em><?php }?>
                                </div>  
                            </div>  
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <button id="submitbtn" type="submit" class="btn btn-primary"><?php echo lang('campaign_unsubscribe_button'); ?></button>
                                    <button type="button" class="btn btn-default" onclick="redirect_url('<?php echo base_url();?>')"><?php echo lang('campaign_unsubscribe_cancel'); ?></button>
                                </div>
                            </div>
                            <input type="hidden" name="p_func" value="Campaign_Unsubscribe">
                        </form>
                    <?php } ?>  
                Language: <a href="?lang=en">English</a> | <a href="?lang=sp">Spanish</a>
            </div><!-- col-sm-6 --> 
        </div><!-- row --> 
        <div class="signup-footer">
            <div class="text-center">
                &copy; 2017 All Rights Reserved. USI Tech Digital Marketing System
            </div>
            <!--
            <div class="pull-right">
                Created By: <a href="#">X</a>
            </div>
            -->
        </div> 
    </div><!-- signuppanel -->  
</section> 
<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/custom.functions.js"></script>
</body>
</html>
