<!-- Begin: Content -->

<form method="post" action="<?php echo base_url();?>campaigns/<?php echo ($idCampaign > 0 ? "edit/{$arCampaignDetails['szUniqueKey']}" : "add");?>" id="formSirTrevor" class="validate-form">	

	<section id="content" class="table-layout">

		<div class="tray tray-center">

			<div class="mw1000 center-block">   

			 

				<?php if($isDetailsSaved){?>

				<div class="alert alert-success p5">

					<i class="fa fa-check pr10"></i>

					<strong> Congratulations!</strong>

					Campaign details has been saved successfully.

				</div>

				<?php } if($isFormError=== TRUE){?>

				<div class="alert alert-danger p5">

					<i class="fa fa-times pr10"></i>

					<strong> Invalid Data!</strong>

					Please fix the errors below and try again.

				</div>

				<?php } else if($isFormError != ''){?>

				<div class="alert alert-danger p5">

					<i class="fa fa-times pr10"></i>

					<strong> Sorry!</strong>

					<?php echo $isFormError;?>

				</div>

				<?php } ?>

                                

				<div class="panel panel-primary panel-border top mb15">

					<div class="panel-heading">

						<span class="panel-title">

						<i class="fa fa-user"></i>

						Basic Information

						</span>

					</div>

					

					<div class="panel-body bg-light" style="padding-bottom:0;">

						<div class="admin-form">						

                                                    <div class="row">

                                                        <label for="szName" class="field-label text-muted fs16 mb10 col-md-12">Name<span class="text-danger">*</span></label>

                                                        <?php $szNameError = form_error('arCampaign[szName]');?>

                                                        <label for="szName" class="field prepend-icon col-md-6<?php if(!empty($szNameError)){?> state-error<?php }?>">

                                                                <div class="input-group mb15">

                                                                  <span class="input-group-addon"><i class="fa fa-user"></i></span>

                                                                  <input type="text" name="arCampaign[szName]" id="szName" class="form-control" placeholder="Campaign Name" value="<?php echo set_input_value('arCampaign[szName]', (isset($arCampaignDetails['szName']) ? $arCampaignDetails['szName'] : ''));?>">

                                                                </div>

                                                        </label>

                                                        <?php if(!empty($szNameError)){?><span class="text-danger" for="szName"><?php echo $szNameError;?></span><?php }?>

                                                    </div>

                                                    

                                                    <!-- <div class="row">

                                                        <div class="col-sm-6">

                                                            <div class="row">

                                                                <label for="dtStartDate" class="field-label text-muted fs16 mb10 col-md-12">Start Date<span class="text-danger">*</span></label>

                                                                <?php if(!isset($dtStartDateError)) $dtStartDateError = form_error('arCampaign[dtStartDate]');?>

                                                                <label for="dtStartDate" class="field col-md-12<?php if(!empty($dtStartDateError)){?> state-error<?php }?>">

                                                                        <div class="input-group mb15">                                                                          

                                                                          <input type="text" name="arCampaign[dtStartDate]" id="dtStartDate" class="form-control datepicker" placeholder="Start Date (MM/DD/YYYY)" value="<?php echo set_input_value('arCampaign[dtStartDate]', (isset($arCampaignDetails['dtStartDate']) ? $arCampaignDetails['dtStartDate'] : date('m/d/Y')));?>">

                                                                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                                                        </div>

                                                                </label>

                                                                <?php if(!empty($dtStartDateError)){?><em class="state-error" for="dtStartDate"><?php echo $dtStartDateError;?></em><?php }?>

                                                            </div>

                                                        </div>

                                                        <div class="col-sm-6">

                                                            <div class="row">

                                                                <label for="dtEndDate" class="field-label text-muted fs16 mb10 col-md-12">End Date<span class="text-danger">*</span></label>

                                                                <?php if(!isset($dtEndDateError)) $dtEndDateError = form_error('arCampaign[dtEndDate]');?>

                                                                <label for="dtEndDate" class="field prepend-icon col-md-12<?php if(!empty($dtEndDateError)){?> state-error<?php }?>">

                                                                    <div class="input-group mb15">

                                                                      <input type="text" name="arCampaign[dtEndDate]" id="dtEndDate" class="form-control datepicker" placeholder="End Date (MM/DD/YYYY)" value="<?php echo set_input_value('arCampaign[dtEndDate]', (isset($arCampaignDetails['dtEndDate']) ? $arCampaignDetails['dtEndDate'] : date('m/d/Y')));?>">

                                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                                                    </div>

                                                                </label>

                                                            </div>

                                                            <?php if(!empty($dtEndDateError)){?><em class="state-error" for="dtEndDate"><?php echo $dtEndDateError;?></em><?php }?>

                                                        </div>

                                                    </div>-->

                                                    <div class="row">

                                                        <?php $isActive = set_input_value('arCampaign[isActive]', (isset($arCampaignDetails['isActive']) ? $arCampaignDetails['isActive'] : ''));?>

                                                        <label for="isActive" class="field-label text-muted fs16 mb10 col-md-12">Is Active<span class="text-danger">*</span></label>

                                                        <?php $isActiveError = form_error('arCampaign[isActive]');?>

                                                        <label for="isActive" class="col-md-2 field prepend-icon<?php if(!empty($isActiveError)){?> state-error<?php }?>">                                                            

                                                            <select name="arCampaign[isActive]" id="isActive" class="form-control mb15" placeholder="Status">

                                                                <option value="1" <?php echo ($isActive == 1 || $isActive == '' ? 'selected' : '');?>>Yes</option>

                                                                <option value="0" <?php echo ($isActive == 0 && $isActive != '' ? 'selected' : '');?>>No</option>

                                                            </select>

                                                        </label>

                                                        <?php if(!empty($isActiveError)){?><span class="text-danger" for="isActive"><?php echo $isActiveError;?></span><?php }?>								

                                                    </div>

                                                            

                                                    <?php if(!empty($arTemplates)){?>

                                                    <div class="row">

                                                        <label class="field-label text-muted fs16 mb10 col-md-12">Emails assigned to this campaign:</label>

                                                        <div class="col-sm-12">

                                                            <table class="table table-bordered">

                                                                <tr>

                                                                    <th>Name</th>

                                                                    <th>Timing</th>

                                                                    <th>Action</th>

                                                                </tr>

                                                                <?php foreach($arTemplates as $template){?>

                                                                <tr>

                                                                    <td><?php echo $template['szName'];?></td>

                                                                    <td><?php echo ($template['iDays'] == 0 ? 'Immediately' : "Day {$template['iDays']}");?></td>

                                                                    <td>

                                                                        <?php 

                                                                        $szActionLinks = '<ul>';

                                                                        $szActionLinks .= "<li><a href='".base_url()."campaigns/removeEmail/{$arCampaignDetails['szUniqueKey']}/{$template['szUniqueKey']}' class='text-info'><i class='fa fa-times-circle'></i> Remove</a></li>";

                                                                        $szActionLinks .= '</ul>';

                                                                        ?>

                                                                        <button type="button" class="btn btn-sm btn-rounded" data-toggle="popover" data-placement="bottom" data-html="true" data-content="<?php echo $szActionLinks;?>"><i class="fa fa-ellipsis-h"></i></button>

                                                                    </td>

                                                                </tr>

                                                                <?php $i++;}?>

                                                            </table>

                                                        </div>                                                        

                                                    </div>

                                                    <?php }?>   

						</div>

					</div>



					<div class="panel-footer text-center">

						<div class="admin-form">

							<a href="<?php echo base_url().'campaigns/all' ?>" class="btn btn-default"> Cancel </a>

							<button type="submit" class="btn btn-primary"><?php echo ($idCampaign > 0 ? 'Save' : 'Add'); ?></button>

							<input type="hidden" name="p_func" value="Save Campaign Details">

						</div>			                    	

					</div>

				</div>

				

			</div>

		</div> 

	</section>

</form>